echo "List the applications..."
for f in *.wgt ; do [ -d "$f" ] && apps_folder_names+=("$f") ; done

echo "Check the applications: 'id' and 'name' in config.xml and icon.png"

config_file="/config.xml"
icon_file="/icon.png"
attribut_id="id="
attribut_name="<name>"

for i in "${apps_folder_names[@]}"; do
  file1="$i"$config_file
  # check if config.xml exists
  if [ -f "$file1" ]; then
    # Get the application id
    if grep -q "$attribut_id" "$file1"; then
      right_part=$(grep -oP "(?<=${attribut_id}).*" "$file1")
#      echo "$i has this id: "$right_part
      ids+=("$right_part")
    fi

    # Get the application name
    if grep -q "$attribut_name" "$file1"; then
      right_part=$(grep -oP "(?<=${attribut_name}).*" "$file1")
#      echo "$i has this name: "$right_part
      names+=("$right_part")
    fi
  else
    echo "ERROR: " $i " has no config.xml file."
    exit 1
  fi
  # Check if an icon image is present
  file2="$i"$icon_file
  if [ ! -f "$file2" ]; then
    echo "ERROR: " $i " has no ICON file icon.png."
    exit 1
  fi
done

# check if the ids are unique
for((j=0; j<${#ids[@]}-1; j++)); do
  for((k=j+1; k<${#ids[@]}; k++)); do
#    echo "IF: " ${ids[$j]} " == " ${ids[$k]}
    if [[ ${ids[$j]} == ${ids[$k]} ]]; then
      echo "ERROR: " ${apps_folder_names[$j]} " and " ${apps_folder_names[$k]} " have a duplicated id in config.xml."
      echo "Id of " ${apps_folder_names[$j]} ": " ${ids[$j]}
      echo "Id of " ${apps_folder_names[$k]} ": " ${ids[$k]}
      exit 1
    fi
  done
done

# Check if the name is unique
for((j=0; j<${#names[@]}-1; j++)); do
  for((k=j+1; k<${#names[@]}; k++)); do
#    echo "IF: " ${names[$j]} " == " ${names[$k]}
    if [[ ${names[$j]} == ${names[$k]} ]]; then
      echo "ERROR: " ${apps_folder_names[$j]} " and " ${apps_folder_names[$k]} " have a duplicated name in config.xml."
      echo "Name of " ${apps_folder_names[$j]} ": " ${names[$j]}
      echo "Name of " ${apps_folder_names[$k]} ": " ${names[$k]}
      exit 1
    fi
  done
done


echo "The program is finished with no error."
exit 0
