# Conversion
Tableaux de numération ou de conversion.

## Auteur
Thomas CRESPIN pour Sésamath ([https://www.sesamath.net](https://www.sesamath.net))

## Licence

Fichier sous licence libre Affero GPL 3 <[https://www.gnu.org/licenses/agpl-3.0.html](https://www.gnu.org/licenses/agpl-3.0.html)>.
Vous pouvez le redistribuer ou le modifier suivant les termes
de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.

## Versions
Version 1
version initiale

## Sources
* Sésamath [ici](https://sesaprof.sesamath.net/doc/outils_tableaux/index.html) et [ici](https://sesaprof.sesamath.net/pages/prof_divers_outils.php)


