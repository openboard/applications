function tirerDans(tableau) {    
    const indexAleatoire = Math.floor(Math.random() * tableau.length);    
    return tableau[indexAleatoire];
}

function tirerNombreEntier(debut, fin) {
    console.log(`Tirage entre ${debut} et ${fin}`)
    // Vérifie que les arguments sont des entiers
    if (!Number.isInteger(debut) || !Number.isInteger(fin)) {
        throw new Error("Les arguments doivent être des entiers.");
    }

    // Vérifie que le début est inférieur ou égal à la fin
    if (debut > fin) {
        console.log(`Tirage demandé impossible. Valeur à retourner = ${fin}`);       
        return fin;
    }

    // Calcule un nombre entier aléatoire entre début et fin
    let resultat = Math.floor(Math.random() * (fin - debut + 1)) + debut;
    console.log("Résultat du tirage : ",resultat);
    return resultat;
}

function tousIdentiques(tableau) {
    if (tableau.length === 0) {
        return true;  // Si le tableau est vide, tous les éléments sont identiques (aucun élément à comparer)
    }
    
    var premierElement = tableau[0];
    
    for (var i = 1; i < tableau.length; i++) {
        if (tableau[i] !== premierElement) {
            return false;  // Dès qu'on trouve un élément différent, retourne false
        }
    }
    
    return true;  // Si tous les éléments sont identiques, retourne true
}

function majUrl(cle, valeur) {
    // Récupère l'URL actuelle
    const url = new URL(window.location);

    // Met à jour ou ajoute le paramètre
    url.searchParams.set(cle, valeur);

    // Modifie l'URL sans recharger la page
    window.history.replaceState({}, '', url);
}



async function litDepuisStockage(cle) {
    console.log('Stockage local - Lecture de la clé ' + cle);

    let valeurAretourner;
    if (openboard) { // Récupération pour Openboard
        try {
            valeurAretourner = await window.sankore.async.preference(prefixeAppli + '-' + cle);
            console.log(cle + "=" + valeurAretourner); // Pour la console
        } catch (error) {
            console.error('Erreur lors de la lecture de la clé ' + cle + ' depuis Openboard:', error);
        }
    } else { // Récupération en Web
        valeurAretourner = localStorage.getItem(prefixeAppli + '-' + cle);
        console.log("lecture depuis stockage " + cle + "=" + valeurAretourner); // Pour la console
    }

    return valeurAretourner;
}

function stocke(cle,valeur){

    console.log("Écriture stockage "+cle+"="+valeur);

    if (openboard){
    window.sankore.setPreference(prefixeAppli+'-'+cle,valeur);

    } else {
    localStorage.setItem(prefixeAppli+'-'+cle,valeur);
    }    
}


function prechargerSon(cheminFichier) {
    return fetch(cheminFichier)
      .then(response => response.arrayBuffer())
      .then(buffer => {
        return new Promise(resolve => {
          audioContext.decodeAudioData(buffer, decodedBuffer => {
            audioBuffer = decodedBuffer;
            resolve();
          });
        });
      })
      .catch(error => {
        console.error('Erreur lors du préchargement du fichier audio', error);
      });
  }
  
  // Fonction pour jouer un son
  function joueSon(son, enBoucle) {
      son.loop = enBoucle; // Définit si le son doit être joué en boucle ou pas
      son.play(); // Joue le son
  }
  
  // Fonction pour arrêter tous les sons
  function arreteSons() {
      sonMusique.pause(); // Pause la musique
      sonScore.pause(); // Pause le son du score
  }


  function objetOuEnfantDe(cible, objet) {
    // Vérifie si la cible est égale à l'objet
    if (cible === objet) {
        return true;
    }
    // Vérifie si la cible est un descendant de l'objet
    let parent = cible.parentNode;
    while (parent) {
        if (parent === objet) {
            return true;
        }
        parent = parent.parentNode;
    }
    // Si aucun cas n'est vérifié, retourne false
    return false;
}

// Fonctionnement des lightbox
function ouvre(div){
    console.log('ouvre',div)
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}
function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}

function pileOuFace(probabiliteTrue) {
    // Génère un nombre aléatoire entre 0 et 1
    var rand = Math.random();    
    // Vérifie si le nombre aléatoire est inférieur à la probabilité donnée
    if (rand < probabiliteTrue) {
        return true; // Retourne true si le nombre aléatoire est inférieur à la probabilité
    } else {
        return false; // Retourne false sinon
    }
}

function supprimerObjet(tableau, objetASupprimer) {
    const index = tableau.indexOf(objetASupprimer);
    if (index !== -1) {
        tableau.splice(index, 1);
    }
}

function supprimerTousLesObjets(tableau, objetASupprimer) {
    for (let i = 0; i < tableau.length; i++) {
        if (tableau[i] === objetASupprimer) {
            tableau.splice(i, 1);
            i--; // on décale l'index pour prendre en compte la suppression de l'élément
        }
    }
}


function basculePleinEcran() {
    // Vérifier si le navigateur prend en charge l'API de plein écran
    if (document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled) {
        // Vérifier si nous sommes actuellement en mode plein écran
        if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
            // Si nous sommes en mode plein écran, sortir du mode plein écran
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        } else {
            // Si nous ne sommes pas en mode plein écran, basculer en mode plein écran
            var element = document.documentElement;
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }
    } else {
        // L'API de plein écran n'est pas prise en charge
        console.log("L'API de plein écran n'est pas prise en charge dans ce navigateur.");
    }
}


function estEnfantDe(element,parent) {
    // Parcours les parents de l'élément donné
    while (element.parentNode) {
        element = element.parentNode;
        // Si un des parents est l'élément menu, retourne vrai
        if (element === parent) {
            return true;
        }
    }
    // Si aucun parent n'est l'élément menu, retourne faux
    return false;
}


function enregistrer(nomDeFichier) {
    const data = {};

    // Parcourir toutes les clés dans le localStorage
    for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);

        // Vérifie si la clé commence par le préfixe spécifié
        if (key.startsWith(prefixeAppli + '-')) {
            // Récupère la valeur de la clé
            const value = localStorage.getItem(key);

            // Si la clé contient "texte-principal", on ne la transforme pas en JSON (on conserve la chaîne telle quelle)
            if (key === "seyes-texte-principal") {
                data[key] = value;
            } else {
                try {
                    // Essaie de parser la valeur comme JSON
                    data[key] = JSON.parse(value);
                } catch (e) {
                    // Si ce n'est pas du JSON, conserve la valeur sous forme de chaîne
                    data[key] = value;
                }
            }
        }
    }

    // Convertit les données en JSON
    const jsonData = JSON.stringify(data, null, 2);

    // Crée un Blob contenant les données JSON
    const blob = new Blob([jsonData], { type: 'application/json' });

    // Crée un lien pour télécharger le fichier
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = nomDeFichier; // Nom du fichier à télécharger
    link.click(); // Lance le téléchargement
}



function ouvrir() {
    // Crée un élément input de type file
    const input = document.createElement('input');
    input.type = 'file'; // Spécifie que c'est un champ de fichier
    input.accept = '.json, .sey, .txt, .doc, .docx';s

    // Ajoute un écouteur d'événements pour l'ouverture du fichier
    input.addEventListener('change', handleFileSelect);

    // Simule un clic sur l'élément input pour ouvrir la boîte de dialogue de fichier
    input.click();
}



