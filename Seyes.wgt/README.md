# Seyes

Seyes affiche une page quadrillée de type cahier, et permet d'y ajouter du texte.
Elle utilise par défaut la police "Belle Allure" mais d'autres sont également disponibles.
On peut changer de type de lignage, et effectuer une mise en forme sommaire (couleurs et soulignement).
Le contenu est sauvegardé localement et peut être partagé par lien.
Cette application fonctionne en ligne sans installation (tous appareils).
Elle existe en version intégrable à Openboard.

## Accès à l'application

### En ligne

Une instance fonctionnelle est disopnible sur https://educajou.forge.apps.education.fr/seyes

### Hors ligne

- Télécharger l'application **[📦 seyes-main.zip](https://forge.apps.education.fr/educajou/seyes/-/archive/main/seyes-main.zip)**
- Extraire le ZIP
- Ouvrir le fichier **index.html**

### Sur Primtux

Seyes est intégrée au système d'exploitation [Primtux](https://primtux.fr/).
Pour l'utiliser, après l'installation de Primtux, installer les applications complémentaires.

## Utilisation

### Écrire

L'application fonctionne sur le principe du traitement de textes. On commence à écrire en haut de la page.
Pour écrire plus bas, effectuer des retours à la ligne au moyen de la touche Entrée du clavier.
Il est possible d'écrire sur la page principale, ainsi que dans la marge.

### Exporter son texte

Utiliser le bouton "partage" pour obtenir une URL (un lien).

## Signaler un bug ou proposer une amélioration

Pour participer au développement en rapportant des bugs ou en proposant une fonctionnalité, vous pouvez déposer un ticket sur cette page :
https://forge.apps.education.fr/educajou/seyes/-/issues

### Comment créer un ticket ?

<iframe title="Créer un ticket sur La Forge des Communs Numériques Éducatifs - Vidéo Tuto" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/058b9ab7-ab65-4d25-b24f-6a34feda013d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F




## Licence

Seyes est une application libre sous licence GNU/GPL.

AA Cursive est une police cursive créée par A. Renaudin partagée sous licence OFL.

Open Dyslexic est une police d'écriture sous licence libre destinée à faciliter la lecture pour les personnes dyslexiques. Elle a été créée par Abelardo Gonzalez.

Belle Allure est une police d'écriture cursive créée par Jean Boyault. Si vous remixez l'application Seyes dans une nouvelle application, merci d'en informer l'auteur de Belle Allure pour lui demander l'autorisation de l'utiliser.


