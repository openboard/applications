# Nombre en lettres
Apprendre à écrire des nombres en lettres. Cet exerciseur propose de remplir un chèque, car c’est la situation concrète d’une utilisation possible de nombre en toutes lettres au quotidien.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Septembre 2022

## Licence
GPL v2

## Versions
Version 1
version initiale

Version 1.1
- Dans OpenBoard, l'application est enregistrée dans le document et ré-affichée avec les mêmes valeurs (montant en euros et niveau).

## Sources
* Arnaud DURAND [https://www.mathix.org/nombre_en_lettres/](https://www.mathix.org/nombre_en_lettres/) et [https://mathix.org/linux/archives/9938](https://mathix.org/linux/archives/9938)
