var nombre = 0;

var niveau = parseInt(1);

var ladate = new Date()
document.getElementById('quand').innerHTML = ladate.getDate() + "/" + (ladate.getMonth() + 1) + "/" + ladate.getFullYear();
boolunmille = false;
bParamLus = 0;

/***** Sauvegarde des parametres *****/
function sauve()
{
	// Sauvegarde du Widget
	if (window.widget) {
        // Quand on quitte le widget
		window.widget.onleave.connect(() => {
		window.sankore.setPreference('nombre',nombre);
		window.sankore.setPreference('niveau',niveau);
		});
	}
}
/***** on recupere les parametres stockes *****/
async function lire()
{
	if (window.sankore){

		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('nombre')!='') {
			nombre=parseInt(await window.sankore.async.preference('nombre'));
			niveau=parseInt(await window.sankore.async.preference('niveau'));
			document.getElementById('niveau').value = 'Niveau ' + niveau;
			bParamLus = 1;
		}
	}
}

function reset() {
	if(bParamLus == 0)
		nombre = Math.floor(Math.pow(1000, parseInt(niveau)) * Math.random());
	else
		bParamLus = 0
	document.getElementById('nombre').innerHTML = nombreespace(nombre) + ' €';
	document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";
	document.getElementById('reponse').innerHTML = "";
	document.getElementById('reponse2').innerHTML = "";
	document.getElementById('nouveaunombre').blur();
	document.body.focus();
	document.getElementById('valider').disabled = false;
}

function clavier(touche) {
	console.log(touche);
	var rep = document.getElementById('reponse').innerHTML + document.getElementById('reponse2').innerHTML;

	if ((touche == "Backspace") || (touche == "Delete")) {
		if (rep.length > 0) {
			rep = rep.substring(0, rep.length - 1);
		}
		document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";
	}
	else {
		if (touche == " ") {
			rep = rep + ' ';
			document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";
		}
		else {
			tableaucaract = 'abcdefghijklmnopqrstuvwxyz';
			if ((tableaucaract.indexOf(touche) != -1)) {
				rep = rep + touche;
				document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";
			}
			else {

				if (touche == '-') {

					rep = rep + '-';
					document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";
				}
				else {
					if (touche == "Enter") {
						valider();
					}
				}
			}
		}

	}

	if (rep.length > 54) {
		var tab1 = rep.slice(0, 54);
		var ind1 = tab1.lastIndexOf("-");
		document.getElementById('reponse').innerHTML = rep.slice(0, ind1);
		document.getElementById('reponse2').innerHTML = rep.slice(ind1);
	}
	else {
		document.getElementById('reponse').innerHTML = rep;
		document.getElementById('reponse2').innerHTML = '';
	}
	event.preventDefault();
}
function nombreespace(nb) {
	var nbtexte = nb.toString();
	var nbtb = nbtexte.split('');
	var nbtexte2 = '';
	var j = 0;
	for (i = nbtb.length; i > 0; i--) {
		if (j == 3) {
			j = 0;
			nbtexte2 = " " + nbtexte2;
		}
		nbtexte2 = nbtb[i - 1] + nbtexte2;
		j++;
	}
	return nbtexte2;
}

function nombreenlettre(nb) {
	var nbtexte = nb.toString();
	var nbtb = nbtexte.split('');


	chiffre = ['', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf'];
	dixquinze = ['dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf'];
	dizaine = ['dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante-dix', 'quatre-vingt', 'quatre-vingt-dix'];
	correction = ''
	var bouldixquinze = 0;
	var pluriel = 0;
	var plurielcentving = 0;
	var et = 0;

	for (k = 0; k < nbtb.length; k++) {
		var l = nbtb.length - k;

		if (l % 3 == 0)//centaine
		{

			if (nbtb[k] != '0') {
				pluriel = 1;
				if (k != 0) {
					correction = correction + '-';
				}
				if (nbtb[k] == '1') {
					correction = correction + 'cent';
					var plurielcentving = 0;
				}
				else {
					correction = correction + chiffre[nbtb[k]] + '-cent';
					plurielcentving = 1;
				}
			}
			else {
				pluriel = 0;
			}
		}

		if (l % 3 == 2)//dizaine
		{
			if (nbtb[k] != '0') {
				pluriel = 1;
				if (nbtb[k] != '1') {
					if ((parseInt(nbtb[k]) < 7)) {
						et = 1;
					}
					else {
						et = 0;
					}
					if (nbtb[k] == '7') {

						if (k != 0) {
							correction = correction + '-';
						}
						correction = correction + 'soixante';
						bouldixquinze = 1;
						plurielcentving = 0;
					}
					else {
						if (nbtb[k] == '9') {

							if (k != 0) {
								correction = correction + '-';
							}

							correction = correction + 'quatre-vingt';

							bouldixquinze = 1;
							plurielcentving = 0;
						}
						else {
							if (k != 0) {
								correction = correction + '-';
							}
							correction = correction + dizaine[nbtb[k] - 1];
							bouldixquinze = 0;
							if (nbtb[k] == '8') {
								plurielcentving = 1;
							}
							else {
								plurielcentving = 0;
							}

						}
					}

				}
				else {
					plurielcentving = 0;
					bouldixquinze = 1;

				}
			}
			else {
				bouldixquinze = 0;
			}
		}
		if (l % 3 == 1) //unité
		{

			if (nbtb[k] != '0') {

				plurielcentving = 0;
				if (k != 0) {
					if ((!bouldixquinze)) {

						correction = correction + '-';
					}
					else {
						if (nbtb[k] == '1') {
							if (k > 0) {
								if (nbtb[k - 1] == '7') //cas du 71 soixante "et" onze
								{
									correction = correction + '-et'
								}
							}

						}
					}

				}

				if (bouldixquinze == 1) {
					if (k != 1) {
						correction = correction + '-' + dixquinze[nbtb[k]];
					}
					else {
						correction = dixquinze[nbtb[k]];
					}


					bouldixquinze = 0;

				}
				else {
					if (nbtb[k] == '1') {
						if (et == 1) {
							if (nbtb[k - 1] != 0) {
								correction = correction + "et-";
							}
						}
					}
					else {
						pluriel = 1;
					}
					if (l == 4) { //on vire le cas qui pourrait généré du un-mille (soit nombre inférieur à 2000, on met mille directement, sinon)
						if (nombre < 2000) {
							boolunmille = true;
						}
						else {
							if ((nombre >= 1000000) && (nbtb[k] == 1) && (nbtb[k - 1] == 0) && (nbtb[k - 2] == 0)) {
								boolunmille = true;
							}
							else {
								boolunmille = false;
								correction = correction + chiffre[nbtb[k]];
							}
						}
					}
					else {
						boolunmille = false;
						correction = correction + chiffre[nbtb[k]];
					}

				}
			}
			else {
				if (bouldixquinze == 1) {
					if (k != 0) {
						correction = correction + '-';
					}

					correction = correction + 'dix';
					bouldixquinze = 0;
				}
			}


			if (l == 10) {
				if (pluriel == 1) {
					correction = correction + '-milliards';
				}
				else {
					correction = correction + '-milliard';
				}
			}
			else {

				if (l == 7) {

					if (pluriel == 1) {
						correction = correction + '-millions';
					}
					else {
						correction = correction + '-million';
					}
				}
				else {
					if (l == 4) {
						if (boolunmille) {
							correction = correction + 'mille';
						}
						else {
							correction = correction + '-mille';
						}
					}
				}
			}

		}
	}
	if (plurielcentving == 1) {
		correction = correction + 's';
	}
	return correction;
}



function valider() {

	var reponse = document.getElementById('reponse').innerHTML + document.getElementById('reponse2').innerHTML;

	var regeuros = new RegExp('[Ee][Uu][Rr][Oo][Ss]?', 'gi');
	reponse = reponse.replace(regeuros, '');
	reponse = reponse.trim();
	reponse = reponse.toLowerCase();
	if (reponse == nombreenlettre(nombre)) {
		document.getElementById('jeu').style.backgroundImage = "url('Chequeapprouve.jpg')";
	}
	else {
		document.getElementById('jeu').style.backgroundImage = "url('Chequerefuse.jpg')";
	}
}

function niveauchange() {
	if (niveau == 4) {
		niveau = 1;
	}
	else {
		niveau++;
	}
	reset();
	document.getElementById('niveau').value = 'Niveau ' + niveau;
	document.getElementById("jeu").focus();
}
function solution() {
	var rep = nombreenlettre(nombre) + ' euros';
	if (rep.length > 54) {
		var tab1 = rep.slice(0, 54);
		var ind1 = tab1.lastIndexOf("-");
		document.getElementById('reponse').innerHTML = rep.slice(0, ind1);
		document.getElementById('reponse2').innerHTML = rep.slice(ind1);
	}
	else {
		document.getElementById('reponse').innerHTML = rep;
		document.getElementById('reponse2').innerHTML = '';
	}
	document.getElementById('valider').disabled = true;
	document.getElementById('jeu').style.backgroundImage = "url('Cheque.jpg')";

}

async function init()
{
	document.getElementById('valider').addEventListener('click', valider, false);
	document.getElementById('nouveaunombre').addEventListener('click', reset, false);
	document.getElementById('niveau').addEventListener('click', niveauchange, false);
	document.getElementById('solution').addEventListener('click', solution, false);
	await lire();
	reset();
	sauve();
}

window.onload = init();
