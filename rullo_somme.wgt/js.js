﻿nb_ligne_colonne=5;
partiegagnee=0;
relatif=0;
tableautemp=new Array();
tableautemp2=new Array();
coor=false;//mettre true pour débugage
for (var i=1;i<1+nb_ligne_colonne;i++)
{
	tableautemp.push(i);
	tableautemp2.push(i);
}

var caseaenlever=melangetableau(tableautemp);
var caseaenlever2=melangetableau(tableautemp2);

document.getElementById('partie').innerHTML="Nombre de parties gagnées : 0";


function switch_relatif()
{
    if(relatif == 0)
        relatif = 1;
    else
        relatif = 0;
init();
}

function changer_de_niveau(nb_niveau)
{
nb_ligne_colonne=2+nb_niveau;
//on redimensionne le tableau de mélange, donc on les remets à zéro puis on rajoute le bon nombre de cases
tableautemp=new Array();
tableautemp2=new Array();

for (var i=1;i<1+nb_ligne_colonne;i++)
{
	tableautemp.push(i);
	tableautemp2.push(i);
}

init();
	
}


function init()
{
	caseaenlever=melangetableau(tableautemp);
	caseaenlever2=melangetableau(tableautemp2);

	var tableau = document.getElementById("idtable");
	tableau.innerHTML='';
		for (var i=0;i<1+nb_ligne_colonne;i++)
		{
			var ligne = tableau.insertRow(-1);//on a ajouté une ligne

			for (var j=0;j<1+nb_ligne_colonne;j++)
			{
				var cellule = ligne.insertCell(-1);//on a une ajouté une cellule
				cellule.id="cell"+i+"-"+j;
				if ((j!=0)&&(i!=0))
				{
                    if(relatif == 0)
                        cellule.innerHTML=Math.trunc(Math.random()*10);
                    else
                        cellule.innerHTML=Math.trunc(Math.random()*20-10);

					cellule.className="bouton4";
					cellule.onclick=function () {change_etat(this.id)};
					cellule.style.color='#555';
				}
				else
				{
					if ((j!=0)||(i!=0))
					{
						cellule.className="bouton3";
					}
					else
					{
						cellule.style.visibility='none';
					}
				}
			}
		}
		for (var i=1;i<1+nb_ligne_colonne;i++)
		{
			var cellule=document.getElementById('cell'+i+'-0');
			var res=0;
			for (j=1;j<1+nb_ligne_colonne;j++)
			{
				
				if ((caseaenlever[j-1]!=i)&&((caseaenlever2[j-1]!=i)||(nb_ligne_colonne<5)))
				{
					res=res+Number(document.getElementById('cell'+i+'-'+j).innerHTML);
				}
				else
				{
					if (coor)
					{
						document.getElementById('cell'+i+'-'+j).style.color="red";
					}
				}
			}
			cellule.innerHTML=res;
			
		}
		for (var i=1;i<1+nb_ligne_colonne;i++)
		{
			var cellule=document.getElementById('cell0-'+i);
			var res=0;
			for (j=1;j<1+nb_ligne_colonne;j++)
			{
				if ((caseaenlever[i-1]!=j)&&((caseaenlever2[i-1]!=j)||(nb_ligne_colonne<5)))
				{
					res=res+Number(document.getElementById('cell'+j+'-'+i).innerHTML);
				}
				else
				{
					if (coor)
					{
						document.getElementById('cell'+j+'-'+i).style.color="red";
					}
				}
			}
			cellule.innerHTML=res;
		}
		veriflignecolonnevalide();
}
function veriflignecolonnevalide()
{
	for (i=1;i<nb_ligne_colonne+1;i++)
	{
	res=0;
	for (x=1;x<nb_ligne_colonne+1;x++)
	{
		if (document.getElementById('cell'+x+'-'+i).style.color=='rgb(85, 85, 85)')
		res=res+Math.trunc(document.getElementById('cell'+x+'-'+i).innerHTML);
	}
	if (res==Math.trunc(document.getElementById('cell0-'+i).innerHTML))
	{
		document.getElementById('cell0-'+i).style.backgroundColor='rgb(168, 255, 171)';
	}
	else
	{
		document.getElementById('cell0-'+i).style.backgroundColor='white';
	}

	
	res=0;
	for (x=1;x<nb_ligne_colonne+1;x++)
	{
		if (document.getElementById('cell'+i+'-'+x).style.color=='rgb(85, 85, 85)')
		res=res+Math.trunc(document.getElementById('cell'+i+'-'+x).innerHTML);
	}
	
	if (res==Math.trunc(document.getElementById('cell'+i+'-0').innerHTML))
	{
		document.getElementById('cell'+i+'-0').style.backgroundColor='rgb(168, 255, 171)';
	}
	else
	{
		document.getElementById('cell'+i+'-0').style.backgroundColor='white';
	}
}
}
function change_etat(id)
{
	
	var chaine=id.substr(4);
	chaine=chaine.split('-');
	var i=chaine[0];
	var j=chaine[1];
	if (document.getElementById('cell'+i+'-'+j).style.color=='rgb(85, 85, 85)')
	{
		document.getElementById('cell'+i+'-'+j).style.color='grey';
		document.getElementById('cell'+i+'-'+j).style.backgroundColor="rgb(255, 211, 196)";
	}
	else
	{
		document.getElementById('cell'+i+'-'+j).style.color='rgb(85, 85, 85)';
		document.getElementById('cell'+i+'-'+j).style.backgroundColor='#f5f5f5';
	}
	res=0;
	for (x=1;x<nb_ligne_colonne+1;x++)
	{
		if (document.getElementById('cell'+x+'-'+j).style.color=='rgb(85, 85, 85)')
		res=res+Math.trunc(document.getElementById('cell'+x+'-'+j).innerHTML);
	}
	if (res==Math.trunc(document.getElementById('cell0-'+j).innerHTML))
	{
		document.getElementById('cell0-'+j).style.backgroundColor='rgb(168, 255, 171)';
	}
	else
	{
		document.getElementById('cell0-'+j).style.backgroundColor='white';
	}

	
	res=0;
	for (x=1;x<nb_ligne_colonne+1;x++)
	{
		if (document.getElementById('cell'+i+'-'+x).style.color=='rgb(85, 85, 85)')
		res=res+Math.trunc(document.getElementById('cell'+i+'-'+x).innerHTML);
	}
	
	if (res==Math.trunc(document.getElementById('cell'+i+'-0').innerHTML))
	{
		document.getElementById('cell'+i+'-0').style.backgroundColor='rgb(168, 255, 171)';
	}
	else
	{
		document.getElementById('cell'+i+'-0').style.backgroundColor='white';
	}
	verification_findepartie();
}

function verification_findepartie()
{
	var bool=true;
	for (x=1;x<nb_ligne_colonne+1;x++)
	{
		bool=bool&&(document.getElementById('cell0-'+x).style.backgroundColor=='rgb(168, 255, 171)')&&(document.getElementById('cell'+x+'-0').style.backgroundColor=='rgb(168, 255, 171)');
		
	}
	if (bool)
	{
		alert('Bravo!! Partie terminée');
		partiegagnee=partiegagnee+1;
		document.getElementById('partie').innerHTML="Nombre de parties gagnées : "+partiegagnee;
		init();
	}
	
}
function melangetableau( tableau)
{
	var tableautemporaire=tableau;
	for(var position=tableautemporaire.length-1; position>=1; position--)
	{
	
		//hasard reçoit un nombre entier aléatoire entre 0 et position
		var hasard=Math.floor(Math.random()*(position+1));
		
		//Echange
		var sauve=tableautemporaire[position];
		tableautemporaire[position]=tableautemporaire[hasard];
		tableautemporaire[hasard]=sauve;
	}
	return tableautemporaire;
}

function pgcd(areel, breel) { // Algorithme d'Euclide  
	var a=Math.abs(areel);
	var b=Math.abs(breel);
  while (b>0) {   
    var r=a%b;  
    a=b;  
    b=r;  
  }   
  return Number(a);  
}

