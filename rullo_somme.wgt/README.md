# Rullo somme
Jeu pour travailler l’addition de nombres relatifs ou naturels.

## Auteur
Arnaud DURAND [https://www.mathix.org]https://www.mathix.org()
juin 2018

## Licence
CC BY-NC-SA

## Versions
Version 1
version initiale

Version 1.1
Ajout d'un bouton 'relatifs/naturels' pour basculer entre de nombres entiers relatifs et naturels. Utile pour les classes de CM1 et CM2.

## Sources
* Arnaud DURAND [ici](https://mathix.org/rullo_somme/) et [ici](https://mathix.org/linux/archives/11000)

