# Labynombre
Jeux de labyrinthe sur la comparaison de nombres relatifs ou décimaux positifs.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
mars 2020

## Licence
GPL v2

## Versions
Version 5
version initiale

## Sources
* Arnaud DURAND [ici](https://www.mathix.org/labynombre/) et [ici](https://mathix.org/linux/archives/13224)
