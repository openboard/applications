﻿canvas = document.getElementById('mon_canvas');
if (canvas.width<canvas.height)
{
	longueur=canvas.width;
}
else
{
	longueur=canvas.height;
}	
bornesup_base=20;
borneinf_base=-20;
arrondidesnombres=10;	
affichesol_bool=false;
precision=30;
marge=50;
fige=false;



decoupe=8;
pas=Math.floor(longueur/decoupe);
rayondisque=pas/3+4;
caseclique=[];
tableau_case=[];
coordonnee_chemins=[];
init_tableau_case();
changer_curseur=false;

function init_tableau_case()
{
	tableau_case=[];
	for (i=0;i<decoupe;i++)
	{
		if (i==0)
		{
			tableau_case.push(["_","_"]);
		}
		else
		{
			if (i%2==0)
			{
					tableau_case.push(["_","_","_","_"]);
			}
			else
			{
				tableau_case.push(["_","_","_"]);
			}
		}
	}
}

function nombrecomprisentre(nombre1,nombre2)
{
	var diff=Math.floor((Number(nombre2)-Number(nombre1)-2/arrondidesnombres)*arrondidesnombres*10)/(arrondidesnombres*10);
	return Number(Math.floor(Math.random()*diff*10+(nombre1+1/arrondidesnombres)*arrondidesnombres)/arrondidesnombres);	
}

function verification(coordonnee_chemins)
{
	var bool=true;
	for (i=1;i<(coordonnee_chemins.length);i++)
	{
		if (coordonnee_chemins[i][0]<coordonnee_chemins[i-1][0])
		{
			bool=bool&&(coordonnee_chemins[i][2]<coordonnee_chemins[i-1][2]);
		}
		else
		{
			bool=bool&&(coordonnee_chemins[i][2]>coordonnee_chemins[i-1][2]);
		}
		
	}
	return bool;
}
function affichesolution()
{
	if (!affichesol_bool)
	{
		clearCanvas(context, canvas);
		dessinerlesarete();
		context.beginPath();//On démarre un nouveau trace
		context.lineWidth = 7;
		context.strokeStyle="#f74d4d";
		context.moveTo(marge+coordonnee_chemins[0][0]*pas,  marge+(coordonnee_chemins[0][1]*2+2)*pas);
		
		for (i=1;i<coordonnee_chemins.length;i++)
		{
			context.lineTo(marge+(coordonnee_chemins[i][0])*pas,  marge+(coordonnee_chemins[i][1]*2+coordonnee_chemins[i][0]%2)*pas);	
		}

		context.stroke();//On trace seulement les lignes pleines
		context.closePath();
		dessinerlesdisquesnumerotes();
		dessinecerclerayon(marge+coordonnee_chemins[0][0]*pas,  marge+(coordonnee_chemins[0][1]*2+2)*pas,rayondisque,"red");	
		for (i=1;i<coordonnee_chemins.length;i++)
		{
			dessinecerclerayon(marge+(coordonnee_chemins[i][0])*pas,  marge+(coordonnee_chemins[i][1]*2+coordonnee_chemins[i][0]%2)*pas,rayondisque,"red");	
			
		}
		document.getElementById('btsolution').style.color="red";
		affichesol_bool=true;
		
	}
	else
	{
		affichesol_bool=false;
		dessinegrille(context,longueur);
		document.getElementById('btsolution').style.color="#444444";
	}
}

function creationchemin()
{
	bornesup=bornesup_base;
	borneinf=borneinf_base;
	coordonnee_chemins=[];
	
	nbcasechemin=0;
	coordonney=Math.floor(Math.random()*2);
	coordonnex=0;
	if (coordonney==0)
	{
		
		tableau_case[coordonnex][0]=nombrecomprisentre(borneinf_base,(bornesup_base+borneinf_base)/2);
		tableau_case[coordonnex][1]="*";
		coordonnee_chemins.push([0,0]);
		
	}
	else
	{
		tableau_case[coordonnex][0]="*";
		tableau_case[coordonnex][1]=nombrecomprisentre(borneinf_base,(bornesup_base+borneinf_base)/2);
		coordonnee_chemins.push([0,1]);
		
	}
	coordonnee_chemins[coordonnee_chemins.length-1].push(tableau_case[coordonnex][coordonney]);
	nbcasechemin++;
	 tab=lesvoisinslibres(coordonnex,coordonney);
	 
	 pasdepb=true;
	while (((coordonnex<7)&&(tab.length>0))&&(pasdepb))
	{
		choixvoisin=Math.floor(Math.random()*tab.length);
		coordonnex_av=coordonnex;
		coordonney_av=coordonney;
		bornesup=bornesup_base;
		borneinf=borneinf_base;
		coordonnex=tab[choixvoisin][0];
		coordonney=tab[choixvoisin][1];
		coordonnee_chemins.push([coordonnex,coordonney]);
		tabtmp=lesvoisinsnonlibresaufdouonvient(coordonnex,coordonney,coordonnex_av,coordonney_av);
		if (coordonnex_av<coordonnex)
		{
			borneinf=Number(tableau_case[coordonnex_av][coordonney_av]);
		}
		else
		{
			bornesup=Number(tableau_case[coordonnex_av][coordonney_av]);
			
		}
		
		for (m=0;m<tabtmp.length;m++)
		{
			
			if (tabtmp[m][0]<coordonnex)
			{
				bornesup=Math.min(bornesup,Number(tableau_case[tabtmp[m][0]][tabtmp[m][1]]));
				
			}
			else
			{
				borneinf=Math.max(borneinf,Number(tableau_case[tabtmp[m][0]][tabtmp[m][1]]));
				
			}
		}
		
		//alert(borneinf+' '+bornesup+' '+tabtmp);
		if (borneinf>=bornesup)
		{
			pasdepb==false;
		}
		else
		{
			tableau_case[coordonnex][coordonney]=nombrecomprisentre(borneinf,bornesup);
			coordonnee_chemins[coordonnee_chemins.length-1].push(tableau_case[coordonnex][coordonney]);
		}
	
		nbcasechemin++;
		tab=lesvoisinslibres(coordonnex,coordonney);
	
		
	}
	if ((coordonnex!=7)||(!pasdepb))
	{
		
		init_tableau_case();
		creationchemin();
	}
	else
	{
		if (verification(coordonnee_chemins))//hack de vérification inutile en temps normal mais bug de chemin non valide
		{
			for (i=0;i<tableau_case.length;i++)
			{
				for (j=0;j<tableau_case[i].length;j++)
				{
						if (tableau_case[i][j]=='_')
						{
							tableau_case[i][j]=nombrecomprisentre(bornesup_base,borneinf_base);
						}
						if (tableau_case[i][j]=='*')
						{
							tableau_case[i][j]=nombrecomprisentre(borneinf_base,(borneinf_base+bornesup_base)/2);
						}
				}
			}
		}
		else
		{
			init_tableau_case();
			creationchemin();
		}
		
	
	}

}
function lesvoisinsnonlibresaufdouonvient(i,j,iori,jori)
{
	var tabrendu=[];
	var tab=lesvoisins(i,j);

	for (k=0;k<tab.length;k++)
	{
		
		if (((tableau_case[tab[k][0]][tab[k][1]]!="*")&&(tableau_case[tab[k][0]][tab[k][1]]!="_"))&&((tab[k][1]!=jori)||(tab[k][0]!=iori)))
		{
			tabrendu.push([tab[k][0],tab[k][1]]);
		}
	}
	
	return tabrendu;
}
function lesvoisinslibres(i,j)
{
	var tabrendu=[];
	var tab=lesvoisins(i,j);

	for (k=0;k<tab.length;k++)
	{
		
		if (tableau_case[tab[k][0]][tab[k][1]]=="_")
		{
			tabrendu.push([tab[k][0],tab[k][1]]);
		}
	}
	
	return tabrendu;
}
function lesvoisins(i,j)
{
	if (i==0)
	{
		return [[1,j],[1,j+1]];
	}
	else
	{
		if (i==7)
		{
			return [[6,j],[6,j+1]];
		}
		else
		{
			if (i%2==0)// colonnes avec 4 cases
			{
					if (j==0)
					{
						return [[i-1,0],[i+1,0]];
					}
					else
					{
						if (j==3)
						{
							return [[i-1,2],[i+1,2]];
						}
						else
						{
							return [[i-1,j-1],[i-1,j],[i+1,j-1],[i+1,j]];
						}
					}
			}
			else
			{
				if (i==1)
				{
					if (j==0)
					{
						return [[i-1,j],[i+1,j],[i+1,j+1]];
					}
					if (j==1)
					{
						return [[i-1,j],[i-1,j-1],[i+1,j],[i+1,j+1]];
					}
					if (j==2)
					{
						return [[i-1,j-1],[i+1,j],[i+1,j+1]];
					}
					
				}
				else
				{
					return [[i-1,j],[i-1,j+1],[i+1,j],[i+1,j+1]];
				}
			}
		}
	}
	
}




function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
}


var canvas = document.getElementById('mon_canvas');
var context = canvas.getContext('2d');



canvas.addEventListener('mousemove', function(evt) 
	{
		if (! affichesol_bool)
		{
		var mousePos = getMousePos(canvas, evt);
		
		changer_curseur=false;
		clearCanvas(context, canvas);
		dessinegrille(context,longueur);
		
		
		var i=Math.round((mousePos.x-marge)/pas);
		var j=Math.round((mousePos.y-marge)/pas);
		if (caseclique.length==0)
		{
			if (i==0)
			{
				if ((j==2)||(j==4))
				{
						changer_curseur=true;
				}
			}
		}
		else
		{
			present=false;
			for (k=0;k<caseclique.length-1;k++)
			{
				present=present||((caseclique[k][0]==i)&&(caseclique[k][1]==j));
			}
			if (!present)
			{
				if (((Math.abs(caseclique[caseclique.length-1][0]-i)==1)&&(Math.abs(caseclique[caseclique.length-1][1]-j)==1))||((caseclique[caseclique.length-1][0]==i)&&(caseclique[caseclique.length-1][1]==j)))
				{
					if ((i+j)%2==0)
					{
						if (i==0)
						{
							if ((j==2)||(j==4))
							{
									changer_curseur=true;
							}
						}
						else
						{
							if ((i<8)&&(j<7))
							changer_curseur=true;
						}
					}
				}
			}
		}
		
		
		if (changer_curseur)
		{
			canvas.style.cursor='pointer';
			
			dessinecerclerayon(i*pas+marge,j*pas+marge,rayondisque,"red");
		}
		else
		{
			canvas.style.cursor='default';
			
		}
		}	
	}, false);
	
	  
canvas.addEventListener('click', function(evt) 
	{
		if (! affichesol_bool)
		{
		var pas=Math.floor(longueur/decoupe);
		if (changer_curseur)
		{
			var mousePos = getMousePos(canvas, evt);
			var i=Math.round((mousePos.x-marge)/pas);
			var j=Math.round((mousePos.y-marge)/pas);
			
			//on reteste pour éviter un bug d chemin vers une case ineistante
			if (caseclique.length==0)
			{
				if (i==0)
				{
					if ((j==2)||(j==4))
					{
							caseclique.push([i,j]);
					}
				}
			}
			else
			{
				present=false;
				for (k=0;k<caseclique.length-1;k++)
				{
					present=present||((caseclique[k][0]==i)&&(caseclique[k][1]==j));
				}
				if (!present)
				{
					if (((Math.abs(caseclique[caseclique.length-1][0]-i)==1)&&(Math.abs(caseclique[caseclique.length-1][1]-j)==1))||((caseclique[caseclique.length-1][0]==i)&&(caseclique[caseclique.length-1][1]==j)))
					{
						if ((i+j)%2==0)
						{
							if (i==0)
							{
								if ((j==2)||(j==4))
								{
									if ((caseclique[caseclique.length-1][0]==i)&&(caseclique[caseclique.length-1][1]==j))
									{
									
										caseclique.pop();
									}
									else
									{
										caseclique.push([i,j]);
									}
								}
							}
							else
							{
								if ((i<8)&&(j<7))
								{
									if ((caseclique[caseclique.length-1][0]==i)&&(caseclique[caseclique.length-1][1]==j))
									{
									
										caseclique.pop();
									}
									else
									{
										caseclique.push([i,j]);
									}
												
									if (i==7)
									{
										dessinegrille(context,longueur);
											validerchemin();
									}
								}
							}
						}
					}
				}
			}
			}	
		dessinegrille(context,longueur);
	}
	},false);
	
function validerchemin()
{
	
	n=1;
	
	var ok=true;
	while (n<caseclique.length)
	{

			i_av=caseclique[n-1][0];
			if (i_av==0)
			{
				j_av=(caseclique[n-1][1]-2)/2;
			}
			else
			{
				if (i_av%2==0)
				{
					j_av=(caseclique[n-1][1])/2;
				}
				else
				{
					j_av=(caseclique[n-1][1]-1)/2;
				}
			}
			i=caseclique[n][0];
			if (i==0)
			{
				j=(caseclique[n][1]-2)/2;
			}
			else
			{
				if (i%2==0)
				{
					j=(caseclique[n][1])/2;
				}
				else
				{
					j=(caseclique[n][1]-1)/2;
				}
			}
			
			if (i_av<i)
			{
				ok=ok&&tableau_case[i][j]>=tableau_case[i_av][j_av];
			}
			else
			{
				ok=ok&&tableau_case[i][j]<=tableau_case[i_av][j_av];
			}
			n++;

	}
	if (ok)
	{
		alert('BRAVO');
	}
	else
	{
		alert('Non ce n\'est pas un chemin valide!');
		reset();
	}
}
function initialisation()
{
	affichesol_bool=false;
	document.getElementById('btsolution').style.color="#444444";
	caseclique=[];
	tableau_case=[];
	coordonnee_chemins=[];
	var canvas = document.getElementById('mon_canvas');
	if(!canvas)
	{
		alert("Impossible de récupérer le canvas");
		return;
	}

	if (canvas.width<canvas.height)
	{
		longueur=canvas.width-30;
	}
	else
	{
		longueur=canvas.height-30;
	}
	var canvas = document.getElementById('mon_canvas');
	if(!canvas)
	{
		alert("Impossible de récupérer le canvas");
		return;
	}

	var context = canvas.getContext('2d');
	if(!context)
	{
		alert("Impossible de récupérer le context du canvas");
		return;
	}
	init_tableau_case();
creationchemin();
	clearCanvas(context, canvas);
dessinegrille(context,longueur);
console.log(coordonnee_chemins);
}

function clearCanvas(context, canvas) {
  context.clearRect(0, 0, canvas.width, canvas.height);
  var w = canvas.width;
  canvas.width = 1;
  canvas.width = w;

    var canvas = document.getElementById('mon_canvas');
        if(!canvas)
        {
            alert("Impossible de récupérer le canvas");
            return;
        }

    var context = canvas.getContext('2d');
        if(!context)
        {
            alert("Impossible de récupérer le context du canvas");
            return;
        }
	

}

function reset()
{
	if (!affichesol_bool)
	{
	var canvas = document.getElementById('mon_canvas');
	if(!canvas)
	{
		alert("Impossible de récupérer le canvas");
		return;
	}

    var context = canvas.getContext('2d');
	if(!context)
	{
		alert("Impossible de récupérer le context du canvas");
		return;
	}
	clearCanvas(context, canvas) ; 
	caseclique=[];
	dessinegrille(context,longueur);
}
}

function dessinecerclerayon(x,y,rayon,colore)
{
	context.beginPath();//On démarre un nouveau trace
	context.save();
	context.setLineDash([0]);
	context.lineWidth="6";
	
	
		 context.strokeStyle=colore;
	
	context.arc(x, y, rayon, 0, 2 * Math.PI);
	context.stroke();//On trace seulement les lignes pleines
	context.restore();
	context.closePath();
}

function dessinedisquerayon(x,y,rayon,colore)
{
	context.beginPath();//On démarre un nouveau trace
	context.save();
	context.setLineDash([0]);
	
	
	
		 context.fillStyle=colore;
	
	context.arc(x, y, rayon+1, 0, 2 * Math.PI);
	context.fill();//On trace seulement les lignes pleines
	context.restore();
	context.closePath();
}

function conversionnombretexte(nb)
{
	var res = nb.toString().replace(".", ",");
	return res;
}
function dessinerlecheminaretes()
{
	context.beginPath();//On démarre un nouveau trace
	context.lineWidth = 7;
	context.strokeStyle="#f74d4d";
	if (caseclique.length>1)
	{
			context.moveTo(marge+caseclique[0][0]*pas,  marge+caseclique[0][1]*pas);
			for (i=0;i<caseclique.length;i++)
			{
				context.lineTo(marge+caseclique[i][0]*pas,  marge+caseclique[i][1]*pas);	
			}
	}
	context.stroke();//On trace seulement les lignes pleines
	context.closePath();
	
}
function dessinerlechemincercles()
{
	for (i=0;i<caseclique.length;i++)
	{
		dessinecerclerayon(marge+caseclique[i][0]*pas,  marge+caseclique[i][1]*pas,rayondisque,"red");
	}
}
function dessinerlesarete()
{
	context.beginPath();//On démarre un nouveau trace
	context.lineWidth = 5;
	context.strokeStyle="#d1d1d1";
	context.setLineDash([0]);
	context.moveTo(marge+0,  marge+pas*2);
	context.lineTo( marge+pas*2, marge+0);
	context.lineTo( marge+pas*7, marge+pas*5);
	context.lineTo( marge+pas*6, marge+pas*6);
	context.lineTo( marge+pas, marge+pas);
	context.moveTo(marge+0,  marge+pas*2);
	context.lineTo( marge+pas*4, marge+pas*6);
	context.lineTo( marge+pas*7, marge+pas*3);
	context.lineTo( marge+pas*4, marge+0);
	context.lineTo( marge+0, marge+pas*4);
	context.lineTo( marge+pas*2, marge+pas*6);
	context.lineTo( marge+pas*7, marge+pas);
	context.lineTo( marge+pas*6, marge+0);
	context.lineTo( marge+pas, marge+pas*5);
	context.stroke();//On trace seulement les lignes pleines
	context.closePath();
}
function dessinerlesdisquesnumerotes()
{
	
	context.beginPath();//On démarre un nouveau trace

	
	context.textAlign="center";
	context.font = '15px Comic Sans MS';
	context.textBaseline="middle";
	dessinedisquerayon(marge+0,  marge+pas*2, rayondisque,"#85e785");
	context.fillText(conversionnombretexte(tableau_case[0][0]), marge+0,  marge+pas*2);
	dessinedisquerayon(marge+0,  marge+pas*4, rayondisque,"#85e785");
	context.fillText(conversionnombretexte(tableau_case[0][1]), marge+0,  marge+pas*4);
	
	dessinedisquerayon(marge+pas,  marge+pas, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[1][0]), marge+pas,  marge+pas);
	dessinedisquerayon(marge+pas,  marge+pas*3, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[1][1]), marge+pas,  marge+pas*3);
	dessinedisquerayon(marge+pas,  marge+pas*5, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[1][2]), marge+pas,  marge+pas*5);
	
	dessinedisquerayon(marge+pas*2,  marge, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[2][0]), marge+pas*2,  marge+0);
	dessinedisquerayon(marge+pas*2,  marge+pas*2, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[2][1]), marge+pas*2,  marge+pas*2);
	dessinedisquerayon(marge+pas*2,  marge+pas*4, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[2][2]), marge+pas*2,  marge+pas*4);
	dessinedisquerayon(marge+pas*2,  marge+pas*6, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[2][3]), marge+pas*2,  marge+pas*6);
	
	dessinedisquerayon(marge+pas*3,  marge+pas, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[3][0]), marge+pas*3,  marge+pas);
	dessinedisquerayon(marge+pas*3,  marge+pas*3, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[3][1]), marge+pas*3,  marge+pas*3);
	dessinedisquerayon(marge+pas*3,  marge+pas*5, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[3][2]), marge+pas*3,  marge+pas*5);

	dessinedisquerayon(marge+pas*4,  marge, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[4][0]), marge+pas*4,  marge+0);
	dessinedisquerayon(marge+pas*4,  marge+pas*2, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[4][1]), marge+pas*4,  marge+pas*2);
	dessinedisquerayon(marge+pas*4,  marge+pas*4, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[4][2]), marge+pas*4,  marge+pas*4);
	dessinedisquerayon(marge+pas*4,  marge+pas*6, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[4][3]), marge+pas*4,  marge+pas*6);
	
	dessinedisquerayon(marge+pas*5,  marge+pas, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[5][0]), marge+pas*5,  marge+pas);
	dessinedisquerayon(marge+pas*5,  marge+pas*3, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[5][1]), marge+pas*5,  marge+pas*3);
	dessinedisquerayon(marge+pas*5,  marge+pas*5, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[5][2]), marge+pas*5,  marge+pas*5);
	
	dessinedisquerayon(marge+pas*6,  marge, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[6][0]), marge+pas*6,  marge+0);
	dessinedisquerayon(marge+pas*6,  marge+pas*2, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[6][1]), marge+pas*6,  marge+pas*2);
	dessinedisquerayon(marge+pas*6,  marge+pas*4, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[6][2]), marge+pas*6,  marge+pas*4);
	dessinedisquerayon(marge+pas*6,  marge+pas*6, rayondisque,"#d1d1d1");
	context.fillText(conversionnombretexte(tableau_case[6][3]), marge+pas*6,  marge+pas*6);
	
	dessinedisquerayon(marge+pas*7,  marge+pas, rayondisque,"#e79385");
	context.fillText(conversionnombretexte(tableau_case[7][0]), marge+pas*7,  marge+pas);
	dessinedisquerayon(marge+pas*7,  marge+pas*3, rayondisque,"#e79385");
	context.fillText(conversionnombretexte(tableau_case[7][1]), marge+pas*7,  marge+pas*3);
	dessinedisquerayon(marge+pas*7,  marge+pas*5, rayondisque,"#e79385");
	context.fillText(conversionnombretexte(tableau_case[7][2]), marge+pas*7,  marge+pas*5);	
	
	context.closePath();
	
}
function dessinegrille(context,longueur)	
{
	clearCanvas(context, canvas);
	dessinerlesarete();
	dessinerlecheminaretes();
dessinerlesdisquesnumerotes();
dessinerlechemincercles();
	
}

window.load=initialisation();
