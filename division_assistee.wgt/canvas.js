﻿decimaux=0;
canvas = document.getElementById('mon_canvas');
context = canvas.getContext('2d');

casesoustrationchoisie=["",""];

longueur=canvas.width;
largeur=canvas.height;

taillefont=30;
largeurfont=0;
endroitoubarre= 3*longueur/4;
oncommencea=0;
precision=30;
marge=50;

tableau_diviseur=[3,4,5,6,7,8,9,11,12,13,14,15];
diviseur=0;
quotient=0;
niveau=1;
positionvirgule=0; // c'est la position de la virgule pour qu'elle soit placée artificiellement
fige=false;

virguleplaceedansquotient=false; //si virgule placée par l'élève

quotient_eleve=""; //quotient trouvé par l'élève
rangchiffreatomber=0; // c'est le combien-tieme chiffre à faire tomber
nbencours=0; // nombre à considérer dans la division, il y a combien de fois "diviseur" dans nbencours

dividende=0;
dividendech="";
endroit_initial=0;
taille_chaine_dividende=0;


soustractions=[];
soustractions.push([dividende]);
boolquotient=true;


changer_curseur=false;


bool_affichage_produit=true;
bool_controle_chiffre_quotient=true;
bool_virgule_automatique=true;
bool_soustraction_automatique=true;

affichedivision();

function switchdecimaux()
{
    if(decimaux == 0)
        decimaux = 1;
    else
        decimaux = 0;
    nouveaunombres();
}

function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
}

function ecritmessage(texte)
{
	if (texte=="")
	{
		fige=false;
		document.getElementById('message').style.display="none";
		
	}
	else
	{
		document.getElementById('message').style.display="";
		if (boolterminee)
		{
			document.getElementById('message').innerHTML="Terminé";		
		}
		else
		{
			fige=true;
			document.getElementById('message').innerHTML=texte+"<br><i>(Appuie sur entree)</i>";
		}
	}
}
canvas.addEventListener('mousemove', function(evt) 
	{
			var mousePos = getMousePos(canvas, evt);
			var x=mousePos.x;
			var y=mousePos.y;
			canvas.style.cursor="auto";
			if (!bool_soustraction_automatique)
			{
				console.log((x>endroitoubarre)&&(!boolquotient));
				if (((x>endroitoubarre)&&(!boolquotient))||((x<endroitoubarre)&&(boolquotient)))
				{
						canvas.style.cursor="pointer";
					
				}
				else
				{
						canvas.style.cursor="auto";
				}
				
				
			}
			
	}, false);
	
	 
canvas.addEventListener('click', function(evt) 
	{
		canvas.style.cursor="auto";
			var mousePos = getMousePos(canvas, evt);
			var x=mousePos.x;
			var y=mousePos.y;
			boolquotient=true;
			if (!bool_soustraction_automatique)
			{
				
				if (x>endroitoubarre)
				{
					boolquotient=true;
					
				}
				else
				{
					
					boolquotient=false;
					casesoustrationchoisie=[-1,-1];
					casesoustrationchoisie[1]=(Math.floor((y-50)/taillefont))%2;
					casesoustrationchoisie[0]=Math.floor(Math.floor((y-50)/taillefont)/2);
					console.log("deb"+casesoustrationchoisie);
					if (soustractions.length-1<=casesoustrationchoisie[0])
					{
						casesoustrationchoisie=[-1,-1];
						boolquotient=true;
					}
					else
					{
						if (casesoustrationchoisie[1]==1)
						{
							if ((soustractions[casesoustrationchoisie[0]].length)<3)
							{
								casesoustrationchoisie=[-1,-1];
								boolquotient=true;
							}
							else
							{
								var virgule = new RegExp('[\.,]', 'gi');
								if ((quotient_eleve.replace(virgule, '')).length<=casesoustrationchoisie[0])
								{
									casesoustrationchoisie=[-1,-1];
									boolquotient=true;
								}
							}
							
						}
					}
					console.log("fin="+casesoustrationchoisie);
					
					//mise en place de la descente du nombre
					if (casesoustrationchoisie[1]==1)
					{
						if (soustractions[casesoustrationchoisie[0]+1][0]=='')
						{
							if (dividendech.length>oncommencea+casesoustrationchoisie[0]+1)
							{
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
							else
							{
								dividendech=dividendech+"0";
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
						}
					}
					
					//fn mise en place
					
				}
				affichedivision();
			}
	},false);


window.addEventListener('keydown', function(evt) 
	{	
		touche=evt.key;
		if (fige)
		{
			if (touche=="Enter")
			{
				ecritmessage("");
			}
		}
		else
		{
			if ((touche=='Enter')&&(!bool_soustraction_automatique))
			{
				
				if (boolquotient)
				{
					var virgule = new RegExp('[\.,]', 'gi');
					if ((quotient_eleve.replace(virgule, '')).length>0)
					{
						casesoustrationchoisie=[(quotient_eleve.replace(virgule, '')).length-1,0];
						boolquotient=!boolquotient;
					}
				}
				else
				{
					if ((Number(soustractions[casesoustrationchoisie[0]+1][0])==0)&&(soustractions.length+oncommencea>=dividendech.length)&&(casesoustrationchoisie[1]==1))// si c'est la touche on vérifie si c'est le dernier reste de la division en regardant à quel chiffre on est
					{
						
							var virgule = new RegExp('[\.,]', 'gi');
							if (quotient==Number((quotient_eleve).replace(virgule, '.') ))
							{
								soustractions[soustractions.length-1]=='0 ';
								ecritmessage("Tu as terminé correctement ta division");
								
							}
							else
							{
								soustractions[soustractions.length-1]=='0 ';
								ecritmessage("Tu as fait une erreur quelque part");
								
								
							}
							
							terminee=true;
							
						
					}
					else
					{
						if ((casesoustrationchoisie[1]==0)&&(soustractions[casesoustrationchoisie[0]].length==3))
						{
							
							casesoustrationchoisie[1]=1;
							//mise en place de la descente du nombre
							if (dividendech.length>oncommencea+casesoustrationchoisie[0]+1)
							{
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
							else
							{
								dividendech=dividendech+"0";
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
							//fn mise en place
						}
						else
						{
							boolquotient=!boolquotient;
						}
					}
				}
				affichedivision();
			}
			//fin si entree
			// debut si autre touche
			if (boolquotient)
			{
				if (bool_soustraction_automatique)
				{
					var regex = /^[0-9]$/g;
					if (regex.test(touche)) // si touche numérique
					{
						if ((!bool_controle_chiffre_quotient)||((Number(nbencours)-Math.floor(touche*diviseur)>=0)&&(Number(nbencours)-Math.floor(touche*diviseur)<diviseur)))
						{
							quotient_eleve=quotient_eleve+(touche.toString());
							nbencours=nbencours-Math.floor(touche*diviseur);
							rangchiffreatomber++;
							soustractions[soustractions.length-1].push((Math.floor(touche*diviseur)).toString());
							if (bool_virgule_automatique)  // on place la virgule automatiquement
							{
								if (rangchiffreatomber==positionvirgule)
								{
									virguleplaceedansquotient=true;
									quotient_eleve=quotient_eleve+",";
									ecritmessage('On place la virgule au quotient car on descend le chiffre des dixièmes.');
								}
							}
							if (rangchiffreatomber<dividendech.length)
							{
								soustractions.push([(nbencours.toString()+dividendech[rangchiffreatomber]),rangchiffreatomber]);
								nbencours=Number(nbencours.toString()+dividendech[rangchiffreatomber]);
								
							}
							else
							{
								if (nbencours==0)
								{
									soustractions.push([(nbencours.toString()),rangchiffreatomber-1]);
									ecritmessage('Terminé');
									boolterminee=true;
								}
								else //le reste est non nul, donc on ajoute un zéro inutile
								{
									if (bool_virgule_automatique) // on place la virgule automatiquement 
									{
										if(!virguleplaceedansquotient)
										{
											virguleplaceedansquotient=true;
											quotient_eleve=quotient_eleve+",";
											dividendech=dividendech.toString();
											positionvirgule=dividendech.length;
											
											
										
										}
									}
									
									soustractions.push([(nbencours.toString()+"0"),rangchiffreatomber]);
									nbencours=Number(nbencours.toString()+"0");
									dividendech=dividendech+"0";
									if (bool_soustraction_automatique)
									{
										ecritmessage('Le reste est non nul donc on ajoute un zéro inutile que l\'on fait descendre');
									}
									
								}
								
							}
						}
						else // contrôle actif des chiffres infructueux
						{
							if (Number(nbencours)-Math.floor(touche*diviseur)>=0)
							{
								ecritmessage('Erreur, le nombre choisi n\'est pas assez grand , nombre considéré ' + nbencours +' produit choisi : ' +touche+'x'+diviseur+'='+Math.floor(touche*diviseur));
							}
							else
							{
								ecritmessage('Erreur, le nombre choisi est trop grand : ' +touche+'x'+diviseur+'='+Math.floor(touche*diviseur)+' et '+Math.floor(touche*diviseur)+' > '+ nbencours );
								
								
							}
						}
						
						
						
					}
					if ((touche=='Backspace')||(touche=='Delete'))
					{
						if (quotient_eleve!='')
						{
							if (quotient_eleve[quotient_eleve.length-1]==",")
							{
								quotient_eleve=quotient_eleve.substring(0,quotient_eleve.length-1);
							}
							quotient_eleve=quotient_eleve.substring(0,quotient_eleve.length-1);
							
							soustractions.pop();
							soustractions[soustractions.length-1].pop();
							rangchiffreatomber--;
							nbencours=Number(soustractions[soustractions.length-1][0]);
							if (dividendech[dividendech.length-1]=="0") // si on a un zéro dans le dividende c'est peut-être un zéro inutile qu'il faut enlever parce qu'on l'avait rajouté
							{
								if (dividendech.length-1>positionvirgule) // le zéro se situe après la virgule
								{
									dividendech=dividendech.substring(0,dividendech.length-1);
								}
							}
						}
					}
					
					if ((!bool_virgule_automatique)&&(touche==',')) //on ajoute la virgule au quotient sur demande de l'élève
					{
						virguleplaceedansquotient=true;
						quotient_eleve=quotient_eleve+",";
					}
				
				}
				else //ecriture du quotient avec soustractions automatique désactivées
				{
					var regex = /^[0-9]$/g;
					if ((touche==',')&&(!virguleplaceedansquotient))
					{
							quotient_eleve=quotient_eleve+touche;
							virguleplaceedansquotient=true;
					}
					if (regex.test(touche)) // si touche numérique
					{
						if (soustractions[soustractions.length-1][0]!="")
						{// on peut écrire un chiffre si la soustraction a été faite
							soustractions.push(["",soustractions[soustractions.length-1][1]+1]);
							quotient_eleve=quotient_eleve+touche;
						}
						
					}
					if ((touche=='Backspace')||(touche=='Delete'))
					{
						
						if (quotient_eleve!='')
						{
							if (quotient_eleve[quotient_eleve.length-1]==",")
							{
								quotient_eleve=quotient_eleve.substring(0,quotient_eleve.length-1);
								virguleplaceedansquotient=false;
							}
							else
							{
								quotient_eleve=quotient_eleve.substring(0,quotient_eleve.length-1);
								soustractions.pop();
								if (soustractions[soustractions.length-1].length==3)
								{
									soustractions[soustractions.length-1].pop();
								}
								
							}
						}
						

					}
				}
				affichedivision();
				
			}
			else // écriture dans les soustractions
			{
				var regex = /^[0-9]$/g;
				
				if ((touche=='Backspace')||(touche=='Delete')) // si touche pas numérique
				{
					//mise en place de la descente du nombre
					if (casesoustrationchoisie[1]==1)
					{
						if (soustractions[casesoustrationchoisie[0]+1][0]=='')
						{
							if (dividendech.length>oncommencea+casesoustrationchoisie[0]+1)
							{
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
							else
							{
								dividendech=dividendech+"0";
								soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
						}
					}
					
					//fn mise en place	
					if (casesoustrationchoisie[1]==1)
					{ 
						if (soustractions.length>casesoustrationchoisie[0]+1)
						{
							
							var chaine=soustractions[casesoustrationchoisie[0]+1][0];
							if (chaine.length>1)
							{
								soustractions[casesoustrationchoisie[0]+1][0]=chaine.substring(0,chaine.length-2)+dividendech[oncommencea+casesoustrationchoisie[0]+1];
							}
							else
							{
								soustractions[casesoustrationchoisie[0]+1][0]="";
								casesoustrationchoisie[1]=0;
							}
						}
						
					
					}
					else//casesoustrationchoisie[1]==0)
					{
					if (soustractions[casesoustrationchoisie[0]].length==3)
						{
							if (soustractions[casesoustrationchoisie[0]][2]!="")
							{
								var chaine=soustractions[casesoustrationchoisie[0]][2];
								soustractions[casesoustrationchoisie[0]][2]=chaine.substring(0,chaine.length-1);
									
							}
							else
							{
								(soustractions[casesoustrationchoisie[0]]).pop();
							}
							
						}
						
					}
					
				
				}
				else
				{
					if (regex.test(touche)) // si touche numérique
					{
						
						if (casesoustrationchoisie[1]==1)
						{ 
							if (soustractions.length>casesoustrationchoisie[0]+1)
							{
								if (soustractions[casesoustrationchoisie[0]+1][0]=="")//ne devrait pas avoir lieu
								{
										soustractions[casesoustrationchoisie[0]+1][0]=dividendech[oncommencea+casesoustrationchoisie[0]+1];
										soustractions[casesoustrationchoisie[0]+1][0]=soustractions[casesoustrationchoisie[0]+1][0]+touche;
								}
								else
								{
									// a modifier
									var chaine=soustractions[casesoustrationchoisie[0]+1][0];
									soustractions[casesoustrationchoisie[0]+1][0]=chaine.substring(0,chaine.length-1)+touche+dividendech[oncommencea+casesoustrationchoisie[0]+1];
									
									//soustractions[casesoustrationchoisie[0]+1][0]=soustractions[casesoustrationchoisie[0]+1][0]+touche;
								}
							}
							else
							{
							
								if ((touche==0)&&(soustractions.length>=dividendech.length))// si c'est la touche on vérifie si c'est le dernier reste de la division en regardant à quel chiffre on est
								{
									
									soustractions[casesoustrationchoisie[0]+1][0]=touche;
									soustractions[casesoustrationchoisie[0]+1][1]=soustractions[casesoustrationchoisie[0]][1];
									var virgule = new RegExp('[\.,]', 'gi');
									if (quotient_eleve==(quotient.toString()).replace(virgule, ',') )
									{
										ecritmessage("Tu as terminé correctement ta division");
									}
									else
									{
										ecritmessage("Tu as fait une erreur quelque part");
									}
									
									terminee=true;
									
								}
								else
								{
									soustractions.push([touche,soustractions[casesoustrationchoisie[0]][1]+1]);
								}
							}
							
						}
						if (casesoustrationchoisie[1]==0)
						{
							if (soustractions[casesoustrationchoisie[0]].length==3)
							{
								soustractions[casesoustrationchoisie[0]][2]=soustractions[casesoustrationchoisie[0]][2]+touche;
							}
							else
							{
								(soustractions[casesoustrationchoisie[0]]).push(touche);
							}
						}
					
						
					}
				}
					affichedivision();
			}
		}
	},false);

async function initialisation()
{
	
	quotient_eleve="";
    diviseur=tableau_diviseur[Math.floor(Math.random()*tableau_diviseur.length)];
    if(decimaux == 0)
    {
        quotient=Number((Math.floor(Math.random()*100)));//.toPrecision(1));
	dividende=Number((diviseur*quotient));
    }
    else
    {
        quotient=Number((Math.floor(Math.random()*100000)/1000).toPrecision(3));
	dividende=Number((diviseur*quotient).toPrecision(6));
    }
	
	
	//dividende=30.36;
	//diviseur=4;
	//quotient=(dividende/diviseur).toPrecision(3);

	if (window.widget) {
        // Quand on quitte le widget
		window.widget.onleave.connect(() => {
		//Sauvegarde des paramètres au format 'chaine de caractères'
		window.sankore.setPreference('diviseur', diviseur);
		window.sankore.setPreference('quotient', quotient);
		window.sankore.setPreference('dividende', dividende);
		window.sankore.setPreference('niveau', niveau);
		window.sankore.setPreference('decimaux', decimaux);
		});
	}

	// Quand on revient sur le widget, on récupère les paramètres stockés
	if((window.sankore) && (await window.sankore.async.preference('diviseur'))) {
			diviseur=parseFloat(await window.sankore.async.preference('diviseur'));
			quotient=parseFloat(await window.sankore.async.preference('quotient'));
			dividende=parseFloat(await window.sankore.async.preference('dividende'));
			niveau=parseInt(await window.sankore.async.preference('niveau'));
			decimaux=parseInt(await window.sankore.async.preference('decimaux'));
			reset();
	}
	else
		nouveaunombres();
}

function nouveaunombres()
{
	quotient_eleve="";
    diviseur=tableau_diviseur[Math.floor(Math.random()*tableau_diviseur.length)];
    if(decimaux == 0)
    {
        quotient=Number((Math.floor(Math.random()*100)));//.toPrecision(1));
	dividende=Number((diviseur*quotient));
    }
    else
    {
        quotient=Number((Math.floor(Math.random()*100000)/1000).toPrecision(3));
	dividende=Number((diviseur*quotient).toPrecision(6));
    }


	//dividende=30.36;
	//diviseur=4;
	//quotient=(dividende/diviseur).toPrecision(3);

	reset();
}

function clearCanvas() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  var w = canvas.width;
  canvas.width = 1;
  canvas.width = w;

   

}

function reset()
{
	for (i=1;i<6;i++)
	{
		document.getElementById('niv'+i).style.backgroundColor='#cecece';
	}
	document.getElementById('niv'+niveau).style.backgroundColor='#d8d69e';
	boolquotient=true;
	casesoustrationchoisie=[-1,-1];
	switch (niveau)
	{
		case 1:
			
			bool_affichage_produit=true;
			bool_controle_chiffre_quotient=true;
			bool_virgule_automatique=true;
			bool_soustraction_automatique=true;
		break;
		case 2:
			bool_affichage_produit=false;
			bool_controle_chiffre_quotient=true;
			bool_virgule_automatique=true;
			bool_soustraction_automatique=true;
		break;
		case 3:
			bool_affichage_produit=false;
			bool_controle_chiffre_quotient=false;
			bool_virgule_automatique=true;
			bool_soustraction_automatique=true;
		break;
		case 4:
			bool_affichage_produit=false;
			bool_controle_chiffre_quotient=false;
			bool_virgule_automatique=false;
			bool_soustraction_automatique=true;
		break;
		case 5:
			bool_affichage_produit=false;
			bool_controle_chiffre_quotient=false;
			bool_virgule_automatique=false;
			bool_soustraction_automatique=false;
		break;
	}
	if (bool_affichage_produit)
	{
		document.getElementById('bool_affichage_produit').style.backgroundColor='#bcff94';
	}
	else
	{
		document.getElementById('bool_affichage_produit').style.backgroundColor='#ffb094';
	}
	if (bool_controle_chiffre_quotient)
	{
		document.getElementById('bool_controle_chiffre_quotient').style.backgroundColor='#bcff94';
	}
	else
	{
		document.getElementById('bool_controle_chiffre_quotient').style.backgroundColor='#ffb094';
	}
	if (bool_virgule_automatique)
	{
		document.getElementById('bool_virgule_automatique').style.backgroundColor='#bcff94';
	}
	else
	{
		document.getElementById('bool_virgule_automatique').style.backgroundColor='#ffb094';
	}
	if (bool_soustraction_automatique)
	{
		document.getElementById('bool_soustraction_automatique').style.backgroundColor='#bcff94';
	}
	else
	{
		document.getElementById('bool_soustraction_automatique').style.backgroundColor='#ffb094';
	}
	
	boolterminee=false;
	terminee=false;
	largeurfont = context.measureText('1').width; 
	positionvirgule=0; // c'est la position de la virgule pour qu'elle soit placée artificiellement

	virguleplaceedansquotient=false; //si virgule placée par l'élève

	quotient_eleve=""; //quotient trouvé par l'élève
	rangchiffreatomber=0; // c'est le combien-tieme chiffre à faire tomber
	

	
	quotient_eleve="";
	
	
	
	dividendech=dividende.toString();
	positionvirgule=dividendech.indexOf('.');

	var virgule = new RegExp('[\.,]', 'gi');
	dividendech=dividendech.replace(virgule, ''); //dividendech n' a pas de virgule pour permettre un écart constant entre les chiffres
	
	
	if (((quotient.toString()).split('.')).length==1)
	{
		endroit_initial=endroitoubarre - (dividendech.length+1)*largeurfont;
	}
	else
	{
		endroit_initial=endroitoubarre - (dividendech.length+1+(((quotient.toString()).split('.'))[1]).length)*largeurfont;
	}

	soustractions=[];
	
	// combien de chiffre à prendre en compte au début
	
	k=0;
	while ((Number((dividende.toString()).substring(0,k+1)<diviseur)&&((positionvirgule>k+1)||(positionvirgule==-1))))
	{
		k++;
	}
	rangchiffreatomber=k;
	oncommencea=k;
	nbencours=Number((dividende.toString()).substring(0,k+1));
	soustractions.push([nbencours.toString(),rangchiffreatomber]);
	ecritmessage("");
	
	affichedivision();
}
function affichesolution()
{
	reset();
	terminee=false;
	boolterminee=true;
	nbencours=Number(soustractions[0][0]);
	i=0;
	reste=1;
	var virgule = new RegExp('[\.,]', 'gi');
	quotient_eleve=(quotient.toString()).replace(virgule, ',');
	while ((!terminee)&&(i<10))
	{
		soustractions[i].push((Number(nbencours)-Number(Number(nbencours)%Number(diviseur))).toString());
		reste=nbencours-Number(soustractions[i][2]);
		console.log(reste);
		if (dividendech.length>(soustractions[i][1]+1))
		{
			soustractions.push([reste.toString()+dividendech[soustractions[i][1]+1],soustractions[i][1]+1]);
			nbencours=Number(reste.toString()+dividendech[soustractions[i][1]+1]);
		}
		else
		{
			
			if (reste==0)
			{
				terminee=true;
				soustractions.push(["0",soustractions[i][1]]);
			}
			else
			{
				soustractions.push([reste.toString()+"0",soustractions[i][1]+1]);
				
				nbencours=Number(reste.toString()+"0");
				dividendech=dividendech+"0";
				
			}
		}
		
		
		i++;
		//console.log(i);
		
		
	}

		affichedivision();

}

function cadre(x,y,longueur,couleur)//x en décalage et y et longueur sont des coordonnées entières en fonction du nombre de caractères à partir de la fin du mot 

{
	context.beginPath();
	if (couleur=="vert")
	{
		context.fillStyle = '#dfffe2';
	}
	else
	{
		context.fillStyle = '#ededed';	
	}
	if (couleur=="vert")
	{
		roundRect(context,endroit_initial+(x-longueur)*largeurfont,50+y*taillefont, longueur*largeurfont, taillefont+10,true);	
	}
	else
	{
		context.fillRect(endroit_initial+(x-longueur)*largeurfont,50+y*taillefont, longueur*largeurfont, taillefont+10);	
		
	}
	context.closePath();
}

function affichedivision()
{
	clearCanvas();
	
	context.font = taillefont+"px opendys";
	
	context.fillText(diviseur,endroitoubarre+10, 50); 
	
	//affichage des produits
	if (bool_affichage_produit)
	{
		for (i=0;i<10;i++)
		{
			context.fillText(i.toString()+"×"+(diviseur.toString())+"="+(i*diviseur).toString(),40	,50+taillefont*i);
		}
	}
	
	if (!bool_soustraction_automatique)
	{
		
		var virgule = new RegExp('[\.,]', 'gi');
		var lim=((quotient_eleve.toString()).replace(virgule, '')).length;
			for (i=0;i<Math.min(lim,soustractions.length);i++)
			{
////
				
				if ((soustractions[i].length==3))
				{
					if (soustractions[i][2]=="")//affichage du signe moins si chaine vide
					{
						context.beginPath();
						context.fillStyle = '#000';
						context.fillText("-",endroit_initial-(oncommencea+Math.max(0,soustractions[i][2].length-soustractions[i][0].length))*largeurfont	,50+taillefont+taillefont*i*2);
						context.closePath();
					}
					cadre(oncommencea+i+1,i*2,Math.max(1,soustractions[i][2].length),"gris");
					if (soustractions[i][2]!="")//cadre du résultat de la soustraction
					{
						if (soustractions.length>i+1)
						{
								cadre(oncommencea+2+i,i*2+1,Math.max(1,soustractions[i+1][0].length),"gris");
						}
						else
						{
								cadre(oncommencea+2+i,i*2+1,Math.max(1,soustractions[i][2].length),"gris");
	

						}
					}
					
				}
				else
				{
					context.beginPath();
					context.fillStyle = '#000';
					context.fillText("-",endroit_initial-(1)*largeurfont	,50+taillefont+taillefont*i*2);
					context.closePath();
					cadre(oncommencea+i+1,i*2,1,"gris");
					
				}
				

			
			}
			
			
	}
	
	
	// affichage du cadre d'écriture si le quotient est selectionne 
	if (boolquotient)
	{
		context.beginPath();
		 context.fillStyle = '#dfffe2';
		//context.fillRect(endroitoubarre+10,50+20,  Math.max(1,(quotient_eleve.length))*largeurfont, taillefont+10);
		roundRect(context,endroitoubarre+10,50+20,  Math.max(1,(quotient_eleve.length))*largeurfont, taillefont+10,true);
			context.closePath();
			
	}
	else // affichage du cadre d'écriture  des soustrations si le quotient n'a pas été cliqué 
	{
		context.beginPath();
		 context.fillStyle = '#ededed';
		context.fillRect(endroitoubarre+10,50+20, Math.max(1,(quotient_eleve.length))*largeurfont, taillefont+10);
		//roundRect(context,endroitoubarre+10,50+20, Math.max(1,(quotient_eleve.length))*largeurfont, taillefont+10,true);
		context.closePath();
		
		
		
		if ((casesoustrationchoisie[0]>-1)&&(casesoustrationchoisie[1]>-1))
		{
			if (casesoustrationchoisie[1]==0)
			{
				
				context.beginPath();

				if ((soustractions[casesoustrationchoisie[0]].length==3))
				{	
					cadre(oncommencea+casesoustrationchoisie[0]+1,casesoustrationchoisie[0]*2,Math.max(1,soustractions[casesoustrationchoisie[0]][2].length),"vert");
				}
				else
				{

					cadre(oncommencea+casesoustrationchoisie[0]+1,casesoustrationchoisie[0]*2,1,"vert");
					
				}
				context.closePath();
			}
			else//if (casesoustrationchoisie[1]==1)
			{
				if ((soustractions[casesoustrationchoisie[0]].length==3))
				{
					//cadre(oncommencea+casesoustrationchoisie[0]+2,casesoustrationchoisie[0]*2+1,Math.max(1,soustractions[casesoustrationchoisie[0]+1][0].length),"vert");
					cadre(oncommencea+casesoustrationchoisie[0]+1,casesoustrationchoisie[0]*2+1,Math.max(1,soustractions[casesoustrationchoisie[0]+1][0].length-1),"vert");

				}
			
			}
		}
		
		
	}	
	
	//on positionne la virgule s'il y en a une
	context.beginPath();
	if (positionvirgule>-1)
	{
		context.fillStyle = '#000';
		context.fillText(",",endroit_initial+(positionvirgule)*largeurfont-10,52);
	}
	
	context.fillStyle = '#000';
	context.moveTo(endroit_initial	,50-taillefont+4);
	context.lineTo(endroit_initial+3	,50-taillefont);
	context.lineTo(endroit_initial+(oncommencea+1)*largeurfont-3,50-taillefont);
	context.lineTo(endroit_initial+(oncommencea+1)*largeurfont,50-taillefont+4);
	context.stroke();
	context.closePath();
	
	context.fillText(quotient_eleve,endroitoubarre+10,50+20+taillefont);
	context.fillText(dividendech, endroit_initial,50);
	
	if (soustractions[0].length==3)
	{
		context.beginPath();
		context.setLineDash([]);
		context.fillStyle = '#000';
		context.lineCap = "round";
		context.fillText("-"				,endroit_initial-(1+Math.max(0,soustractions[0][2].length-soustractions[0][0].length))*largeurfont	,50+taillefont);
		context.fillText(soustractions[0][2],endroit_initial+(	soustractions[0][1]+1-(soustractions[0][2].toString()).length)*largeurfont	,50+taillefont);
		context.moveTo(						 endroit_initial-(1+Math.max(0,soustractions[0][2].length-soustractions[0][0].length))*largeurfont,50+taillefont+4);
		context.lineTo(endroit_initial+((soustractions[0][0].toString()).length)*largeurfont,50+taillefont+4);
		context.stroke();
		context.closePath();
	}
	
	
	if (soustractions.length>1)
	{
		for (i=1;i<soustractions.length;i++)
		{
			
			if (((boolterminee)&&(i<soustractions.length-1))||(!boolterminee))
			{
				if (soustractions[i][0]!="")// on ne descend le chiffre que si le résultat de la soustraction a été calculé
				{
					context.beginPath();
					context.setLineDash([1, 3]);
					context.lineCap = "round";
					context.fillStyle = '#000';
					context.moveTo(endroit_initial+(0.5+soustractions[i][1])*largeurfont	,50+4);
					context.lineTo(endroit_initial+(0.5+soustractions[i][1])*largeurfont,50+(i-1)*2*taillefont+taillefont+4);
					context.stroke();
					context.closePath();
				}
			}
			context.fillText((soustractions[i][0]), endroit_initial+(1+soustractions[i][1]-((soustractions[i][0]).toString()).length) *largeurfont,50+i*2*taillefont);
			if (soustractions[i].length==3)
			{
				context.fillText("-"				,endroit_initial+(soustractions[i][1]-Math.max(((soustractions[i][2]).toString()).length,((soustractions[i][0]).toString()).length)) *largeurfont	,50+i*2*taillefont+taillefont);
				context.fillText(soustractions[i][2],endroit_initial+(1+soustractions[i][1]- ((soustractions[i][2]).toString()).length )*largeurfont		,50+i*2*taillefont+taillefont);
				context.beginPath();
				context.setLineDash([]);
				context.lineCap = "round";
				context.fillStyle = '#000';
				context.moveTo(endroit_initial+(soustractions[i][1]-((soustractions[i][0]).toString()).length) *largeurfont	,50+i*2*taillefont+taillefont+4);
				context.lineTo(endroit_initial+(1+soustractions[i][1]-((soustractions[i][0]).toString()).length) *largeurfont	+((soustractions[i][0].toString()).length)*largeurfont,50+i*2*taillefont+taillefont+4);
				context.stroke();
				context.closePath();
				

			}
		}
	}
	//barre de la division
	context.beginPath();
	context.setLineDash([]);
	context.lineWidth = 5;
	context.lineCap = "round";
	context.fillStyle = '#000';
	context.moveTo(endroitoubarre, 10);
	context.lineTo(endroitoubarre, largeur-10);
	context.moveTo(endroitoubarre, 60);
	context.lineTo(longueur-10, 60);
	context.stroke();
	context.closePath();


}

// ajout fonction rectangle avec coin arrondi https://stackoverflow.com/questions/1255512/how-to-draw-a-rounded-rectangle-on-html-canvas

function roundRect(ctx, x, y, width, height,fill, stroke, radius) 
{
  if (typeof stroke === 'undefined') {
    stroke = true;
  }
  if (typeof radius === 'undefined') {
    radius = 5;
  }
  if (typeof radius === 'number') {
    radius = {tl: radius, tr: radius, br: radius, bl: radius};
  } else {
    var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
    for (var side in defaultRadius) {
      radius[side] = radius[side] || defaultRadius[side];
    }
  }
  ctx.beginPath();
  ctx.moveTo(x + radius.tl, y);
  ctx.lineTo(x + width - radius.tr, y);
  ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
  ctx.lineTo(x + width, y + height - radius.br);
  ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
  ctx.lineTo(x + radius.bl, y + height);
  ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
  ctx.lineTo(x, y + radius.tl);
  ctx.quadraticCurveTo(x, y, x + radius.tl, y);
  ctx.closePath();
  if (fill) {
    ctx.fill();
  }
  if (stroke) {
    ctx.stroke();
  }

}
//
window.onload=initialisation;
