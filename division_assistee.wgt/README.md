# Division assistée
Générateur de division décimale ou euclidienne assistée avec correction possible.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Mars 2020

## Licence
GPLv2

## Versions
Version 8 -
version initiale

Version 8.1 -
Ajout d'un bouton 'euclidienne/décimale' pour basculer entre une division décimale et euclidienne. Utile pour les classes de CM1 et CM2.

Version 8.2 -
Ajout de la persistance des paramètres. Si on redémarre OpenBoard, la page avec l'application s'ouvrira avec la même division.

A FAIRE - Le mode euclidien propose uniquement des divisions avec un reste nul. Cela lasse les élèves. Il faudrait améliorer ce mode pour qu'il génère aléatoirement des résultats avec des restes.

## Sources
* Arnaud DURAND [https://www.mathix.org/division_assistee/](https://www.mathix.org/division_assistee/) et [https://mathix.org/linux/archives/13371](https://mathix.org/linux/archives/13371)


