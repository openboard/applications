// Auto Multiples 2024 Arnaud Champollion, licence GNU/GPL 3.0

const body=document.body;
const spanMultiplicateurs = document.querySelectorAll('.span-multiplicateur');
const spanResultats = document.querySelectorAll('.span-resultat');
const spanListe = document.getElementById('span-liste');
const lignesCalcul = document.querySelectorAll('.ligne-multiple');
const boutonValider = document.getElementById('bouton-valider');
const clavier = document.getElementById('clavier');
const ligne1portrait = document.getElementById('ligne-1-portrait');
const ligne2portrait = document.getElementById('ligne-2-portrait');
const zonePrincipale = document.getElementById('zone-principale');
const boutonsClavier = document.querySelectorAll('.bouton-nombre');
const boutonClavier = document.getElementById('bouton-clavier');
const divApropos = document.getElementById('a-propos');
const darkbox = document.getElementById('darkbox');
const divEntete = document.getElementById('en-tete');

const ordreBoutonsPortrait = ['1','2','3','4','5','6','7','8','9','0','gomme','entree']

//////////////// TEST SI TACILE OU ORDI //////////////
let tactile;
let ordi=true;
let clavier_on=true;
tactile = "ontouchstart" in document.documentElement;

// Décommmenter ci-dessous pour forcer le mode tactile
// tactile = true;

margeLignes=8;
paddingLignes=8;

clavierTactileForce=false;

if (tactile){inputNombre = document.getElementById("input-nombre-tactile");}
else {inputNombre = document.getElementById("input-nombre-ordi");}

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
//----------------------------

// On affiche l'Input Nombre obtenu.
inputNombre.classList.remove('hide');

if (tactile){
    // Pour les écrans tactiles
    boutonClavier.classList.add('hide');
    inputNombre.innerText='';
    inputNombre.addEventListener('touchstart',raz);
} else {
    boutonValider.classList.remove('hide');
    inputNombre.focus();
    inputNombre.value='';
    majLargeurInputNombre();
    inputNombre.addEventListener('input',majLargeurInputNombre);
    inputNombre.addEventListener('mousedown',raz);
}
if (openboard){
    divEntete.classList.add('hide');
    body.style.background='none';
    inputNombre.style.borderColor='#000000';
    clavierOn;
    boutonValider.classList.add('hide');
    spanListe.style.fontSize = inputNombre.style.fontSize = '24px';
    margeLignes = paddingLignes = 4;
    lignesCalcul.forEach(ligne => {
        ligne.style.fontSize = '24px';
        ligne.style.margin = margeLignes+'px';
        ligne.style.padding = paddingLignes+'px';
    });
}
////////////////////////////////////////////////////

///// TEST SI NOMBRE DANS L'URL //////
async function majDepuisUrl() {
    // Récupérer la partie de l'URL après le symbole #
    const fragmentUrl = window.location.hash.substring(1);  
    // Tester si c'est un nombre entier
    const nombreUrl = parseInt(fragmentUrl);
    if (!isNaN(nombreUrl)) {
        if (tactile) {
            inputNombre.innerText = nombreUrl;
        } else {
            inputNombre.value = nombreUrl;
        }
        calcule();
    } else {
        try {
            // Utilisation de await pour attendre que la promesse se résolve
            const nombreStocke = await litDepuisStockage('nombre');
            console.log('Récupération du nombre ' + nombreStocke);
            if (!isNaN(nombreStocke)){
                if (tactile) {
                    inputNombre.innerText = nombreStocke;
                } else {
                    inputNombre.value = nombreStocke;
                }
                calcule();
            }
        } catch (error) {
            console.error('Erreur lors de la récupération du nombre depuis le stockage :', error);
        }
    }
}

async function litDepuisStockage(cle) {
    if (openboard){ //Récupération pour Openboard

        valeurAretourner = await window.sankore.async.preference('automultiples-'+cle);
        console.log("lecture "+cle+"="+valeurAretourner); // Pour la console
    
      } else { // Récupération en Web
    
        valeurAretourner = localStorage.getItem('automultiples-'+cle);
        console.log("lecture depuis stockage "+cle+"="+valeurAretourner); // Pour la console
    
      } 

      return valeurAretourner;
}

function stocke(cle,valeur){
    console.log("stockage "+cle+"="+valeur);
    if (openboard){
      window.sankore.setPreference('automultiples-'+cle,valeur);
    } else {
      localStorage.setItem('automultiples-'+cle,valeur);
    }
  }

function clavierTactile(mode){
    console.log('clavierTactile');

    if (mode==='raz'){clavierTactileForce=true}
    else {clavierTactileForce=!clavierTactileForce;}

    if (clavierTactileForce){
        clavier.classList.remove('hide');
        if (openboard){
            boutonValider.classList.add('hide');
        }
        boutonClavier.innerHTML='Masquer le clavier';
    } else {
        clavier.classList.add('hide');
        if (!openboard){
            boutonValider.classList.remove('hide');
        }
        boutonClavier.innerHTML='Afficher le clavier';
    }
}

function raz(){
    console.log('raz');

    inputNombre.value='';    
    majUrl('');    
    spanMultiplicateurs.forEach(span => {
        span.innerHTML='&nbsp&nbsp';
    });
    spanResultats.forEach(span => {
        span.innerHTML='&nbsp&nbsp';
        span.classList.remove('resultat');
    });
    lignesCalcul.forEach(ligne => {
        ligne.style.width=null;
    });
    
    if (tactile && !openboard){
        clavierOn();
    } else if(openboard){
        clavierTactile('raz');
    }
}

function clavierOn(){

    console.log('clavierOn');
    
    clavier.classList.remove('hide');
    inputNombre.innerText='';

    majUrl('');
    
    spanMultiplicateurs.forEach(span => {
        span.innerText=null;
    });
    spanResultats.forEach(span => {
        span.innerText=null;
        span.classList.remove('resultat');
    });
    lignesCalcul.forEach(ligne => {
        ligne.style.width=null;
    });
}

function ouvre(div){
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}

function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}



function insere(chiffre) {

    if (chiffre==='e'){calcule();}

    else if (chiffre==='r'){
        if (tactile){inputNombre.innerText=inputNombre.innerText.slice(1);}
        else {inputNombre.value=inputNombre.value.slice(1);}
    }

    else {
        if (tactile){inputNombre.innerText+=chiffre;}
        else {inputNombre.value+=chiffre;} 
    }
}

function calcule(){

    // Réinitialiser la largeur des éléments dans lignesCalcul
    lignesCalcul.forEach(function(element) {
        element.style.width = null;
    });

    let multiplicateur;

    // Déterminer la valeur de multiplicateur en fonction de tactile
    if (tactile) {
        multiplicateur = parseInt(inputNombre.innerText);
    } else {
        multiplicateur = parseInt(inputNombre.value);
    }   

    let multiplicande = 2;

    // Si multiplicateur est un nombre
    if (!isNaN(multiplicateur)) {

        // Stocker la valeur de multiplicateur
        stocke('nombre', multiplicateur);

        // Gérer l'affichage du clavier ou du bouton de validation
        if (openboard || tactile){
            clavier.classList.add('hide');
            clavierTactileForce = false;
            boutonClavier.innerHTML = 'Afficher le clavier';
        } else {
            boutonValider.classList.remove('hide');
        }        
        
        // Mettre à jour les éléments spanMultiplicateurs avec multiplicateur
        spanMultiplicateurs.forEach(span => {
            span.innerText = multiplicateur;
        });

        // Mettre à jour les éléments spanResultats avec les résultats de la multiplication
        spanResultats.forEach(span => {
            span.innerText = multiplicande * multiplicateur;
            span.classList.add('resultat');
            multiplicande += 1;            
        });

        // Trouver la largeur maximale des éléments spanResultats
        let largeurMaximale = trouverLargeurMaximale(spanResultats);

        // Appliquer la largeur maximale à tous les éléments spanResultats
        spanResultats.forEach(function(element) {
            console.log('largeurMaximale ' + largeurMaximale)
            element.style.width = (largeurMaximale - 11) + 'px';
        });

        // Trouver la largeur maximale des éléments lignesCalcul
        largeurMaximale = trouverLargeurMaximale(lignesCalcul);

        // Appliquer la largeur maximale à tous les éléments lignesCalcul
        lignesCalcul.forEach(function(element) {
            element.style.width = (largeurMaximale - paddingLignes * 2) + 'px';
        });

        // Mettre à jour l'URL avec la valeur de multiplicateur
        majUrl(multiplicateur);

    } else {
        // Si multiplicateur n'est pas un nombre, vider inputNombre
        if (tactile) {
            inputNombre.innerText = '';
        } else {
            inputNombre.value = '';
        }  
    }
}

// Fonction pour trouver la largeur réelle parmi les éléments (sans padding ni bordure)
function trouverLargeurMaximale(elements) {
    let largeurMax = 0;
    elements.forEach(function(element) {
        const largeur = element.offsetWidth;
        if (largeur > largeurMax) {
            console.log('plus grand '+element.innerHTML)
            largeurMax = Math.round(largeur)+1;
        }
    });
    return largeurMax;
}

function majLargeurInputNombre() {
    let largeur=inputNombre.value.length;
    if (largeur===0){largeur=1;}
    inputNombre.style.width=largeur+'em';
}

function majUrl(multiplicateur){
        // Obtenir l'URL actuelle
    var currentURL = window.location.href;

    // Séparer l'URL en deux parties: avant et après le #
    var parts = currentURL.split('#');
    var baseURL = parts[0]; // URL avant #

    // Construire la nouvelle URL
    var newURL;
    
    if (multiplicateur===''){
        newURL = baseURL;
    } else {
        newURL = baseURL + '#' + multiplicateur;
    }    

    // Mettre à jour l'URL
    window.history.pushState({ path: newURL }, '', newURL);
}


// Fonction pour détecter le changement d'orientation
function detectOrientation() {
    if (window.matchMedia("(orientation: portrait)").matches) {
        let numero=1;
        let ligne=ligne1portrait;
        ordreBoutonsPortrait.forEach(item => {
            ligne.appendChild(document.getElementById('bouton-'+item));
            if (numero===6){ligne=ligne2portrait;}
            numero+=1;
        });
        clavier.style.flexDirection='row';
        clavier.style.flexWrap='wrap';
        clavier.classList.add('clavier-bas');
        clavier.classList.remove('clavier-droite');
    } else {
        clavier.style.flexDirection=null;
        clavier.classList.remove('clavier-bas');
        clavier.classList.add('clavier-droite');
        document.getElementById('ligne-1').appendChild(document.getElementById('bouton-7'));
        document.getElementById('ligne-1').appendChild(document.getElementById('bouton-8'));
        document.getElementById('ligne-1').appendChild(document.getElementById('bouton-9'));
        document.getElementById('ligne-2').appendChild(document.getElementById('bouton-4'));
        document.getElementById('ligne-2').appendChild(document.getElementById('bouton-5'));
        document.getElementById('ligne-2').appendChild(document.getElementById('bouton-6'));
        document.getElementById('ligne-3').appendChild(document.getElementById('bouton-1'));
        document.getElementById('ligne-3').appendChild(document.getElementById('bouton-2'));
        document.getElementById('ligne-3').appendChild(document.getElementById('bouton-3'));
        document.getElementById('ligne-4').appendChild(document.getElementById('bouton-0'));
        document.getElementById('ligne-4').appendChild(document.getElementById('bouton-gomme'));
        document.getElementById('ligne-4').appendChild(document.getElementById('bouton-entree'));
    }
}

document.addEventListener('keydown',function(event) {
    if(event.keyCode === 13){calcule();}

});

// Ajouter un écouteur d'événement pour l'événement hashchange
window.addEventListener('hashchange', majDepuisUrl);

// Appeler la fonction pour mettre à jour l'application au chargement de la page
window.addEventListener('load', majDepuisUrl);

// Appelez la fonction au chargement de la page et chaque fois que l'orientation de l'appareil change
detectOrientation();
window.addEventListener("orientationchange", detectOrientation);
window.addEventListener("resize", detectOrientation);

