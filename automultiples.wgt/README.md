# Auto multiples

Automultiples est une application qui affiche les multiples d'un nombre.

Elle peut être notamment utile en technique opératoire de la division, lorsque l'objectif de la séance n'est pas justement de chercher ces multiples.

## Licence

Cette application est écrite par Arnaud Champollion et partagée sous licence libre GNU/GPL

Sources et téléchargement de versions hors-ligne / Openboard sur la Forge.

## Démonstration

Une instance en fonctionnement est utilisable à l'adresse suivante :

[https://educajou.forge.apps.education.fr/automultiples/](https://educajou.forge.apps.education.fr/automultiples/)

## Auteur

Arnaud Champollion [https://educajou.forge.apps.education.fr/](https://educajou.forge.apps.education.fr/)

mars 2024

## Versions

Version 1

version initiale


## Sources
* Arnaud Champollion [ici](https://forge.apps.education.fr/educajou/automultiples) et [ici](https://educajou.forge.apps.education.fr/automultiples/)
