# Droite interactive

## Description

Droite interactive permet de travailler sur les nombres entiers, avec une droite graduée réglable, ainsi que des blocs, points et marqueurs.

## Démonstration

[https://educajou.forge.apps.education.fr/droite-interactive/](https://educajou.forge.apps.education.fr/droite-interactive/)