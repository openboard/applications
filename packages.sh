#
# Version number
if [ $# -eq 0 ]
  then
    echo "No VERSION supplied"
    VERSION_M="0.1.10"
else
    VERSION_M="$1"
    echo "The VERSION will be " $VERSION_M
fi


#
# Check the project integrity
echo "************************************************************************************************"
echo "*   Check the integrity of the applications..."
echo "************************************************************************************************"
./check_applications.sh
if [[ $? -ne 0 ]]; then
    echo "Please, check the applications issue above before building the packages."
    exit 1
fi
echo ""

#
# Get the Git information
echo "************************************************************************************************"
echo "*   Get the last Git commit SHA..."
echo "************************************************************************************************"
git rev-parse --verify HEAD
echo "and write it in a file..."
git rev-parse --verify HEAD > last_commit_sha.txt
echo "and keep the 8th first caracters..."
last_commit_sha=$(head -c 8 ./last_commit_sha.txt)
echo $last_commit_sha
VERSION_BUILD=$last_commit_sha
echo "The version of the packages will be: "$VERSION_M"."$VERSION_BUILD
echo "Update the version number in changelog file..."
sed 's/VERSION/'$VERSION_M'.'$VERSION_BUILD'/' ./changelog.template > ./changelog
echo ""

#
# Build the DEB package
echo "************************************************************************************************"
echo "*   Build the DEB package..."
echo "************************************************************************************************"
echo "Create the source folder with the version names..."
deb_folder_name="openboard-applications-"$VERSION_M"."$VERSION_BUILD
cd ./installer/linux/deb/
mkdir -p $deb_folder_name
mkdir -p $deb_folder_name"/debian"
echo "Copy files from the source folder..."
cp ./openboard-applications_src/debian/* $deb_folder_name"/debian"
echo "Copy other files, changelog and readme..."
cp ../../../changelog $deb_folder_name"/debian"
cp ../../../index.en.md $deb_folder_name"/debian"
mv $deb_folder_name"/debian/"index.en.md $deb_folder_name"/debian/"README.Debian

echo "Generate DEB package..."
export EMAIL="seb.vallet@tutanota.com"
export DEBFULLNAME="Sebastien Vallet"
cd $deb_folder_name
dh_make -i --native -v      # dh-make package
dpkg-buildpackage -us -uc   # dpkg-dev package

echo "The DEB package is created."
cd ../../../..
mv -f ./installer/linux/deb/openboard-applications*.deb ./
echo "The DEB package is located in the root folder."
echo ""

#
# Build the Windows installer
echo "************************************************************************************************"
echo "*   Build the Windows installer..."
echo "************************************************************************************************"
echo "Generate the Windows installer..."
echo "Generate the .nsi file with the VERSION code."
sed 's/VERSION_M/'$VERSION_M'/' ./installer/win/openboard-applications.nsi.template > ./installer/win/openboard-applications.nsi
sed -i 's/VERSION_BUILD/'$VERSION_BUILD'/' ./installer/win/openboard-applications.nsi
cd ./installer/win/
makensis ./openboard-applications.nsi
echo "The Windows installer is created."
cd ../..
mv -f ./installer/win/openboard-applications*.exe ./
echo "The Windows installer is moved in the root folder."
echo "************************************************************************************************"
echo ""
