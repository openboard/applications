# Get the latest release tag
RELEASE_NAME="$(curl -s https://forge.apps.education.fr/api/v4/projects/611/releases | jq '.[0].tag_name' -r)"
echo "The latest release tag: "$RELEASE_NAME

# Get the first link of the latest release
RELEASE_LINKS="$(curl -s https://forge.apps.education.fr/api/v4/projects/611/releases/$RELEASE_NAME/assets/links/ | jq '.[0].url' -r)"
# Get the second link of the latest release
RELEASE_LINKS+=("$(curl -s https://forge.apps.education.fr/api/v4/projects/611/releases/$RELEASE_NAME/assets/links/ | jq '.[1].url' -r)")
# Check which one is for windows and which one is for Debian
if [[ " ${RELEASE_LINKS[0]} " =~ [[:space:]]".exe"[[:space:]] ]]; then
    WIN_LATEST_RELEASE_LINK=${RELEASE_LINKS[0]}
    DEB_LATEST_RELEASE_LINK=${RELEASE_LINKS[1]}
else
    WIN_LATEST_RELEASE_LINK=${RELEASE_LINKS[1]}
    DEB_LATEST_RELEASE_LINK=${RELEASE_LINKS[0]}
fi
echo "The Windows link: "${WIN_LATEST_RELEASE_LINK}
echo "The Debian link: "${DEB_LATEST_RELEASE_LINK}

# Update the README files with the release links
sed 's@WIN_LATEST_RELEASE_LINK@'$WIN_LATEST_RELEASE_LINK'@' ./index.md.template > ./index.md
sed -i 's@DEB_LATEST_RELEASE_LINK@'$DEB_LATEST_RELEASE_LINK'@' ./index.md

sed 's@WIN_LATEST_RELEASE_LINK@'$WIN_LATEST_RELEASE_LINK'@' ./index.en.md.template > ./index.en.md
sed -i 's@DEB_LATEST_RELEASE_LINK@'$DEB_LATEST_RELEASE_LINK'@' ./index.en.md

echo "The README files are updated with these links."
