# Mathadorix
Une version numérique du jeu mathador. Application pour tous les élèves. Permet de s’exercer au calcul mental.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Décembre 2014

## Licence
GPLv3

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/)>.

## Versions
Version 0.1
version initiale

version 0.2
Ajout de la persistance des paramètres. Si on redémarre OpenBoard, la page avec l'application s'ouvrira avec les mêmes nombres.

## Sources
* Arnaud DURAND [https://mathix.org/mathador/](https://mathix.org/mathador/)
