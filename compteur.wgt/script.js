const divConteneur = document.getElementById('conteneur');
const boutonsGauche = document.getElementById('boutons-gauche');
const boutonsDroite = document.getElementById('boutons-droite');
const inputNombre = document.getElementById("input-nombre");
const inputAnimation = document.getElementById("input-vitesse");
const listeDeroulante = document.getElementById("liste-deroulante");
const outilPlus = document.getElementById("outil-plus");
const outilMoins = document.getElementById("outil-moins");
const outilsPlusMoins = [outilMoins,outilPlus];
const boutonPanneau = document.getElementById('bouton-panneau');
const panneau = document.getElementById('panneau');
const boutonsPanneau = document.querySelectorAll('.bouton-panneau');
const casesListe = document.getElementById('cases-liste');
const darkbox = document.getElementById('darkbox');
const divApropos = document.getElementById('apropos');
const educajou = document.getElementById('educajou');
const checkboxCasesAuto = document.getElementById('checkbox-cases-auto');


// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);
//----------------------------

if (openboard){
    document.body.classList.add('openboard');
    educajou.href='https://educajou.forge.apps.education.fr/';
}

boutonsPanneau.forEach(bouton => {
    bouton.classList.remove('actif');
});

let listeCasesCochees = [];
let autoCases = true;
let largeurColonne = ((divConteneur.offsetWidth - 6 ) / 6) - 16;
let epaisseurOmbre = largeurColonne / 10;
let n = 0;
let actionContinueEnCours=false;
let continuousActionInterval=null;
let frequence=300;
let nombreDeDecimales=0;
let nombreDEntiers=0;
let chiffresEntiers=3;
let chiffresDecimaux=0;
let nombreUrl=false;
let partieEntiere;
let partieDecimale;
let sensPlusMoins=1;
let limiterHauteur=false;
let panneauVisible = false;
let delaiAnimation = 100;
let titresEntiers = ['UNITÉS','DIZAINES','CENTAINES'];
let titresDecimales = ['DIXIÈMES','CENTIÈMES','MILLIÈMES'];
let groupesEntiers = ['UNITÉS','MILLIERS','MILLIONS','MILLIARDS'];
let valeursListeDeroulante = {
    "millième(s)": -3,
    "centième(s)": -2,
    "dixième(s)": -1,
    "unité(s)": 0,
    "dizaine(s)": 1,
    "centaine(s)": 2,
    "millier(s)": 3,
    "dizaine(s) de milliers": 4,
    "centaine(s) de milliers": 5,
    "million(s)": 6,
    "dizaine(s) de millions": 7,
    "centaine(s) de millions": 8,
    "milliard(s)": 9,
    "dizaine(s) de milliards": 10,
    "centaine(s) de milliards": 11
  };

// On vérifie si des paramètres sont dans l'URL
function checkUrl() {
    console.log('Lecture URL');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    // Primtux
    if (urlParams.get('primtuxmenu')==="true") {
        educajou.style.display='none';
    }

    // Nombre
    if (urlParams.get('nombre')) {
        let chaine = urlParams.get('nombre');
        console.log('nombre URL '+chaine);

        nombreCustom(chaine);
    }
}

function nombreCustom(chaine) {
    let parties = chaine.split(',');
    console.log('nombreCustom chaine='+chaine)

    if (parties.length === 2) { // Vérifie qu'il y a une partie entière et une partie décimale
        partieEntiere = parties[0]; // Partie entière
        partieDecimale = parties[1]; // Partie décimale
        
        chiffresEntiers = partieEntiere.length;
        chiffresDecimaux = partieDecimale.length;

        console.log('partieEntiere '+partieEntiere);
        console.log('partieDecimale '+partieDecimale);

        nombreUrl = true;

    } else if (!isNaN(parseInt(chaine, 10))) { // Vérifie si la chaîne est un nombre entier
        partieEntiere = chaine;
        chiffresEntiers = partieEntiere.length;
        partieDecimale = '';
        chiffresDecimaux = 0;

        nombreUrl = true;

    } else {
        console.error("La chaîne n'est pas au bon format.");
    }
}

function appliqueReglages() {
    console.log('Application des réglages');
    inputNombre.value=1;
    inputAnimation.value = 1000 - delaiAnimation;
    console.log('Réglage du curseur de vitesse à '+inputAnimation.value);    
    outilPlus.classList.add('actif');    
    checkboxCasesAuto.checked=autoCases;
}

function changeCasesAuto(valeur){
    console.log("Change Cases Auto "+valeur)
    autoCases = valeur;
    stocke('auto-cases',valeur.toString());
    const cases = document.querySelectorAll('#cases-liste>input');
    cases.forEach(input => {
        input.disabled=valeur;
    });
    if (autoCases){
        autoCocheCases();
    }
}

function autoCocheCases() {
    const casesNombres = document.querySelectorAll('.case-nombre');
    const casesInput = document.querySelectorAll('#cases-liste>input');
    casesInput.forEach(input => {
        // Vérifie si l'ID de l'input est présent dans les IDs des cases-nombre
        const isChecked = Array.from(casesNombres).some(caseNombre => caseNombre.id === input.value);        
        // Coche l'input si isChecked est vrai, sinon décoche-le
        input.checked = isChecked;
        console.log('isCkecked '+isChecked);
        // Récupérer l'option correspondante dans la liste déroulante
        const option = listeDeroulante.querySelector(`option[value="${input.value}"]`);
        // Afficher ou masquer l'option en fonction de l'état de la case à cocher
        option.classList.toggle("hidden", !input.checked);
        majListeCasesCochees(input.value,isChecked);
    });
}

async function verifieStockageLocal() {
    console.log('Lecture stockage local');
    try {
        const vitesse = await litDepuisStockage('vitesse');
        if (!isNaN(parseInt(vitesse))){
            console.log('trouvé vitesse '+vitesse)
            delaiAnimation = 1000-parseInt(vitesse);
            console.log('delaiAnimation='+delaiAnimation);
        }
    } catch (error) {
        console.error('Erreur lors de la récupération du nombre depuis le stockage :', error);
    }
    
    try {
        const chaine = await litDepuisStockage('nombre');
        if (chaine){
            console.log('trouvé nombre '+chaine);
            nombreCustom(chaine)
        }
    } catch (error) {
        console.error('Erreur lors de la récupération du nombre depuis le stockage :', error);
    } 

    try {
        const autoCasesStockage = await litDepuisStockage('auto-cases');
        if (autoCasesStockage==='true' || autoCasesStockage==='false'){
            console.log('trouvé auto-cases '+autoCasesStockage);
            autoCases = autoCasesStockage === 'true';
            checkboxCasesAuto.checked = autoCases;
            console.log(autoCases);
        } else {
            console.log('Pas de réglage auto-cases');
        }
    } catch (error) {
        console.error('Erreur lors de la récupération du nombre depuis le stockage :', error);
    }    

    try {
        const listeCasesCocheesStockage = await litDepuisStockage('liste-cases-cochees');
        if (listeCasesCocheesStockage){
            console.log('liste-cases-cochees '+listeCasesCocheesStockage);
            listeCasesCochees = listeCasesCocheesStockage.split(',');
        } else {
            console.log('Pas de réglage liste-cases-cochees');
        }
    } catch (error) {
        console.error('Erreur lors de la récupération du nombre depuis le stockage :', error);
    } 
   
}

async function litDepuisStockage(cle) {
    console.log('Lecture de la clé '+cle);

if (openboard){ //Récupération pour Openboard

    valeurAretourner = await window.sankore.async.preference('compteur-'+cle);
    console.log("lecture "+cle+"="+valeurAretourner); // Pour la console

  } else { // Récupération en Web

    valeurAretourner = localStorage.getItem('compteur-'+cle);
    console.log("lecture depuis stockage "+cle+"="+valeurAretourner); // Pour la console

  } 

  return valeurAretourner;
}

function stocke(cle,valeur){
console.log("stockage "+cle+"="+valeur);
if (openboard){
  window.sankore.setPreference('compteur-'+cle,valeur);
} else {
  localStorage.setItem('compteur-'+cle,valeur);
}
}

// Début du programme
async function executeFunctions() {
    await verifieStockageLocal();
    await checkUrl();
    await appliqueReglages();
    await creerColonnes();
    await creerCases();    
    await majTaille();
}
executeFunctions();
///////////////////////

function creerCases() {

    // Ajouter les options à la liste déroulante et créer les cases à cocher
    Object.entries(valeursListeDeroulante).forEach(([libelle, valeur], index, array) => {
        // Création de l'option dans la liste déroulante
        const option = document.createElement("option");
        option.value = valeur;
        option.textContent = libelle;
        listeDeroulante.appendChild(option);

        // Création de la case à cocher
        const checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.value = valeur;
        if (listeCasesCochees.includes(valeur.toString())) {
            checkbox.checked=true;
        }
        checkbox.id = `checkbox_${libelle}`;
        const label = document.createElement("label");
        label.textContent = libelle;
        label.setAttribute("for", `checkbox_${libelle}`);
        casesListe.appendChild(checkbox);
        casesListe.appendChild(label);

        // Ajouter un gestionnaire d'événements pour détecter les changements dans les cases à cocher
        checkbox.addEventListener("change", function() {
            // Récupérer l'option correspondante dans la liste déroulante
            const option = listeDeroulante.querySelector(`option[value="${valeur}"]`);
            // Afficher ou masquer l'option en fonction de l'état de la case à cocher
            option.classList.toggle("hidden", !checkbox.checked);
            majListeCasesCochees(valeur,checkbox.checked);
        });

        // Masquer l'option correspondante si la case à cocher est décochée par défaut
        option.classList.toggle("hidden", !checkbox.checked);

        // Aller à la ligne
        casesListe.appendChild(document.createElement("br"));
    });

    listeDeroulante.value = 0;
    changeCasesAuto(autoCases);
}

function majListeCasesCochees(valeur, ajout) {
    const index = listeCasesCochees.indexOf(valeur.toString());
    console.log('majListeCasesCochees ' + valeur + ' ' + ajout + 'index='+index);
    if (ajout && index === -1) {
        listeCasesCochees.push(valeur);
    } else if (!ajout && index !== -1) {
        console.log('suppression de '+valeur)
        listeCasesCochees.splice(index, 1);
    }
    console.log(listeCasesCochees);
    stocke('liste-cases-cochees',listeCasesCochees.join(','));
}


function majNombre() {
    const casesEntiers = Array.from(document.getElementsByClassName('case-nombre'))
    .filter(element => !element.parentNode.classList.contains('decimale'));
    const casesDecimales = document.querySelectorAll('.decimale > .case-nombre');

    // Récupération des innerHTML des cases entières
    const innerHTMLCasesEntiers = Array.from(casesEntiers).map(caseEntier => caseEntier.innerHTML).join('');

    // Récupération des innerHTML des cases décimales
    const innerHTMLCasesDecimales = Array.from(casesDecimales).map(caseDecimale => caseDecimale.innerHTML).join('');

    // Concaténation des deux chaînes avec une virgule entre les cas entiers et décimaux
    const resultat = innerHTMLCasesEntiers + (innerHTMLCasesDecimales.length > 0 ? ',' + innerHTMLCasesDecimales : '');
    console.log('Nouveau nombre à stocker '+resultat);
    stocke('nombre',resultat);

}


function vitesseAnimation(valeur) {
    delaiAnimation = 1000 - parseInt(valeur);
    stocke('vitesse',parseInt(valeur));
}



function active(bouton,valeur){
    outilsPlusMoins.forEach(bouton => {
        bouton.classList.remove('actif')
    });
    bouton.classList.add('actif');
    sensPlusMoins = valeur;
}

function visibilitePanneau() {
    if (panneauVisible){
        panneau.style.right=null;
        boutonPanneau.style.right=null;     
    } else {
        panneau.style.right='0px';
        boutonPanneau.style.right='calc(100% + 5px)';
    }
    panneauVisible=!panneauVisible;
}

function boutonEnfonce(caseNombre,valeur){
    if (!actionContinueEnCours){
        plusmoins(caseNombre,valeur);
        actionContinueEnCours=true;
        continuousActionInterval = setInterval(function() {
            plusmoins(caseNombre,valeur);
            // Accélérer la fréquence à 100ms après le premier délai de 300ms
            if (frequence === 300) {
                frequence = 100;
                clearInterval(continuousActionInterval); // Arrête l'ancien intervalle
                continuousActionInterval = setInterval(function() {
                    plusmoins(caseNombre,valeur);
                }, frequence);
            }
        }, frequence);
    }
}


function boutonRelache(){
    actionContinueEnCours=false;
    frequence = 300;
    if (continuousActionInterval){clearInterval(continuousActionInterval);}
}






function creerColonnes() {
    console.log('---- Création initiale des colonnes ----')   
    for (let i = 0; i < chiffresEntiers; i++) {
        if (nombreUrl){chiffre = partieEntiere[chiffresEntiers-1-i]}
        else {chiffre = false}
        creerColonne('gauche',chiffre);
    }
    for (let i = 0; i < chiffresDecimaux; i++) {
        if (nombreUrl){chiffre = partieDecimale[i]}
        else {chiffre = false}
        creerColonne('droite',chiffre);
    }
}

function majTaille(){
    if (n>4){
    document.documentElement.style.setProperty('--taille-police-case', (100 / n) + 'vw');
    document.documentElement.style.setProperty('--largeur-colonne','calc((100% - 100px) / '+n+')');
    } else {
        document.documentElement.style.setProperty('--taille-police-case', (100 / 5) + 'vw');
        document.documentElement.style.setProperty('--largeur-colonne','calc((100% - 100px) / '+5+')');
    }
    
}

function creerColonne(cote,chiffre){
    let nouvelleColonne = document.createElement('div');
    nouvelleColonne.classList.add('colonne');

    let nouvelleCaseTitre = document.createElement('div');
    nouvelleCaseTitre.classList.add('case-titre');
    
    let nouveauGroupe = document.createElement('div');
    nouveauGroupe.classList.add('groupe');

    let nouveauBoutonHaut = document.createElement('button');
    nouveauBoutonHaut.classList.add('bouton-compteur','haut');

    let nouveauBoutonBas = document.createElement('button');
    nouveauBoutonBas.classList.add('bouton-compteur','bas');

    let nouvelleCaseNombre = document.createElement('div');
    nouvelleCaseNombre.classList.add('case-nombre');
    if (chiffre){
        nouvelleCaseNombre.innerHTML = chiffre;
    } else {
        nouvelleCaseNombre.innerHTML = '0';
    }
    nouvelleCaseNombre.style.boxShadow = 'inset 0 0 ' + epaisseurOmbre + 'px #ffffff';
    nouvelleCaseTitre.style.boxShadow = 'inset 0 0 ' + epaisseurOmbre/4 + 'px #ffffff';

    // Pour le bouton Haut
nouveauBoutonHaut.addEventListener('mousedown', function() {
    boutonEnfonce(nouvelleCaseNombre, 1);
});

nouveauBoutonHaut.addEventListener('touchstart', function(event) {
    event.preventDefault(); // Empêcher le comportement par défaut pour éviter les retards ou les double événements
    boutonEnfonce(nouvelleCaseNombre, 1);
});

nouveauBoutonHaut.addEventListener('mouseup', function() {
    boutonRelache(nouvelleCaseNombre, 1);
});

nouveauBoutonHaut.addEventListener('touchend', function(event) {
    event.preventDefault(); // Empêcher le comportement par défaut pour éviter les retards ou les double événements
    boutonRelache(nouvelleCaseNombre, 1);
});

nouveauBoutonHaut.addEventListener('mouseleave', function() {
    boutonRelache(nouvelleCaseNombre, 1);
});


// Pour le bouton Bas
nouveauBoutonBas.addEventListener('mousedown', function() {
    boutonEnfonce(nouvelleCaseNombre, -1);
});

nouveauBoutonBas.addEventListener('touchstart', function(event) {
    event.preventDefault(); // Empêcher le comportement par défaut pour éviter les retards ou les double événements
    boutonEnfonce(nouvelleCaseNombre, -1);
});

nouveauBoutonBas.addEventListener('mouseup', function() {
    boutonRelache(nouvelleCaseNombre, 1);
});

nouveauBoutonBas.addEventListener('touchend', function(event) {
    event.preventDefault(); // Empêcher le comportement par défaut pour éviter les retards ou les double événements
    boutonRelache(nouvelleCaseNombre, 1);
});

nouveauBoutonBas.addEventListener('mouseleave', function() {
    boutonRelache(nouvelleCaseNombre, 1);
});

    nouvelleColonne.appendChild(nouvelleCaseTitre);
    nouvelleColonne.appendChild(nouveauBoutonHaut);
    nouvelleColonne.appendChild(nouvelleCaseNombre);
    nouvelleColonne.appendChild(nouveauBoutonBas);
    if (cote==='droite'){
        nouvelleColonne.classList.add('decimale');
        if (nombreDeDecimales===0){
            nouvelleCaseNombre.classList.add('premieredecimale');            
        }
        
        if (nombreDeDecimales<3){
            nouvelleCaseTitre.innerHTML=titresDecimales[nombreDeDecimales];
        } else {
            nouvelleCaseTitre.innerHTML='&times10<span class="exposant">-' + (nombreDeDecimales+1) + '</span>';
        }
        nouvelleCaseNombre.id= -1-nombreDeDecimales;
        divConteneur.insertBefore(nouvelleColonne, boutonsDroite);
        nombreDeDecimales+=1;
    } else if (cote==='gauche'){

        nouvelleCaseTitre.innerHTML=titresEntiers[nombreDEntiers%3];
        nouvelleCaseNombre.id=nombreDEntiers;

        let groupe;

        if (nombreDEntiers>=0 && nombreDEntiers<3){
            nouvelleCaseNombre.classList.add('unites');
            nouvelleCaseTitre.classList.add('unites');
            groupe='unites';
        }
        if (nombreDEntiers>2 && nombreDEntiers<6){
            nouvelleCaseNombre.classList.add('milliers');
            nouvelleCaseTitre.classList.add('milliers');
            groupe='milliers';
        }
        if (nombreDEntiers>5 && nombreDEntiers<9){
            nouvelleCaseNombre.classList.add('millions');
            nouvelleCaseTitre.classList.add('millions');
            groupe='millions';
        }
        if (nombreDEntiers>8 && nombreDEntiers<12){
            nouvelleCaseNombre.classList.add('milliards');
            nouvelleCaseTitre.classList.add('milliards');
            groupe='milliards';
        }

        if (nombreDEntiers%3===0){
            if (nombreDEntiers<11){
                nouveauGroupe.innerHTML=groupesEntiers[nombreDEntiers/3];
            } else {
                nouveauGroupe.innerHTML='&times10<span class="exposant">' + nombreDEntiers + '</span>';
            }
            nouveauGroupe.style.width='100%';
            nouveauGroupe.style.boxShadow = 'inset 0 0 ' + epaisseurOmbre/4 + 'px #ffffff';
            nouvelleCaseTitre.appendChild(nouveauGroupe);            
        } else {
            let nombreDentiersduGroupe = nombreDEntiers%3 + 1;
            let caseGroupe = document.querySelector('.groupe');
            caseGroupe.style.width = 'calc(' + 100 * nombreDentiersduGroupe + '% + ' + (5 * (nombreDentiersduGroupe-1)) + 'px)'; 
        }

        divConteneur.insertBefore(nouvelleColonne, boutonsGauche.nextSibling);
        nombreDEntiers+=1;        
    }
    n+=1;
    majTaille();
    majNombre();

    if (autoCases){
        autoCocheCases();
    }

    return nouvelleCaseNombre;


}

function supprimeColonne(position){
    const colonnes = Array.from(divConteneur.querySelectorAll('.colonne'));
    const decimales = Array.from(divConteneur.querySelectorAll('.decimale'));
    const nbColonnes = colonnes.length;
    const nbDecimales = decimales.length;
    const nbEntiers = nbColonnes - nbDecimales;
    let sens;
    if (position===-1){position = colonnes.length - 1;sens='decimal'}
    else {sens='entier'}
    if ((sens==='entier' && nbEntiers>1)||(sens==='decimal' && nbDecimales>0)){
        colonnes[position].remove();
        if (sens==='decimal'){nombreDeDecimales-=1;}
        else {nombreDEntiers-=1;}
        n-=1;
        majTaille();

        if (sens==='entier' && nombreDEntiers%3!=0){            
            let nombreDentiersduGroupe = nombreDEntiers%3;
            let caseGroupe = document.querySelector('.groupe');
            caseGroupe.style.width = 'calc(' + 100 * nombreDentiersduGroupe + '% + ' + (5 * (nombreDentiersduGroupe-1)) + 'px)'; 
        }
   
    }

    majNombre();

    if (autoCases){
        autoCocheCases();
    }

}

function plusmoinspuissdix() {
    let nombre = parseInt(inputNombre.value);
    if (!isNaN(nombre)) {
        let puissance = listeDeroulante.value;
        let caseNombre = document.getElementById(puissance);
        if (!caseNombre) {
            let nouvelleCase;
            while (!nouvelleCase || nouvelleCase.id != puissance) {
                if (puissance >= 0) {
                    nouvelleCase = creerColonne('gauche', 0);
                } else {
                    nouvelleCase = creerColonne('droite', 0);
                }
            }
            caseNombre = nouvelleCase; // Met à jour caseNombre avec la nouvelle colonne créée
        }

        caseNombre.style.color='yellow';
        for (let i = 0; i < nombre; i++) {
            setTimeout(() => {
                plusmoins(caseNombre, sensPlusMoins);
            }, i * delaiAnimation);
        }

        // Après la boucle, réinitialiser la couleur avec un délai supplémentaire
        setTimeout(() => {
            caseNombre.style.color = null;
        }, nombre * delaiAnimation + 300); // Ajoutez ici le délai supplémentaire (en millisecondes) avant que la couleur revienne à la normale
    }
}


function plusmoins(caseNombre,valeur) {
    const nombreActuel = parseInt(caseNombre.innerHTML);
    const cases = Array.from(divConteneur.querySelectorAll('.case-nombre'));
    const position = cases.indexOf(caseNombre);
    let nouveauNombre = nombreActuel + valeur;
    let nombreDePuissancesSup = 0;
    let tour=0;
    cases.forEach(caseNombre => {        
        if (tour<position) {
            let valeur = parseInt(caseNombre.innerHTML);        
            nombreDePuissancesSup+=valeur;
        }
        tour+=1;
    });

    if (nouveauNombre === 10){ // Plus
        if (position > 0){
            nouveauNombre = 0;
            plusmoins(cases[position - 1],1);
        } else {
            let nouvelleColonne = creerColonne('gauche');
            majTaille();
            nouveauNombre = 0;
            nouvelleColonne.innerHTML = '1';
        }

    } else if (nouveauNombre === -1){ // Moins        
        if (position > 0 && nombreDePuissancesSup>0) {
            nouveauNombre = 9;
            plusmoins(cases[position - 1],-1);
        } else {
            nouveauNombre = 0;
        }
    }

    caseNombre.innerHTML = nouveauNombre;

    majNombre();
}


window.onload = function() {
    checkHeight;
};

window.onresize = function() {
    checkHeight;
};

function checkHeight() {
    let height = window.innerHeight;

    if (height < 450) {
        limiterHauteur = true;
    } else {
        limiterHauteur = false;
    }

}


function clic(event){

    let cible = event.target;

    if (panneauVisible && !objetOuEnfantDe(cible,panneau) && cible!=boutonPanneau){
        visibilitePanneau();
    }

}

function objetOuEnfantDe(cible, objet) {
    // Vérifie si la cible est égale à l'objet
    if (cible === objet) {
        return true;
    }
    // Vérifie si la cible est un descendant de l'objet
    let parent = cible.parentNode;
    while (parent) {
        if (parent === objet) {
            return true;
        }
        parent = parent.parentNode;
    }
    // Si aucun cas n'est vérifié, retourne false
    return false;
}

// Touch events
document.addEventListener("touchstart", clic);
/*
document.addEventListener("touchend", release);


*/

// emp$echer le scroll tactile
document.addEventListener('touchmove', function(event) {
    event.preventDefault();
}, { passive: false });


// Mouse events
document.addEventListener("mousedown", clic);
/*
document.addEventListener("mousemove", function(event) {
    move(event); // Call move function with event parameter
});
document.addEventListener("mouseup", release);

*/


function ouvre(div){
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}

function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}
