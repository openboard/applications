# compteur

## Description

Cette application affiche un compteur et permet d'ajouter/soustraire une ou plusieurs unités, dizaines, centaines.

## Démonstration

Sur la version en ligne: [https://educajou.forge.apps.education.fr/compteur/](https://educajou.forge.apps.education.fr/compteur/)

## Fonctionnement

Pour faire fonctionner le compteur, utilisez les boutons au-dessus et en-dessous des chiffres. Ce compteur fonctionne uniquement avec des nombres positifs, donc vous ne pouvez pas, par exemple, enlever une dizaine si le nombre affiché est inférieur à 10. Mais si par exemple le nombre est 100, il est possible d'enlever une dizaine, ce qui cassera la centaine.

Pour ajouter une colonne d'entiers, utilisez le bouton dédié vert à gauche.

Pour ajouter une colonne de décimales, utilisez le bouton dédié vert à droite.

Pour ajouter / soustraire plusieurs unités, plusieurs dizaines ... utilisez le panneau du haut : choisissez + ou -, ainsi que le nombre choisi, et cliquez sur OK. (Vous pouvez régler les options de la liste déroulante afin de ne faire apparaître que celles qui sont pertinentes pour vos élèves.)

## Licence

Compteur est développée par Arnaud Champollion. Elle est sous licence libre GNU/GPL.

Cette application fait partie de la suite Éducajou et elle est partagée sur la Forge des Communs Numériques de Apps Éducation.

Voir les sources sur la page Compteur de la Forge.

## Crédits

Remerciements à Olivier Jeulin pour la création des icônes "ajouter/supprimer une colonne", sous licence Crative Commons BY SA 4.0.
