var	langue_defaut="fr", syslang=langue_defaut,
	papier = Raphael("ligne","100%","100%"),// Occupe tout le div // Dimensions maximales du Canvas
	trait=[],nombre=[], visible=[], //Graduations & nombres
	nombre_graduations,
	mode_defaut='edition',mode=mode_defaut,mode_sombre='false',//Mode d'affichage
	// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	couleur_defaut="#"+blanc,couleur_graduation="#"+rouge,couleur_sous_graduation="#"+noir, couleur_ligne="#"+noir,couleur_fond='transparent',couleur_nombre="#"+bleu,couleur_sous_nombre="#"+vert,couleur_numero="#"+noir,couleur_parties="#"+noir,
	pas=10,pas_min=1,pas_max=100,// Pas de graduation, de n en n
	origine=0, // Valeur de départ de la droite
	hauteur_graduation=30,hauteur_graduation_min=10,hauteur_graduation_max=50,// Hauteur des traits de graduations
	hauteur_sous_graduation=10,hauteur_sous_graduation_min=10,hauteur_sous_graduation_max=50,
	epaisseur_ligne=5,epaisseur_ligne_min=1,epaisseur_ligne_max=20,
	epaisseur_graduation=5,epaisseur_graduation_min=1,epaisseur_graduation_max=10,
	epaisseur_sous_graduation=5,epaisseur_sous_graduation_min=1,epaisseur_sous_graduation_max=10,
	taille_nombre=30,taille_max_nombre=60,taille_min_nombre=20,// Taille de police pour les nombres
	taille_sous_nombre=20,taille_sous_nombre_min=5,taille_sous_nombre_max=60,// Taille de police pour les nombres intermédiairess
	marge_gauche=50, marge_haut=15, // Marge à gauche et au dessus
	label=[],// Etiquettes des nombres pour la graduation
	nb_unite=3, // Nombre de graduation initialisation
	min_unite=1,max_unite=20,// Nb min et max de graduations
	separateur_decimal=",",
	longueur_ligne=600,longueur_min=100, longueur_max=1800, // Longueur de la ligne, min et max
	longueur_pas=(longueur_ligne/nb_unite), longueur_pas_min=100,longueur_pas_max=500,// Longueur d'une unité
	longueur_sous_unite=20, // Longueur d'une sous_unité
	numero,parties=2,parties_min=1,parties_max=10,decalage=0,
	style_nombre={"font-size": taille_nombre, "fill": couleur_nombre},style_sous_nombre={"font-size": taille_sous_nombre, "fill": couleur_sous_nombre},style_numero={"font-size": taille_sous_nombre, "fill": couleur_numero},style_parties={"font-size": taille_sous_nombre, "fill": couleur_parties},// Style des nombres sur la ligne
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne},style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation},style_sous_graduation={"stroke-width":epaisseur_sous_graduation , "stroke":couleur_sous_graduation},// Style des traits
	x0=0,y0=hauteur_graduation+taille_nombre;// Coordonnées de départ de la ligne dans le Widget
// #####################################
// ##### INITIALISATIONS
// #####################################
function init_reglages(){
	//Curseurs
	// Longueur d'une unité
	$( "#longueur_pas" ).attr({
	       "max" : longueur_pas_max,
	       "min" : longueur_pas_min,
		"value":longueur_pas,
		"step":50
    });
	$("#longueur_pas").next('label').text(longueur_pas);
	// Longueur de la ligne
	$( "#longueur_ligne" ).attr({
	       "max" : longueur_max,
	       "min" : longueur_min,
		"value":longueur_ligne,
		"step":50
    });
	$("#longueur_ligne").next('label').text(longueur_ligne);
	// Dénominateur, partage de l'unité
	$( "#parties" ).attr({
	       "max" : parties_max,
	       "min" : parties_min,
		"value":parties,
		"step":1
    });
	$("#parties").next('label').text(parties);
	//Unité de départ
	$( "#origine" ).attr({
		"value":origine,
		"step":1
    });
	//Pas de comptage
	$( "#pas" ).attr({
	       "max" : pas_max,
	       "min" : pas_min,
		"value":pas,
		"step":1
    });
	// Decalage : nb de nombres intermédiairess avant la 1ère unité
	// parties-1 graduations possibles avant
	$( "#decalage" ).attr({
	       "max" : parties-1,
	       "min" : 0,
		"value":decalage,
		"step":1
    });
	// Taille des chiffres des unités
	$( "#taille_nombre" ).attr({
	       "max" : taille_max_nombre,
	       "min" : taille_min_nombre,
		"value":taille_nombre,
		"step":1
    });
	$("#taille_nombre").next('label').text(taille_nombre);
	// Taille des chiffres des nombres intermédiairess
	$( "#taille_sous_nombre" ).attr({
	       "max" : taille_sous_nombre_max,
	       "min" : taille_sous_nombre_min,
		"value":taille_sous_nombre,
		"step":1
    });
	$("#taille_sous_nombre").next('label').text(taille_sous_nombre);
	//Épaisseur des traits
	$( "#epaisseur_ligne" ).attr({
	       "max" : epaisseur_ligne_max,
	       "min" : epaisseur_ligne_min,
		"value":epaisseur_ligne,
		"step":1
	});
	$("#epaisseur_ligne").next('label').text(epaisseur_ligne);
	
	$( "#epaisseur_graduation" ).attr({
	       "max" : epaisseur_graduation_max,
	       "min" : epaisseur_graduation_min,
		"value":epaisseur_graduation,
		"step":1
	});
	$("#epaisseur_graduation").next('label').text(epaisseur_graduation);
	$( "#epaisseur_sous_graduation" ).attr({
	       "max" : epaisseur_sous_graduation_max,
	       "min" :epaisseur_sous_graduation_min,
		"value":epaisseur_sous_graduation,
		"step":1
	});
	$("#epaisseur_sous_graduation").next('label').text(epaisseur_sous_graduation);
	// Hauteur des graduations
	$( "#hauteur_graduation" ).attr({
	       "max" : hauteur_graduation_max,
	       "min" : hauteur_graduation_min,
		"value":hauteur_graduation,
		"step":1
	});
	$("#hauteur_graduation").next('label').text(hauteur_graduation);
	$( "#hauteur_sous_graduation" ).attr({
	       "max" : hauteur_sous_graduation_max,
	       "min" : hauteur_sous_graduation_min,
		"value":hauteur_sous_graduation,
		"step":1
	});
	$("#hauteur_sous_graduation").next('label').text(hauteur_sous_graduation);
	}
function init_palette() {
//initialisation de la palette de couleurs
$('#couleur_graduation_unite').colorPicker({pickerDefault: couleur_graduation, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_graduation=newValue; style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation};trace_ligne();}}); //Couleur des graduations
$('#couleur_sous_graduation').colorPicker({pickerDefault: couleur_graduation, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_sous_graduation=newValue; style_sous_graduation={"stroke-width":epaisseur_sous_graduation , "stroke":couleur_sous_graduation};trace_ligne();}}); //Couleur des sous-graduations
$('#couleur_ligne').colorPicker({pickerDefault: couleur_ligne, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_ligne=newValue;style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne};trace_ligne();}}); //Couleur de la ligne
$('#couleur_fond').colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false,onColorChange : function(id, newValue) { couleur_fond=newValue;$("body").css("background-color",couleur_fond)}}); //Couleur de la ligne
$('#couleur_unite').colorPicker({pickerDefault: couleur_nombre, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_nombre=newValue;style_nombre={"font-size": taille_nombre, "fill":couleur_nombre};trace_ligne();}}); //Couleur des nombres
$('#couleur_numero').colorPicker({pickerDefault: couleur_numero, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_numero=newValue;style_numero={"font-size": taille_sous_nombre, "fill":couleur_numero};trace_ligne();}}); //Couleur du numéro
maj_palette(); // Actualisation des couleurs et des styles
}
function maj_palette(){
// Sélection de la couleur active pour l'aperçu
	$("#couleur_ligne").val(couleur_ligne).change();// Couleur active
	$("#couleur_fond").val(couleur_fond).change();// Couleur active
	$("#couleur_graduation_unite").val(couleur_graduation).change();// Couleur active
	$("#couleur_sous_graduation").val(couleur_sous_graduation).change();// Couleur active
	$("#couleur_unite").val(couleur_nombre).change();// Couleur active
	$("#couleur_numero").val(couleur_numero).change();// Couleur active
	$("#couleur_parties").val(couleur_parties).change();// Couleur active
// Mise à jour des styles
	style_nombre={"font-size": taille_nombre, "fill": couleur_nombre};
	style_sous_nombre={"font-size": taille_sous_nombre, "fill": couleur_sous_nombre};
	style_numero={"font-size": taille_sous_nombre, "fill": couleur_numero};
	style_parties={"font-size": taille_sous_nombre, "fill": couleur_parties};
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne};
	style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation};
	style_sous_graduation={"stroke-width":epaisseur_sous_graduation , "stroke":couleur_sous_graduation};// Style des traits		
	$("body").css("background-color",couleur_fond);
	}
function init_lang(){
	 // Traduction de l'interface
	$('#reglages').attr('title',sankoreLang[syslang].Reglages);
	$('#titre1').text(sankoreLang[syslang].Ligne);
	$('#titre1a').text(sankoreLang[syslang].Longueur);
	$('#titre1b').text(sankoreLang[syslang].Epaisseur);
	$('#titre1c').text(sankoreLang[syslang].Origine);
	$('#titre1d').text(sankoreLang[syslang].Pas);
	$('#titre1e').text(sankoreLang[syslang].Decalage);
	//
	$('#titre2').text(sankoreLang[syslang].Graduations);
	$('#titre2a').text(sankoreLang[syslang].Longueur);
	$('#titre2b').text(sankoreLang[syslang].Epaisseur);
	$('#titre2c').text(sankoreLang[syslang].Taille);
	$('#titre2d').text(sankoreLang[syslang].Hauteur);
	//
	$('#titre3').text(sankoreLang[syslang].Divisions);
	$('#titre3a').text(sankoreLang[syslang].Parties);
	$('#titre3b').text(sankoreLang[syslang].Epaisseur);
	$('#titre3c').text(sankoreLang[syslang].Taille);
	$('#titre3d').text(sankoreLang[syslang].Hauteur);
	$('#titre4').text(sankoreLang[syslang].Epaisseur);
	$('#titre4a').text(sankoreLang[syslang].Ligne);
	$('#titre4b').text(sankoreLang[syslang].Graduations);
	$('#titre4c').text(sankoreLang[syslang].Sous_Graduations);
	$('#titre5').text(sankoreLang[syslang].Hauteur);
	$('#titre5a').text(sankoreLang[syslang].Graduations);
	$('#titre5b').text(sankoreLang[syslang].Sous_Graduations);
	 //
	$('#infos').attr('title',sankoreLang[syslang].Infos);
	 $('#infos').html(sankoreLang[syslang].Txt_infos);
	separateur_decimal=sankoreLang[syslang].Separateur_decimal; //  la virgule en Français, le point en Anglais... 
	}
async function init() {
	for (var i=0;i<parseInt(longueur_ligne/longueur_sous_unite)+1;i++){visible[i]=true}// Tous les nombres affichés par défaut
	if (window.sankore) {
	// Arrivée sur le widget 
	window.widget.onenter.connect(() => {$('#bouton_reglages').show();});// Affiche le bouton des paramètres
	// Sortie du Widget
	window.widget.onleave.connect(() => { 
		$('#bouton_reglages').hide();// Cache le bouton
		//Sauvegarde des paramètres au format 'chaine de caractères'
		window.sankore.setPreference('Nombre unites', nb_unite);
		window.sankore.setPreference('Valeur unite', pas);
		window.sankore.setPreference('Taille nombre', taille_nombre);
		window.sankore.setPreference('Taille sous nombre', taille_sous_nombre);
		window.sankore.setPreference('Origine', origine);
		window.sankore.setPreference('Pas',pas);
		window.sankore.setPreference('Longueur ligne', longueur_ligne);
		window.sankore.setPreference('Longueur unite', longueur_pas);
		window.sankore.setPreference('parties', parties);
		window.sankore.setPreference('Couleur graduation',couleur_graduation);
		window.sankore.setPreference('Couleur sous graduation',couleur_sous_graduation);
		window.sankore.setPreference('Couleur ligne',couleur_ligne);
		window.sankore.setPreference('Couleur fond',couleur_fond);
		window.sankore.setPreference('Couleur nombre',couleur_nombre);
		window.sankore.setPreference('Couleur numero',couleur_numero);
		window.sankore.setPreference('Couleur parties',couleur_parties);
		window.sankore.setPreference('mode', mode);
		window.sankore.setPreference('Decalage', decalage);
		window.sankore.setPreference('Epaisseur ligne',epaisseur_ligne);
		window.sankore.setPreference('Epaisseur graduation',epaisseur_graduation);
		window.sankore.setPreference('Epaisseur sous graduation',epaisseur_sous_graduation);
		window.sankore.setPreference('Hauteur graduation',hauteur_graduation);
		window.sankore.setPreference('Hauteur sous graduation',hauteur_sous_graduation);
		window.sankore.setPreference('Mode sombre', $('body').hasClass('mode_sombre'));
		window.sankore.setPreference('Visible', JSON.stringify(visible));
		window.sankore.setPreference('Affiche unites', $('#affiche_unites').prop('checked'));
		window.sankore.setPreference('Affiche divisions', $('#affiche_divisions').prop('checked'));
	});
	// Détection de la Langue
	try{
		syslang = window.sankore.lang.substr(0,2);
		sankoreLang[syslang].search;
	} catch(e){
		syslang = langue_defaut;
               }
	// Quand on revient sur la page / copie le widget, on récupère les paramètres stockés
	if (await window.sankore.async.preference('Nombre unites')) {// Récupération des paramètres sauvegardés s'ils existent (retour sur la page)
		nb_unite=parseInt(await window.sankore.async.preference('Nombre unites'));
		pas=parseInt(await window.sankore.async.preference('Valeur unite'));
		taille_nombre=parseInt(await window.sankore.async.preference('Taille nombre'));
		taille_sous_nombre=parseInt(await window.sankore.async.preference('Taille sous nombre'));
		origine=parseInt(await window.sankore.async.preference('Origine'));
		pas=parseInt(await window.sankore.async.preference('Pas'));
		longueur_ligne=parseFloat(await window.sankore.async.preference('Longueur ligne'));
		longueur_pas=parseFloat(await window.sankore.async.preference('Longueur unite'));
		$('#ligne').width(longueur_ligne+20); // adaptation de la largeur du conteneur
		parties=parseInt(await window.sankore.async.preference('parties'));
		couleur_graduation=await window.sankore.async.preference('Couleur graduation');
		couleur_sous_graduation=await window.sankore.async.preference('Couleur sous graduation');
		couleur_ligne=await window.sankore.async.preference('Couleur ligne');
		couleur_fond=await window.sankore.async.preference('Couleur fond');
		couleur_nombre=await window.sankore.async.preference('Couleur nombre');
		couleur_numero=await window.sankore.async.preference('Couleur numero');
		couleur_parties=await window.sankore.async.preference('Couleur parties');
		mode=await window.sankore.async.preference('mode');
		decalage=parseInt(await window.sankore.async.preference('Decalage'));
		epaisseur_ligne=parseInt(await window.sankore.async.preference('Epaisseur ligne'));
		epaisseur_graduation=parseInt(await window.sankore.async.preference('Epaisseur graduation'));
		epaisseur_sous_graduation=parseInt(await window.sankore.async.preference('Epaisseur sous graduation'));
		hauteur_graduation=parseInt(await window.sankore.async.preference('Hauteur graduation'));
		hauteur_sous_graduation=parseInt(await window.sankore.async.preference('Hauteur sous graduation'));
		window.sankore.async.preference("'Mode sombre'","true").then(value => {$('body').addClass('mode_sombre')});
		visible=JSON.parse(await window.sankore.async.preference('Visible'));// Découpage de la chaîne en valeurs du tableau
		$('#affiche_unites').prop('checked',(await window.sankore.async.preference('Affiche unites')==='true')); // Case cochée ou non
		$('#affiche_divisions').prop('checked',(await window.sankore.async.preference('Affiche divisions')==='true')); // Case cochée ou non
	}
	}
	init_lang();// Traduction de l'interface
	init_reglages();// Mise à jour des paramètres
	init_palette(); // Mise à jour des palettes
	// Boite de dialogue des paramètres
	$( "#reglages" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'let top', 
                                    at: 'center bottom', 
                                    of: "#widget"}, 
		beforeClose: function() {mode='vue'},
		close: function() {
			$('#ligne').width(longueur_ligne+20);
			mode_affichage(mode);}
	});
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'left top', 
                                    at: 'center center', 
                                    of: '#widget'}, 
	});
	$('#ligne').width(longueur_ligne+20);// Adaptation du conteneur de la ligne
	$('#ligne').height(taille_nombre+hauteur_graduation+taille_sous_nombre*2+10);// Adaptation du conteneur de la ligne
	mode_affichage(mode);// Mode d'affichage
	//Tracé de la ligne
	trace_ligne();
}
// #####################################
// ##### TRAÇAGE DE LA LIGNE GRADUÉE
// #####################################
Raphael.fn.ligne = function (cx, cy,longueur,nb_tirets, style_ligne) {//Fonction créant le svg à placer sur le canvas
	var paper = this,chart = this.set();
	var cx_depart=cx,cy_depart=cy; // Sauvegarde des coordonnées d'origine
	longueur_sous_unite=parseFloat(longueur_pas/parties);
	nombre_graduations=parseInt(longueur/longueur_sous_unite)+1;//Nombre de graduations sur la ligne
	//Tracé d'un tiret de graduation
	function tiret(cx, cy, taille, params) {
		return paper.path(["M", cx, cy-taille, "L", cx, cy, "z"]).attr(params);
	};
	// Tracé des graduations et écriture des nombres
	numero=decimal(origine-decalage*pas/parties,3); // Premier nombre de la ligne
	for (var i = 0; i <nombre_graduations; i++) {
		numero=decimal(origine+(i-decalage)*pas/parties,3).toString().replace(".",separateur_decimal);// Nombre correspondant à la graduation, au millième
		 if ((i-decalage)%parties==0){ //Selon la position sur la graduation
			trait[i]=tiret(cx, cy, hauteur_graduation,style_graduation).data("id", i);//Tiret de la graduation, , l'id permet de le repérer sur la ligne
			nombre[i]=papier.text(cx,cy-hauteur_graduation-taille_nombre/2,numero).attr(style_nombre).data("id", i);// Ecriture du nombre
			nombre[i].data("type", 'unite');
		}else{
			trait[i]=tiret(cx, cy, hauteur_sous_graduation,style_sous_graduation).data("id", i);//On ajoute le tiret aux autres dans le Canvas, l'id permet de le repérer sur la ligne
			nombre[i]=papier.text(cx,cy-taille_sous_nombre-hauteur_sous_graduation,numero).attr(style_numero).data("id", i);// Ecriture du nombre intermédiaire
			nombre[i].data("type", 'division');
		}
		trait[i].click(function(){// Un clic sur la graduation affiche le nombre correspondant
			nombre[this.data("id")].show();
			visible[this.data("id")]=true;
		});	
		nombre[i].click(function(){// Un clic sur le nombre le masque
			this.hide();
			visible[this.data("id")]=false;
		});
		cx+=longueur_sous_unite; // passage à la graduation suivante
		if (!visible[i]){nombre[i].hide()}// //Masque le nombre s'il doit être caché
	}
	// Tracé de la ligne en dernier afin qu'elle soit au 1er plan
	chart.push(paper.path(["M", cx_depart-epaisseur_graduation/2, cy_depart, "L", cx_depart+longueur, cy_depart, "z"]).attr(style_ligne));
	
	return chart;//On envoie le dessin
};

function trace_ligne(){
	$('#ligne').height(taille_nombre+hauteur_graduation+taille_sous_nombre*2.5);// Adaptation du conteneur de la ligne
	y0=hauteur_graduation+taille_nombre;
	//Calcul du nombre d'unités sur la ligne
	nb_unite=parseInt(longueur_ligne/longueur_pas);
	// Trace une ligne partagée en nb_unite
	papier.clear();// On efface le Canvas
	papier.ligne(x0+marge_gauche, y0, longueur_ligne, nb_unite, style_ligne);//On trace le ligne
}
// ###############################################
// ##### ADAPTATION DU WIDGET AUX DIMENSIONS DE LA FENÊTRE
// ###############################################
function redimensionne(largeur,longueur) {
	// Redimensionne le widget sankore
	var margex=50,margey=50; // épaisseur de la fenêtre sankore
	if (window.sankore)
		window.sankore.resize(largeur+margex,longueur+margey);
}
// #####################################
// ##### MODE AFFICHAGE  DU WIDGET
// #####################################
function mode_affichage(style_affichage){
	mode=style_affichage;
	// ##################
	// ##### MODE VUE #####
	// ##################
	if (mode=="vue") {
		$("#bouton_reglages").hide();
		// On cache les réglages
		$( "#reglages" ).dialog( "close" );//Fermeture des paramètres
		var widgetX=$("#ligne").width(),widgetY=$("#ligne").height();
		redimensionne(widgetX,widgetY);
	};
	// #####################
	// ##### MODE EDITION #####
	// ####################
	if (mode=="edition") {
		// On affiche les réglages
		var widgetX=Math.max($("#ligne").width(),widget.width),widgetY=$("#ligne").height()+300;
		redimensionne(widgetX,widgetY);
		$( "#reglages" ).css( "display","flex" );//Affichage des paramètres
		$( "#reglages" ).dialog( "option", "position", { my: "left top", at: "left bottom", of:"#ligne" } );
		$( "#reglages" ).dialog( "open" );//Affichage des paramètres
	};
}
// #####################################
// ##### CORRECTION DU BUG js POUR LES DÉCIMAUX
// #####################################
function decimal(x,y){
// Transforme le nb X en nb décimal avec une précision de Y chiffres après la virgule
var reponse = Math.round(x*Math.pow(10,y))/Math.pow(10,y);
return reponse;
}
// #####################################
// ##### GESTION DES ÉVÉNEMENTS
// #####################################
// Un clic sur le bouton fait apparaître la boite de dialogue des paramètres
$("#bouton_reglages").click(function(){
	mode='edition';
	mode_affichage(mode);
});
// Nombre sur les graduations principales
$( "#affiche_unites" ).click( function(){
	var etat=$(this).prop('checked'), i=decalage;
	while (i<nombre_graduations){
		if (etat){nombre[i].show()}else{nombre[i].hide()}
		visible[i]=etat;
		i+=parties;// Passage à l'unité suivante
	}
});
$( "#affiche_divisions" ).click( function(){
	var cpt,etat=$(this).prop('checked');
	for (cpt=0;cpt<nombre_graduations;cpt++){
		if (nombre[cpt].data('type')=='division'){
			if (etat){nombre[cpt].show()}else{nombre[cpt].hide()}
			visible[cpt]=etat;
		};
	}
});
// Affichage des infos
$('#bouton_infos').click(function(){
	$('#infos').dialog('open');
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul_mode;
	$('body').toggleClass('mode_sombre');
	if ($('body').hasClass('mode_sombre')) {coul_mode='#'+blanc;couleur_fond='#'+noir} else {coul_mode='#'+noir;couleur_fond='#'+blanc};
	couleur_nombre=coul_mode;
	couleur_sous_nombre=coul_mode;
	couleur_numero=coul_mode;
	couleur_parties=coul_mode;
	couleur_ligne=coul_mode;
	couleur_graduation=coul_mode;
	couleur_sous_graduation=coul_mode; 
	maj_palette(); // Actualisation des palettes et styles
	trace_ligne();
	});
// Réactions aux déplacements des curseurs
	// Longueur d'une unité
$('#longueur_pas').mousemove(function(){
	longueur_pas=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	// Fraction de l'unité
$('#parties').mousemove(function(){
	parties=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	$( "#decalage" ).attr({"max" : parties-1});//adaptation du décalge à la valeur du dénominateur
	//Adaptation du décalage en fonction du dénominateur pour ne pas dépasser/arriver à l'unité précédente
	if (decalage>=parties) {decalage=parties-1;$( "#decalage" ).attr({"value" : decalage});$('#decalage').next('label').text(decalage)};//Si le
	trace_ligne();
	});
	//Unité de démarrage de la ligne
$('#origine').change(function(){
	origine=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	//Pas de comptage : de n en n
$('#pas').change(function(){
	pas=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	// Longueur de la ligne
$('#longueur_ligne').mousemove(function(){
	longueur_ligne=parseFloat(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	$('#ligne').width(longueur_ligne+20); // adaptation de la largeur du conteneur
	trace_ligne();
	var 	widgetX=Math.max($('#ligne').width(),$('#reglages').width()),widgetY=window.widget.height+20;// adaptation de la largeur du Widget
	redimensionne(widgetX,widgetY);	
	});
	//Nombre de nombres intermédiairess avant la première unité
$('#decalage').change(function(){
	decalage=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	//Taille de la police pour les unités
$('#taille_nombre').mousemove(function(){
	taille_nombre=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_nombre={"font-size": taille_nombre, "fill": couleur_nombre};// adapte le style
	trace_ligne();
	});
	//Taille de la police pour les nombres intermédiaires
$('#taille_sous_nombre').mousemove(function(){
	taille_sous_nombre=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_numero={"font-size": taille_sous_nombre, "fill": couleur_numero};// adapte le style
	style_parties={"font-size": taille_sous_nombre, "fill": couleur_parties};// adapte le style
	trace_ligne();
	});
	//Épaisseur de la ligne
$('#epaisseur_ligne').mousemove(function(){
	epaisseur_ligne=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne};
	trace_ligne();
	});
	//Épaisseur de la Graduation
$('#epaisseur_graduation').mousemove(function(){
	epaisseur_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_graduation={"stroke-width":epaisseur_graduation, "stroke":couleur_graduation};
	trace_ligne();
	});
	//Épaisseur de la Sous Graduation
$('#epaisseur_sous_graduation').mousemove(function(){
	epaisseur_sous_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_sous_graduation={"stroke-width":epaisseur_sous_graduation, "stroke":couleur_sous_graduation};
	trace_ligne();
	});
	// Hauteur de la Graduation
$('#hauteur_graduation').mousemove(function(){
	hauteur_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	//Épaisseur de la Sous Graduation
$('#hauteur_sous_graduation').mousemove(function(){
	hauteur_sous_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
