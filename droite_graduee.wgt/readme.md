# Droite des fractions
Application pour Opens-Sankoré / Openboard 
Graduer une ligne avec des nombres entiers.

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Mars 2024

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 1.0
* Traçage d'une droite graduée [a,b] avec un pas
* Paramétrage des dimensions, couleurs

### 1.1
* Possibilité de graduer avec des nombres négatifs

### 1.2
* Possibilité de graduer avec des décimaux (correction du bug JS pour les décimaux)

### 2.0
* Sauvegarde/restauration des paramètres pour conserver le widget en état lors de changemet de page ou de copie.

### 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Ajout de paramètres
    * Taille des polices utilisées
    * épaisseur des lignes/graduations
    * hauteur des graduations
    * couleurs des nombres, fractions, graduations
    * mode sombre pour un affichage sur tableau noir.
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures

### 3.1
* Gestion de la couleur de fond pour un affichage sur une page de couleur
* Afficher/masquer tout ou partie des nombres

### 3.2
* Portage de l'application vers **Openboard 1.7**
* Gestion de la transparence

## Sources

### Icones :
* Solar Linear Icons [https://www.svgrepo.com/](https://www.svgrepo.com/)

### Bibliothèques :
* Raphael JS [https://dmitrybaranovskiy.github.io/raphael/](https://dmitrybaranovskiy.github.io/raphael/)
* Laktek colorPicker [https://github.com/laktek/really-simple-color-picker](https://github.com/laktek/really-simple-color-picker)
* Jquery 3.7.1 [https://jquery.com/](https://jquery.com/)

