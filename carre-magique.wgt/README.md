# Carré magique
Générateur de carrés magiques avec des nombres relatifs (somme ou produit).

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Septembre 2018

## Licence
CC BY-NC-SA v3

## Versions
Version 1
version initiale

Version 1.1
- Introduit la fonctionnalité 'entier naturel' pour le primaire.
- Cache toujours les même cases après l'affichage de la solution.
- Dans OpenBoard, le carré magique est enregistré dans le document et réaffiché avec les mêmes valeurs.

## Sources
* Arnaud DURAND [https://mathix.org/linux/archives/11258](https://mathix.org/linux/archives/11258) et [https://mathix.org/carre-magique/](https://mathix.org/carre-magique/)


