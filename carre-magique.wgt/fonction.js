relatif=0;
cases_cachees=true;
difficulte=1;
somme=true;
cases_cachees_id = [];
a = 0;
b = 0;
x = 0;
bParamLus = 0;

/***** Sauvegarde des parametres *****/
function sauve()
{
	// Sauvegarde du Widget
	if (window.widget) {
        // Quand on quitte le widget
		window.widget.onleave.connect(() => {
		window.sankore.setPreference('relatif',relatif);
		window.sankore.setPreference('cases_cachees',cases_cachees);
		window.sankore.setPreference('difficulte',difficulte);
		window.sankore.setPreference('somme',somme);
		window.sankore.setPreference('cases_cachees_id0',cases_cachees_id[0]);
		window.sankore.setPreference('cases_cachees_id1',cases_cachees_id[1]);
		window.sankore.setPreference('cases_cachees_id2',cases_cachees_id[2]);
		window.sankore.setPreference('cases_cachees_id3',cases_cachees_id[3]);
		window.sankore.setPreference('cases_cachees_id4',cases_cachees_id[4]);
		window.sankore.setPreference('a',a);
		window.sankore.setPreference('b',b);
		window.sankore.setPreference('x',x);
		});
	}
}
/***** on recupere les parametres stockes *****/
async function lire()
{
	if (window.sankore){

		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('relatif')!='') {
			relatif=parseInt(await window.sankore.async.preference('relatif'));
			cases_cachees=await window.sankore.async.preference('cases_cachees');
			difficulte=parseInt(await window.sankore.async.preference('difficulte'));
			somme=await window.sankore.async.preference('somme');
			cases_cachees_id[0]=await window.sankore.async.preference('cases_cachees_id0');
			cases_cachees_id[1]=await window.sankore.async.preference('cases_cachees_id1');
			cases_cachees_id[2]=await window.sankore.async.preference('cases_cachees_id2');
			cases_cachees_id[3]=await window.sankore.async.preference('cases_cachees_id3');
			cases_cachees_id[4]=await window.sankore.async.preference('cases_cachees_id4');
			a=parseInt(await window.sankore.async.preference('a'));
			b=parseInt(await window.sankore.async.preference('b'));
			x=parseInt(await window.sankore.async.preference('x'));
			bParamLus = 1;
		}
	}
}

function switchrelatif()
{
    if(relatif == 0)
    {
        relatif = 1;
        document.getElementById('relatifs').value="Entiers relatifs";
    }
    else
    {
        relatif = 0;
        document.getElementById('relatifs').value="Entiers naturels";
    }
    genere();
}

function generesomme()
{
	affichetout();
	if(bParamLus == 0)
	{
		if(relatif == 0)
		{
		a2 = Math.floor((Math.random() * 4*difficulte + 1) );
		b2 = Math.floor((Math.random() * 4*difficulte + 1) );
		x2 = Math.floor((Math.random() * 4*difficulte + 1) );
		[b, a, x] = [a2,b2,x2].sort();
		if(2*b > x)
			x = 2*b;
		}
		else
		{
		a = Math.floor((Math.random() * 10*difficulte) - 5*difficulte);
		b = Math.floor((Math.random() * 10*difficulte) - 5*difficulte);
		x = Math.floor((Math.random() * 10*difficulte) - 5*difficulte);
		}
		cachedescases();
	}
	else
	{
		bParamLus = 0;
		cachelescases();
	}

	// The square looks like:
	//-----------------------------------------
	//|   x+a     |   x-b-b       |   x-b+a+a |
	//|---------------------------------------|
	//|   x-b-b+a+a | x-b+a       |   x       |
	//|---------------------------------------|
	//|   x-b     |   x+a+a       |   x-b-b+a |
	//-----------------------------------------
	document.getElementById('c32').innerHTML=x;
	document.getElementById('c11').innerHTML=x+a;
	document.getElementById('c23').innerHTML=x+a+a;
	document.getElementById('c13').innerHTML=x-b;
	document.getElementById('c21').innerHTML=x-b-b;
	document.getElementById('c33').innerHTML=x-b-b+a;
	document.getElementById('c31').innerHTML=x-b+a+a;
	document.getElementById('c12').innerHTML=x-b-b+a+a;
	document.getElementById('c22').innerHTML=x-b+a;

	document.getElementById('cacheaffiche').value="Correction";
}

function genereproduit()
{
	affichetout();
	if(bParamLus == 0)
	{
		if(relatif == 0)
		{
			a = Math.floor((Math.random() * 2 + difficulte) );
			b = Math.floor((Math.random() * 2 + difficulte) );
			x = a*a*b*b;
			// Workaround if the square is filled by the value 1
			if(x == 1)
			{
				a = 1; b = 2; x = 2; x = a*a*b*b;
			}
		}
		else
		{
			a = Math.floor((Math.random() * 4*difficulte) - 2*difficulte);
			while (a==0)
			{
				a = Math.floor((Math.random() * 4*difficulte) - 2*difficulte);
			}
			b = Math.floor((Math.random() * 4*difficulte) - 2*difficulte);
			while (b==0)
			{
				b = Math.floor((Math.random() * 4*difficulte) - 2*difficulte);
			}
			x = Math.floor((Math.random() * 4) - 2)*a*a*b*b;
			while (x==0)
			{
				x = Math.floor((Math.random() * 4) - 2)*a*a*b*b;
			}
		}
		cachedescases();
	}
	else
	{
		bParamLus = 0;
		cachelescases();
	}

	document.getElementById('c32').innerHTML=x;
	document.getElementById('c11').innerHTML=x*a;
	document.getElementById('c23').innerHTML=x*a*a;
	document.getElementById('c13').innerHTML=x/b;
	document.getElementById('c21').innerHTML=x/b/b;
	document.getElementById('c33').innerHTML=x/b/b*a;
	document.getElementById('c31').innerHTML=x/b*a*a;
	document.getElementById('c12').innerHTML=x/b/b*a*a;
	document.getElementById('c22').innerHTML=x/b*a;

	document.getElementById('cacheaffiche').value="Correction";
cases_cachees=true;
}

function cachelescases()
{
	for (i=0;i<5;i++)
		document.getElementById(cases_cachees_id[i]).style.color='white';
	cases_cachees=true;
}

function cachedescases()
{
	var de=Math.floor(Math.random()*8)+1;
	var case_restante_a_garder=Math.floor(Math.random()*6)+1;

	switch (de) {
  case 1:
	tab=['c21','c22','c23','c31','c32','c33'];
    break;
    case 2:
	tab=['c11','c12','c13','c31','c32','c33'];
    break;
      case 3:
	tab=['c11','c12','c13','c21','c22','c23'];
    break;
      case 4:
	tab=['c12','c13','c22','c23','c32','c33'];
    break;
      case 5:
	tab=['c11','c13','c21','c23','c31','c33'];
    break;
      case 6:
	tab=['c11','c12','c21','c22','c31','c32'];
    break;
      case 7:
	tab=['c12','c13','c21','c23','c31','c32'];
    break;
      case 8:
	tab=['c11','c12','c21','c23','c32','c33'];
    break;
	}
	tab.splice(case_restante_a_garder,1);
	cases_cachees_id = tab;
	cachelescases();
}

function affichetout()
{
		tab=['c11','c12','c13','c21','c22','c23','c31','c32','c33'];
	for (i=0;i<9;i++)
	{
		document.getElementById(tab[i]).style.color='#7D7D7D';
	}
	cases_cachees=false;
}

function cache_affiche()
{
	if (cases_cachees)
	{
		document.getElementById('cacheaffiche').value="Cache des cases";
		affichetout();
	}
	else
	{
		document.getElementById('cacheaffiche').value="Correction";
		cachelescases();
	}
}
function changediff()
{
	difficulte=difficulte+1;
	if (((difficulte==4)&&(somme))||((difficulte==3)&&(!somme)))
	{
		difficulte=1;
	}
	document.getElementById('difficulte').src='diff'+difficulte+'.png';
	genere();
}
function genere()
{
		if (somme)
	{
		generesomme();
	}
	else
	{
		genereproduit();
	}
}
function changeoperation()
{
	somme=!somme;
	if (somme)
	{
		document.getElementById('operation').src='operationplus.png';
		document.getElementById('consigne').innerHTML='Complétez le carré magique (par la somme)';
	}
	else
	{
		document.getElementById('operation').src='operationfois.png';
		document.getElementById('consigne').innerHTML='Complétez le carré magique (par le produit)';
		if (difficulte==3)
		{
			difficulte=2;
			document.getElementById('difficulte').src='diff'+difficulte+'.png';
		}
	}
	genere();
}

async function init()
{
	await lire();
	genere();
	sauve();
}
window.load=init();
