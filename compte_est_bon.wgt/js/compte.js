/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


function $(i){return document.getElementById(i)}
var _p=[1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,25,50,75,100], nbtotal_tuiles=_p.length;
var _c,_t=new Array(6),_m,_e={"which":13,"target":null};
var Tgn={
	c:null,// valeur cible initiale
	b:null,// valeur cible bis (initiale ou modifiée)
	n:null,// nombre de plaques valides
	m:null,// message compte, approximation ou erreur
	r:null,// réponse littérale
	s:null,// spectre des valeurs
	t:new Array(42),// tableau des plaques selon les niveaux
	z:null,// temps de calcul

	/* Fonction calculant la solution */
	sol:function(c,p,mode){
		var i,j,k,o=[],q=[],l=p.length,x;
		this.z=-(new Date().getTime());
		if (c=='') c=0;c=parseInt(c);
		if (typeof(c)!='number' || c<1) {
			this.m='Valeur cible erronée';
			this.r='<br>toutes les valeurs entières positives sont ici admises !';
			this.z=0;
			return
		}
		this.c=parseInt(c);
		this.b=this.c;
		this.s={};
	  
		j=0;
		for (i=0;i<l;i++) {
			if (isNaN(k=parseInt(p[i]))) continue;
			q[j++]=k;
		}
		if (j<3) {
			this.m='Nombre de plaques insuffisant';
			this.r='<br>trois plaques vierges au plus sont acceptées !';
			this.z=0;
			return
		}
		this.m='Le compte est bon';
		// Classement décroissant 
		this.n=j;
		for (i=0;i<j;i++) {
			k=i;
			while(0<k && o[k-1]<q[i]) o[k]=o[--k];
			o[k]=q[i];
		}
		// Enregitrement plaques ordonnées (le max dégrade o) et test si une plaque convient
		for(i=0;i<j;i++) {
			Tgn.t[6*j+i]=o[i];
			if (this.c==o[i]) {
				this.r='<br>'+o[i];
				this.z=0;return
			}
		}
		// Calcul du max modifié 2010.09.19
		x=1;
		while (o[k=j-1]==1) 
			if (5<j && o[j-2]==1 && o[j-3]==1 && o[j-4]==2 && o[j-5]==2 && o[j-6]==2) {
				x=27;
				j=j-6;
			}
			else if (3<j && o[j-2]==1 && o[j-3]==2 && o[j-4]==2) {
					x=9;
					j=j-4;
				}
				else {
					v=o[--k]+1;
					while(0<k && o[k-1]<v) 
						o[k]=o[k--];
					o[k]=v;j=j-1;
				}
		k=j-1;
		while(-1<k) x*=o[k--];
			
		// Cible modifiée si supérieure à max
		if (x<this.c) {this.b=x;this.m=''+x;}
		while (!this.dcp(this.n)) {
			i=this.b+1;
			while(typeof(this.s[i])=='undefined') i++;
			j=this.b-1;
			while(0<j && typeof(this.s[j])=='undefined') j--;
			if ((this.b-j)<=(i-this.b)) this.b=j;else this.b=i;
			this.m=''+this.b;this.s={};
		}
		this.z+=new Date().getTime();
		},
  // Modifié 2010.09.19 plus de tri (mais un test initial) pour ne reprendre qu'une fois les valeurs

/***** Décompostion du calcul *****/		
	// Modifié le 2010.09.21 seules les opérations utiles sont reprises
	dcp:function(a){
		var i,j,k,v=(a<<3)-a,u=v-a,x,y,w;
		for (i=u;i<v;i++)	for (j=u;j<v;j++){
			if (i==j || (x=this.t[i])<(y=this.t[j])) continue;

			// addition
			if ((w=x+y)==this.b) {this.r='<br> '+x+' + '+y+' = '+w;return 1;}
			
			// seule définition des plaques à reprendre
			k=u-5;for (var l=u;l<v;l++) {if (l==i || l==j) continue;
			this.t[k++]=this.t[l];}
			k=u-6;
			this.s[w]=1;this.t[k]=w;
			if (this.dcp(a-1)==1) {
				if (-1<this.r.search(new RegExp(' '+w+' '))) this.r='<br> '+this.t[i]+' + '+this.t[j]+' = '+w+this.r;
				return 1;
			}
			// soustraction
			if (x!=y && x!=y<<1) {
				if ((w=x-y)==this.b) {this.r='<br> '+x+' - '+y+' = '+w;return 1;}
				this.s[w]=1;this.t[k]=w;
				if (this.dcp(a-1)==1){
					if (-1<this.r.search(new RegExp(' '+w+' ')))	this.r='<br> '+this.t[i]+' - '+this.t[j]+' = '+w+this.r;
					return 1;
				}
			}
			// multiplication
			if (1<x && 1<y) {
				if ((w=x*y)==this.b) {this.r='<br> '+x+' x '+y+' = '+w;return 1;}
				this.s[w]=1;this.t[k]=w;
				if (this.dcp(a-1)==1){
					if (-1<this.r.search(new RegExp(' '+w+' ')))	this.r='<br> '+this.t[i]+' x '+this.t[j]+' = '+w+this.r;
					return 1;
					}
				}
			// division
			if (1<y && x%y==0 && x!=y*y) {
				if ((w=x/y)==this.b) {this.r='<br> '+x+' / '+y+' = '+w;return 1;}
				this.s[w]=1;this.t[k]=w;
				if (this.dcp(a-1)==1){
					if (-1<this.r.search(new RegExp(' '+w+' '))) this.r='<br> '+this.t[i]+' / '+this.t[j]+' = '+w+this.r;
					return 1;
				}
			}
		}
		return 0
	}
	/***** Fin decomposition *******/
};
/***** Initialisation *****/
async function ini(){
	var i,j,p={},t;
	do {_c=100+Math.floor(900*Math.random())} while (_c==1000);
	/* Tirage aléatoire des 6 plaques */
  for (i=0;i<6;i++) {
    do {j=Math.floor(nbtotal_tuiles*Math.random())} while (j==nbtotal_tuiles || typeof(p[j])=='number');
		_t[i]=_p[j];p[j]=1;}
	$('idc').value=_c;/* Valeur Cible à trouver */
	$('isl').innerHTML='';/* Effacement du Contenu de la Solution*/
	/* Création des Tuiles */
	t="";i=0;while(i<6) t+='<input onclick="sol()" id="idp'+i+'" type="text" value="'+_t[i++]+' ">';
	$('ipl').innerHTML=t;
	await lire();
	sauve();
	setTimeout('sol()',7);/*Recherche de la Solution */
}
/***** Fin Initialisation *****/
/***** ntr() *****/
function ntr(e){var o=!e?window.event.keyCode:e.which,t=!e?window.event.srcElement:e.target;
/* Vérification des modifications apportées aux nombres des ardoises */
/* Seuls les chiffres sont autorisés, les autres caractères sont effacés */
	if (t.id.substr(0,2)=='id' && t.value!='' && (t.value==0 || isNaN(t.value))) {t.value='';$('isl').innerHTML='';return}
	if (t.id.substr(0,2)=='id' && t.value<0) {t.value=-t.value;$('isl').innerHTML='';return}
	if (o!=13 || t.id.substr(0,2)!='id') return true;
	setTimeout('sol()',7);
}
/***** Sauvegarde des paramètres au format 'chaine de caractères' *****/
function sauve()
{
	// Sauvegarde du Widget
	if (window.widget) {
        // Quand on quitte le widget
		window.widget.onleave.connect(() => {
		window.sankore.setPreference('idc',$('idc').value);
		window.sankore.setPreference('idp0',$('idp0').value);
		window.sankore.setPreference('idp1',$('idp1').value);
		window.sankore.setPreference('idp2',$('idp2').value);
		window.sankore.setPreference('idp3',$('idp3').value);
		window.sankore.setPreference('idp4',$('idp4').value);
		window.sankore.setPreference('idp5',$('idp5').value);
		});
	}
}
/***** on récupère les paramètres stockés *****/
async function lire()
{
	if (window.sankore){

		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('idc')!='') {
			$("idc").value=await window.sankore.async.preference('idc');
			$("idp0").value=await window.sankore.async.preference('idp0');
			$("idp1").value=await window.sankore.async.preference('idp1');
			$("idp2").value=await window.sankore.async.preference('idp2');
			$("idp3").value=await window.sankore.async.preference('idp3');
			$("idp4").value=await window.sankore.async.preference('idp4');
			$("idp5").value=await window.sankore.async.preference('idp5');
			$("idc").value=await window.sankore.async.preference('idc');
		}
	}
}

/***** Calcul et affichage de la solution *****/
function sol(){
/* Récupération des nombres au cas où il y ait eu une modification */
_c=$('idc').value;
	for (i=0;i<6;i++) _t[i]=$('idp'+i).value;
	$('isl').innerHTML='';
	Tgn.sol(_c,_t);_m=Tgn.m;
	$('isl').innerHTML=_m;/* Message Compte est bon Approché...*/
	$('isl').innerHTML=Tgn.m+' '+Tgn.r;
}
function edt(){
	$('isl').innerHTML=' ';
}
document.onkeyup=ntr;

/* Cache un élément */
function masque(element)
{
// Fonction masquant un élément ayant l'identifiant id='element' de la page HTML
document.getElementById(element).style.display="none";
}

/* Affiche un élément */
function demasque(element)
{
// Fonction démasquant un élément ayant l'identifiant id='element' de la page HTML
document.getElementById(element).style.display="inline";
}
function dialogue(elem){
	
	demasque(elem);
	var x=Math.max(document.getElementById(elem).offsetWidth,window.innerWidth)+delta,y=Math.max(document.getElementById(elem).offsetHeight,window.innerHeight)+delta;
	window.resizeTo(x,y);
	}
