# Le compte est bon
Application pour Opens-Sankoré / Openboard 
Application reprenant le jeu du compte est bon. Tirage aléatoire des nombres et du total. Possibilité d’entrer ses propres nombres. Affichage d’une solution à la demande.

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Novembre 2017

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditez les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions.

## Versions

### 1.0
- Application issue de ce [lien](https://www.openedu.fr/appliquettes/compte_est_bon.wgt.zip) sur cette [page](https://www.openedu.fr/open-sankore/applications-pour-sankore/).

### 1.1
- Enregistrement des paramètres de l'application dans le document Openboard en supportant Openboard >= 1.7 . Ainsi le nombre cible et les autres nombres sont retrouvés à la réouverture du document.

## Sources
* François Le Cléac'h [https://openedu.fr](https://openedu.fr)
