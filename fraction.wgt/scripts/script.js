var	langue_defaut="fr";
/* Marge pour la fenetre de l'application */
var delta=10,margeX=60,margeY=50,widgetX,widgetY,
	numerateur='1',denominateur='2', // Format chaîne de caractère pour pouvoir utiliser des fractions en  lettres a/b par exemple
// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	couleur_defaut="#"+blanc,couleur_numerateur="#"+gris,couleur_barre_fraction="#"+noir, couleur_denominateur="#"+gris,couleur_fond=transparent;

// INITIALISATION DE L'APPLICATION
async function init(){
	//Détection de la langue
		try{
			syslang = window.sankore.lang.substr(0,2);
			sankoreLang[syslang].search;
                } catch(e){
			syslang = langue_defaut;
                }
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		title:sankoreLang[syslang].Infos,
		width:"auto",
		position: {my: 'left top', 
                                    at: 'center center', 
                                    of: '#ligne'}, 
		close: function( event, ui ) {agrandir_widget()} //Redimensionne le widget à la fermeture
	});
	// Sauvegarde du Widget
	if (window.sankore) {
        // Quand on quitte le widget
	window.widget.onleave.connect(() => {
		sankore.setPreference('parametres','ok');
		//Sauvegarde des paramètres au format 'chaine de caractères'
		sankore.setPreference('numerateur',numerateur);// Sauvegarde du numérateur.
		sankore.setPreference('denominateur',denominateur);// Sauvegarde du dénominateur.
		sankore.setPreference('couleur_numerateur',couleur_numerateur);// Sauvegarde de la couleur du numérateur.
		sankore.setPreference('couleur_denominateur',couleur_denominateur);// Sauvegarde de la couleur du dénominateur.
		sankore.setPreference('couleur_barre_fraction',couleur_barre_fraction);// Sauvegarde de la couleur de la barre de fraction.
		sankore.setPreference('couleur_fond',couleur_fond);// Sauvegarde de la couleur de l'arrière plan.
		});

	// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await sankore.async.preference('parametres')=='ok') {
			numerateur=await sankore.async.preference('numerateur');// Restauration du numérateur.
			denominateur=await sankore.async.preference('denominateur');// Restauration du dénominateur.
			couleur_numerateur=await sankore.async.preference('couleur_numerateur');// Restauration de la couleur des numérateurs.
			couleur_denominateur=await sankore.async.preference('couleur_denominateur');// Restauration de la couleur des dénominateurs.
			couleur_barre_fraction=await sankore.async.preference('couleur_barre_fraction');			
			couleur_fond=await sankore.async.preference('couleur_fond');
		}	
	}
	// Mise à jour des éléments
	$('#numerateur').html(numerateur);
	$('#denominateur').html(denominateur);
	$('#numerateur').css('color',couleur_numerateur);
	$('#denominateur').css('color',couleur_denominateur);
	$('#barre_fraction').css('color',couleur_barre_fraction);
	$('body').css('background-color',couleur_fond);
	init_palette();
	init_lang();//Traduction de l'interface
	modeVue();
}
function init_palette() {
//initialisation de la palette de couleurs
$('#couleur_numerateur') // Couleur du numérateur
	.colorPicker({pickerDefault: couleur_numerateur, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_numerateur=newValue;$('#numerateur').css('color',couleur_numerateur);}}) // Action déclenchée à la sélection de couleur de la palette
	.val(couleur_numerateur).change(); // Initialisation de la couleur active
$('#couleur_barre_fraction') // Couleur de la barre de fraction
	.colorPicker({pickerDefault: couleur_barre_fraction, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_barre_fraction=newValue;$('#barre_fraction').css('color',couleur_barre_fraction);}})  // Action déclenchée à la sélection de couleur de la palette
	.val(couleur_barre_fraction).change(); // Initialisation de la couleur active
$('#couleur_denominateur') // Couleur du dénominateur
	.colorPicker({pickerDefault: couleur_denominateur, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_denominateur=newValue;$('#denominateur').css('color',couleur_denominateur);}})  // Action déclenchée à la sélection de couleur de la palette
	.val(couleur_denominateur).change(); // Initialisation de la couleur active
$('#couleur_fond') // Couleur du fond
	.colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_fond=newValue;$('body').css('background-color',couleur_fond);}}) // Action déclenchée à la sélection de couleur de la palette
	.val(couleur_fond).change(); // Initialisation de la couleur active
}
function init_lang(){
// Traduction de l'interface
	$('#infos').html(sankoreLang[syslang].Txt_infos);
	}
// Gestion des Événement 
$(".fraction")
	.focus(function(){//Passage en mode édition si le contenu de la fraction devient actif
		modeEdition();
		})
	.keydown(function(){// Adaptation de la largeur du widget en fonction de la taille du nombre
		agrandir_widget();
	})
	.blur(function(){ // Récupération des éléments de la fraction lorsqu'on perd le focus
		numerateur=$('#numerateur').html();
		denominateur=$('#denominateur').html();
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul_mode;
	$('body').toggleClass('mode_sombre');
	if ($('body').hasClass('mode_sombre')) {coul_mode='#'+blanc;couleur_fond='#'+noir;} else {coul_mode='#'+noir;couleur_fond='#'+blanc;};
	couleur_numerateur=coul_mode;
	couleur_denominateur=coul_mode; 
	couleur_barre_fraction=coul_mode; 
	$("#couleur_numerateur").val(couleur_numerateur).change();// Adaptation du colorpicker
	$("#couleur_denominateur").val(couleur_denominateur).change();// Adaptation du colorpicker
	$(".fraction").css('color',coul_mode);// Adaptation du colorpicker
	$("#couleur_barre_fraction").val(couleur_barre_fraction).change();// Adaptation du colorpicker
	$("#barre_fraction").css('color',couleur_barre_fraction);// Adaptation du colorpicker
	$("#couleur_fond").val(couleur_fond).change();// Adaptation du colorpicker
	$("body").css('background-color',couleur_fond);// Adaptation du colorpicker
});
// Affichage des infos
$('#bouton_infos').click(function(){
	if (window.sankore)
		window.sankore.resize(400,300);
	$('#infos').dialog('open');
	});
// Fermeture du menu
$('.quitter').click(function(){modeVue();});

// Affichages
function modeVue() // Mode Aperçu
{
	$('#menu').hide();
	reduire_widget();
}
function modeEdition() // Mode Édition
{
	$('#menu').show();
	agrandir_widget();
}
/* Réduction de la taille de la fenêtre */
function reduire_widget(){
	widgetX=$("#fraction").width()+margeX;
	widgetY=$("#fraction").height()+delta;
	if (window.sankore)
		window.sankore.resize(widgetX,widgetY);//Reduction de la fenêtre pour l'aperçu
	}
/* Agrandissement de la taille de la fenêtre */
function agrandir_widget(){
	widgetX=$("#fraction").width()+$("#menu").width()+margeX;
	widgetY=$("#fraction").height()+margeY;
	if (window.sankore)
		window.sankore.resize(widgetX,widgetY); // Agrandissement de la fenêtre pour l'édition
}
/* Initialisation de l'application */



