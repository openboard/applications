# Droite des fractions
Application pour Opens-Sankoré / Openboard 
Graduer une ligne en fractions.

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Mars 2024

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 0.1
* affiche une fraction a/b
* Possibilité de modifier le numérateur et dénominateur

### 0.2
*Centrer numérateur et dénominateur

### Version 0.3
* Ajustement automatique de la fenêtre

### 0.5
* Sauvegarde automatique des paramètres

### 0.6
* Palette de couleurs pour le numérateur et dénominateur

### 0.7
*Bouton annuler

### 0.8
* Boite de dialogue "A propos"

### 1.0
* mode sombre
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures

### 1.1
* Portage vers Openboard 1.7
* Gestion de la transparence

## Sources

### Icones :
* Solar Linear Icons [https://www.svgrepo.com/](https://www.svgrepo.com/)

### Bibliothèques :
* Laktek colorPicker [https://github.com/laktek/really-simple-color-picker](https://github.com/laktek/really-simple-color-picker)
* Jquery 3.7.1 [https://jquery.com/](https://jquery.com/)

