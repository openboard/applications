# Conversion

Permet de placer une grandeur dans un tableau de conversion. Reconnaît automatiquement la quantité saisie et le place dans le tableau, il suffit ensuite de déplacer la virgule!

## Comment utiliser l'outil conversion?

De façon simple et intuitive :
- Il faut écrire la quantité à convertir dans la zone de texte en haut, par exemple : « 1,2m », « 42hg », « 42cl », « 0,2m² ».
- Ensuite, le tableau apparaît et il suffit avec la souris (en maintenant le clic gauche enfoncé) de déplacer la virgule!
Et c’est tout!!.

## Auteur

Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)

Février 2019

## Licence
CC BY-NC-SA Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditez les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions.

## Versions

Version 9 - version initiale

Version 9.1 - Ajout d'un bouton d'aide pour afficher des instructions d'utilisation et des infos sur l'application.

## Sources

* Arnaud DURAND [https://www.mathix.org/conversion/](https://www.mathix.org/conversion/) et [https://mathix.org/linux/archives/11334](https://mathix.org/linux/archives/11334)


