virgule_visible=true;
decalage=100;
positionnombre_x = 0;
xsouris_debut=0;
xtablette_debut=0;
fige=true;
nombre_tab=[];
num_placeunite=0;
position_unite_sur_tableau_x=13; // place du  chiffre le plus à droite
position_unite_sur_tableau_y_ligne1=327;
ecart_entre_case=108;
unite_choisi="";
unite_choisi_metre=false;
unite_choisi_metre2=false;
unite_choisi_metre3=false;
unite_choisi_gramme=false;
unite_choisi_litre=false;
unite_choisi_are=false;

nbencoursdetraitement="";
tab_unites_metre=['km','hm','dam','m','dm','cm','mm'];
tab_unites_metre_entier=['kilomètre','hectomètre','décamètre','mètre','décimètre','centimètre','millimètre'];
tab_unites_metrecarre=[' ','km²',' ','hm²',' ','dam²',' ','m²',' ','dm²',' ','cm²',' ','mm²'];
tab_unites_metrecarre_entier=['kilomètre','carré','hectomètre','carré','décamètre','carré','mètre','carré','décimètre','carré','centimètre','carré','millimètre','carré'];
tab_unites_metrecube=[' ',' ','km³',' ',' ','hm³',' ',' ','dam³',' ',' ','m³',' ',' ','dm³',' ',' ','cm³',' ',' ','mm³'];
tab_unites_metrecube_entier=[' ','kilomètre','cube',' ','hectomètre','cube',' ','décamètre','cube',' ','mètre','cube',' ','décimètre','cube',' ','centimètre','cube',' ','millimètre','cube'];
tab_unites_metre=['km','hm','dam','m','dm','cm','mm'];
tab_unites_metre_entier=['kilomètre','hectomètre','décamètre','mètre','décimètre','centimètre','millimètre'];

tab_unites_gramme=['t','q',' ','kg','hg','dag','g','dg','cg','mg'];
tab_unites_gramme_entier=['tonne','quintal',' ','kilogramme','hectogramme','décagramme','gramme','décigramme','centigramme','milligramme'];
tab_unites_litre=['kl','hl','dal','l','dl','cl','ml'];
tab_unites_litre2=['kL','hL','daL','L','dL','cL','mL'];
tab_unites_litre_entier=['kilolitre','hectolitre','décalitre','litre','décilitre','centilitre','millilitre'];

tab_unites_are=[' ','ha',' ','a',' ','ca',' '];
tab_unites_are_entier=[' ','hectare',' ','are',' ','centiare',' '];

canvas = document.getElementById('mon_canvas');

const divAide = document.getElementById('aide');

if(!canvas)
{
	alert("Impossible de récupérer le canvas");
}
context = canvas.getContext('2d');
if(!context)
{
	alert("Impossible de récupérer le context du canvas");
}
document.getElementById("nb").value="";
function reajustevariables()
{
	
	switch(unite_choisi)
	{
		case 'm':
			position_unite_sur_tableau_x=6; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=327;
			ecart_entre_case=108;
			decalage=100;
		break;
		case 'm²':
			position_unite_sur_tableau_x=6; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=313;
			ecart_entre_case=73;
			decalage=30;
		break;
		case 'm³':
			position_unite_sur_tableau_x=6; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=313;
			ecart_entre_case=50;
			decalage=0;
		break;
		case 'g':
			position_unite_sur_tableau_x=6; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=313;
			ecart_entre_case=93;
			decalage=80;
		break;
		case 'l':
			position_unite_sur_tableau_x=13; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=327;
			ecart_entre_case=108;
			decalage=100;
		break;
		case 'L':
			position_unite_sur_tableau_x=13; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=327;
			ecart_entre_case=108;
			decalage=100;
		break;
		case 'a':
			position_unite_sur_tableau_x=13; // place du  chiffre le plus à droite
			position_unite_sur_tableau_y_ligne1=327;
			ecart_entre_case=108;
			decalage=100;
		break;
	}
	
	
	
	 clearCanvas(context, canvas) ;
			affichetableau();

	
	 
}
function initialisation()
 {
	 document.getElementById('nb').value='';
	 document.getElementById('progressBar').style.display='none';
	 
	 //affichetableau();
	 
 }
function clearCanvas(context, canvas) {
	context.clearRect(0, 0, canvas.width, canvas.height);
	var w = canvas.width;
	canvas.width = 1;
	canvas.width = w;

}
 
function placernombre(nb)
{
	
	if (document.getElementById("virgule").checked)
	{
		virgule_visible=true;
	}
	else
	{
		virgule_visible=false;
	}
	clearCanvas(context, canvas);
	nb=nb.replace('^2', '²');
	nb=nb.replace('^3', '³');

	nombre_tab=[];
	num_placeunite=-1;
	nb=nb.replace(/ /g, "");//on vire les espaces
	
	filtrechiffrevirgule=/([0-9]*)(,)?(\.)?([0-9]*)/g;
	
	 unite=nb.replace(filtrechiffrevirgule, "");
	
	unite=unite.trim();// c'est l'unité de mesure
	
	//booleen pour savoir quel type d'unité on a 
	
	unite_choisi_metre=(tab_unites_metre.indexOf(unite)!=-1)
	if (unite_choisi_metre)
	{
		unite_choisi="m";
	}
	unite_choisi_metre2=(tab_unites_metrecarre.indexOf(unite)!=-1)
	if (unite_choisi_metre2)
	{
		unite_choisi="m²";
	}
	unite_choisi_metre3=(tab_unites_metrecube.indexOf(unite)!=-1)
	if (unite_choisi_metre3)
	{
		unite_choisi="m³";
		
	}
	unite_choisi_gramme=(tab_unites_gramme.indexOf(unite)!=-1)
	if (unite_choisi_gramme)
	{
		unite_choisi="g";
	}
	unite_choisi_litre=(tab_unites_litre.indexOf(unite)!=-1)
	if (unite_choisi_litre)
	{
		unite_choisi="l";
		
	}	
	unite_choisi_litre=(tab_unites_litre2.indexOf(unite)!=-1)
	if (unite_choisi_litre)
	{
		unite_choisi="L";
		
	}
	
	
	unite_choisi_are=(tab_unites_are.indexOf(unite)!=-1)
	if (unite_choisi_are)
	{
		unite_choisi="a";
	}
	
	reajustevariables();
	document.getElementById("conversionml").style.display="none";
	nb=nb.replace(unite, "");

	switch(unite_choisi)
	{
		case 'm':
			num_colonne_unite=tab_unites_metre.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
		break;
		case 'm²':
			num_colonne_unite=tab_unites_metrecarre.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
			
			if (num_colonne_unite==3)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en ha";
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' ha';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==5)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en a";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' a';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==7)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en ca";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' ca';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
		break;
		case 'm³':
			num_colonne_unite=tab_unites_metrecube.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
			if (num_colonne_unite==11)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en kL";
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' kl';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==14)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en L";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' l';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==17)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en mL";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' ml';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
		break;
		case 'g':
			num_colonne_unite=tab_unites_gramme.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
		break;
		case 'l':
			num_colonne_unite=tab_unites_litre.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
			if (num_colonne_unite%3==0)
			{
				document.getElementById("conversionml").style.display="";
				if (num_colonne_unite==0)
				{
					document.getElementById("conversionml").value="Convertir en m³";
					document.getElementById("conversionml").onclick=function() {document.getElementById("nb").value=nb+' m³';placernombre(nb+' m³');positionnombre_x=0;}//11
				}
				if (num_colonne_unite==3)
				{
					document.getElementById("conversionml").value="Convertir en dm³";
					document.getElementById("conversionml").onclick=function() {document.getElementById("nb").value=nb+' dm³';placernombre(nb+' dm³');positionnombre_x=0;}//14
				}
				if (num_colonne_unite==6)
				{
					document.getElementById("conversionml").value="Convertir en mm³";
					document.getElementById("conversionml").onclick= function() {document.getElementById("nb").value=nb+' cm³';placernombre(nb+' cm³');positionnombre_x=0;}//17
				}
				
			}
		break;
		case 'L':
			num_colonne_unite=tab_unites_litre2.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
			unite_choisi='l';
			if (num_colonne_unite%3==0)
			{
				document.getElementById("conversionml").style.display="";
				if (num_colonne_unite==0)
				{
					document.getElementById("conversionml").value="Convertir en m³";
					document.getElementById("conversionml").onclick=function() {document.getElementById("nb").value=nb+' m³';placernombre(nb+' m³');positionnombre_x=0;}//11
				}
				if (num_colonne_unite==3)
				{
					document.getElementById("conversionml").value="Convertir en dm³";
					document.getElementById("conversionml").onclick=function() {document.getElementById("nb").value=nb+' dm³';placernombre(nb+' dm³');positionnombre_x=0;}//14
				}
				if (num_colonne_unite==6)
				{
					document.getElementById("conversionml").value="Convertir en mm³";
					document.getElementById("conversionml").onclick= function() {document.getElementById("nb").value=nb+' cm³';placernombre(nb+' cm³');positionnombre_x=0;}//17
				}
				
			}
		break;
		case 'a':
			num_colonne_unite=tab_unites_are.indexOf(unite);// donne le numéro de la colonne de l'unité de mesure.
			if (num_colonne_unite==1)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en hm²";
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=A+' hm²';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==3)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en dam²";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' dam²';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
			if (num_colonne_unite==5)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en m²";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nb+' m²';placernombre(nbencoursdetraitement);positionnombre_x=0;}
			}
		break;
	}
	
	
	
	var nombre_tab_tmp=nb.split('');
	var long=nombre_tab_tmp.length;
	var j =0;
	
	
	if (num_colonne_unite!=-1)
	{
	
		for (i=0;i<long;i++)
		{
			if ((nombre_tab_tmp[i]==',')||(nombre_tab_tmp[i]=='.'))
			{
				num_placeunite=i-1;
				//console.log(i-1);
			}
			else
			{
				nombre_tab[j]=nombre_tab_tmp[i];
				j=j+1;
				
			}
			
		}
		
		var long_sans_virgule=nombre_tab.length;
		
		if (num_placeunite==-1)
		{
			num_placeunite=nombre_tab.length-1;
		}
		
		
		for(i=0;i<long;i++)
		{
			context.restore();
			positionx=decalage+num_colonne_unite*ecart_entre_case+(i-num_placeunite)*ecart_entre_case+ecart_entre_case/2;
			positiony=position_unite_sur_tableau_y_ligne1+90;
			
			
			context.font = "80px OpenDyslexicalta";
			context.textAlign = "center"; 
			//console.log(nombre_tab[i]);
			switch (nombre_tab[i]) {
				case '0':
					//context.drawImage(n0,positionx,positiony);
					context.fillText("0",positionx,positiony);
			
				break;
				case '1':
					//context.drawImage(n1,positionx,positiony);
					context.fillText("1",positionx,positiony);
				break;
				case '2':
					//context.drawImage(n2,positionx,positiony);
					context.fillText("2",positionx,positiony);
			
				break;
				case '3':
					//context.drawImage(n3,positionx,positiony);
					context.fillText("3",positionx,positiony);
				break;
				case '4':
					//context.drawImage(n4,positionx,positiony);
					context.fillText("4",positionx,positiony);
				break;
				case '5':
					//context.drawImage(n5,positionx,positiony);
					context.fillText("5",positionx,positiony);
				break;
				case '6':
					//context.drawImage(n6,positionx,positiony);
					context.fillText("6",positionx,positiony);
				break;
				case '7':
					//context.drawImage(n7,positionx,positiony);
					context.fillText("7",positionx,positiony);
				break;
				case '8':
					//context.drawImage(n8,positionx,positiony);
					context.fillText("8",positionx,positiony);
				break;
				case '9':
					//context.drawImage(n9,positionx,positiony);
					context.fillText("9",positionx,positiony);
				break;
			}
			
		}
		
		//calcul position de la virgule
		position_virgule=positionnombre_x+decalage+num_colonne_unite*ecart_entre_case+ecart_entre_case/2;//38 largeur de la flèche
	
		//calcul de quelle colonne unité est choisie
		//num_colonne_nouvelle_unite=Math.floor((positionnombre_x+position_unite_sur_tableau_x+num_colonne_unite*ecart_entre_case+ecart_entre_case/3)/ecart_entre_case);
		num_colonne_nouvelle_unite=Math.floor((positionnombre_x+position_unite_sur_tableau_x+num_colonne_unite*ecart_entre_case+ecart_entre_case/2)/ecart_entre_case);
	
		//affichage du bouton de conversion l m³ en fonction de la nouvelle unité  num_colonne_nouvelle_unite num_colonne_unite
		if (unite_choisi=='m³')
		{
			nbconverti=(Number(nb.replace(',','.')))*Math.pow(10,num_colonne_nouvelle_unite-num_colonne_unite);
			document.getElementById("conversionml").style.display="none";
			if (num_colonne_nouvelle_unite==11)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en kL";
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' kl';positionnombre_x=0;placernombre(nbencoursdetraitement);}
			}
			if (num_colonne_nouvelle_unite==14)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en L";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' l';positionnombre_x=0;placernombre(nbencoursdetraitement);}
			}
			if (num_colonne_nouvelle_unite==17)
			{
				document.getElementById("conversionml").style.display="";
				document.getElementById("conversionml").value="Convertir en mL";				
				document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' ml';positionnombre_x=0;placernombre(nbencoursdetraitement);}
			}
		}
		if (unite_choisi=='l')
		{
			document.getElementById("conversionml").style.display="none";
			nbconverti=(Number(nb.replace(',','.')))*Math.pow(10,num_colonne_nouvelle_unite-num_colonne_unite);
			if (num_colonne_nouvelle_unite%3==0)
			{
				document.getElementById("conversionml").style.display="";
				if (num_colonne_nouvelle_unite==0)
				{
					document.getElementById("conversionml").value="Convertir en m³";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' m³';positionnombre_x=0;placernombre(nbencoursdetraitement);}//11
				}
				if (num_colonne_nouvelle_unite==3)
				{
					document.getElementById("conversionml").value="Convertir en dm³";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' dm³';positionnombre_x=0;placernombre(nbencoursdetraitement);}//14
				}
				if (num_colonne_nouvelle_unite==6)
				{
					document.getElementById("conversionml").value="Convertir en cm³";
					document.getElementById("conversionml").onclick= function() {nbencoursdetraitement=nbconverti+' cm³';positionnombre_x=0;placernombre(nbencoursdetraitement);}//17
				}
				
			}
		}	
		if (unite_choisi=='m²')
		{
			document.getElementById("conversionml").style.display="none";
			nbconverti=(Number(nb.replace(',','.')))*Math.pow(10,num_colonne_nouvelle_unite-num_colonne_unite);
		
				
				if (num_colonne_nouvelle_unite==3)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en ha";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' ha';positionnombre_x=0;placernombre(nbencoursdetraitement);}//11
				}
				if (num_colonne_nouvelle_unite==5)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en a";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' a';positionnombre_x=0;placernombre(nbencoursdetraitement);}//14
				}
				if (num_colonne_nouvelle_unite==7)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en ca";
					document.getElementById("conversionml").onclick= function() {nbencoursdetraitement=nbconverti+' ca';positionnombre_x=0;placernombre(nbencoursdetraitement);}//17
				}
				
		
		}
		if (unite_choisi=='a')
		{
			document.getElementById("conversionml").style.display="none";
			nbconverti=(Number(nb.replace(',','.')))*Math.pow(10,num_colonne_nouvelle_unite-num_colonne_unite);
		
				
				if (num_colonne_nouvelle_unite==1)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en hm²";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' hm²';positionnombre_x=0;placernombre(nbencoursdetraitement);}//11
				}
				if (num_colonne_nouvelle_unite==3)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en dam²";
					document.getElementById("conversionml").onclick=function() {nbencoursdetraitement=nbconverti+' dam²';positionnombre_x=0;placernombre(nbencoursdetraitement);}//14
				}
				if (num_colonne_nouvelle_unite==5)
				{
					document.getElementById("conversionml").style.display="";
					document.getElementById("conversionml").value="Convertir en m²";
					document.getElementById("conversionml").onclick= function() {nbencoursdetraitement=nbconverti+' m²';positionnombre_x=0;placernombre(nbencoursdetraitement);}//17
				}
				
		
		}
		//rajout des zéros inutiles devant
		position_premierchiffre=decalage+num_colonne_unite*ecart_entre_case+(0-num_placeunite)*ecart_entre_case;
		
		
		
		//nombredezeroa_ajouter_avant=Math.trunc((position_premierchiffre-position_virgule+ecart_entre_case/3)/ecart_entre_case);
		nombredezeroa_ajouter_avant=Math.trunc((position_premierchiffre-position_virgule+2*ecart_entre_case/3)/ecart_entre_case);
	
		
		context.save();
		context.fillStyle = "rgb(140,221,131)";
		context.textAlign = "center"; 
		if (0<nombredezeroa_ajouter_avant+1)
		{
			for (i=0;i<nombredezeroa_ajouter_avant;i++)
			{
				
		
				positionx=position_premierchiffre+(-i-1)*ecart_entre_case+ecart_entre_case/2;
				positiony=position_unite_sur_tableau_y_ligne1+90;
				//console.log(positionx);
				context.fillText("0",positionx,positiony);
				//context.drawImage(n0_in,positionx,positiony);
			}
			
			//console.log(positionnombre_x);
		}
		//rajout des zéros inutiles derrière
		//nombredezeroa_ajouter_apres=Math.trunc((positionnombre_x-78/3)/78)-num_placeunite+long-1);
		//nombredezeroa_ajouter_apres=num_placeunite-long+1-Math.trunc((positionnombre_x-ecart_entre_case/4)/ecart_entre_case);
		position_dernierchiffre=decalage+num_colonne_unite*ecart_entre_case+(long_sans_virgule-num_placeunite)*ecart_entre_case;
		
		nombredezeroa_ajouter_apres=Math.trunc((position_virgule-position_dernierchiffre+ecart_entre_case)/ecart_entre_case);
		
		
		//context.moveTo(position_virgule,0);
		//context.lineTo(position_virgule,68);
		//context.lineTo(position_premierchiffre,68);
		//context.lineTo(position_dernierchiffre,68);
		//context.fill();
		//console.log(nombredezeroa_ajouter_avant);
		
		
		if (0<nombredezeroa_ajouter_apres)
		{
			for (i=0;i<nombredezeroa_ajouter_apres;i++)
			{
				
				positionx=position_dernierchiffre+i*ecart_entre_case+ecart_entre_case/2;
				positiony=position_unite_sur_tableau_y_ligne1+90;
				//console.log(positionx);
				//context.drawImage(n0_in,positionx,positiony);
				context.fillText("0",positionx,positiony);
			}
		}
		context.restore();
		
		
		
		
		
		affichetableau();
		//affichage de la virgule pour qu'elle soit au dessus
		//context.drawImage(virgule,position_virgule,positiony);
		metajourmessage(num_colonne_unite,num_colonne_nouvelle_unite,nb,nombre_tab,num_placeunite);
		//context.drawImage(fleche,position_virgule,0);
		//dessin de la flèche
		context.save();
		context.beginPath();
		context.fillStyle = 'rgb(255, 19, 19)';
		context.moveTo(position_virgule-38,4);//76 largeur de la flèche, donc milieu à 38px
		context.lineTo(position_virgule+20-38,4);
		context.lineTo(position_virgule+20-38,0);
		context.lineTo(position_virgule+56-38,0);
		context.lineTo(position_virgule+56-38,4);
		context.lineTo(position_virgule+76-38,4);
		context.lineTo(position_virgule+38-38,14);
		context.lineTo(position_virgule-38,4);
		
		
		context.fill();
		context.restore();
		
		if (virgule_visible)
		{
		//dessin de la virgule
		context.save();
		context.beginPath();
		context.fillStyle = 'rgb(255, 19, 19)';
		positiony=350;
		context.moveTo(position_virgule+63-38,positiony+60);
		context.lineTo(position_virgule+72-38,positiony+60);
		context.lineTo(position_virgule+72-38,positiony+68);
		context.lineTo(position_virgule+65-38,positiony+81);
		context.lineTo(position_virgule+60-38,positiony+81);
		context.lineTo(position_virgule+60-38,positiony+81);
		context.lineTo(position_virgule+63-38,positiony+68);
		context.lineTo(position_virgule+63-38,positiony+68);
		context.fill();
		context.restore();
	}
		affichebordblanc();
	}
	else
	{
		clearCanvas(context, canvas);
		document.getElementById('message').innerHTML='Veuillez indiquer une unité de mesure correcte.';
	}
}

function metajourmessage(colonne_unitebase,colonne_nouvelleunite,nombre,nombre_tab,num_placeunite)
{
	switch(unite_choisi)
	{
		case 'm':
			unite=tab_unites_metre[colonne_nouvelleunite];
		break;
		case 'm²':
			unite=tab_unites_metrecarre[colonne_nouvelleunite];
		break;
		case 'm³':
			unite=tab_unites_metrecube[colonne_nouvelleunite];
		break;
		case 'g':
			unite=tab_unites_gramme[colonne_nouvelleunite];
		break;
		case 'l':
			unite=tab_unites_litre[colonne_nouvelleunite];
		break;
		case 'a':
			unite=tab_unites_are[colonne_nouvelleunite];
		break;
	}
	
	nouvellepositionchiffre=num_placeunite+colonne_nouvelleunite-colonne_unitebase; //par rapport au tableau nombre_tab
	chaine='';
	nombre_tab_sans_zero_adroite=[];
	boolquedeszerosrencontree=true;
	for (i=nombre_tab.length-1;i>=0;i--)
	{
		if (boolquedeszerosrencontree)
		{
				if (nombre_tab[i]!=0)
				{
					boolquedeszerosrencontree=false;
					nombre_tab_sans_zero_adroite[i]=nombre_tab[i];
				}
		}
		else
		{
			nombre_tab_sans_zero_adroite[i]=nombre_tab[i];
		}
	}
		
	compteurespace=0;
	if (( unite!=' ')&&(unite!=undefined))
	{
		if (nouvellepositionchiffre<0) //nombre du genre 0,
		{
			chaine='0,';
			nouvellepositionchiffre++;
			while (nouvellepositionchiffre<0)
			{
				chaine=chaine+'0';
				nouvellepositionchiffre++;
				compteurespace++;
				if (compteurespace%3==0)
				{
					chaine=chaine+' ';
				}
			}
			for (i=0;i<nombre_tab_sans_zero_adroite.length;i++)
			{
				chaine=chaine+nombre_tab_sans_zero_adroite[i];
				compteurespace++;
				if (compteurespace%3==0)
				{
					chaine=chaine+' ';
				}
			}
		}
		else
		{
			if (nouvellepositionchiffre>=nombre_tab_sans_zero_adroite.length) //nombre entier
			{
				compteurespace=-nouvellepositionchiffre%3-1;
				
				for (i=0;i<nombre_tab_sans_zero_adroite.length;i++)
				{
						chaine=chaine+nombre_tab_sans_zero_adroite[i];
						compteurespace++;
						if (compteurespace%3==0)
						{
							chaine=chaine+' ';
						}
				}
				while (nouvellepositionchiffre>nombre_tab_sans_zero_adroite.length)
				{
					chaine=chaine+'0';
					nouvellepositionchiffre--;
					compteurespace++;
					if (compteurespace%3==0)
					{
						chaine=chaine+' ';
					}
				}
					chaine=chaine+'0';
				
			}
			else
			{
				console.log(nombre_tab_sans_zero_adroite.length,+' '+nouvellepositionchiffre);
				compteurespace=-nouvellepositionchiffre%3-1;
				for (i=0;i<nombre_tab_sans_zero_adroite.length;i++)
				{
					
						chaine=chaine+nombre_tab_sans_zero_adroite[i];
						
						if ((i==nouvellepositionchiffre)&&(i<nombre_tab_sans_zero_adroite.length-1))
						{
							chaine=chaine+',';	
							compteurespace++;
						}
						else
						{
							compteurespace++;
							if (compteurespace%3==0)
							{
								chaine=chaine+' ';
							}
						}
				}
			}
		}
		console.log(nombre_tab_sans_zero_adroite);
		var nb=(document.getElementById('nb').value).replace('^2', '²');
	nb=nb.replace('^3', '³');
	document.getElementById('message').innerHTML=(nb).replace(".",",")+'='+chaine+' '+unite;		
		
	
	
	
	}
	else
	{
		document.getElementById('message').innerHTML='Pas de conversion possible';
	}
}

function affichetableau()
{
	//clearCanvas(context, canvas);
	une_barre_sur_deux=false;
	une_barre_sur_trois=false;
 	switch(unite_choisi)
	{
		case 'm':
			tableau_unite=tab_unites_metre;
			tableau_unite_entiere=tab_unites_metre_entier;
			
		break;
		case 'm²':
			tableau_unite=tab_unites_metrecarre;
			tableau_unite_entiere=tab_unites_metrecarre_entier;
			une_barre_sur_deux=true;
		break;
		case 'm³':
			tableau_unite=tab_unites_metrecube;
			tableau_unite_entiere=tab_unites_metrecube_entier;
			une_barre_sur_trois=true;
		break;
		case 'g':
			tableau_unite=tab_unites_gramme;
			tableau_unite_entiere=tab_unites_gramme_entier;
		break;
		case 'L':
		case 'l':
			tableau_unite=tab_unites_litre;
			tableau_unite_entiere=tab_unites_litre_entier;
		break;
		
		case 'a':
			tableau_unite=tab_unites_are;
			tableau_unite_entiere=tab_unites_are_entier;
		break;
	}
	
	for (i=0;i<(tableau_unite_entiere.length+1);i++)
	{
		if ((((une_barre_sur_deux)&&(i%2==0))||((une_barre_sur_trois)&&(i%3==0)))||((!une_barre_sur_deux)&&(!une_barre_sur_trois)))
		{
			context.lineWidth=2; 
			context.moveTo(i*ecart_entre_case+decalage, 0);
			context.lineTo(i*ecart_entre_case+decalage, 445);
			context.stroke(); 
		}
	
		
	}
	for (i=1;i<(tableau_unite_entiere.length+1);i++)
	{
		
			context.font = "32px OpenDyslexicalta";
			context.textAlign = "center"; 
			context.save();
		if (!une_barre_sur_trois)
		{
			context.translate(i*ecart_entre_case-ecart_entre_case/2+decalage+10,120);
			context.rotate( -Math.PI / 2);
			context.fillText(tableau_unite_entiere[i-1],0,0)
			context.translate(-i*ecart_entre_case-ecart_entre_case/2+decalage-10,-120);
			context.restore();
		}
		else//petit trick pour centrer les unités m³
		{
			context.translate(i*ecart_entre_case-ecart_entre_case/2+decalage-10,120);
			context.rotate( -Math.PI / 2);
			context.fillText(tableau_unite_entiere[i-1],0,0)
			context.translate(-i*ecart_entre_case-ecart_entre_case/2+decalage+10,-120);
			context.restore();
		}
	}
	for (i=0;i<tableau_unite_entiere.length;i++)
	{
		context.save();
		context.font = "44px OpenDyslexicalta";
		context.textAlign = "center"; 
		if (une_barre_sur_deux) //petit trick pour centrer les unités m²
		{
			context.fillText(tableau_unite[i],i*ecart_entre_case+decalage,position_unite_sur_tableau_y_ligne1-20)	
		}
		else
		{
			if (une_barre_sur_trois)//petit trick pour centrer les unités m³
			{
				context.fillText(tableau_unite[i],i*ecart_entre_case-ecart_entre_case/2+decalage,position_unite_sur_tableau_y_ligne1-20)	
			}
			else
			{
				context.fillText(tableau_unite[i],i*ecart_entre_case+ecart_entre_case/2+decalage,position_unite_sur_tableau_y_ligne1-20)
			}
		}
		context.restore();
	}
	context.moveTo(decalage, 240);
	context.lineTo(tableau_unite_entiere.length*ecart_entre_case+decalage, 240);
	context.stroke(); 
	context.moveTo(decalage, position_unite_sur_tableau_y_ligne1);
	context.lineTo(tableau_unite_entiere.length*ecart_entre_case+decalage, position_unite_sur_tableau_y_ligne1);
	context.stroke(); 
	context.moveTo(decalage,0);
	context.lineTo(tableau_unite_entiere.length*ecart_entre_case+decalage,0);
	context.stroke(); 
	context.moveTo(decalage, 445);
	context.lineTo(tableau_unite_entiere.length*ecart_entre_case+decalage,445);
	context.stroke(); 
	

}
function getMousePos(canvas, evt) 
{
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
}

function affichebordblanc()
{
	var grd=context.createLinearGradient(0,0,decalage/2,0);
	
	grd.addColorStop(0, "rgba(255,255,255, 1)");
	grd.addColorStop(1, "rgba(255,255,255, 0)");
	context.fillStyle=grd;
	context.fillRect(0,0,decalage/2,canvas.height);
	
	var grd2=context.createLinearGradient(canvas.width-decalage/2,0,canvas.width,0);
	grd2.addColorStop(0, "rgba(255,255,255, 0)");
	//grd.addColorStop(1, "rgba(255,255,255, 0)");
	grd2.addColorStop(1, "rgba(255,255,255, 1)"); 
	context.fillStyle=grd2;

	context.fillRect(canvas.width-decalage/2,0,decalage/2,canvas.height);
	
	
}
canvas.addEventListener('mousedown', function(evt) {
	fige=false;
	var mousePos = getMousePos(canvas, evt);
	xsouris_debut=mousePos.x;
}, false);

canvas.addEventListener("touchstart", function(evt) {
fige=false;
	var touches = evt.touches;
	var rect = canvas.getBoundingClientRect();
	xtablette_debut=touches[0].clientX-rect.left;

});
	
canvas.addEventListener('mouseup', function(evt) {
	fige=true;
}, false);

canvas.addEventListener("touchend", function(evt) {
	 evt.preventDefault();
	fige=true;
}, false);

  canvas.addEventListener("touchleave", function(evt) {
	   evt.preventDefault();
	fige=true;
}, false);

canvas.addEventListener("touchmove", function(evt) {
	 evt.preventDefault();
	var touches = evt.touches;
	var rect = canvas.getBoundingClientRect();
	
	if (fige==false)
	{
	
		xtablette=touches[0].clientX-rect.left;
		ytablette=touches[0].clientY-rect.top;
		
		positionnombre_x=positionnombre_x-(xtablette_debut-xtablette);
		xtablette_debut=xtablette;
		placernombre(document.getElementById('nb').value);
		
	}
});

canvas.addEventListener('mousemove', function(evt) {
	if (fige==false)
	{
		var mousePos = getMousePos(canvas, evt);
		var xsouris=mousePos.x;
		positionnombre_x=positionnombre_x-(xsouris_debut-xsouris);
		xsouris_debut=xsouris;
		placernombre(nbencoursdetraitement);
	}
});

function ouvre(div){
    div.classList.remove('hide');
}

function ferme(div){
    div.classList.add('hide');
}
