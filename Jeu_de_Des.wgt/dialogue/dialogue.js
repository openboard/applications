/* Rendre de la boite de dialogue déplaçable */
$.fn.draggable = function(){
    var $this = this,
    ns = 'draggable_'+(Math.random()+'').replace('.',''),
    mm = 'mousemove.'+ns,
    mu = 'mouseup.'+ns,
    $w = $(window),
    isFixed = ($this.css('position') === 'fixed'),
    adjX = 0, adjY = 0;

    $this.mousedown(function(ev){
        var pos = $this.offset();
        if (isFixed) {
            adjX = $w.scrollLeft(); adjY = $w.scrollTop();
        }
        var ox = (ev.pageX - pos.left), oy = (ev.pageY - pos.top);
        $this.data(ns,{ x : ox, y: oy });
        $w.on(mm, function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if (isFixed) {
                adjX = $w.scrollLeft(); adjY = $w.scrollTop();
            }
            var offset = $this.data(ns);
            $this.css({left: ev.pageX - adjX - offset.x, top: ev.pageY - adjY - offset.y});
        });
        $w.on(mu, function(){
            $w.off(mm + ' ' + mu).removeData(ns);
        });
    });

    return this;
};
// Fonction pour ouvrir une boîte de dialogue
        function dialogue_ouvre(dialogId) {
		$('.dialogue').css('z-index',1); /* Passage de toutes les boites de dialogue à l'arrière plan */
		let dialog=$(dialogId);
		dialog.css('z-index',1000); /* Passage de la boite au 1er plan */
		dialog.show();
		dialog.draggable();
        }

// Fonction pour fermer une boîte de dialogue
function dialogue_ferme(dialogId) {
	$(dialogId).hide(); 
}


