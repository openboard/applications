let 	langue_defaut="fr",  syslang=langue_defaut; //Français par défaut
const canevas = document.getElementById('plateau');
const ctx = canevas.getContext('2d');
const couleur_de = 'white';
const taille_de = 60, taille_point=10; // Taille d'un dé, Grosseur du point
const couleur_point='black', couleur_contour_de='white',epaisseur_contour_de=5;
let	largeur_jeu=800,hauteur_jeu=660,//Dimensions du tapis de jeu
	max_points=4,// valeur maximale d'une face
	nb_des=3,//Nombre de dés à lancer
	pointX=taille_de/2,pointY=taille_de/2,// Coordonnées des points du dés
	espace_de=50,// Escpace minimum entre 2 dés
	total;// Total des dés obtenus

// Calcul de la place prise par 1 dé
// En fonction de la taille du dé et de l'espace entre 2 dés
// Afin qu'ils ne se touchent pas (bug affichage)
let deltaX=taille_de*2+espace_de,deltaY=taille_de*2+espace_de;
// Calcul du nombre de places possibles en fonction de la place occupée par 1 dé
let nbpos=Math.floor(largeur_jeu/deltaX)*Math.floor(hauteur_jeu/deltaY);
// Création d'un tableau à 2 dimensions avec "nbpos" lignes et 3 colonnes
// contenant les positions possibles d'un dé sur le plateau de jeu
// Correspondant aux coordonnées de la position de chaque dé (x,y) et à son état (libre,occupé)
let positions = new Array(nbpos);
//1ère Position du dé (en haut à gauche)
let pos1 = deltaX/2;
let pos2 = deltaY/2;
/**
* Copyright :: http://www.ceriously.com/projects/dice/
*/
let c = "";
let  debug = false;
let  kill = false; //Sécurité du script (évite les boucles infinies)
$(document).ready(function() {
// Associer les boutons à l'ouverture des boites de dialogue
	$(".btn").click(function(){
		ide=this.value;/* Récupération du nom bouton */
		switch (ide){ /* Selon le type de bouton */
			case 'apropos': /* Ouverture de la boite */
			case 'param':
			case 'MessageDialog':
			case 'ConfirmDialog':
				dialogue_ouvre('#'+ide);
				break;
			case 'jouer': /* Lancer de dés */
					dessine(nb_des);
				break;
			case 'total':/* Calcul de la somme */
				dialogue_ouvre('#'+ide);
				break;
			default:
				break;
		}
	});
 // Associer les boutons à la fermeture des boites de dialogue
	$(".btn-quitter").click(function(){
		ide=$(this).closest('.dialogue');/* Récupération du nom de la boite */
		dialogue_ferme(ide);
	}); 
});
async function init(){
	init_lang(); // Traduction de l'interface
	// Initialisation des variables
	// Nombre de dés lancés
	$('#nb_des').val(nb_des);
	// Valeur maximum d'une face de dé
	$('#max_points').val(max_points);
	// Initialisation du tableau contenant les coordonnées de la position 
	// de chaque dé (x,y) et à son état (libre,occupé)
	for (let i=0; i<nbpos; i++) {
        positions[i] = [pos1,pos2,0];//0=libre,1=occupé
		pos1 += deltaX;// On décalle vers la droite pour le suivant
		// Si on arrive en bout de ligne (dépassement de la limite du plateau)
		// On passe à la ligne suivante et on revient à pos1 initial
		if (pos1>largeur_jeu-taille_de) {pos1=deltaX/2;pos2 += deltaY};
	}
	// Actions liées aux changements du nombre de dés
	// ou de valeur maximum d'une face
	$('#nb_des').change(function(){
		nb_des=$(this).val();
		});
	$('#max_points').change(function(){
		max_points=$(this).val();
		});
	// Dimensionement du plateau de jeu
	canevas.width=largeur_jeu;
	canevas.height=hauteur_jeu;
	// Lancement des dés
	dessine(nb_des);
}
function init_lang(){
	//Détection de la langue
	try{
		syslang = sankore.locale().substr(0,2);
        } catch(e){
		syslang = langue_defaut;
	}
	// Chargement du fichier de langue
	sankoreLang[syslang].search;
	// Traduction de l'interface
	$('#titre_param').text(sankoreLang[syslang].Parametres);
	$('#titre_des').text(sankoreLang[syslang].Des);
	$('#titre_points').text(sankoreLang[syslang].Points);
	$('#titre_total').text(sankoreLang[syslang].Total);
	$('#txt_total').text(sankoreLang[syslang].Txt_Total);
	 //
	$('#infos').text(sankoreLang[syslang].Infos);
	 $('#txt_infos').html(sankoreLang[syslang].Txt_Infos);
	}


function dessine_de() {
	if(kill) return;
	dessine_carre(); //Dessin du carré correspondant au dé
	//Dessin du reflet
	ctx.fillStyle = "rgba(200,200,200,0.4)";
	ctx.beginPath();
	let x_reflet=taille_de-5,y_relfet=taille_de-5;
	ctx.moveTo(-x_reflet,y_relfet);
	ctx.lineTo(x_reflet,y_relfet);
	ctx.bezierCurveTo(0, 0, -x_reflet, y_relfet, -x_reflet, -y_relfet); //courbure du reflet
	ctx.closePath();
	ctx.fill();
	//Dessin des points
	let n = Math.floor(Math.random()*max_points)+1; //1-max_points points
	ctx.save();
	ctx.fillStyle = "black";//Couleur des points
	switch (n) {
	case 1:
		dessine_point(0,0);
		break;
	case 2:
        dessine_point(pointX,pointY);
        dessine_point(-pointX,-pointY);
		break;
	case 3:
        dessine_point(0,0);
        dessine_point(pointX,pointY);
        dessine_point(-pointX,-pointY);
		break;
	case 4:
        dessine_point(pointX,pointY);
        dessine_point(-pointX,-pointY);
        dessine_point(-pointX,pointY);
        dessine_point(pointX,-pointY);
		break;
	case 5:
        dessine_point(0,0);
        dessine_point(pointX,pointY);
        dessine_point(-pointX,-pointY);
        dessine_point(-pointX,pointY);
        dessine_point(pointX,-pointY);
		break;
	case 6:
        dessine_point(pointX,pointY);
        dessine_point(-pointX,-pointY);
        dessine_point(-pointX,pointY);
        dessine_point(pointX,-pointY);
        dessine_point(pointX,0);
        dessine_point(-pointX,0);
		break;
	}
	// On ajoute la valeur du dé à la somme totale
	total+=n;
	$('#reponse').text(total);// Mise à jour du total dans la boite de dialogue
    ctx.restore();
}

function dessine_point(x,y) {
    ctx.beginPath();
    ctx.arc(x,y,taille_point,0,Math.PI*2,true);
    ctx.closePath();
    ctx.fill();
}

function dessine_carre() {
	ctx.strokeStyle = "#000000";//Contour du dé
	ctx.fillStyle = "rgba(255,255,255,1.0)"; //Couleur du dé  blanc
	ctx.beginPath();
	ctx.moveTo(-taille_de, -taille_de);
	ctx.lineTo(taille_de,-taille_de);
	ctx.lineTo(taille_de,taille_de);
	ctx.lineTo(-taille_de,taille_de);
	ctx.lineTo(-taille_de,-taille_de);
	ctx.closePath();
	ctx.stroke();
	ctx.fill();
}

function dessine(num) {
	kill = false;
	// Effacement du plateau de jeu
	ctx.clearRect(0,0,canevas.width,canevas.height);
	// Initialisation du tableau contenant les coordonnées de la position 
	// de chaque dé (x,y) et à son état (libre,occupé)
	// Toutes les places sont libres au départ
	for (let i=0; i<nbpos; i++) {
		positions[i][2] =0;//0=libre,1=occupé
	}
	total=0;
	//Dessin du nombre de dés
		// Détermination de la position sur le plateau
		for (let i=0; i<num; i++) {
			let counter = 0;
			let j=Math.floor(Math.random()*nbpos);
			// Tant que la position est occupée
			while(positions[j][2]==1) {
				// Choix aléatoire d'une des positions possibles
				// sur le plateau de jeu
				j=Math.floor(Math.random()*nbpos)
				counter++;
				if (counter >= 1000) { //Tue le processus s'il est trop long (évite une boucle infinie)
					alert("Are you trying to crash your browser!? Ceriously...");
					kill = true;
					break;
				}
			}
			if(kill) break;
			// La position est occupée maintenant
			positions[j][2]=1;
			// Calcul de l'angle de rotation du dé
			let r = (Math.random()*175) * Math.PI / 180;
			ctx.save();
			ctx.translate(positions[j][0], positions[j][1]);// Placement du dé à la position aléatoire
				// Rotation du dé
			ctx.rotate(r);
			dessine_de();
			ctx.restore();
	}
}