var version = "2.3", author = "Fanch Le Cléac'h", authorHref = "https://openedu.fr";
if(window.sankore)
{
	version = window.widget.version;
	author = window.widget.author;
	authorHref = window.widget.authorHref;
}
var sankoreLang = {
"fr":{
	Parametres:"Paramètres",
	Des:"Dés",
	Points:"Points",
	Total:"TOTAL",
	Txt_Total:"Somme des dés",
	Infos:"À propos...",
	Txt_Infos:"<h1>Jeu de D&eacute;s</h1>"
	+"<h2>Application pour OpenBoard 1.7+</h2>"
	+"<div style='text-align:center'>Version "+version+"</div>"
	+"<h2>"+author+"</h2>"
	+"<p>Adaptation du script Dice disponible &agrave; l'adresse ci-dessous</p>"
	+"<p><a href='http://www.ceriously.com/projects/dice/'>http://www.ceriously.com/projects/dice/</a></p>",
	Site_Web:"<a href='"+authorHref+"'>"+authorHref+"</a>"
},
"en":{
	Parametres:"Parameters",
	Des:"Dices",
	Points:"Points",
	Total:"TOTAL",
	Txt_Total:"Dices sum",
	Infos:"About ...",
	Txt_Infos:"<h1>Dice game</h1>"
	+"<h2>App for OpenBoard 1.7+</h2>"
	+"<h3>Version "+version+"</h3>"
	+"<h2>"+author+"</h2>"
	+"<p>Adapted from script below</p>"
	+"<p><a href='http://www.ceriously.com/projects/dice/'>http://www.ceriously.com/projects/dice/</a></p>",
	Site_Web:"<a href='"+authorHref+"'>"+authorHref+"</a>"
},
"de":{
	Parametres:"Einstellungen",
	Des:"Würfeln",
	Points:"Punkten",
	Total:"GESAMTZAHL",
	Txt_Total:"Summe den Würfeln",
	Infos:"Über ...",
	Txt_Infos:"<h1>Würfelspiel</h1>"
	+"<h2>App für OpenBoard 1.7+</h2>"
	+"<h3>Version "+version+"</h3>"
	+"<h2>"+author+"</h2>"
	+"<p>Adaptiert von das folgende Skript </p>"
	+"<p><a href='http://www.ceriously.com/projects/dice/'>http://www.ceriously.com/projects/dice/</a></p>",
	Site_Web:"<a href='"+authorHref+"'>"+authorHref+"</a>"
}
};
