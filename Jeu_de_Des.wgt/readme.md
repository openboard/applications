# Dés
Application pour Openboard 
Possibilité d'effectuer des lancers aléatoires de 1 à 6 dés ayant des valeurs faciales de 1 à 6 points.
## Auteur
François Le Cléac'h https://openedu.fr
Septembre 2024
## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions
### 1.0
* lancer de dés
* choix du nombre de dés
* choix de la valeur faciale

### 2.0
* Accessibilité : Modification du menu
### 2.2
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant de l'application pour les mises à jours ultérieures
### 2.3
* Version Openboard 1.7 et supérieure
* Modification des icônes du menu
* Refonte des boîtes de dialogue

## Sources
* Adaptation du Script Dice pour Open Sankoré http://www.ceriously.com/projects/dice/
* Bibliothèque Jquery 3.7.1
