# Horloge École

***

## Nom
Horloge École

## Description
Il s'agit de reprendre dans un format actuel (en Html, css et Vanilla Javascript), l'horloge interactive au format flash (.swf) proposée par Patrick Finot dans le cadre du projet "Ecolaweb.com", aujourd'hui disparu et qui était proposée sous licence CC-BY-SA.
Cette horloge était extrêmement bien conçue pour être utilisée avec un vidéoprojecteur en classe afin d'aider à l'apprentissage de l'heure avec des aiguilles ou sous forme digitale.
Application fonctionnelle et en cours de développement (voir [feuille de route](#feuille-de-route) plus bas).

Version utilisable en ligne : [https://thierrym.forge.aeif.fr/horloge-ecole/](https://thierrym.forge.aeif.fr/horloge-ecole/)

Autres info disponibles ici : [https://lofurol.fr/joomla/logiciels-libres/332-developper-en-html-css-et-javascript](https://lofurol.fr/joomla/logiciels-libres/332-developper-en-html-css-et-javascript)

## Capture d'écran
![Capture d'écran](images/capture1.png)

## Installation
Cette application fonctionne avec un navigateur Web. Il suffit de récupérer les fichiers de ce dépôt et de lancer le fichier "index.html" dans un navigateur Internet.

## Utilisation
Cette appliquette web est la reprise en html, css et javascript, d’une appliquette flash intitulée "horloge interactive" conçue par Patrick Finot dans le cadre du projet "Ecolaweb.com", aujourd'hui disparu.
L’idée est de proposer aux enseignant⋅es et élèves, une horloge pour aider à l’apprentissage de la lecture de l’heure, qui soit projetable sur un grand écran type VPI/ENI et intégrable dans Openboard. Elle pourra aussi être utilisée avec des tablettes.

Il existe 2 modes d’utilisation :
* un mode "**horloge interne**" qui affiche l’heure actuelle de l’ordinateur sous 2 formes : la forme digitale et celle avec des aiguilles. Ici on voit l’heure défiler comme sur une horloge ou montre à aiguilles classique.
* Un mode "**horloge interactive**" qui affiche une heure fixe que l’on peut choisir et sur laquelle on peut agir de différentes manières.


Dans le mode interactif, elle propose des aiguilles manipulables plus épaisses (liées ou indépendantes pour augmenter la difficulté) permettant de fabriquer manuellement une heure soit directement, soit en essayant de reproduire l’heure affichée par l’horloge digitale (en masquant les aiguilles normales).
Il est aussi possible d’incrémenter les heures, minutes et secondes selon un pas que l’on fixe pour voir comment évolue l’heure dans le temps.
Il est aussi possible de tirer une heure au hasard.

Elle peut être utilisée de nombreuses manières en masquant certains éléments : indications sur l’horloge, affichage ou non des aiguilles, affichage ou non de l’horloge digitale...
On peut aussi accéder directement à 2 affichages correspondant à 2 types d'exercices différents : placer les aiguilles d'après une heure digitale ou retrouver l'heure digitale à partir de l'heure affichée par les aiguilles.
La correction se fait directement en appuyant sur le bouton "Vérifier".

Cette application a été développée initialement par Thierry Munoz, ERUN de la circonscription de Lézignan, Corbières et Minervois du département de l’Aude, en mai-juin 2023 et mise à jour en février 2024. Elle est proposée sous la licence GNU General Public Licence v3.0.

## Support
L'application est fournie telle quelle sans support.

## Feuille de route
* ~~Déplacement des aiguilles manipulables sur écran tactile (touchscreen)~~
* ~~Création d'une extension pour Openboard -> Via un fichier "config.xml"~~
* ~~Améliorer la mise en forme pour un affichage optimal quel que soit le type d'écran utilisé (ordinateur, tablette, ...)~~
* ~~Proposer un tirage au sort aléatoire de l'heure.~~
* ~~Proposer un mode "exercice" dans les 2 sens : retrouver l'heure digitale à partir des aiguilles ou placer les aiguilles à partie de l'heure digitale.~~
* ~~Proposer une correction automatique lorsqu'on écrit une heure digitale d'après la position des aiguilles.~~
* ~~Proposer une correction automatique lorsqu'on place les aiguilles manipulables selon une heure donnée.~~
* ~~Lier les aiguilles manipulables afin que l'action sur une influe sur les 2 autres et rendre ce mode de fonctionnement débrayable (pour augmenter la difficulté lorsqu'on doit placer les aiguilles manuellement).~~
* ~~Choisir de travailler avec les secondes ou pas.~~
* ~~Adaptation pour des tablettes tactiles.~~
* Rajouter des info-bulles.
* Proposer des scénarios pédagogiques d'utilisation sous forme de fiches, vidéo, ...
* Empaquetage sous forme d'un paquet .deb (notamment pour l'intégrer dans Primtux)
* Transformation en web application (manifest.json)
* Accessibilité.
* Internationalisation.

## Intégration dans Openboard
Sous Ubuntu, il faut recopier le contenu du dossier "horloge-ecole.wgt" qui se trouve dans le dossier "Openboard" dans le dossier /opt/openboard/library/applications.

## Contribution
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Auteur et remerciements
Merci à Patrick Finot, auteur de l'application "horloge-interactive.swf" dont j'ai repris le concept ainsi qu'aux auteur⋅es de OpenAI qui m'a permis de me passer de librairies externes Javascript tout en me permettant d'approfondir mes connaissances par ses propositions.
Voir le projet Sugarizer qui propose aussi une horloge interactive : [https://sugarizer.org/videos/clock.gif](https://sugarizer.org/videos/clock.gif)
Source : [https://github.com/llaske/sugarizer/blob/master/activities/Clock.activity/js/activity.js](https://github.com/llaske/sugarizer/blob/master/activities/Clock.activity/js/activity.js).

## License
GNU General Public Licence v3.0

## Statut du projet
Actif
