# Rullo produit
Un jeu pour travailler sur les critères de divisibilité.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
juin 2018

## Licence
CC BY-NC-SA

## Versions
Version 1
version initiale

## Sources
* Arnaud DURAND [ici](https://mathix.org/rullo_produit/) et [ici](https://mathix.org/linux/archives/11000)


