var	langue_defaut="fr",syslang=langue_defaut;
var	papier = Raphael("ligne","100%","100%"),// Occupe tout le div // Dimensions maximales du Canvas
	trait=[],nombre=[], frac1=[],frac2=[],frac3=[],visible=[], //Graduations & nombres, frac1, frac2 et frac3 sont les 3 composantes de la fraction
	nombre_graduations,
	mode_defaut='edition',mode=mode_defaut,mode_sombre='false',//Mode d'affichage
	// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	couleur_defaut="#"+blanc,couleur_fond='transparent', couleur_graduation="#"+rouge,couleur_sous_graduation="#"+noir, couleur_ligne="#"+noir,couleur_nombre="#"+bleu,couleur_fraction="#"+vert,couleur_fraction="#"+noir,couleur_fraction="#"+noir,couleur_fraction="#"+noir,
	val_unite=1,// Pas de graduation, de n en n
	origine=0, // Valeur de départ de la droite
	hauteur_graduation=30,hauteur_graduation_min=10,hauteur_graduation_max=50,// Hauteur des traits de graduations
	hauteur_sous_graduation=10,hauteur_sous_graduation_min=10,hauteur_sous_graduation_max=50,
	epaisseur_ligne=5,epaisseur_ligne_min=1,epaisseur_ligne_max=20,
	epaisseur_graduation=5,epaisseur_graduation_min=1,epaisseur_graduation_max=10,
	epaisseur_sous_graduation=5,epaisseur_sous_graduation_min=1,epaisseur_sous_graduation_max=10,
	epaisseur_barre_fraction=2,//
	taille_nombre=30,taille_max_nombre=60,taille_min_nombre=20,// Taille de police pour les nombres
	taille_fraction=20,taille_fraction_min=10,taille_fraction_max=60,// Taille de police pour les fractions
	marge_gauche=50, marge_haut=15, // Marge à gauche et au dessus
	label=[],// Etiquettes des nombres pour la graduation
	nb_unite=3, // Nombre de graduation initialisation
	min_unite=1,max_unite=20,// Nb min et max de graduations
	longueur_ligne=600,longueur_min=100, longueur_max=1800, // Longueur de la ligne, min et max
	longueur_unite=(longueur_ligne/nb_unite), longueur_unite_min=100,longueur_unite_max=500,// Longueur d'une unité
	longueur_sous_unite=20, // Longueur d'une sous_unité
	numero,numerateur,denominateur=3,denominateur_min=1,denominateur_max=10,decalage=0,
	style_nombre={"font-size": taille_nombre, "fill":couleur_nombre},style_fraction={"font-size": taille_fraction, "fill":couleur_fraction},style_fraction={"font-size": taille_fraction, "fill":couleur_fraction},style_fraction={"font-size": taille_fraction, "fill":couleur_fraction},// Style des nombres sur la ligne
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne},style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation},style_barre_fraction={"stroke-width":epaisseur_barre_fraction , "stroke":couleur_fraction},style_sous_graduation={"stroke-width":epaisseur_sous_graduation , "stroke":couleur_sous_graduation},// Style des traits
	x0=0,y0=hauteur_graduation+taille_nombre;// Coordonnées de départ de la ligne dans le Widget
// #####################################
// ##### INITIALISATIONS
// #####################################
function init_reglages(){
	//Curseurs
	// Longueur d'une unité
	$( "#longueur_unite" ).attr({
	       "max" : longueur_unite_max,
	       "min" : longueur_unite_min,
		"value":longueur_unite,
		"step":50
    });
	$("#longueur_unite").next('label').text(longueur_unite);
	// Longueur de la ligne
	$( "#longueur_ligne" ).attr({
	       "max" : longueur_max,
	       "min" : longueur_min,
		"value":longueur_ligne,
		"step":50
    });
	$("#longueur_ligne").next('label').text(longueur_ligne);
	// Dénominateur, partage de l'unité
	$( "#denominateur" ).attr({
	       "max" : denominateur_max,
	       "min" : denominateur_min,
		"value":denominateur,
		"step":1
    });
	$("#denominateur").next('label').text(denominateur);
	//Unité de départ
	$( "#origine" ).attr({
	       "max" : 100,
	       "min" : 0,
		"value":origine,
		"step":1
    });
	$("#origine").next('label').text(origine);
	// Decalage : nb de fractions avant la 1ère unité
	// denominateur-1 graduations possibles avant
	$( "#decalage" ).attr({
	       "max" : denominateur-1,
	       "min" : 0,
		"value":decalage,
		"step":1
    });
	$("#decalage").next('label').text(decalage);
	// Taille des chiffres des unités
	$( "#taille_nombre" ).attr({
	       "max" : taille_max_nombre,
	       "min" : taille_min_nombre,
		"value":taille_nombre,
		"step":1
    });
	$("#taille_nombre").next('label').text(taille_nombre);
	// Taille des chiffres des fractions
	$( "#taille_fraction" ).attr({
	       "max" : taille_fraction_max,
	       "min" : taille_fraction_min,
		"value":taille_fraction,
		"step":1
    });
	$("#taille_fraction").next('label').text(taille_fraction);
	//Épaisseur des traits
	$( "#epaisseur_ligne" ).attr({
	       "max" : epaisseur_ligne_max,
	       "min" : epaisseur_ligne_min,
		"value":epaisseur_ligne,
		"step":1
	});
	$("#epaisseur_ligne").next('label').text(epaisseur_ligne);
	
	$( "#epaisseur_graduation" ).attr({
	       "max" : epaisseur_graduation_max,
	       "min" : epaisseur_graduation_min,
		"value":epaisseur_graduation,
		"step":1
	});
	$("#epaisseur_graduation").next('label').text(epaisseur_graduation);
	$( "#epaisseur_sous_graduation" ).attr({
	       "max" : epaisseur_sous_graduation_max,
	       "min" :epaisseur_sous_graduation_min,
		"value":epaisseur_sous_graduation,
		"step":1
	});
	$("#epaisseur_sous_graduation").next('label').text(epaisseur_sous_graduation);
	// Hauteur des graduations
	$( "#hauteur_graduation" ).attr({
	       "max" : hauteur_graduation_max,
	       "min" : hauteur_graduation_min,
		"value":hauteur_graduation,
		"step":1
	});
	$("#hauteur_graduation").next('label').text(hauteur_graduation);
	$( "#hauteur_sous_graduation" ).attr({
	       "max" : hauteur_sous_graduation_max,
	       "min" : hauteur_sous_graduation_min,
		"value":hauteur_sous_graduation,
		"step":1
	});
	$("#hauteur_sous_graduation").next('label').text(hauteur_sous_graduation);
	}
function init_palette() {
//initialisation de la palette de couleurs
$('#couleur_graduation_unite').colorPicker({pickerDefault: couleur_graduation, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_graduation=newValue; style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation};trace_ligne();}}); //Couleur des graduations
$('#couleur_sous_graduation').colorPicker({pickerDefault: couleur_graduation, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) {couleur_sous_graduation=newValue; style_sous_graduation={"stroke-width":epaisseur_sous_graduation , "stroke":couleur_sous_graduation};trace_ligne();}}); //Couleur des sous-graduations
$('#couleur_ligne').colorPicker({pickerDefault: couleur_ligne, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_ligne=newValue;style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne};trace_ligne();}}); //Couleur de la ligne
$('#couleur_fond').colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false,onColorChange : function(id, newValue) { couleur_fond=newValue;$("body").css("background-color",couleur_fond)}}); //Couleur de la ligne
$('#couleur_unite').colorPicker({pickerDefault: couleur_nombre, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_nombre=newValue;style_nombre={"font-size": taille_nombre, fill:couleur_nombre};trace_ligne();}}); //Couleur des nombres
$('#couleur_fraction').colorPicker({pickerDefault: couleur_fraction, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fraction=newValue;style_fraction={"font-size": taille_fraction, fill:couleur_fraction};style_barre_fraction={"stroke-width":epaisseur_barre_fraction,"stroke":couleur_fraction};trace_ligne();}}); //Couleur du numérateur de la fraction
maj_palette(); // Actualisation des couleurs et des styles
}
function maj_palette() {
// Sélection de la couleur active pour l'aperçu
	$("#couleur_ligne").val(couleur_ligne).change();// Couleur active
	$("#couleur_fond").val(couleur_fond).change();// Couleur active
	$("#couleur_graduation_unite").val(couleur_graduation).change();// Couleur active
	$("#couleur_sous_graduation").val(couleur_sous_graduation).change();// Couleur active
	$("#couleur_unite").val(couleur_nombre).change();// Couleur active
	$("#couleur_fraction").val(couleur_fraction).change();// Couleur active
// Définition des styles
	style_nombre={"font-size": taille_nombre, "fill":couleur_nombre},style_fraction={"font-size": taille_fraction, "fill":couleur_fraction},style_barre_fraction={"stroke-width":epaisseur_barre_fraction , "stroke":couleur_fraction};// Styles des nombres affichés
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne},style_graduation={"stroke-width":epaisseur_graduation , "stroke":couleur_graduation},style_sous_graduation={"stroke-width":epaisseur_sous_graduation, "stroke":couleur_sous_graduation},// Style des traits
	$("body").css("background-color",couleur_fond);
}
function init_lang(){
	 // Traduction de l'interface
	$('#reglages').attr('title',sankoreLang[syslang].Reglages);
	$('#titre1').text(sankoreLang[syslang].Ligne);
	$('#titre1a').text(sankoreLang[syslang].Longueur);
	$('#titre1b').text(sankoreLang[syslang].Epaisseur);
	$('#titre1c').text(sankoreLang[syslang].Origine);
	$('#titre1d').text(sankoreLang[syslang].Decalage);
	
	$('#titre2').text(sankoreLang[syslang].Unite);
	$('#titre2a').text(sankoreLang[syslang].Longueur);
	$('#titre2b').text(sankoreLang[syslang].Epaisseur);
	$('#titre2c').text(sankoreLang[syslang].Taille);
	$('#titre2d').text(sankoreLang[syslang].Hauteur);
	
	$('#titre3').text(sankoreLang[syslang].Fractions);
	$('#titre3a').text(sankoreLang[syslang].Nombre);
	$('#titre3b').text(sankoreLang[syslang].Epaisseur);
	$('#titre3c').text(sankoreLang[syslang].Taille);
	$('#titre3d').text(sankoreLang[syslang].Hauteur);
	
	$('#titre4').text(sankoreLang[syslang].Numerotation);
	$('#titre4a').text(sankoreLang[syslang].Ligne);
	$('#titre4b').text(sankoreLang[syslang].Graduations);
	$('#titre4c').text(sankoreLang[syslang].Sous_Graduations);
	$('#titre5a').text(sankoreLang[syslang].Graduations);
	$('#titre5b').text(sankoreLang[syslang].Sous_Graduations);
	 
	  $('#infos').attr('title',sankoreLang[syslang].Infos);
	  $('#infos').html(sankoreLang[syslang].Txt_infos);
	}
async function init() {
	for (var i=0;i<parseInt(longueur_ligne/longueur_sous_unite)+1;i++){visible[i]=true}// Tous les nombres affichés par défaut
    if(window.sankore)
    {
	// Arrivée sur le widget 
	window.widget.onenter.connect(() => {$('#bouton_reglages').show();});// Affiche le bouton des paramètres
	// Sortie du Widget
	window.widget.onleave.connect(() => { 
		$('#bouton_reglages').hide();// Cache le bouton
		//Sauvegarde des paramètres au format 'chaine de caractères'
		window.sankore.setPreference('Nombre unites', nb_unite);
		window.sankore.setPreference('Valeur unite', val_unite);
		window.sankore.setPreference('Taille nombre', taille_nombre);
		window.sankore.setPreference('Taille fraction', taille_fraction);
		window.sankore.setPreference('Origine', origine);
		window.sankore.setPreference('Pas',val_unite);
		window.sankore.setPreference('Longueur ligne', longueur_ligne);
		window.sankore.setPreference('Longueur unite', longueur_unite);
		window.sankore.setPreference('Denominateur', denominateur);
		window.sankore.setPreference('Couleur graduation',couleur_graduation);
		window.sankore.setPreference('Couleur sous graduation',couleur_sous_graduation);
		window.sankore.setPreference('Couleur ligne',couleur_ligne);
		window.sankore.setPreference('Couleur fond',couleur_fond);
		window.sankore.setPreference('Couleur nombre',couleur_nombre);
		window.sankore.setPreference('Couleur numerateur',couleur_fraction);
		window.sankore.setPreference('Couleur barre fraction',couleur_fraction);
		window.sankore.setPreference('Couleur denominateur',couleur_fraction);
		window.sankore.setPreference('mode', mode);
		window.sankore.setPreference('Decalage', decalage);
		window.sankore.setPreference('Epaisseur ligne',epaisseur_ligne);
		window.sankore.setPreference('Epaisseur graduation',epaisseur_graduation);
		window.sankore.setPreference('Epaisseur sous graduation',epaisseur_sous_graduation);
		window.sankore.setPreference('Hauteur graduation',hauteur_graduation);
		window.sankore.setPreference('Hauteur sous graduation',hauteur_sous_graduation);
		window.sankore.setPreference('Mode sombre', $('body').hasClass('mode_sombre'));
		window.sankore.setPreference('Visible', JSON.stringify(visible));
		window.sankore.setPreference('Affiche unites', $('#affiche_unites').prop('checked'));
		window.sankore.setPreference('Affiche divisions', $('#affiche_divisions').prop('checked'));
	});
	//Détection de la langue
	try{
        syslang = window.sankore.lang.substr(0,2);
		sankoreLang[syslang].search;
	} catch(e){
		syslang = langue_defaut;
		}
	// Quand on revient sur la page / copie le widget, on récupère les paramètres stockés
	if (await window.sankore.async.preference('Nombre unites')) {// Récupération des paramètres sauvegardés s'ils existent (retour sur la page)
		nb_unite=parseInt(await window.sankore.async.preference('Nombre unites'));
		val_unite=parseInt(await window.sankore.async.preference('Valeur unite'));
		taille_nombre=parseInt(await window.sankore.async.preference('Taille nombre'));
		taille_fraction=parseInt(await window.sankore.async.preference('Taille fraction'));
		origine=parseInt(await window.sankore.async.preference('Origine'));
		val_unite=parseInt(await window.sankore.async.preference('Pas'));
		longueur_ligne=parseFloat(await window.sankore.async.preference('Longueur ligne'));
		longueur_unite=parseFloat(await window.sankore.async.preference('Longueur unite'));
		$('#ligne').width(longueur_ligne+20); // adaptation de la largeur du conteneur
		denominateur=parseInt(await window.sankore.async.preference('Denominateur'));
		couleur_graduation=await window.sankore.async.preference('Couleur graduation');
		couleur_sous_graduation=await window.sankore.async.preference('Couleur sous graduation');
		couleur_ligne=await window.sankore.async.preference('Couleur ligne');
		couleur_fond=await window.sankore.async.preference('Couleur fond');
		couleur_nombre=await window.sankore.async.preference('Couleur nombre');
		couleur_fraction=await window.sankore.async.preference('Couleur numerateur');
		mode=await window.sankore.async.preference('mode');
		decalage=parseInt(await window.sankore.async.preference('Decalage'));
		epaisseur_ligne=parseInt(await window.sankore.async.preference('Epaisseur ligne'));
		epaisseur_graduation=parseInt(await window.sankore.async.preference('Epaisseur graduation'));
		epaisseur_sous_graduation=parseInt(await window.sankore.async.preference('Epaisseur sous graduation'));
		hauteur_graduation=parseInt(await window.sankore.async.preference('Hauteur graduation'));
		hauteur_sous_graduation=parseInt(await window.sankore.async.preference('Hauteur sous graduation'));
		window.sankore.async.preference("'Mode sombre'","true").then(value => {$('body').addClass('mode_sombre')});
		visible=JSON.parse(await window.sankore.async.preference('Visible'));// Découpage de la chaîne en valeurs du tableau
		$('#affiche_unites').prop('checked',(await window.sankore.async.preference('Affiche unites')==='true')); // Case cochée ou non
		$('#affiche_divisions').prop('checked',(await window.sankore.async.preference('Affiche divisions')==='true')); // Case cochée ou non
    }
    }
	init_lang();//Traduction de l'interface
	init_reglages();//Mise à jour des paramètres
	init_palette(); //Initialisation des palettes
	// Boite de dialogue des paramètres après les initialisations
	$( "#reglages" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'let top', 
                                    at: 'center bottom', 
                                    of: "#widget"}, 
		beforeClose: function() {mode='vue'},
		close: function() {
			$('#ligne').width(longueur_ligne+20);
			mode_affichage(mode);}
	});
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'left top', 
                                    at: 'center center', 
                                    of: '#widget'}, 
	});
	$('#ligne').width(longueur_ligne+20).height(taille_nombre+hauteur_graduation+taille_fraction*2+10);// Adaptation du conteneur de la ligne	
	mode_affichage(mode);// Mode d'affichage	
	//Tracé de la ligne
	trace_ligne();
}
// #####################################
// ##### TRAÇAGE DE LA LIGNE GRADUÉE
// #####################################
Raphael.fn.ligne = function (cx, cy,longueur,nb_tirets, style_ligne) {//Fonction créant le svg à placer sur le canvas
	var cpt=(nb_unite+1),cx0=cx,longueur_barre_fraction;
	var paper = this,chart = this.set(),les_unites= this.set(),longueur_sous_unite=parseFloat(longueur_unite/denominateur);
	var cx_depart=cx,cy_depart=cy;
		nombre_graduations=parseInt(longueur/longueur_sous_unite)+1;//Nombre de graduations sur la ligne
	//Tracé d'un tiret de graduation
	function tiret(cx, cy, taille, params) {
		return paper.path(["M", cx, cy-taille, "L", cx, cy, "z"]).attr(params);
	};

	for (var i = 0; i <nombre_graduations; i++) {
		numerateur=(origine*denominateur+i-decalage);//Calcul du numérateur de la fraction
		if ((i-decalage)%denominateur==0){ //Selon la position sur la graduation
			trait[i]= tiret(cx, cy, hauteur_graduation,style_graduation)
			.data("id",i)//Tiret de la graduation
			.click(function(){// Un clic sur le nombre le masque
				nombre[this.data("id")].show();
				visible[this.data("id")]=true;
				});
			nombre[i]=papier.text(cx,cy-hauteur_graduation-taille_nombre/2,numerateur/denominateur)
			.attr(style_nombre)// Ecriture du numérateur
			.data({"type":'unite',"id":i})
			.click(function(){// Un clic sur le nombre le masque
				nombre[this.data("id")].hide();
				visible[this.data("id")]=false;
				});
			if (!visible[i]){
				nombre[i].hide();
				}
		}else{
			trait[i]= tiret(cx, cy, hauteur_sous_graduation,style_sous_graduation)
				.data("id",i) //Tiret de la sous-graduation
				.click(function(){// Un clic sur le nombre le masque
					frac1[this.data("id")].show();
					frac2[this.data("id")].show();
					frac3[this.data("id")].show();
					visible[this.data("id")]=true;
				});
			longueur_barre_fraction=(1+(numerateur>99)+(numerateur>9))*taille_fraction/2;//Adaptation de la longueur de la barre de fraction en fonction du nombre de chiffres du numérateur
			frac1[i]=papier.text(cx,cy+taille_fraction,numerateur)
				.attr(style_fraction) // Ecriture du numérateur
				.data({"type":'division',"id":i})
			.click(function(){// Un clic sur le nombre le masque
				frac1[this.data("id")].hide();
				frac2[this.data("id")].hide();
				frac3[this.data("id")].hide();
				visible[this.data("id")]=false;
				});
			frac2[i]=papier.path( ["M", cx-longueur_barre_fraction/2 , cy+taille_fraction*1.5, "L", cx+longueur_barre_fraction/2, cy+taille_fraction*1.5] )
				.attr(style_barre_fraction)// Tracé de la barre de fraction
				.data({"type":'division',"id":i})
				.click(function(){// Un clic sur le nombre le masque
					frac1[this.data("id")].hide();
					frac2[this.data("id")].hide();
					frac3[this.data("id")].hide();
					visible[this.data("id")]=false;
				});
			frac3[i]=papier.text(cx,cy+taille_fraction*2,denominateur)
				.attr(style_fraction).show()// Ecriture du dénominateur
				.data({"type":'division',"id":i})
				.click(function(){// Un clic sur le nombre le masque
					frac1[this.data("id")].hide();
					frac2[this.data("id")].hide();
					frac3[this.data("id")].hide();
					visible[this.data("id")]=false;
				});
			if (!visible[i]){
				frac1[i].hide();
				frac2[i].hide();
				frac3[i].hide();
				}
		}
		cx+=longueur_sous_unite; // passage à la suivante
			}
	// Tracé de la ligne en dernier afin qu'elle soit au 1er plan
		chart.push(paper.path(["M", cx_depart-epaisseur_graduation/2, cy_depart, "L", cx_depart+longueur, cy_depart, "z"]).attr(style_ligne));

	return chart;//On envoie le dessin
};

function trace_ligne(){
	$('#ligne').height(taille_nombre+hauteur_graduation+taille_fraction*2.5);// Adaptation du conteneur de la ligne
	y0=hauteur_graduation+taille_nombre;
	//Calcul du nombre d'unités sur la ligne
	nb_unite=parseInt(longueur_ligne/longueur_unite);
	// Trace une ligne partagée en nb_unite
	papier.clear();// On efface le Canvas
	papier.ligne(x0+marge_gauche, y0, longueur_ligne, nb_unite, style_ligne);//On trace le ligne
}
// ###############################################
// ##### ADAPTATION DU WIDGET AUX DIMENSIONS DE LA FENÊTRE
// ###############################################
function redimensionne(largeur,longueur) {
	// Redimensionne le widget sankore
	var margex=50,margey=50; // épaisseur de la fenêtre sankore
    if(window.sankore)
        window.sankore.resize(largeur+margex,longueur+margey);
}
// #####################################
// ##### MODE AFFICHAGE  DU WIDGET
// #####################################
function mode_affichage(style_affichage){
	mode=style_affichage;
	// ##################
	// ##### MODE VUE #####
	// ##################
	if (mode=="vue") {
		$("#bouton_reglages").hide();
		// On cache les réglages
		$( "#reglages" ).dialog( "close" );//Fermeture des paramètres
		var widgetX=$("#ligne").width(),widgetY=$("#ligne").height();
		redimensionne(widgetX,widgetY);
	};
	// #####################
	// ##### MODE EDITION #####
	// ####################
	if (mode=="edition") {
		// On affiche les réglages
		var widgetX=Math.max($("#ligne").width(),widget.width),widgetY=$("#ligne").height()+300;
		redimensionne(widgetX,widgetY);
		$( "#reglages" ).css( "display","flex" );//Affichage des paramètres
		$( "#reglages" ).dialog( "option", "position", { my: "left top", at: "left bottom", of:"#ligne" } );
		$( "#reglages" ).dialog( "open" );//Affichage des paramètres
	};
}
// #####################################
// ##### CORRECTION DU BUG js POUR LES DÉCIMAUX
// #####################################
function decimal(x,y){
// Transforme le nb X en nb décimal avec une précision de Y chiffres après la virgule
var reponse = Math.round(x*Math.pow(10,y))/Math.pow(10,y);
return reponse;
}
// #####################################
// ##### GESTION DES ÉVÉNEMENTS
// #####################################
// Un clic sur le bouton fait apparaître la boite de dialogue des paramètres
$("#bouton_reglages").click(function(){
	mode='edition';
	mode_affichage(mode);
});
// Nombre sur les graduations principales
$( "#affiche_unites" ).click( function(){
	var etat=$(this).prop('checked');
	nombre.forEach(function(e){
		var x=e.data('id');
		if (etat){
			nombre[x].show();
			}else{
			nombre[x].hide();
			}
		visible[x]=etat;
		});
});
$( "#affiche_divisions" ).click( function(){
	var etat=$(this).prop('checked');
	frac1.forEach(function(e){
		var x=e.data('id');
		if (etat){
			frac1[x].show();
			frac2[x].show();
			frac3[x].show();
			}else{
			frac1[x].hide();
			frac2[x].hide();
			frac3[x].hide();
			}
		visible[x]=etat;
		});
});
// Affichage des infos
$('#bouton_infos').click(function(){
	$('#infos').dialog('open');
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul_mode;
	$('body').toggleClass('mode_sombre');
	if ($('body').hasClass('mode_sombre')) {coul_mode='#'+blanc;couleur_fond='#'+noir} else {coul_mode='#'+noir;couleur_fond='#'+blanc};
	// Définition des couleurs
	couleur_nombre=coul_mode;
	couleur_fraction=coul_mode;
	couleur_ligne=coul_mode; 
	couleur_graduation=coul_mode;
	couleur_sous_graduation=coul_mode; 
	$("body").css("background-color",couleur_fond);
	maj_palette(); // Actualisation des palettes 
	trace_ligne();
	});
// Réactions aux déplacements des curseurs
	// Longueur d'une unité
$('#longueur_unite').mousemove(function(){
	longueur_unite=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	// Fraction de l'unité
$('#denominateur').mousemove(function(){
	denominateur=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	$( "#decalage" ).attr({"max" : denominateur-1});//adaptation du décalge à la valeur du dénominateur
	//Adaptation du décalage en fonction du dénominateur pour ne pas dépasser/arriver à l'unité précédente
	if (decalage>=denominateur) {decalage=denominateur-1;$( "#decalage" ).attr({"value" : decalage});$('#decalage').next('label').text(decalage)};//Si le
	trace_ligne();
	});
	//Unité de démarrage de la ligne
$('#origine').mousemove(function(){
	origine=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	// Longueur de la ligne
$('#longueur_ligne').mousemove(function(){
	longueur_ligne=parseFloat(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	$('#ligne').width(longueur_ligne+20); // adaptation de la largeur du conteneur
	trace_ligne();
	var 	widgetX=Math.max($('#ligne').width(),$('#reglages').width()),widgetY=$('#ligne').height()+$('#reglages').height()+60;// adaptation de la largeur du Widget
	redimensionne(widgetX,widgetY);	
	});
	//Nombre de fractions avant la première unité
$('#decalage').mousemove(function(){
	decalage=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	//Taille de la police pour les unités
$('#taille_nombre').mousemove(function(){
	taille_nombre=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_nombre={"font-size": taille_nombre, "fill":couleur_nombre};// adapte le style
	trace_ligne();
	});
	//Taille de la police pour la fraction
$('#taille_fraction').mousemove(function(){
	taille_fraction=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_fraction={"font-size": taille_fraction, "fill":couleur_fraction};// adapte le style
	if (taille_fraction>20) {epaisseur_barre_fraction=2}else{epaisseur_barre_fraction=1};
	style_barre_fraction={"stroke-width":epaisseur_barre_fraction , "stroke":couleur_fraction};
	trace_ligne();
	});
	//Épaisseur de la ligne
$('#epaisseur_ligne').mousemove(function(){
	epaisseur_ligne=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_ligne={"stroke-width":epaisseur_ligne, "stroke":couleur_ligne};
	trace_ligne();
	});
	//Épaisseur de la Graduation
$('#epaisseur_graduation').mousemove(function(){
	epaisseur_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_graduation={"stroke-width":epaisseur_graduation, "stroke":couleur_graduation};
	trace_ligne();
	});
	//Épaisseur de la Sous Graduation
$('#epaisseur_sous_graduation').mousemove(function(){
	epaisseur_sous_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	style_sous_graduation={"stroke-width":epaisseur_sous_graduation, "stroke":couleur_sous_graduation};
	trace_ligne();
	});
	// Hauteur de la Graduation
$('#hauteur_graduation').mousemove(function(){
	hauteur_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
	//Épaisseur de la Sous Graduation
$('#hauteur_sous_graduation').mousemove(function(){
	hauteur_sous_graduation=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_ligne();
	});
