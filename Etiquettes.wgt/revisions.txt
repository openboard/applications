Version 2.5 Juin 2020

Version 2.1
- Refonte intégrale de l'application initiale de 2017.
- Ajout des onglets.

Version 2.2
- Positionnement des étiquettes sur la pages avec passage à la ligne en bout de page.
- Choix du découpage (par lettre, mot ou ligne).

Version 2.3
- Modification des polices de caractère.

Version 2.4
- Paramétrage des bordures.
- Choix de la taille des caractères.

Version 2.5
- Mise en forme des onglet "Aide" & "Information"
- Finalisation de la feuille de style
- Prise en compte de nouvelles polices de caractères

Version 2.6
- Correction du bug d'affichage du bouton de génération des étiquettes.
- Correction d'un bug sous Open Sankoré (texte souligné dans l'aperçu)
- Réduction du poids du Widget (bibliothèques Jquery compressées)

À faire
- Fond personnalisé pour les étiquettes (Seyes/Lignes/quadrillé)
- Police de texte barrée
- Ajout de polices

