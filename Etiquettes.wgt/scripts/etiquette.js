/*
Cette application est sous licence CC-BY-NC-SA
Licence CC-BY-NC-SA

Vous êtes autorisé à :

Partager : copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter : remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Attribution : Vous devez créditer l'Oeuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la faÃ§on dont vous avez utilisé son Oeuvre.
Pas d'Utilisation Commerciale : Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
Partage dans les Mêmes Conditions : Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée.
Pas de restrictions complémentaires : Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.
Texte intégral de la licence :

https://creativecommons.org/licenses/by-nc-sa/2.0/fr/legalcode
*/

//##### Variables globales 
//##### Définition des valeurs initiales
	var 	taille_police=60,
		nom_police='Cursive standard',
		gras='',
		italique='',
		souligne='',
		couleur_police='black',
		couleur_bordure='black',
		couleur_fond='white',
		rayon_bordure=20,
		style_bordure='solid',
		epaisseur_bordure=5,
		largeX=800,largeY=600,//Dimensions de la fenêtre grand format
		WidgetX=650,WidgetY=450,//Dimensions normales du Widget
		palette_defaut=[
			['black', 'white', 'grey','lightgrey','red','blue'],
			['orange', 'yellow', 'green', 'lightblue', 'violet']
		],
		palette_fond=[
			['black', 'white', 'grey','lightgrey','red','blue'],
			['orange', 'yellow', 'green', 'lightblue', 'violet','rgba(0,0,0,0)']
		];
		
/* Fonction souligne_texte issue du site https://scriptstock.wordpress.com/2012/06/12/html5-canvas-text-underline-workaround/ */
function souligne_texte (context, text, x, y, color, textSize, align){
	//Récupère la largeur du texte
	var textWidth =context.measureText(text).width;

	//variable stockant la position de départ du trait (horizontal)
	var startX;
	//variable stockant la position de départ du trait (vertical)
	// Position du trait en fonction de la taille de la police
	// var startY = y+(parseInt(textSize)/15); /* Trait collé au texte */
	var startY = y+(parseInt(textSize)/2); /* Trait sous le texte */

	//variable stockant la position de fin du trait (horizontal)
	var endX;

	//variable stockant la position de départ du trait (vertical)
	//Identique car le trait est horizontal
	var endY = startY;

	//Epaisseur du trait en fonction de la taille de police
	var underlineHeight = parseInt(textSize)/15;

	// L'épaisseur minimum est fixée à 1 sinon le trait ne sera pas visible
	if(underlineHeight < 1){
		underlineHeight = 1;
	}
  
	context.beginPath();
	if(align == "center"){
		startX = x - (textWidth/2);
		endX = x + (textWidth/2);
	}else if(align == "right"){
		startX = x-textWidth;
		endX = x;
	}else{
		startX = x;
		endX = x + textWidth;
	}
	context.strokeStyle = color;
	context.lineWidth = underlineHeight;
	context.moveTo(startX,startY);
	context.lineTo(endX,endY);
	context.stroke();
}
//################ 
//##### RECTARRONDI
//################
// Fonction traÃ§ant un rectangle correspondant à l'étiquette
function RectArrondi(ctxt,x, y, w, h, radius, epaisseur,bordure,fond){
	//##### Conversion du code couleur en chaîne de caractères
	fond=fond.toString();
	//##### Déduction de l'épaisseur de la bordure
	x=x+epaisseur; // Abscisse de départ
	y=y+epaisseur; // Ordonnée de départ
	w=w-epaisseur*2; // Largeur, sans compter la bordure
	h=h-epaisseur*2; // Hauteur, sans compter la bordure
	//##### Dessin du rectangle
	var r = x + w;
	var b = y + h;
	ctxt.beginPath();
  	//##### Couleur et épaisseur de la bordure du rectangle
	ctxt.strokeStyle=bordure;
	ctxt.lineWidth=epaisseur;
	//##### Tracé du rectangle
	ctxt.moveTo(x+radius, y);
	ctxt.lineTo(r-radius, y);
	ctxt.quadraticCurveTo(r, y, r, y+radius);
	ctxt.lineTo(r, y+h-radius);
	ctxt.quadraticCurveTo(r, b, r-radius, b);
	ctxt.lineTo(x+radius, b);
	ctxt.quadraticCurveTo(x, b, x, b-radius);
	ctxt.lineTo(x, y+radius);
	ctxt.quadraticCurveTo(x, y, x+radius, y);
	ctxt.stroke();
	//##### Remplissage du fond du rectangle
	ctxt.fillStyle = fond;
	ctxt.fill();
}
//##############
//##### ETIQUETTE
//##############
//##### Fonction mettant en forme l'étiquette
function etiquette(){
	//##### Récupération du texte
	var texte=$('#mot').val();
	var etiquettes="";
	var type_decoupage=$('input[name=decoupage]:checked').val();
	switch (type_decoupage){
		case 'lettre':
			texte= texte.replace(/\s+/g,""); 
			etiquettes=texte.split("");// la chaîne de découpe est vide.		
			break;
		case 'mot':
			etiquettes=texte.split(/\s/);// la chaîne de découpe contient le code ESPACE.
			break;
		case 'ligne':
			etiquettes=texte.split(/\r\n|\r|\n/);// la chaîne de découpe contient les codes Retour à la ligne et Saut de ligne.
			break;
	}
	//##### Sélection du Canvas pour y dessiner les étiquettes
	var canvas = document.getElementById('canevas');
	var context = canvas.getContext('2d');
	//##### Paramètres de l'étiquettes
	//##### Taille et nom de la police
	var font=italique+gras+taille_police+'pt "'+nom_police + '"';
	var posX=-600,posY=-400;
	//##### Marge
	var margex=taille_police+10,margey=taille_police*2;
	if (nom_police.substr(0,7)=='Cursive standard') { // Adaptation pour la police cursive qui monte plus haut et descend plus bas
		margey+=taille_police+10;
	}
	//##### Police
	context.font = font ;

	//##### Découpage du texte en étiquette	
	for (var i=0; i<etiquettes.length;i++){
		//##### Récupération des dimensions du texte
		var nb_etiquettes = context.measureText(etiquettes[i]);
		var largeur = nb_etiquettes.width,hauteur=taille_police;
		//##### Ajustement du Canvas pour accueillir le texte
		canvas.width=largeur+margex;
		canvas.height=hauteur+margey;
		//##### Coordonnées pour centrer le texte sur l'étiquette
		var x = canvas.width/2;
		var y = canvas.height/2;
		
		//##### Dessin de la bordure
		
		//##### Récupération de la bordure d'étiquette choisie
		//##### Si la bordure est en pointillé
		var type_bordure=$('input[name=bordure]:checked').val();
		
		if ($('input[name=style_bordure]:checked').val()=='dashed'){
			context.setLineDash([2, 2]);
			}
		
		switch (type_bordure){
			case 'sans' :
				//##### Bordure transparente
				RectArrondi(context,0,0,canvas.width,canvas.height,0,0,'rgba(0,0,0,0)',couleur_fond);
				break;
			case 'droit' :
				if ($('input[name=style_bordure]:checked').val()=='double'){ // Si la bordure est DOUBLE l'épaisseur de chacune est simple
					RectArrondi(context,0,0,canvas.width,canvas.height,0,epaisseur_bordure,couleur_bordure,couleur_fond);// Bordure extérieure
					RectArrondi(context,10,10,canvas.width-20,canvas.height-20,0,epaisseur_bordure,couleur_bordure,couleur_fond);// Bordure intérieure
				} else {// Sinon la bordure est SIMPLE et l'épaisseur est double
					RectArrondi(context,0,0,canvas.width,canvas.height,0,epaisseur_bordure*2,couleur_bordure,couleur_fond);
				}
				break;
			case 'arrondi' :
				if ($('input[name=style_bordure]:checked').val()=='double'){ // Si la bordure est DOUBLE l'épaisseur de chacune est simple
					RectArrondi(context,0,0,canvas.width,canvas.height,rayon_bordure,epaisseur_bordure,couleur_bordure,couleur_fond);// Bordure extérieure
					RectArrondi(context,10,10,canvas.width-20,canvas.height-20,rayon_bordure/2,epaisseur_bordure,couleur_bordure,couleur_fond); // Bordure intérieure
				} else {// Sinon la bordure est SIMPLE et l'épaisseur est double
					RectArrondi(context,0,0,canvas.width,canvas.height,rayon_bordure,epaisseur_bordure*2,couleur_bordure,couleur_fond);
				}
				break;		
		}
		context.font =font;
		context.textAlign = 'center';
		context.textBaseline = 'middle';
		context.fillStyle = couleur_police;
		//##### Ajout du texte
		context.fillText(etiquettes[i], x, y);
		//##### Souligne le texte si besoin
		if (souligne=='souligne'){
			souligne_texte(context,etiquettes[i],x,y,couleur_police,taille_police,'center');
			}
		
		//##### Transfert de l'étiquette sur la page
		//##### Qualité à 0.1 pour gagner du temps de chargement sur la page
		var adresse=canvas.toDataURL('image/png',0.1);
		// Décallage pour voir toutes les étiquettes
		sankore.addObject(adresse,'100%','100%',posX+canvas.width/2,posY);
		
		
		if (posX>600){ // Si on arrive en bout de ligne
			posX=-600; // On revient à la ligne
			posY+=canvas.height; // On passe à la ligne du dessous
		}else {
			posX=posX+canvas.width; // Sinon on place l'étiquette à la suite de la précédente
		};
	}
}
//#################
//##### ONGLETS
//#################
//##### Gestion des onglets
function onglet(evt, nom_onglet) {
	var i, contenu_onglet, lien_onglet;
	contenu_onglet = document.getElementsByClassName("contenu_onglet");//On masque le contenu de tous les onglets
	for (i = 0; i < contenu_onglet.length; i++) {
		contenu_onglet[i].style.display = "none";
	}
	lien_onglet = document.getElementsByClassName("lien_onglet");// On désactive les lien
	for (i = 0; i < lien_onglet.length; i++) {
		lien_onglet[i].className = lien_onglet[i].className.replace(" active", "");
	}
	document.getElementById(nom_onglet).style.display = "block";//On affiche l'onglet sélectionné
	evt.currentTarget.className += " active"; // On active le lien
	
}

//#################
//##### INIT
//#################
//##### Initialisation
function init(){	
	//##### Sélection du 1er onglet par défaut #####
	document.getElementById('font').style.display = "block";//On affiche l'onglet sélectionné
	document.getElementsByClassName("lien_onglet")[0].className += " active"; // On active le lien
	
	//##### Type de découpage par défaut : les mots
	$('input[name=decoupage][value=mot]').prop("checked", true);
	
	//##### Initialisation de Police de caractères utilisée
	$( 'input:radio[name=police][value="'+nom_police+'"]').prop( "checked", true ); // Sélection de la police dans le menu
	$('#mot').css("font-family", nom_police); // police de l'étiquette
	$('#mot').css("color", couleur_police); // couleur de la police de l'étiquette
	$('#mot').css('font-size',taille_police); // taille de la police de l'étiquette

	//##### BORDURE
	$('#mot').css('border-width', epaisseur_bordure);	
	$('#mot').css('border-style', style_bordure);
	$('#mot').css('border-color', couleur_bordure);
	$( 'input:radio[name=style_bordure][value="'+style_bordure+'"]').prop( "checked", true ); // Sélection du style de la bordure dans le menu
	
	//##### Initialisation de la palette 
	$("#couleur_bordure").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,//masque la palette après sélection
		color: couleur_bordure,
		palette: palette_defaut,
		change: function(color) {
			var t=color.toHexString();
			couleur_bordure=t;
			// Bordure de l'étiquette
			$('#mot').css('border-color', t);
		}
	});	
	//##### Modification de la forme Bordure
	$( "input:radio[name=bordure]" ).on( "click", function() {
		epaisseur_bordure= $( "#curseur_epaisseur_bordure" ).slider( "value" );
		var le_style_de_la_bordure=$( "input:radio[name=style_bordure]:checked").val();
		
		$('#mot').css('border-width', epaisseur_bordure);
		if (le_style_de_la_bordure=='double'){// En cas de double bordure sélectionnée
					$('#mot').css('border-width', epaisseur_bordure+4);
				}
		$('#mot').css('border-style',le_style_de_la_bordure);
		$('#style_bordure').show();
		
		switch ($(this).val()){
			case 'sans' :
				$('#mot').css('border-style','none');
				$('#style_bordure').hide();
				break;
			case 'droit' :		
				$('#mot').css('border-radius', 0);
				
				break;
			case 'arrondi' :
				$('#mot').css('border-radius', 10);
		
			break;		
		};
	});
	//##### Modification du style de la Bordure 
	$( "input:radio[name=style_bordure]" ).on( "click", function() {
		
		$('#mot').css('border-style', this.value);
		$('#mot').css('border-width',epaisseur_bordure);
		if(this.value=='double'){
			$('#mot').css('border-width', $( "#curseur_epaisseur_bordure" ).slider( "value" )+4);
		}
		
	});
	//##### Modification de l'épaisseur de la Bordure
	$( function() {$( "#curseur_epaisseur_bordure" ).slider({
		value:epaisseur_bordure,
		min: 1,
		max: 10,
		step: 1,
		slide: function( event, ui ) {
				$( "#epaisseur_bordure" ).val( ui.value); // Affichage de la taille de la police
				
				epaisseur_bordure=ui.value; // modification de la taille de la police dans les étiquettes
				if ($('input[type=radio][name=style_bordure]:checked').val()=='double'){//pour la bordure double on l'augmente de 4px pour qu'elle soit visible au niveau 1
					epaisseur_bordure+=4
				}
				$('#mot').css('border-width', epaisseur_bordure);
			}
		});
	$( "#epaisseur_bordure" ).val( $( "#curseur_epaisseur_bordure" ).slider( "value" ));	 // Affichage de la valeur initiale
	});

	
	//##### TEXTE
	
	//##### Initialisation de la palette TEXTE
	$("#couleur_texte").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,//masque la palette après sélection
		color: couleur_police,
		palette: palette_defaut,
		change: function(color) {
			var t=color.toHexString();
			couleur_police=t;
			$('#mot').css('color',t)
		}
	});
	
	//##### Initialisation de la palette FOND
	$("#couleur_fond").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,//masque la palette après sélection
		color: couleur_fond,
		palette: palette_fond,
		change: function(color) {
			var t=color.toHexString();
			couleur_fond=color;
			$('#mot').css('background', color)
		}
	});

	//##### Choix de la POLICE de caractères de l'étiquette
	$('input[type=radio][name=police]').change(function() {
		$('#mot').css("fontFamily", $('input[type=radio][name=police]:checked').val());
		nom_police=$(this).val();
	});
	//##### Choix de la taille de Police
	$( function() {$( "#curseur_taille_police" ).slider({
		value:taille_police,
		min: 20,
		max: 100,
		step: 10,
		slide: function( event, ui ) {
				$( "#taille_police" ).val( ui.value ); // Affichage de la taille de la police		
				taille_police=ui.value; // modification de la taille de la police dans les étiquettes
				$('#mot').css('font-size',ui.value)
			}
		});
	$( "#taille_police" ).val( $( "#curseur_taille_police" ).slider( "value" ));	 // Affichage de la valeur initiale

	});
	//##### Choix du STYLE de texte
	$('#gras').click(function(){
		if (this.checked) {
			$('#mot').css("font-weight",'bold');
					gras='bold '; // Ne pas supprimer l'espace après bold ! Cela créerai une erreur dans la génération de la police de l'étiquette. (c.f. fonction etiquettes())
				} else {
					$('#mot').css("font-weight",'');
					gras='';
			}
	});
	$('#souligne').click(function(){
		if (this.checked) {
			$('#mot').css("text-decoration",'underline');
				souligne='souligne';
			} else {
				$('#mot').css("text-decoration",'')
				souligne='';
			};
	});
	$('#italique').click(function(){
		if (this.checked) {
			$('#mot').css("font-style",'italic');
				italique='italic ';// Ne pas supprimer l'espace après bold ! Cela créerai une erreur dans la génération de la police de l'étiquette. (c.f. fonction etiquettes())
			} else {
				$('#mot').css("font-style",'')
				italique='';
			};
	});
	
}
