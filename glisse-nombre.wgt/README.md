# Glisse nombre
Permet de faire glisser un nombre dans le tableau des rangs des chiffres. Idéal pour travailler sur la multiplication/division par 10 100 1000, ...

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Décembre 2023

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditez les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions.

## Versions
Version 3
version initiale

## Sources
* Arnaud DURAND [https://mathix.org/glisse-nombre/index.html](https://mathix.org/glisse-nombre/index.html) et [https://mathix.org/glisse-nombre/glisse-nombre.zip](https://mathix.org/glisse-nombre/glisse-nombre.zip)


