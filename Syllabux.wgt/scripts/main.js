/*Ce logiciel a été créé par Arnaud Champollion, il est placé sous licence libre GNU/GPL.
*/

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);

// Les zones
const panneau=document.getElementById('zone_1');
panneau.style.position="relative";
panneau.style.transition="all 1s";
panneau.visible=true;
const fenetre_options=document.getElementById('options');
const fenetre_apropos=document.getElementById('apropos');

const zone_principale=document.getElementById('zone_2');

// Les boutons
const bouton_replier=document.getElementById('bouton_replier');
const lettres_voyelle=document.getElementById('lettres_voyelle');
const fond_voyelle=document.getElementById('fond_voyelle');
const lettres_consonne=document.getElementById('lettres_consonne');
const fond_consonne=document.getElementById('fond_consonne');
const opacite=document.getElementById('opacite');

// Liste des graphèmes par catégories

// Consonnes
liste_consonnes_1=['l','m','n','p','r','s','t','ch']
liste_consonnnes_2=['b','d','f','v',,'j','z']
liste_consonnnes_3=['c','ç','g','ge','gu','qu']
liste_consonnnes_4=['br','cr','dr','fr','gr','pr','tr','vr']
liste_consonnnes_5=['bl','cl','fl','gl','pl','vl']
liste_consonnnes_6=['h','ph','chl','chr','th','sh','sch','x']

//Voyelles
liste_voyelles_1=['a','e','i','o','u','y','é']
liste_voyelles_2=['è','ê','ë']
liste_voyelles_3=['ai','au','eau','ei','eu','ou','oi','ui','uy']
liste_voyelles_4=['an','en','in','on','un','yn']
liste_voyelles_5=['ain','ein','oin']
liste_voyelles_6=['ail','eil','euil','ouil']
liste_voyelles_7=['abl','abr','abs','acl','acr','act','adr','affl','alf','afr','agl','agr','alt','alv','apl','apr','arb','arl','art','arv','asp','ast']
liste_voyelles_8=['ébl','ébr','écl','écr','ect','èdr','effl','èfl','effr','égl','égr','ègr','elf','elt','elv','épl','épr','erb','erl','ert','erv','esp','est']

//Liste des listes
liste_liste_consonnes=[liste_consonnes_1,liste_consonnnes_2,liste_consonnnes_3,liste_consonnnes_4,liste_consonnnes_5,liste_consonnnes_6]
liste_liste_voyelles=[liste_voyelles_1,liste_voyelles_2,liste_voyelles_3,liste_voyelles_4,liste_voyelles_5,liste_voyelles_6]

// Listes concaténées
liste_consonnes=liste_consonnes_1.concat(liste_consonnnes_2,liste_consonnnes_3,liste_consonnnes_4,liste_consonnnes_5,liste_consonnnes_6)
liste_voyelles = liste_voyelles_1.concat(liste_voyelles_2, liste_voyelles_3,liste_voyelles_4,liste_voyelles_5,liste_voyelles_6)

// Listes de vérification
liste_impossible=['ouild','euilt','ailfr','euilsh','ailbl','ynvl'];
const voyelles_impossible_cedille = /^[eéèêëiy]/i; // Voyelles impossibles après la cédille

// Liste des formes possibles pour le tirage aléatoire
formes_tirages = ['cv','vc'];


// Graphèmes cochés au départ
liste_consonnes_choisies=['l','m','n','p','r','s','t']
liste_voyelles_choisies=['a','e','i','o','u']

// Stockage des tirages aléatoires
liste_tirages=[];
liste_tirages_consonnes=[];
liste_tirages_consonnes2=[];
liste_tirages_voyelles=[];


// Variables diverses de départ
let son;
let cadre=true;
let position = -2;// Position de départ dans la liste des tirages. "-2" pour que lors de l'appui sur le bouron "précédent" on affiche l'avant-dernière syllabe.
let forme='cv'; // Forme de la syllabe 'cv" (consonne voyelle), 'vc' ou 'cvc'
let memoire_consonne_3=''; // En cas de passage de la forme 'cvc' à 'cv' ou 'vc', conserve en mémoire le phonème supprimé. 
let diaporama=false; // Démarre sans mode diaporama (défilement automatique)
let syllabaire_on=false; // Démarre sans le syllabaire affiché
var selectForme = document.getElementById("choix_forme");
var selectFond = document.getElementById("fond");

selectForme.value = "cv"; // Positionne le formulaire de forme sur "Consonne Voyelle"
panneau_choix=null;
const button = document.getElementById("bouton_precedent");
button.disabled = true; // Désactive le bouton précédent car pas de tirage en mémoire au début.
modediaporama="suivre";
// Variables liées aux options
let checkbox = document.getElementById("encadrer");
checkbox.addEventListener('change', function() {
  cadre = this.checked;
  bordure();
  stocke("cadre",cadre);
});
document.getElementById("mode_diaporama").value="suivre";
let delai=1;
let couleur_lettres_consonne;
let couleur_fond_consonne;
let couleur_lettres_voyelle;
let couleur_fond_voyelle;
let opacite_fond_grapheme;





promise = lecture_parametres()
promise.then( () => {
  initialisation();
});

// Definition des fonctions

function initialisation() {

  // Réglage des formulaires d'après les données
  document.getElementById("curseur_duree_diaporama").value=delai;
  updateValue(delai);
  document.getElementById("encadrer").checked=cadre;
  document.getElementById("police").value=police;
  selectForme.value=forme;
  selectFond.value = fond; // Positionne le formulaire de fond
  change_police(police);

  fond_consonne.value=couleur_fond_consonne;
  fond_voyelle.value=couleur_fond_voyelle;
  lettres_consonne.value=couleur_lettres_consonne;
  lettres_voyelle.value=couleur_lettres_voyelle;
  opacite.value=opacite_fond_grapheme;

  //Restauration du fond
  change_fond(fond);


  // Les trois phonèmes affichés au centre de la page
  const graph_1 = document.getElementById('graph_1');
  const graph_2 = document.getElementById('graph_2');
  const graph_3 = document.getElementById('graph_3');
  graph_1.classList.add('consonne');
  graph_2.classList.add('voyelle');
  graph_3.classList.add('consonne');

  // Boucle de création des cases à cocher pour le choix des consonnes
  var toto=1;
  liste_liste_consonnes.forEach(function(liste) {
    let classe_cases="case_consonne_liste_"+toto
    const checkboxes_consonnes = document.getElementById('checkboxes_consonnes');
    let categorie = document.createElement('div');
    checkboxes_consonnes.appendChild(categorie);
    categorie.id=liste.name;
    categorie.classList.add('categorie');
    const zone_boutons = document.createElement('div');
    categorie.appendChild(zone_boutons);
    zone_boutons.classList.add('zone_boutons');
    bouton_cocher_tout=document.createElement('input');
    zone_boutons.appendChild(bouton_cocher_tout);
    bouton_cocher_tout.type="button";
    bouton_cocher_tout.value="tout";
    bouton_cocher_tout.addEventListener("click",function() {cocherTout(classe_cases,liste_consonnes_choisies)});
    bouton_decocher_tout=document.createElement('input');
    zone_boutons.appendChild(bouton_decocher_tout);
    bouton_decocher_tout.type="button";
    bouton_decocher_tout.value="rien";
    bouton_decocher_tout.addEventListener("click",function() {decocherTout(classe_cases,liste_consonnes_choisies)});

    liste.forEach(function(consonne) {
        const checkbox = document.createElement('input');
        checkbox.classList.add("case_consonne");
        checkbox.classList.add(classe_cases);
        checkbox.type = 'checkbox';
        checkbox.name = 'consonne';
        checkbox.value = consonne;
        checkbox.checked = false;
        checkbox.style.marginRight = '10px';
        const label = document.createElement('label');
        label.textContent = consonne;
        let option = document.createElement('span');
        option.classList.add('option');
        categorie.appendChild(option);
        option.appendChild(checkbox);
        option.appendChild(label);
        option.addEventListener('click', function() {
          if (checkbox.checked===false) {
            checkbox.checked=true;
            option.style.borderColor="red";
            liste_consonnes_choisies.push(checkbox.value);
          } else {
            checkbox.checked=false;
            option.style.borderColor=null;
            const index = liste_consonnes_choisies.indexOf(checkbox.value);
          if (index > -1) {
            liste_consonnes_choisies.splice(index, 1);
          }
          }
          syllabaire();syllabaire();
          stocke("liste_consonnes_choisies", JSON.stringify(liste_consonnes_choisies));
        });


    });
    toto=toto+1
  });

  // Boucle de création des cases à cocher pour le choix des voyelles
  toto=1
  liste_liste_voyelles.forEach(function(liste) {
    let classe_cases="case_voyelle_liste_"+toto;
    const checkboxes_voyelles = document.getElementById('checkboxes_voyelles');
    let categorie = document.createElement('div');
    checkboxes_voyelles.appendChild(categorie);
    categorie.id=liste.name;
    categorie.classList.add('categorie');
    const zone_boutons = document.createElement('div');
    categorie.appendChild(zone_boutons);
    zone_boutons.classList.add('zone_boutons');
    bouton_cocher_tout=document.createElement('input');
    zone_boutons.appendChild(bouton_cocher_tout);
    bouton_cocher_tout.type="button";
    bouton_cocher_tout.value="tout";
    bouton_cocher_tout.addEventListener("click",function() {cocherTout(classe_cases,liste_voyelles_choisies)});
    bouton_decocher_tout=document.createElement('input');
    zone_boutons.appendChild(bouton_decocher_tout);
    bouton_decocher_tout.type="button";
    bouton_decocher_tout.value="rien";
    bouton_decocher_tout.addEventListener("click",function() {decocherTout(classe_cases,liste_voyelles_choisies)});

    liste.forEach(function(voyelle) {
      const checkbox = document.createElement('input');
      checkbox.classList.add("case_voyelle");
      checkbox.classList.add(classe_cases);
      checkbox.type = 'checkbox';
      checkbox.name = 'voyelle';
      checkbox.value = voyelle;
      checkbox.checked = false;
      checkbox.style.marginRight = '10px';
      const label = document.createElement('label');
      label.textContent = voyelle;
      let option = document.createElement('span');
      option.classList.add('option');
      categorie.appendChild(option);
      option.appendChild(checkbox);
      option.appendChild(label);

      option.addEventListener('click', function() {
        if (checkbox.checked===false) {
          checkbox.checked=true;
          option.style.borderColor="red";
          liste_voyelles_choisies.push(checkbox.value);
        } else {
          checkbox.checked=false;
          option.style.borderColor=null;
          const index = liste_voyelles_choisies.indexOf(checkbox.value);
        if (index > -1) {
          liste_voyelles_choisies.splice(index, 1);
        }
        }
        syllabaire();syllabaire();
        stocke("liste_voyelles_choisies", JSON.stringify(liste_voyelles_choisies));
      });
    });
    toto=toto+1;
  });

  // Application des couleurs

  maj_couleurs();


  // Disparition du logo au bout de 5 secondes

  setTimeout(function() {
    var element = document.getElementById("zone_mentions");
    element.style.bottom = "-200px";
  }, 5000);


  const cases_a_cocher = document.querySelectorAll('input[type="checkbox"]');
  for (let i = 0; i < cases_a_cocher.length; i++) {
    const case_a_cocher = cases_a_cocher[i];
    const valeur_case_a_cocher = case_a_cocher.value;

    // Coche la case si sa valeur est contenue dans le tableau liste_consonnes_choisies
    if (liste_consonnes_choisies.includes(valeur_case_a_cocher)) {
      case_a_cocher.checked = true;
      case_a_cocher.parentNode.style.borderColor="red";
    }
  }
  for (let i = 0; i < cases_a_cocher.length; i++) {
    const case_a_cocher = cases_a_cocher[i];
    const valeur_case_a_cocher = case_a_cocher.value;

    // Coche la case si sa valeur est contenue dans le tableau liste_consonnes_choisies
    if (liste_voyelles_choisies.includes(valeur_case_a_cocher)) {
      case_a_cocher.checked = true;
      case_a_cocher.parentNode.style.borderColor="red";
    }
  }


  calcul_delai()


  const spanElements = document.querySelectorAll('#syllabe span');

  spanElements.forEach(spanElement => {
    spanElement.addEventListener('click', function() {
      if (this.style.color !== 'rgba(0, 0, 0, 0)'){
        this.ancienne_valeur=this.innerHTML;
        //this.innerHTML = '&nbsp&nbsp';
        this.style.color='rgba(0, 0, 0, 0)';
      } else {
        //this.innerHTML = this.ancienne_valeur;
        this.style.color=null;
      }
    });
  });



  // Détection du changement de sens de la fenêtre
  window.addEventListener('resize', function(event) {
    if (panneau.visible==false){
        if (window.innerWidth > window.innerHeight) {
          panneau.style.left="-140px";zone_principale.style.width="100%";bouton_replier.style.left="0px";zone_principale.style.left="-140px";zone_principale.style.marginRight="-140px";
          panneau.style.top=null;bouton_replier.style.top=null;zone_principale.style.height=null;zone_principale.style.top=null;zone_principale.style.marginBottom=null;
          bouton_replier.style.backgroundImage="url(images/fleche_droite.png)";
        } else {
          panneau.style.top="-140px";bouton_replier.style.top="0px";zone_principale.style.height="100%";zone_principale.style.top="-120px";zone_principale.style.marginBottom="-120px";
          panneau.style.left=null;zone_principale.style.width=null;bouton_replier.style.left=null;zone_principale.style.left=null;zone_principale.style.marginRight=null;
          bouton_replier.style.backgroundImage="url(images/fleche_bas.png)";
        }
    }
  });


  document.getElementById("formes").addEventListener("change", function(event) {
    if (event.target.type === "checkbox") {
      if (event.target.checked) {
        formes_tirages.push(event.target.value);
      } else {
        const index = formes_tirages.indexOf(event.target.value);
        if (index !== -1) {
          formes_tirages.splice(index, 1);
        }
      }
    }
  });


  document.getElementById("logo").addEventListener('click', function() {
    if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
    if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
    if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
    if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
    if (document.getElementById("apropos").style.display==="block"){lightbox_ferme('apropos');}
    else {lightbox('apropos');}
  });

}


// Restauration des paramètres utilisateur
async function lecture_parametres()
{
  let valeurArecuperer;

  valeurArecuperer=await lit("forme");
  console.log(valeurArecuperer)
  if (valeurArecuperer){forme=valeurArecuperer;}

  valeurArecuperer=await lit("liste_consonnes_choisies");
  console.log(valeurArecuperer)
  if (valeurArecuperer){liste_consonnes_choisies=JSON.parse(valeurArecuperer);}

  valeurArecuperer=await lit("liste_voyelles_choisies");
  console.log(valeurArecuperer)
  if (valeurArecuperer){liste_voyelles_choisies=JSON.parse(valeurArecuperer);}

  valeurArecuperer=await lit("delai");
  console.log(valeurArecuperer)
  if (valeurArecuperer){delai=valeurArecuperer}

  valeurArecuperer=await lit("police");
  console.log(valeurArecuperer)
  if (valeurArecuperer){police=valeurArecuperer}
  else {police="Arial"}

  valeurArecuperer=await lit("fond");
  console.log(valeurArecuperer)
  if (valeurArecuperer){fond=valeurArecuperer}
  else {fond="url(images/lac.jpg)"}

  valeurArecuperer=await lit("cadre");
  console.log(valeurArecuperer)
  if (valeurArecuperer==="true"){cadre=true;}
  else {cadre=false;}

  valeurArecuperer=await lit("son");
  console.log(valeurArecuperer)
  if (valeurArecuperer==="true"){son=true;}
  else {son=false;}

  valeurArecuperer=await lit("couleur_lettres_consonne");
  console.log(valeurArecuperer)
  if (valeurArecuperer){couleur_lettres_consonne=valeurArecuperer}
  else {couleur_lettres_consonne='#000000'}

  valeurArecuperer=await lit("couleur_lettres_voyelle");
  console.log(valeurArecuperer)
  if (valeurArecuperer){couleur_lettres_voyelle=valeurArecuperer}
  else {couleur_lettres_voyelle='#000000'}

  valeurArecuperer=await lit("couleur_fond_consonne");
  console.log(valeurArecuperer)
  if (valeurArecuperer){couleur_fond_consonne=valeurArecuperer}
  else {couleur_fond_consonne='#8ae234'}

  valeurArecuperer=await lit("couleur_fond_voyelle");
  console.log(valeurArecuperer)
  if (valeurArecuperer){couleur_fond_voyelle=valeurArecuperer}
  else {couleur_fond_voyelle='#729fcf'}

  valeurArecuperer=await lit("opacite_fond_grapheme");
  console.log(valeurArecuperer)
  if (valeurArecuperer){opacite_fond_grapheme=valeurArecuperer}
  else {opacite_fond_grapheme='0.6'}

  //Restauration de l'état du syllabaire
  valeurArecuperer= await lit("syllabaire_on");
  if (valeurArecuperer){
    if (lit("syllabaire_on")==="true"){
      syllabaire();
    }
  }
}

function cocherTout(type_case,type_liste) {
  var cases = document.getElementsByClassName(type_case);
  for (var i = 0; i < cases.length; i++) {
    if (cases[i].checked === false) {
      type_liste.push(cases[i].value);
      cases[i].checked = true;
      cases[i].parentNode.style.borderColor = "red";

      }
    }
    syllabaire();syllabaire();  
    if (type_liste=liste_consonnes_choisies){stocke("liste_consonnes_choisies", JSON.stringify(liste_consonnes_choisies));}
    if (type_liste=liste_voyelles_choisies){stocke("liste_voyelles_choisies", JSON.stringify(liste_voyelles_choisies));}
   
}

function decocherTout(type_case,type_liste) {
  var cases = document.getElementsByClassName(type_case);
  for (var i = 0; i < cases.length; i++) {
    if (cases[i].checked === true) {
      const index = type_liste.indexOf(this.value);
      type_liste.splice(index, 1);
      cases[i].checked = false;
      cases[i].parentNode.style.borderColor = null;
      }
    }
    syllabaire();syllabaire();
    if (type_liste=liste_consonnes_choisies){stocke("liste_consonnes_choisies", JSON.stringify(liste_consonnes_choisies));}
    if (type_liste=liste_voyelles_choisies){stocke("liste_voyelles_choisies", JSON.stringify(liste_voyelles_choisies));}
        
}




// Pour tirer une valeur au sort dans une liste
function alea(liste) {
  if (liste.length>0){
  const randomIndex = Math.floor(Math.random() * liste.length);
  return liste[randomIndex];
  } else {
  return '';
  }
}

// Pour vider les syllabes
function reset_syllabe() {
  graph_1.innerHTML='';
  graph_2.innerHTML='';
  graph_3.innerHTML='';
}

// Tirage au sort d'une nouvelle syllabe
function tirage(mode) {
  calcul_delai;
  reset_syllabe();
  if (mode==='manuel'){
    if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
    if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
    if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
    if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
  }
  if (modediaporama==="melanger" && syllabaire_on===false){
    let index = Math.floor(Math.random() * formes_tirages.length);
    let valeur = formes_tirages[index];
    document.getElementById("choix_forme").value = valeur;
    change_forme(valeur);
  }
  const button = document.getElementById("bouton_precedent");
  button.disabled = false;
  if (mode==='manuel' && diaporama){tirage_auto(0)}
  let nouvelle_syllabe=''
  let nouvelle_consonne=''
  let nouvelle_consonne2=''
  let nouvelle_voyelle=''
  // Tirage au sort ...
  if (position === -2){
    let tour=0;
    while (tour===0 || verification_tirage(nouvelle_consonne,nouvelle_voyelle,nouvelle_syllabe)===false){
    nouvelle_consonne=alea(liste_consonnes_choisies)
    nouvelle_consonne2=alea(liste_consonnes_choisies)
    nouvelle_voyelle=alea(liste_voyelles_choisies)
    tour=tour+1    
    if (forme==='cv'){nouvelle_syllabe=nouvelle_consonne+nouvelle_voyelle}
    if (forme==='vc'){nouvelle_syllabe=nouvelle_voyelle+nouvelle_consonne}
    if (forme==='cvc'){nouvelle_syllabe=nouvelle_consonne+nouvelle_voyelle+nouvelle_consonne2}
    }
    nouvelle_syllabe=nouvelle_syllabe.replace(/np/g, "mp").replace(/nb/g, "mb").replace(/nm/g, "mm")
    liste_tirages.push(nouvelle_syllabe)
    liste_tirages_consonnes.push(nouvelle_consonne)
    liste_tirages_voyelles.push(nouvelle_voyelle)
    liste_tirages_consonnes2.push(nouvelle_consonne2)
  // ... ou remontée dans la liste après utilisation du bouton précédent
  } else {    
    position=position+2;
    nouvelle_syllabe=liste_tirages[liste_tirages.length + position]
    nouvelle_consonne=liste_tirages_consonnes[liste_tirages_consonnes.length + position]
    nouvelle_voyelle=liste_tirages_voyelles[liste_tirages_voyelles.length + position]
    nouvelle_consonne2=liste_tirages_consonnes2[liste_tirages_consonnes2.length + position]
    position=position-1;
  }

  if (forme==='cv'){
    graph_1.innerHTML=nouvelle_consonne;
    graph_2.innerHTML=nouvelle_voyelle;
  }
  if (forme==='vc'){
    graph_1.innerHTML=nouvelle_voyelle;
    graph_2.innerHTML=nouvelle_consonne;
  }
  if (forme==='cvc'){
    graph_1.innerHTML=nouvelle_consonne;
    graph_2.innerHTML=nouvelle_voyelle;
    graph_3.innerHTML=nouvelle_consonne2;
  }    
  verif_syllabe()
}

// Vérification et ajustement des tirages
function verification_tirage(nouvelle_consonne,nouvelle_voyelle,nouvelle_syllabe){
  let verif=true;
  if ( ( nouvelle_consonne === 'ç' || nouvelle_consonne === 'ge' ) && voyelles_impossible_cedille.test(nouvelle_voyelle)){
    verif=false;
  }
  if (liste_voyelles_5.includes(nouvelle_voyelle) && forme !== 'cv'){verif=false}
  if (nouvelle_syllabe === liste_tirages[liste_tirages.length-1]){verif=false}
  if (liste_impossible.includes(nouvelle_syllabe)){verif=false;}
  return verif;  
}

// Fonction diaporama
function tirage_auto() {
  if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
  if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
  if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
  if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
  if (diaporama === false){
    delai=calcul_delai();
    tirage('auto');document.getElementById('bouton_play').style.backgroundPositionY='-38px';diaporama=true;intervalId = setInterval(tirage, delai);}
  else {clearInterval(intervalId);document.getElementById('bouton_play').style.backgroundPositionY=null;diaporama=false;}
}

function calcul_delai() {
  delai=(document.getElementById("curseur_duree_diaporama").value)*1000; 
  return delai;
}

function precedent() {
  if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
  if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
  if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
  if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
  if (diaporama){tirage_auto(0)}
  if (liste_tirages.length+position >=0){
  nouvelle_consonne=liste_tirages_consonnes[liste_tirages_consonnes.length + position]
  nouvelle_voyelle=liste_tirages_voyelles[liste_tirages_voyelles.length + position]
  nouvelle_consonne2=liste_tirages_consonnes2[liste_tirages_consonnes2.length + position]
  position = position - 1
  if (forme==='cv'){graph_1.innerHTML=nouvelle_consonne;graph_2.innerHTML=nouvelle_voyelle;}
  if (forme==='vc'){graph_1.innerHTML=nouvelle_voyelle;graph_2.innerHTML=nouvelle_consonne;}
  if (forme==='cvc'){graph_1.innerHTML=nouvelle_consonne;graph_2.innerHTML=nouvelle_voyelle;graph_3.innerHTML=nouvelle_consonne2}
  if (liste_tirages.length+position < 0){
  const button = document.getElementById("bouton_precedent");
  button.disabled = true;
  }
  }
}

// Choix de la police
function change_police(police) {
  document.getElementById("syllabe").style.fontFamily = police;
  document.getElementById("colonne_gauche").style.fontFamily = police;
  document.getElementById("colonne_droite").style.fontFamily = police;
  document.getElementById("colonne_milieu").style.fontFamily = police;
  stocke("police",police);
}


// Changement de forme (consonne, voyelle, consonne ...)
function change_forme(valeur) {
  let ancienne_forme=forme;
  forme=valeur;
  stocke("forme", valeur);
  if (syllabaire_on){syllabaire();syllabaire();}
  if (ancienne_forme==='cv' && forme==='vc'){
    let temp = graph_1.innerHTML;
    graph_1.innerHTML = graph_2.innerHTML;
    graph_2.innerHTML = temp;
  }
  if (ancienne_forme==='vc' && forme==='cv'){
    let temp = graph_1.innerHTML;
    graph_1.innerHTML = graph_2.innerHTML;
    graph_2.innerHTML = temp;
  }
  if (ancienne_forme==='cvc' && forme==='cv'){
    memoire_consonne_3=graph_3.innerHTML;
    graph_3.innerHTML = '';
  }
  if (ancienne_forme==='cvc' && forme==='vc'){
    let temp = graph_1.innerHTML;
    graph_1.innerHTML = graph_2.innerHTML;
    graph_2.innerHTML = temp;
    graph_3.innerHTML = '';
  }
  if (ancienne_forme==='cv' && forme==='cvc'){
    graph_3.innerHTML = memoire_consonne_3;
  }
  if (ancienne_forme==='vc' && forme==='cvc'){
    let temp = graph_1.innerHTML;
    graph_1.innerHTML = graph_2.innerHTML;
    graph_2.innerHTML = temp;
    graph_3.innerHTML = memoire_consonne_3;
  }
  graph_1.className = "";
  graph_2.className = "";
  graph_3.className = "";
  if (forme==='cv'){
    graph_1.classList.add('consonne');
    graph_2.classList.add('voyelle');
  }
  if (forme==='vc'){
    graph_1.classList.add('voyelle');
    graph_2.classList.add('consonne');
  }
  if (forme==='cvc'){
    graph_1.classList.add('consonne');
    graph_2.classList.add('voyelle');
    graph_3.classList.add('consonne');
  }
  if (forme==='cvc' && syllabaire_on){
    document.getElementById('syllabe').style.left='-7.5vw';
    } else {document.getElementById('syllabe').style.left='0vw';}
  maj_couleurs();
  verif_syllabe();
  // Stocker la valeur de la variable "forme" dans un cookie
}

// Actions des boutons "consonne" et "voyelle"
function choix_consonnes() {
  if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
  if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
  if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
  if (panneau_choix==null){document.getElementById('choix_consonnes').style.display='block';panneau_choix='consonnes';}
}
function choix_voyelles() {
  if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
  if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
  if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
  if (panneau_choix==null){document.getElementById('choix_voyelles').style.display='block';panneau_choix='voyelles';}
}


// Actions des boutons de fermeture
function fermeture_choix_consonnes() {
  panneau_choix=null;
  document.getElementById('choix_consonnes').style.display='none';
}
function fermeture_choix_voyelles() {
  panneau_choix=null;
  document.getElementById('choix_voyelles').style.display='none';
}


// Mise à jour de l'étiquette de la glissière de choix du délai du diaporama
function updateValue(newValue) {
  document.getElementById("valueDisplay").textContent = ' ' +newValue + ' s';
  if(diaporama){tirage_auto();tirage_auto();}
}

// Affichage du syllabaire
function syllabaire(){
  if (syllabaire_on){
    let form = document.getElementById("formes");
    let elements = form.elements;
    for (var i = 0, len = elements.length; i < len; ++i) {
       elements[i].disabled = false;
    }
    document.getElementById("mode_diaporama").disabled=false;
    document.getElementById('colonne_gauche').style='display: none;';
    document.getElementById('colonne_droite').style='display: none;';
    document.getElementById('colonne_milieu').style='display: none;';
    document.getElementById('syllabe').style.left='0vh';
    elements = document.querySelectorAll('.etiquette');
    for (let i = 0; i < elements.length; i++) {
      elements[i].remove();
    }
    syllabaire_on=false;
    stocke("syllabaire_on","false");
  } else {
    var form = document.getElementById("formes");
    var elements = form.elements;
    for (var i = 0, len = elements.length; i < len; ++i) {
       elements[i].disabled = true;
    }
    document.getElementById("mode_diaporama").disabled=true;
    document.getElementById('colonne_gauche').style='display: block;'
    document.getElementById('colonne_droite').style='display: block;'
    if (forme==='cv'){
    syllabaire_affiche(liste_consonnes_choisies,'gauche');
    syllabaire_affiche(liste_voyelles_choisies,'droite');
    }
    if (forme==='vc'){
      syllabaire_affiche(liste_consonnes_choisies,'droite');
      syllabaire_affiche(liste_voyelles_choisies,'gauche');
      }
    if (forme==='cvc'){
      syllabaire_affiche(liste_consonnes_choisies,'gauche');
      syllabaire_affiche(liste_voyelles_choisies,'milieu');
      syllabaire_affiche(liste_consonnes_choisies,'droite');
      document.getElementById('syllabe').style.left='-7.5vw';
      } else {document.getElementById('syllabe').style.left='0vh';}
    // Récupération de l'élément HTML correspondant à la liste déroulante "police"
    let listePolice = document.getElementById("police");
    // Récupération de la valeur sélectionnée dans la liste déroulante
    let valeurPolice = listePolice.value; 
    change_police(valeurPolice);       
    syllabaire_on=true;
    stocke("syllabaire_on","true");
    maj_couleurs();

  }
}


function syllabaire_affiche(liste_mots,cote){
  liste_mots.forEach(function(phoneme){
  let etiquette = document.createElement('div');
  if (cote==='gauche'){zone=document.getElementById('colonne_gauche');}
  else if (cote==='droite'){zone=document.getElementById('colonne_droite');}
  else {zone=document.getElementById('colonne_milieu');zone.style='display: block;'}
  zone.appendChild(etiquette);
  etiquette.classList.add('etiquette');
  if (liste_mots===liste_voyelles_choisies){etiquette.classList.add('voyelle');etiquette.setAttribute('id', phoneme);}
  else {
    etiquette.classList.add('consonne');
    if (forme==='cvc' && cote==='droite'){etiquette.setAttribute('id', '2'+phoneme);}
    else {etiquette.setAttribute('id', phoneme);}
  }
  etiquette.innerHTML=phoneme;
  etiquette.addEventListener('click', () => {
    syllabe(etiquette.textContent,cote);
  });

  });
}

function syllabe(phoneme,cote){
  if (cote==='gauche'){graph_1.innerHTML=phoneme;}
  else if (cote==='milieu'){graph_2.innerHTML=phoneme;}
  else if (cote==='droite' && forme!=='cvc'){graph_2.innerHTML=phoneme;}
  else if (cote==='droite' && forme==='cvc'){graph_3.innerHTML=phoneme;}
  verif_syllabe();
}

function verif_syllabe(){
  let graph1=graph_1.innerHTML;
  let graph2=graph_2.innerHTML;
  let graph3=graph_3.innerHTML;
  if (graph1 === 'ç' && voyelles_impossible_cedille.test(graph2)){
    graph_1.innerHTML = 'c';
  }
  if (graph1 === 'ge' && voyelles_impossible_cedille.test(graph2)){
    graph_1.innerHTML = 'g';
  }
  if (liste_voyelles_5.includes(graph1)){graph_2.innerHTML='';}
  if (graph1.endsWith("n") && /^[mbp]/i.test(graph2)) {graph_1.innerHTML = graph_1.innerHTML.slice(0, -1) + "m";}
  if (graph2.endsWith("n") && /^[mbp]/i.test(graph3)) {graph_2.innerHTML = graph_2.innerHTML.slice(0, -1) + "m";}
  if (graph1.endsWith("m") && /^[mbp]/i.test(graph2)===false && forme ==="vc") {graph_1.innerHTML = graph_1.innerHTML.slice(0, -1) + "n";}
  if (graph2.endsWith("m") && /^[mbp]/i.test(graph3)===false && graph3!=='' && forme !=="cv") {graph_2.innerHTML = graph_2.innerHTML.slice(0, -1) + "n";}
  if (graph1==="gu" && /^[u]/i.test(graph2)){graph_1.innerHTML='g';}
  if (graph1==="qu" && /^[u]/i.test(graph2)){graph_2.innerHTML=graph_2.innerHTML.substring(1);}
  if (syllabaire_on){bordure()};
}

function bordure() {
  const etiquettes = document.querySelectorAll('.etiquette');
  etiquettes.forEach(etiquette => {
    etiquette.classList.remove('select');
  });
  if (cadre)
  {
    let graph__1_a_selectionner = document.getElementById(graph_1.innerHTML);
    graph__1_a_selectionner.classList.add("select");
    let graph__2_a_selectionner = document.getElementById(graph_2.innerHTML);
    graph__2_a_selectionner.classList.add("select");
    if (forme==='cvc'){
      let graph__3_a_selectionner = document.getElementById('2'+graph_3.innerHTML);
      graph__3_a_selectionner.classList.add("select");
    }
  }  
}



function visibilite_panneau(){
 if (panneau.visible){
  
  if (window.innerWidth > window.innerHeight) {panneau.style.left="-140px";zone_principale.style.width="100%";bouton_replier.style.left="0px";zone_principale.style.left="-140px";zone_principale.style.marginRight="-140px";}
  else {panneau.style.top="-140px";bouton_replier.style.top="0px";zone_principale.style.height="100%";zone_principale.style.top="-120px";zone_principale.style.marginBottom="-120px";}
  if (window.innerWidth > window.innerHeight){bouton_replier.style.backgroundImage="url(images/fleche_droite.png)";}
  else {bouton_replier.style.backgroundImage="url(images/fleche_bas.png)";}
 
  panneau.visible=false;
  
 } else {
  panneau.style.left="0px";
  if (window.innerWidth > window.innerHeight) {zone_principale.style.width=null;bouton_replier.style.left=null;zone_principale.style.left=null;zone_principale.style.marginRight=null;}
  else {panneau.style.top=null;bouton_replier.style.top=null;zone_principale.style.height=null;zone_principale.style.top=null;zone_principale.style.marginBottom=null;}
  bouton_replier.style.backgroundImage=null; 
  bouton_replier.style.right="0px";  
  panneau.visible=true;
 }




}


function lightbox(fenetre) {
  if (fenetre_apropos.style.display==="block"){lightbox_ferme('apropos')}
  if (fenetre_options.style.display==="block"){lightbox_ferme('options')}
  if (panneau_choix=='consonnes'){fermeture_choix_consonnes();}
  if (panneau_choix=='voyelles'){fermeture_choix_voyelles();}
  document.getElementById(fenetre).style.display = 'block';
}

function lightbox_ferme(fenetre) {
  document.getElementById(fenetre).style.display = null;
}

function change_fond(choix){
  document.body.style.backgroundImage = choix;
  stocke("fond",choix);
}

function change_modediaporama(mode){
  modediaporama=mode;
  if (mode==="suivre"){document.getElementById("formes").style.display="none";}
  else {document.getElementById("formes").style.display=null;}

}


function stocke(cle,valeur){
  console.log("stockage "+cle+"="+valeur);
  if (openboard){
    window.sankore.setPreference('syllabux-'+cle,valeur);
  } else {
    localStorage.setItem('syllabux-'+cle,valeur);
  }
}

async function lit(cle){

  let valeurAretourner;

  if (openboard){ //Récupération pour Openboard

    valeurAretourner = await window.sankore.async.preference('syllabux-'+cle);
    console.log("lecture "+cle+"="+valeurAretourner); // Pour la console

  } else { // Récupération en Web

    valeurAretourner = localStorage.getItem('syllabux-'+cle);
    console.log("lecture "+cle+"="+valeurAretourner); // Pour la console

  } 

  return valeurAretourner;
  
}

function entendre() {
  if (son){
    let utterance = new SpeechSynthesisUtterance(graph_1.innerHTML+graph_2.innerHTML+graph_3.innerHTML);
    utterance.lang='fr-FR';
    utterance.pitch=1.4;
    utterance.rate=0.8;
    speechSynthesis.speak(utterance);
    }
}

function couleur(type,classe,teinte){

  const elements = document.getElementsByClassName(classe);
  if (type==="fond"){
    let couleur_finale=hexToRgba(teinte, opacite_fond_grapheme);
    if (classe==="consonne"){couleur_fond_consonne=teinte;stocke('couleur_fond_consonne',couleur_fond_consonne)}
    else {couleur_fond_voyelle=teinte,stocke('couleur_fond_voyelle',couleur_fond_voyelle)}
    for (let i = 0; i < elements.length; i++) {
      elements[i].style.backgroundColor = couleur_finale;
    }
  } else {
    if (classe==="consonne"){couleur_lettres_consonne=teinte;stocke('couleur_lettres_consonne',couleur_lettres_consonne)}
    else {couleur_lettres_voyelle=teinte;stocke('couleur_lettres_voyelle',couleur_lettres_voyelle)}
    for (let i = 0; i < elements.length; i++) {
      elements[i].style.color = teinte;
    }
  }
}

function hexToRgba(hex, opacity) {
  let r, g, b;

  // Remove the # symbol if present
  hex = hex.replace('#', '');

  // Handle 3-digit hex codes
  if (hex.length === 3) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }

  // Convert the hex values to decimal
  r = parseInt(hex.substring(0, 2), 16);
  g = parseInt(hex.substring(2, 4), 16);
  b = parseInt(hex.substring(4, 6), 16);

  // Return the RGBA color string
  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
}

function reglage_opacite(taux){
  opacite_fond_grapheme=taux;
  couleur('fond','consonne',fond_consonne.value);
  couleur('fond','voyelle',fond_voyelle.value);
  stocke('opacite_fond_grapheme',opacite_fond_grapheme);
}

function maj_couleurs(){
  couleur('fond','consonne',couleur_fond_consonne);
  couleur('fond','voyelle',couleur_fond_voyelle);
  couleur('lettres','consonne',couleur_lettres_consonne);
  couleur('lettres','voyelle',couleur_lettres_voyelle);
}

