const divHaut = document.getElementById('haut');
const divMilieu = document.getElementById('milieu');

const divVoile = document.getElementById('voile');
const divTrou = document.getElementById('trou');
const inputOpacite = document.getElementById('inputOpacite');


let rectDivMilieu = divMilieu.getBoundingClientRect();

let dragEnCours = false;
let trou = {rayon: 100};
let rectDivTrou;
let opaciteVoile = 0.8;
inputOpacite.value = opaciteVoile;
let startXclic;
let startYclic;
let diffXclic;
let diffYclic;

let prefixeAppli = 'spotlight';
// Est-on dans Openboard ? ////
let openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);
if (openboard){
    document.body.classList.add('openboard');
}
///////////////////////////////


console.log(rectDivMilieu.width,rectDivMilieu.height)
trou.x=rectDivMilieu.width/2;
trou.y=rectDivMilieu.height/2;


///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {    
    await checkReglages();
    appliqueReglages();
}
executeFunctions();
//////////////////////////////////////////////////////////////

async function checkReglages() {
    console.log('--- Lecture stockage local');
    let valeur;

    // Fonction générique pour récupérer une valeur avec validation
    async function recuperer(cle, parseFunction = (v) => v, validateFunction = (v) => true) {
        let valeur = await litDepuisStockage(cle);
        if (valeur !== null && valeur !== undefined) {
            valeur = parseFunction(valeur);
            if (validateFunction(valeur)) {
                return valeur;
            }
        }
        return undefined;
    }

    valeur = await recuperer(
        'opacite',
        parseFloat,
        (v) => !isNaN(v) && v >= 0 && v <= 1 // Validation pour opacité (entre 0 et 1)
    );
    if (valeur !== undefined) opaciteVoile = valeur;

    valeur = await recuperer(
        'x',
        parseFloat,
        (v) => !isNaN(v) // Validation de base pour une coordonnée x
    );
    if (valeur !== undefined) trou.x = valeur;

    valeur = await recuperer(
        'y',
        parseFloat,
        (v) => !isNaN(v) // Validation de base pour une coordonnée y
    );
    if (valeur !== undefined) trou.y = valeur;

    valeur = await recuperer(
        'rayon',
        parseFloat,
        (v) => !isNaN(v) && v > 0 // Validation pour un rayon (doit être positif)
    );
    if (valeur !== undefined) trou.rayon = valeur;
}


function appliqueReglages() {
    changeOpacite(opaciteVoile);
    deplacerTrou(trou.x,trou.y);
    agrandirTrou(null,trou.rayon);
}




function changeOpacite(opacite) {
    opaciteVoile = parseFloat(opacite);
    let position = `${trou.x}px ${trou.y}px`;
    let rayon = `${trou.rayon}px`;
    let valeur = `
    radial-gradient(
      circle at ${position},
      transparent ${rayon},
      rgba(0, 0, 0, ${opaciteVoile}) ${trou.rayon}px
    )
    `
    console.log(valeur)
    divVoile.style.background = valeur;

    sauvegarde();

    
}



function clic(event) {
    let cible = event.target;
    const isTouchEvent = event.touches && event.touches.length > 0;
    const x = isTouchEvent ? event.touches[0].clientX : event.clientX;
    const y = isTouchEvent ? event.touches[0].clientY : event.clientY;
    if (cible === divTrou) {
        console.log('clic sur le trou');
        rectDivTrou = divTrou.getBoundingClientRect();
        trou.x = rectDivTrou.left - rectDivMilieu.left + rectDivTrou.width/2;
        trou.y = rectDivTrou.top - rectDivMilieu.top + rectDivTrou.height/2;
        startXclic = x - rectDivMilieu.left;
        startYclic = y - rectDivMilieu.top;
        diffXclic = startXclic - trou.x;
        diffYclic = startYclic - trou.y;
        dragEnCours = true;
        divMilieu.classList.add('drag');
    }
}

function move(event) {
    if (dragEnCours) {
        const isTouchEvent = event.touches && event.touches.length > 0;
        const x = isTouchEvent ? event.touches[0].clientX : event.clientX;
        const y = isTouchEvent ? event.touches[0].clientY : event.clientY;
        let clicX = x - rectDivMilieu.left;
        let clicY = y - rectDivMilieu.top;
        let nouvellePositionX = clicX - diffXclic;
        let nouvellePositionY = clicY - diffYclic;
        deplacerTrou(nouvellePositionX,nouvellePositionY);
    }
}

function release(event) {
    if (dragEnCours) {
        const isTouchEvent = event.touches && event.touches.length > 0;
        const x = isTouchEvent ? event.touches[0].clientX : event.clientX;
        const y = isTouchEvent ? event.touches[0].clientY : event.clientY;
        let clicX = x - rectDivMilieu.left;
        let clicY = y - rectDivMilieu.top;
        let nouvellePositionX = clicX - diffXclic;
        let nouvellePositionY = clicY - diffYclic;
        majPositionTrou(nouvellePositionX,nouvellePositionY);
        dragEnCours = false;
        divMilieu.classList.remove('drag');
    }
}

function molette(event) {
    if (event.deltaY < 0) {
        agrandirTrou(1);
    } else {
        agrandirTrou(-1);
    }
}

function agrandirTrou(sens,rayon) {
    let position = `${trou.x}px ${trou.y}px`;
    if (!rayon){
        trou.rayon += sens*10;
    } else {
        trou.rayon = rayon;
    }
    let rayonPx = `${trou.rayon}px`;
    let valeur = `
    radial-gradient(
      circle at ${position},
      transparent ${rayonPx},
      rgba(0, 0, 0, ${opaciteVoile}) ${trou.rayon}px
    )
    `
    divVoile.style.background = valeur;
    divTrou.style.width = divTrou.style.height = trou.rayon*2 + 'px';
    sauvegarde();


}

function deplacerTrou(x,y) {

    let position = `${x}px ${y}px`;
    let rayon = `${trou.rayon}px`;

    console.log('rayon',rayon)

    let valeur = `
    radial-gradient(
      circle at ${position},
      transparent ${rayon},
      rgba(0, 0, 0, ${opaciteVoile}) ${rayon}
    )
    `

    divVoile.style.background = valeur;

    divTrou.style.left = x + 'px';
    divTrou.style.top = y + 'px';

}

function majPositionTrou(x,y) {
    trou.x = x;
    trou.y = y;
    sauvegarde();

}

function sauvegarde(){
    stocke('opacite',opaciteVoile);
    stocke('x',trou.x);
    stocke('y',trou.y);
    stocke('rayon',trou.rayon);

}



// Attache les événements pour la souris et le tactile avec { passive: false } pour touchstart et touchmove
document.addEventListener('mousedown', clic);
document.addEventListener('mousemove', move);
document.addEventListener('mouseup', release);
document.addEventListener('touchstart', clic, { passive: false });
document.addEventListener('touchmove', move, { passive: false });
document.addEventListener('touchend', release);
document.addEventListener('wheel', molette);
