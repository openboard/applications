var	langue_defaut="fr",syslang,
	damier = new Array, // Tableau conservant les couleurs des cases
	lbl=new Array,//Etiquettes
	widgetX,widgetY, // Dimensions du Widget
	X_canvas=600,Y_canvas=450,// Dimensions maximales du Canvas
	papier = Raphael("grille","100%","100%"),// Occupe tout le div
	margex=25,margey=25,// Marges entre le widget et la fenêtre
	x0 = 10,// Abscisse de démarrage du grille (gauche) et marge autour du dessin
        y0 = 10,// Ordonnée de démarrage du Canvas (haut) et marge autour du dessin
        dx = 50, dy = 50,// Largeur d'une case,  Hauteur d'une case
	dx_min=50,dy_min=50,dx_max=200, dy_max=200, // Dimensions minimales/maximales d'une case
	dx_step=10,dy_step=10,// PAS d'augmentation en hauteur et largeur des cases
	mode_defaut='edition',mode=mode_defaut,//Mode d'affichage
	nb_lignes=2,nb_lignes_min=1,nb_lignes_max=10, nb_colonnes=3, nb_colonnes_min=1,nb_colonnes_max=10,// Dimensions initiales du quadrillage
	epaisseur_ligne=3,epaisseur_ligne_min=1,epaisseur_ligne_max=5, // Epaisseur du contour
	taille_police=14, taille_police_max=40,taille_police_min=8, epaisseur_barre=2,
	// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	//Couleur par défaut (vide), de la fraction et des lignes du quadrillage 
	couleur_fond="#"+blanc,couleur_fraction="#"+rouge, couleur_ligne="#"+noir,couleur_etiquette='#'+bleu,
	// Styles des étiquettes
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_etiquette},
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};

// #####################################
// ##### MODIFICATION DE LA COULEUR
// #####################################
function couleur(couleur_part) {
	if (couleur_part) {return couleur_fraction;} else {return couleur_fond;};
}

// #####################################
// ##### AFFICHAGE / MASQUAGE DES ETIQUETTES
// #####################################
function affiche_etiquettes() {
// Affichage de l'étiquette sous la forme a/b)
	var nb_parts=nb_lignes*nb_colonnes;
	if (document.getElementById("etiquettes").checked) {
		if (nb_parts==1) {
			lbl["1 1a"].show();
			lbl["1 1b"].show();
		}
		else {
			for	(var k=0; k < nb_lignes; k++){ // Boucle sur le nombre de lignes
				for (var l=0;l<nb_colonnes;l++){ // Boucle sur le nombre de colonnes
					lbl[k+' '+l+"a"].show();lbl[k+' '+l+"-"].show();lbl[k+' '+l+"b"].show();
				}
			}
		}
	}
	else  {
		if (nb_parts==1) {
			lbl["1 1a"].hide();
			lbl["1 1b"].hide();
		} 
		else{
			for	(var k=0; k < nb_lignes; k++){ // Boucle sur le nombre de lignes
				for (var l=0;l<nb_colonnes;l++){ // Boucle sur le nombre de colonnes
					lbl[k+' '+l+"a"].hide();lbl[k+' '+l+"-"].hide();lbl[k+' '+l+"b"].hide();
				}
			}
		}
	}
}
// #####################################
// ##### INITIALISATIONS
// #####################################
async function init() {
	init_damier (nb_lignes_max, nb_colonnes_max); // initialise les couleurs des cases
	syslang = langue_defaut;
	// Si dans OpenBoard
	if(window.sankore)
	{
		// Arrivée sur le widget
		window.widget.onenter.connect(() => {$('#bouton_reglages').show();});// Affiche le bouton des paramètres
		// Sortie du Widget
		window.widget.onleave.connect(() => {
			$('#bouton_reglages').hide();// Cache le bouton
			//Sauvegarde des paramètres au format 'chaine de caractères'
			window.sankore.setPreference('Lignes', nb_lignes);
			window.sankore.setPreference('Colonnes', nb_colonnes);
			window.sankore.setPreference('Couleur fraction', couleur_fraction);
			window.sankore.setPreference('Couleur ligne', couleur_ligne);
			window.sankore.setPreference('Epaisseur ligne', epaisseur_ligne);
			window.sankore.setPreference('Couleur etiquette', couleur_etiquette);
			window.sankore.setPreference('Couleur fond', couleur_fond);
			window.sankore.setPreference('Largeur case', dx);
			window.sankore.setPreference('Hauteur case', dy);
			window.sankore.setPreference('Etiquettes',$("#etiquettes").prop('checked'));//Affichage des étiquettes
			window.sankore.setPreference('Taille Police', taille_police);
			window.sankore.setPreference('mode', mode);
			// Références des cases coloriées
			for	(var k=0; k < nb_lignes; k++){ // Boucle sur le nombre de lignes
				for (var l=0;l<nb_colonnes;l++){ // Boucle sur le nombre de colonnes
					window.sankore.setPreference((k+','+l),damier[k+','+l]);// Couleur de la case
				}
			}
		});
		//Détection de la langue
		try{
			syslang = window.sankore.lang.substr(0,2);
			sankoreLang[syslang].search;
		} catch(e){
			syslang = langue_defaut;
		}
		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('Lignes')) {
			nb_lignes=parseInt(await window.sankore.async.preference('Lignes'));
			nb_colonnes=parseInt(await window.sankore.async.preference('Colonnes'));
			couleur_fraction=await window.sankore.async.preference('Couleur fraction');
			couleur_ligne=await window.sankore.async.preference('Couleur ligne');
			epaisseur_ligne=await window.sankore.async.preference('Epaisseur ligne');
			couleur_etiquette=await window.sankore.async.preference('Couleur etiquette');
			couleur_fond=await window.sankore.async.preference('Couleur fond');
			dx=parseInt(await window.sankore.async.preference('Largeur case'));
			dy=parseInt(await window.sankore.async.preference('Hauteur case'));
			$("#etiquettes").prop('checked',JSON.parse(await window.sankore.async.preference('Etiquettes')));//Valeur logique stockée sous forme de chaîne, donc à convertir en boolean
			taille_police=await window.sankore.async.preference('Taille Police');
			style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
			style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_etiquette},
			mode=await window.sankore.async.preference('mode');
			// Références des cases coloriées
			for	(var k=0; k < nb_lignes; k++){ // Boucle sur le nombre de lignes
				for (var l=0;l<nb_colonnes;l++){ // Boucle sur le nombre de colonnes
					(damier[k+','+l])=JSON.parse(await window.sankore.async.preference((k+','+l)));// Couleur de la case , Valeur logique stockée sous forme de chaîne, donc à convertir en boolean
				}
			}
		}
	}
	init_lang(); // Traduction de l'interface
	init_reglages();//Mise à jour des paramètres
	init_palette(); //Initialisation des palettes
    	maj_palette(); // Actualisation des couleurs et des styles
	// Boite de dialogue des paramètres
	$( "#reglages" ).dialog({
		autoOpen: false,
		width:500,
		height:250,
		position: {my: "left top", at: "left bottom", of: '#widget'}, 
		beforeClose: function() {mode='vue'},
	close: function() {
		mode_affichage(mode);
		}
	});
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'left top', at: 'center center', of: '#widget'}
	});
	//Trace le quadrillage(nb_lignes x nb_colonnes)
	adapte_canvas("grille",X_canvas,Y_canvas);
	quadrillage(nb_lignes, nb_colonnes);
	mode_affichage(mode);// Mode d'affichage
}
// #####################################
function init_reglages(){
	//Curseurs
	// Nombre de colonnes
	$( "#colonnes" ).attr({
	       "max" : nb_colonnes_max,
	       "min" : nb_colonnes_min,
		"value":nb_colonnes,
		"step":1
    });
    // Nombre de liges
	$( "#lignes" ).attr({
	       "max" : nb_lignes_max,
	       "min" : nb_lignes_min,
		"value":nb_lignes,
		"step":1
    });
	// Hauteur d'une case
	$( "#hauteur" ).attr({
	       "max" : dy_max,
	       "min" : dy_min,
		"value":dy,
		"step":dy_step
    });
    // Largeur d'une case
	$( "#largeur" ).attr({
	       "max" : dx_max,
	       "min" : dx_min,
		"value":dx,
		"step":dx_step
    });

	$( "#epaisseur_ligne" ).attr({
	       "max" : epaisseur_ligne_max,
	       "min" : epaisseur_ligne_min,
		"value":epaisseur_ligne,
		"step":1
    });
	// Police de caractère
	$( "#taille_police" ).attr({
	       "max" : taille_police_max,
	       "min" : taille_police_min,
		"value":taille_police,
		"step":1
    });
    //Mise à jour des paramètres du Widget    	
	$("#colonnes").next('label').text(nb_colonnes); //Mise à jour du curseur COLONNES
	$("#lignes").next('label').text(nb_lignes); //Mise à jour du curseur LIGNES
	$("#hauteur").next('label').text(dy); //Mise à jour du curseur Hauteur
	$("#largeur").next('label').text(dx); //Mise à jour du curseur Largeur
	$("#epaisseur_ligne").next('label').text(epaisseur_ligne);// Affiche la taille de police initiale
    	$("#taille_police").next('label').text(taille_police);// Affiche la taille de police initiale
}

// #####################################
// ##### INITIALISATION DE LA GRILLE
// #####################################
function init_damier(x,y) {
// Initialisation de la grille
for (var k=0; k < x; k++){
			for (var l=0;l<y;l++){
				damier[k+','+l]=(false);
			}
	}
}
// #####################################
// ##### INITIALISATION DE LA PALETTE
// #####################################
function init_palette() {
//Initialisation des palettes de couleurs
	// Palette pour les cases
	$('#fraction').colorPicker({pickerDefault: couleur_fraction, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fraction=newValue;quadrillage(nb_lignes, nb_colonnes);}});
	// Palette pour les lignes
	$('#contour').colorPicker({pickerDefault: couleur_ligne, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_ligne=newValue;quadrillage(nb_lignes, nb_colonnes);}});
	// Palette pour le fond
	$('#couleur_fond').colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fond=newValue;couleur_fond=newValue;quadrillage(nb_lignes, nb_colonnes);}});
	// Palette pour les étiquettes
	$('#couleur_etiquette').colorPicker({pickerDefault: couleur_etiquette,colors: palette_base,showHexField: false,onColorChange : function(id, newValue) {couleur_etiquette=newValue;quadrillage(nb_lignes, nb_colonnes);}
	}); //Couleur des nombres de la fraction
}
function maj_palette() {
// Sélection de la couleur active pour l'aperçu
	$("#couleur_ligne").val(couleur_ligne).change();// Couleur active
	$("#couleur_fond").val(couleur_fond).change();// Couleur active
	$("#fraction").val(couleur_fraction).change();// Couleur active
	$("#couleur_etiquette").val(couleur_etiquette).change();// Couleur active
// Définition des styles
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_etiquette},
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
}

function init_lang(){
	 // Traduction de l'interface
	$('#reglages').attr('title',sankoreLang[syslang].Reglages);
	$('#txt_grille').text(sankoreLang[syslang].Grille);
	$('#txt_case').text(sankoreLang[syslang].Case);
	$('#txt_fond').text(sankoreLang[syslang].Fond);
	$('#txt_colonnes').text(sankoreLang[syslang].Colonnes);
	$('#txt_lignes').text(sankoreLang[syslang].Lignes);
	$('#txt_hauteur').text(sankoreLang[syslang].Hauteur);
	$('#txt_largeur').text(sankoreLang[syslang].Largeur);
	$('#txt_epaisseur').text(sankoreLang[syslang].Epaisseur);
	$('#txt_etiquettes').text(sankoreLang[syslang].Etiquettes);
	$('#txt_taille').text(sankoreLang[syslang].Taille);
	$('#txt_epaisseur').text(sankoreLang[syslang].Epaisseur);
	 
	  $('#infos').attr('title',sankoreLang[syslang].Infos);
	  $('#infos').html(sankoreLang[syslang].Txt_infos);
	}
// #####################################
// ##### GESTION DES ÉVÉNEMENTS
// #####################################
// Un clic sur le bouton fait apparaître la boite de dialogue des paramètres
$("#bouton_reglages").click(function(){
	mode_affichage('edition');
});
// Affichage des infos
$('#bouton_infos').click(function(){
	$('#infos').dialog('open');
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul1,coul2;
	$('body').toggleClass('mode_sombre');
	if ($('body').hasClass('mode_sombre')) {
		coul1='#'+blanc;
		coul2='#'+noir;
		} else {
		coul1='#'+noir;
		coul2='#'+blanc;
	};
	couleur_ligne=coul1;
	couleur_fond=coul2;
	couleur_etiquette=coul1;
	couleur_fond=coul2;
	quadrillage(nb_lignes, nb_colonnes);
	});
// Réactions aux déplacements des curseurs
	// Ajout / retrait de lignes/colonnes
$('#colonnes').mousemove(function(){
	nb_colonnes=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	quadrillage(nb_lignes, nb_colonnes);
	});
$('#lignes').mousemove(function(){
	nb_lignes=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	quadrillage(nb_lignes, nb_colonnes);
	});
	// Dimesions d'une case
$('#hauteur').mousemove(function(){
	dy=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	quadrillage(nb_lignes, nb_colonnes);
	});
$('#largeur').mousemove(function(){
	dx=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	quadrillage(nb_lignes, nb_colonnes);
	});
	// Lignes du quadrillage
$('#epaisseur_ligne').mousemove(function(){
	epaisseur_ligne=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	quadrillage(nb_lignes, nb_colonnes);
	});
	// Affichage des étiquettes
$('#etiquettes').click(function(){
	affiche_etiquettes();
	});
	// Taille de la police
$('#taille_police').mousemove(function(){
	taille_police=parseInt(this.value);// recupere la valeur du curseur
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne 
	quadrillage(nb_lignes, nb_colonnes);
	});
// #####################################
// ##### MODE AFFICHAGE
// #####################################
function mode_affichage(style_affichage){
	mode=style_affichage;
	// ##################
	// ##### MODE VUE #####
	// ##################
	if (mode=="vue") {
		// On cache les réglages
		$("#bouton_reglages").hide();
		$( "#reglages" ).dialog( "close" );//Ouverture des paramètres
		widgetX=$("#grille").width()+margex,widgetY=$("#grille").height()+margey;
	};
	// #####################
	// ##### MODE EDITION #####
	// ####################
	if (mode=="edition") {
		// On affiche les réglages
		$( "#reglages" ).dialog( "open" );//Ouverture des paramètres
		widgetX=Math.max($("#grille").width()+margex,X_canvas),widgetY=Math.max($("#grille").height()+margey,Y_canvas);
	};
	redimensionne(widgetX,widgetY);
}

// #####################################
// ##### ADAPTATION DU CANVAS
// #####################################
function adapte_canvas(id,largeur,hauteur) {
//Adaptation de la hauteur et largeur du canvas
$("#"+id).width(largeur+"px");
 $("#"+id).height(hauteur+"px");
}
// #####################################
// ##### REDIMENSIONNEMENT DE LA FENETRE
// #####################################
function redimensionne(largeur,longueur) {
// Redimensionne le widget sankore
if(window.sankore)
	window.sankore.resize(largeur,longueur);
}
// #####################################
// ##### TRAÇAGE DU QUADRILLAGE
// #####################################
function quadrillage(nb_lignes,nb_colonnes) {
	maj_palette(); // Actualisation des couleurs et des styles
	//Tracé d'une barre horizontale de la fraction
	function barre(cx, cy, taille, params) {
		return papier.path(["M", cx-taille/2, cy, "L", cx+taille/2, cy, "z"]).attr(params);
	};
	var nb_parts=nb_lignes*nb_colonnes, x_etiquette,y_etiquette;// Coordonnées;
	papier.clear(); // Efface le dessin précédent
	x=x0;// Coordonnées de démarrage du dessin
	y=y0;
	if (nb_parts==1) {
		var p=papier.rect(x,y,dx,dy);// Définition du rectangle
		p.attr({stroke:couleur_ligne, "stroke-w idth":epaisseur_ligne,fill: couleur(damier['1,1']),cursor:"pointer"});// Couleur du contour, de la case et apparence du curseur
		p.data("coord", ('1,1')); // Sauvegarde des coordonnées de la case
		p.click(function() { // Gestion du clic sur la grille
			damier[this.data("coord")]=!damier[this.data("coord")];// Inversion de la couleur
			this.attr({fill:couleur(damier[this.data("coord")])});// On attribut la couleur à la case
			if (couleur_fond=='transparent') {	quadrillage(nb_lignes, nb_colonnes)}; // Gestion bug transparence
		});
		lbl["1 1a"] = papier.text(x+dx/2 , y+dy/2-10,"1").attr(style_etiquette).hide(); 
		lbl["1 1b"] = papier.text(x+dx/2 , y+dy/2+10,"unité").attr(style_etiquette).hide();
	}
	else {
	for	(var k=0; k < nb_lignes; k++){ // Boucle sur le nombre de lignes
		for (var l=0;l<nb_colonnes;l++){ // Boucle sur le nombre de colonnes
			var p=papier.rect(x,y,dx,dy);// Définition du rectangle
			p.attr({stroke:couleur_ligne, "stroke-width":epaisseur_ligne, fill: couleur(damier[k+','+l]),cursor:"pointer"});// Couleur du contour, de la case et apparence du curseur
			p.data("coord", (k+','+l)); // Sauvegarde des coordonnées de la case
			p.click(function() { // Gestion du clic sur la grille
				damier[this.data("coord")]=!damier[this.data("coord")];// Inversion de la couleur
				this.attr({fill:couleur(damier[this.data("coord")])});// On attribut la couleur à la case
			if (couleur_fond=='transparent') {	quadrillage(nb_lignes, nb_colonnes)}; // Gestion bug transparence
			});
			// Étiquette
			x_etiquette=x+dx/2,y_etiquette=y+dy/2;// Coordonnées
			lbl[k+' '+l+"a"] = papier.text(x_etiquette , y_etiquette-taille_police/2-2 ,"1").attr(style_etiquette).hide();// Ecriture du numérateur
			lbl[k+' '+l+"-"] =  barre( x_etiquette, y_etiquette, taille_police, style_barre).hide();// Tracé de la barre de fraction
			lbl[k+' '+l+"b"] =  papier.text(x_etiquette , y_etiquette+taille_police/2+2,nb_parts).attr(style_etiquette).hide();// Ecriture du dénominateur
			x+=dx;// On passe à la colonne suivante
		}
		x=x0;// On revient à la marge
		y+=dy;// On passe à la ligne suivante
	}
}
	affiche_etiquettes(); // Affiche ou non les étiquettes (valeurs) sur les fractions en fonction de la Checkbox
	adapte_canvas("grille",dx*nb_colonnes+x0*2,dy*nb_lignes+y0*2);
}
