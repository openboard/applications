# Fractions du quadrillage
Application pour Opens-Sankoré / Openboard 
Représenter une fraction sous forme d'une portion d'un quadrillage

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Mars 2024

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 0.1
affichage d'un quadrillage 

### 0.2
coloration d'une case au clic

### 1.0
Sauvegarde des couleurs des cases pour éviter 
de repartir sur une grille vierge quand on modifie le nombre
de lignes ou de colonnes

### 1.1
Le clic sur une case ayant déjà la couleur de la palette l'efface.

### 1.2
Bouton pour effacer le dessin

### 1.3
Change toutes les cases ayant la couleur de la fraction lors du changement de couleur.

### 1.4
Ajout d'une palette pour les couleurs des lignes.

### 1.5
Simplification du code

### 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Ajout de paramètres
    * Taille des polices utilisées
    * épaisseur des lignes/graduations
    * hauteur des graduations
    * couleurs des nombres, fractions, graduations
    * mode sombre pour un affichage sur tableau noir.
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures

### 3.1
* Portage de l'application vers **Openboard 1.7**
* Gestion de la transparence

## Sources

### Icones :
* Solar Linear Icons [https://www.svgrepo.com/](https://www.svgrepo.com/)

### Bibliothèques :
* Raphael JS [https://dmitrybaranovskiy.github.io/raphael/](https://dmitrybaranovskiy.github.io/raphael/)
* Laktek colorPicker [https://github.com/laktek/really-simple-color-picker](https://github.com/laktek/really-simple-color-picker)
* Jquery 3.7.1 [https://jquery.com/](https://jquery.com/)

