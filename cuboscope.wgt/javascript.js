vue = 1;
souriscliquedroit = false;
pas = 0;
taillemax = 0;
espaceentrecube = 4;
//taille du cube (côté)
cotecube = 25;
cote = cotecube + espaceentrecube;
nombre = "1";
affichedecnombre = false;
tailletexte = 30;
espaceentrecolonne = 10;
base = 10;
opacite = 0.4; //transparence du dessin caré rouge
rangmaxapv = 0;
trier = true;
rangrangement = 0;
taille = 0.4;
decalage_affichage = [];
document.getElementById("taille").value = 0.4;
document.getElementById("rangrangement").value = 0;
sin30 = Math.cos(Math.PI / 6)

function clearCanvas() {
    contextdessin.clearRect(0, 0, dessin.width, dessin.height);
    var w = dessin.width;
    dessin.width = 1;
    dessin.width = w;

}


function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();

    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function changergp1molette() {
    nombre = document.getElementById('nombre').value.toString();
    if ((nombre.indexOf(',') > -1) || (nombre.indexOf('.') > -1)) {

        rangmaxapv = Math.max(nombre.indexOf(','), nombre.indexOf('.')) - nombre.length + 1;
    } else {
        rangmaxapv = 0;
    }
}




dessin = document.getElementById('canvas-dessin');
contextdessin = dessin.getContext('2d');
if (!contextdessin) {
    alert("Impossible de récupérer le context du canvas");

}

function placerplaquenb(x, y, nb, coef) {
    if (coef == undefined) {
        coef = 1;
    }
    taille = Number(document.getElementById("taille").value);

    for (i = 0; i < nb; i++) {
        placerplaque(x + (base - 1) * (cote - espaceentrecube) * sin30 * taille * coef, y - (i - 0.5 - nb) * (cote - espaceentrecube) * taille * coef, coef)
    }
}

function placerplaque(x, y, coef) {
    if (y - 2 * cote * coef < dessin.height) {
        if (coef == undefined) {
            coef = 1;
        }
        taille = Number(document.getElementById("taille").value);
        contextdessin.save();
        contextdessin.translate(x, y);
        var xA = -(cote - espaceentrecube) * taille * coef * (base - 1);
        var yA = base / 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        var xB = -(cote - espaceentrecube) * taille * (base - 1) * coef;
        var yB = -(cote - espaceentrecube) * taille * coef + base / 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        var xC = (cote - espaceentrecube) * sin30 * taille * coef;
        var yC = -(cote - espaceentrecube) * 1.5 * taille * coef;
        var xD = (base + 1) * (cote - espaceentrecube) * sin30 * taille * coef;
        var yD = ((base - 3) / 2) * (cote - espaceentrecube) * taille * coef;
        var xE = (base + 1) * (cote - espaceentrecube) * sin30 * taille * coef;
        var yE = ((base - 1) / 2) * (cote - espaceentrecube) * taille * coef;
        var xF = base * (cote - espaceentrecube) * sin30 * taille * coef - (cote - espaceentrecube) * taille * coef * (base - 1);
        var yF = base * (cote - espaceentrecube) * 0.5 * taille * coef + base / 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        var xG = base * (cote - espaceentrecube) * sin30 * taille * coef - (cote - espaceentrecube) * taille * coef * (base - 1);
        var yG = (base - 2) * (cote - espaceentrecube) * 0.5 * taille * coef + base / 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        contextdessin.beginPath();
        contextdessin.fillStyle = "#eeeeee";
        contextdessin.moveTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#c6c5c7";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#dad9db";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xG, yG);
        contextdessin.fill();
        contextdessin.closePath();

        borne = (10 * coef)
        //borne = (Math.pow(10, rangv % 3) * coef)
        for (var i = 0; i < borne + 1; i++) {
            contextdessin.beginPath();
            contextdessin.strokeStyle = "#a9a8aa";
            contextdessin.moveTo(xA + i * (xF - xA) / borne, yA + i * (yF - yA) / borne);
            contextdessin.lineTo(xB + i * (xG - xB) / borne, yB + i * (yG - yB) / borne);
            contextdessin.lineTo(xC + i * (xD - xC) / borne, yC + i * (yD - yC) / borne);

            contextdessin.moveTo(xF + i * (xE - xF) / borne, yF + i * (yE - yF) / borne);
            contextdessin.lineTo(xG + i * (xD - xG) / borne, yG + i * (yD - yG) / borne);
            contextdessin.lineTo(xB + i * (xC - xB) / borne, yB + i * (yC - yB) / borne);
            contextdessin.stroke();
            contextdessin.closePath();

        }

        borne = (coef)
        for (var i = 0; i < borne + 1; i++) {
            contextdessin.beginPath();
            contextdessin.strokeStyle = "#d1d1d1";
            contextdessin.moveTo(xA + i * (xB - xA) / borne, yA + i * (yB - yA) / borne);
            contextdessin.lineTo(xF + i * (xG - xF) / borne, yF + i * (yG - yF) / borne);
            contextdessin.lineTo(xE + i * (xD - xE) / borne, yE + i * (yD - yE) / borne);

            contextdessin.stroke();
            contextdessin.closePath();
        }

        contextdessin.beginPath();
        contextdessin.strokeStyle = "#a9a8aa";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.stroke();
        contextdessin.closePath();

        contextdessin.beginPath();
        contextdessin.strokeStyle = "#f4f4f4";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.stroke();
        contextdessin.closePath();
        contextdessin.restore();
    }
}

function placerlignenb(x, y, nb, coef) {
    if (coef == undefined) {
        coef = 1;
    }
    taille = Number(document.getElementById("taille").value);
    if (nb < base) {
        for (i = 0; i < nb; i++) {
            placerligne(x - (i - (base - 1)) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
        }
    } else {

        var nbligne = nb % base;
        var nbplaque = Math.floor(((nb) - nbligne) / base);
        for (j = nbplaque; j > 0; j--) {
            for (i = 0; i < base; i++) {

                placerligne(x - (i - (base - 1)) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + 3 + 2 * j) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)

            }
        }
        for (i = 0; i < nbligne; i++) {
            placerligne(x - (i - (base - 1)) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
        }
    }

}

function placerligne(x, y, coef) {
    if (y - 2 * cote * coef < dessin.height) {
        if (coef == undefined) {
            coef = 1;
        }
        taille = Number(document.getElementById("taille").value);
        contextdessin.save();
        contextdessin.translate(x, y);

        var xA = 0;
        var yA = 0;
        var xB = 0;
        var yB = -(cote - espaceentrecube) * taille * coef;
        var xC = (cote - espaceentrecube) * sin30 * taille * coef;
        var yC = -(cote - espaceentrecube) * 1.5 * taille * coef;
        var xD = (base + 1) * (cote - espaceentrecube) * sin30 * taille * coef;
        var yD = (base - 3) / 2 * (cote - espaceentrecube) * taille * coef;
        var xE = (base + 1) * (cote - espaceentrecube) * sin30 * taille * coef;
        var yE = (base - 1) / 2 * (cote - espaceentrecube) * taille * coef;
        var xF = base * (cote - espaceentrecube) * sin30 * taille * coef;
        var yF = base * (cote - espaceentrecube) * 0.5 * taille * coef;
        var xG = base * (cote - espaceentrecube) * sin30 * taille * coef;
        var yG = (base - 2) * (cote - espaceentrecube) * 0.5 * taille * coef;
        contextdessin.beginPath();
        contextdessin.fillStyle = "#eeeeee";
        contextdessin.moveTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#c6c5c7";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#dad9db";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xG, yG);
        contextdessin.fill();
        contextdessin.closePath();
        borne = (10 * coef)
        // borne = (Math.pow(10, rangv % 3) * coef)
        for (var i = 0; i < borne + 1; i++) {
            contextdessin.beginPath();
            contextdessin.strokeStyle = "#a9a8aa";
            contextdessin.moveTo(xA + i * (xF - xA) / borne, yA + i * (yF - yA) / borne);
            contextdessin.lineTo(xB + i * (xG - xB) / borne, yB + i * (yG - yB) / borne);
            contextdessin.lineTo(xC + i * (xD - xC) / borne, yC + i * (yD - yC) / borne);

            contextdessin.stroke();
            contextdessin.closePath();

        }
        borne = (coef)
        for (var i = 0; i < borne + 1; i++) {
            contextdessin.beginPath();
            contextdessin.strokeStyle = "#d1d1d1";
            contextdessin.moveTo(xA + i * (xB - xA) / borne, yA + i * (yB - yA) / borne);
            contextdessin.lineTo(xF + i * (xG - xF) / borne, yF + i * (yG - yF) / borne);
            contextdessin.lineTo(xE + i * (xD - xE) / borne, yE + i * (yD - yE) / borne);
            contextdessin.moveTo(xF + i * (xE - xF) / borne, yF + i * (yE - yF) / borne);
            contextdessin.lineTo(xG + i * (xD - xG) / borne, yG + i * (yD - yG) / borne);
            contextdessin.lineTo(xB + i * (xC - xB) / borne, yB + i * (yC - yB) / borne);
            contextdessin.stroke();
            contextdessin.closePath();
        }
        contextdessin.beginPath();
        contextdessin.strokeStyle = "#a9a8aa";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.stroke();
        contextdessin.closePath();


        contextdessin.beginPath();
        contextdessin.strokeStyle = "#f4f4f4";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.stroke();
        contextdessin.closePath();
        contextdessin.restore();
    }
}

function placercubenb(x, y, nb, coef) {
    if (coef == undefined) {
        coef = 1;
    }
    taille = Number(document.getElementById("taille").value);
    if (nb < base) {
        for (i = 0; i < nb; i++) {

            placercube(x + i * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
        }
    } else //alors on met tous les cubes en un tas
    {
        var nbpetitcube = nb % base;
        var nbligne = Math.floor(((nb) % (base * base) - nbpetitcube) / base);
        var nbplaque = Math.floor((nb - nbpetitcube - nbligne * base) / (base * base));
        if ((nbpetitcube != 0) || (nbligne != 0)) {
            var decalysupp = 2 * nbplaque;
        } else {
            var decalysupp = 2 * nbplaque + 2;
        }
        for (j = 0; j < nbplaque; j++) {
            for (i = 0; i < base; i++) {
                for (k = 0; k < base; k++) {
                    placercube(x + (i - k) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + k - 2 * j + decalysupp + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
                }
            }
        }
        for (i = 0; i < base; i++) {
            for (k = 0; k < nbligne; k++) {
                placercube(x + (i - k) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + k - 2 * nbplaque + decalysupp + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
            }
        }
        for (i = 0; i < nbpetitcube; i++) {
            placercube(x + (i - nbligne) * (cote - espaceentrecube) * sin30 * taille * coef, y + (i + nbligne - 2 * nbplaque + decalysupp + 3) * (cote - espaceentrecube) * 0.5 * taille * coef, coef)
        }
    }
}



function placercube(x, y, coef) {

    if (y - 2 * cote * coef < dessin.height) {
        if (coef == undefined) {
            coef = 1;
        }
        contextdessin.save();
        contextdessin.translate(x, y);
        var xA = 0;
        var yA = 0;
        var xB = 0;
        var yB = -(cote - espaceentrecube) * taille * coef;
        var xC = (cote - espaceentrecube) * sin30 * taille * coef;
        var yC = -(cote - espaceentrecube) * 1.5 * taille * coef;
        var xD = 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        var yD = -(cote - espaceentrecube) * taille * coef;
        var xE = 2 * (cote - espaceentrecube) * sin30 * taille * coef;
        var yE = 0;
        var xF = (cote - espaceentrecube) * sin30 * taille * coef;
        var yF = (cote - espaceentrecube) * 0.5 * taille * coef;
        var xG = (cote - espaceentrecube) * sin30 * taille * coef;
        var yG = -(cote - espaceentrecube) * 0.5 * taille * coef;
        contextdessin.beginPath();
        contextdessin.fillStyle = "#eeeeee";
        contextdessin.moveTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#c6c5c7";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.fill();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.fillStyle = "#dad9db";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xG, yG);
        contextdessin.fill();
        contextdessin.closePath();


        borne = (coef)
        for (var i = 0; i < borne + 1; i++) {
            contextdessin.beginPath();
            contextdessin.strokeStyle = "#d1d1d1";
            contextdessin.moveTo(xA + i * (xF - xA) / borne, yA + i * (yF - yA) / borne);
            contextdessin.lineTo(xB + i * (xG - xB) / borne, yB + i * (yG - yB) / borne);
            contextdessin.lineTo(xC + i * (xD - xC) / borne, yC + i * (yD - yC) / borne);

            contextdessin.moveTo(xF + i * (xE - xF) / borne, yF + i * (yE - yF) / borne);
            contextdessin.lineTo(xG + i * (xD - xG) / borne, yG + i * (yD - yG) / borne);
            contextdessin.lineTo(xB + i * (xC - xB) / borne, yB + i * (yC - yB) / borne);

            contextdessin.moveTo(xA + i * (xB - xA) / borne, yA + i * (yB - yA) / borne);
            contextdessin.lineTo(xF + i * (xG - xF) / borne, yF + i * (yG - yF) / borne);
            contextdessin.lineTo(xE + i * (xD - xE) / borne, yE + i * (yD - yE) / borne);

            contextdessin.stroke();
            contextdessin.closePath();

        }

        contextdessin.beginPath();
        contextdessin.strokeStyle = "#a9a8aa";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.stroke();
        contextdessin.closePath();
        contextdessin.beginPath();
        contextdessin.strokeStyle = "#f4f4f4";
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xB, yB);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xF, yF);
        contextdessin.stroke();
        contextdessin.closePath();
        contextdessin.restore();
    }
}

function placercuberouge(x, y, seul, coef) {
    if (coef == undefined) {
        coef = 1;
    }
    contextdessin.save();
    contextdessin.translate(x, y + 3 * (cote - espaceentrecube) * 0.5 * taille * coef);
    var xA = 0;
    var yA = 0;
    var xB = 0;
    var yB = -(cote - espaceentrecube) * taille * coef;
    var xC = (cote - espaceentrecube) * sin30 * taille * coef;
    var yC = -(cote - espaceentrecube) * 1.5 * taille * coef;
    var xD = 2 * (cote - espaceentrecube) * sin30 * taille * coef;
    var yD = -(cote - espaceentrecube) * taille * coef;
    var xE = 2 * (cote - espaceentrecube) * sin30 * taille * coef;
    var yE = 0;
    var xF = (cote - espaceentrecube) * sin30 * taille * coef;
    var yF = (cote - espaceentrecube) * 0.5 * taille * coef;
    var xG = (cote - espaceentrecube) * sin30 * taille * coef;
    var yG = -(cote - espaceentrecube) * 0.5 * taille * coef;
    contextdessin.beginPath();

    contextdessin.fillStyle = "#ffdddd";
    contextdessin.globalAlpha = opacite;
    contextdessin.moveTo(xB, yB);
    contextdessin.lineTo(xC, yC);
    contextdessin.lineTo(xD, yD);
    contextdessin.lineTo(xG, yG);
    contextdessin.lineTo(xB, yB);
    contextdessin.fill();
    contextdessin.closePath();

    contextdessin.beginPath();
    contextdessin.fillStyle = "#ff8d8e";
    contextdessin.globalAlpha = opacite;
    contextdessin.moveTo(xA, yA);
    contextdessin.lineTo(xB, yB);
    contextdessin.lineTo(xG, yG);
    contextdessin.lineTo(xF, yF);
    contextdessin.lineTo(xA, yA);
    contextdessin.fill();
    contextdessin.closePath();
    contextdessin.beginPath();
    if (seul) {
        contextdessin.fillStyle = "#ffb5b6";
        contextdessin.globalAlpha = opacite;
        contextdessin.moveTo(xG, yG);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xG, yG);
        contextdessin.fill();
        contextdessin.closePath();

        contextdessin.beginPath();
        contextdessin.globalAlpha = opacite;
        contextdessin.strokeStyle = "#a9a8aa";
        contextdessin.moveTo(xA, yA);
        contextdessin.lineTo(xB, yB);
        contextdessin.lineTo(xC, yC);
        contextdessin.lineTo(xD, yD);
        contextdessin.lineTo(xE, yE);
        contextdessin.lineTo(xF, yF);
        contextdessin.lineTo(xA, yA);
        contextdessin.stroke();
    }
    contextdessin.closePath();
    contextdessin.beginPath();
    contextdessin.strokeStyle = "#ffb5b6";
    contextdessin.moveTo(xG, yG);
    contextdessin.lineTo(xB, yB);
    contextdessin.moveTo(xG, yG);
    contextdessin.lineTo(xD, yD);
    contextdessin.moveTo(xG, yG);
    contextdessin.lineTo(xF, yF);
    contextdessin.stroke();
    contextdessin.globalAlpha = opacite;
    contextdessin.closePath();
    contextdessin.globalAlpha = 1;
    contextdessin.restore();
}





function calcultailleauto(longueur) {

    if (trier) {

        var dec = 0;
        for (i = 0; i < longueur; i++) {
            var b = Math.floor(i / 3);
            a = i % 3;
            if (a == 0) {
                a = base + 1;
            } else {
                a = 2 * base + 1;
            }
            dec = dec + a * Math.pow(base, b);
        }
        taille = (dessin.width / (dec * (cote - espaceentrecube) + espaceentrecolonne));

        if (affichedecnombre) {
            a = (longueur - 1) % 3;
            switch (a) {
                case 0:
                    a = base / 2 + 2;
                    break;
                case 1:
                    a = base + 1;
                    break;
                case 2:
                    a = 2 * base + 1;
                    break;

            }
            taille2 = ((dessin.height - 50) / (a * (Math.pow(base, Math.floor((longueur - 1) / 3)) * (cote - espaceentrecube))));
        } else {
            a = (longueur - 1) % 3;
            switch (a) {
                case 0:
                    a = base / 2 + 2;
                    break;
                case 1:
                    a = base + 1;
                    break;
                case 2:
                    a = 2 * base + 1;
                    break;

            }
            taille2 = (dessin.height / (a * (Math.pow(base, Math.floor((longueur - 1) / 3)) * (cote - espaceentrecube))));


        }
        taille = Math.min(taille, taille2);
    } else {

        taille = (dessin.height / ((2 * base + 1) * (Math.pow(base, Math.floor((longueur - 1) / 3)) * (cote - espaceentrecube))));


    }
    document.getElementById("taille").value = taille;

}


function calcultailleautodec(longueur, rangv) {

    var dec = 0;
    for (i = 0; i < longueur; i++) {
        var b = Math.floor((i - rangv) / 3);
        a = (3 + i - rangv) % 3;
        if (a == 0) {
            a = base + 1;
        } else {
            a = 2 * base + 1;
        }
        dec = dec + a * Math.pow(base, b);
    }
    taille = (dessin.width / (dec * (cote - espaceentrecube) + espaceentrecolonne));
    a = (longueur - rangv - 1) % 3;
    switch (a) {
        case 0:
            a = base / 2 + 2;
            break;
        case 1:
            a = base + 1;
            break;
        case 2:
            a = 2 * base + 1;
            break;

    }
    if (affichedecnombre) {
        taille2 = ((dessin.height - 50) / (a * (Math.pow(base, Math.floor((longueur - rangv - 1) / 3)) * (cote - espaceentrecube))));
    } else {
        taille2 = (dessin.height / (a * (Math.pow(base, Math.floor((longueur - rangv - 1) / 3)) * (cote - espaceentrecube))));
    }
    taille = Math.min(taille, taille2);
    document.getElementById("taille").value = taille;

}

function decomposition(nombre) {
    var nb = nombre;
    var tab = [];
    while (nb >= base) {
        tab.push(nb % base);
        nb = Math.floor(nb / base)
    }
    tab.push(nb);
    return tab;
}



function decompositiondec(nombre) {
    var nb = nombre.toString();
    var tab = [];
    var i = nb.length - 1;
    rangv = 0;
    while (i > -1) {
        if ((nb[i] == ',') || (nb[i] == '.')) {
            rangv = nb.length - i - 1;
        } else {
            tab.push(nb[i]);
        }
        i--;
    }
    switch (rangv % 3) {
        case 1:
            //tab.push("0");
            //tab.push("0");
            tab = (["0", "0"]).concat(tab);
            rangv = rangv + 2;
            break;
        case 2:
            //tab.push("0");
            tab = (["0"]).concat(tab);
            rangv = rangv + 1;
            break;
    }
    return [tab, rangv];
}



function dessinerlafiguredec(nombre) {
    var tab = decompositiondec(nombre);

    rangv = tab[1];
    tab = tab[0];
    dessinerlafigure((tab.reverse()).join(''), rangv)
}

function dessinerlafigure(nombre, rangv) {
    if (rangv == undefined) {
        rangv = 0;
    }
    else { console.log(nombre) }
    clearCanvas(dessin);
    var tab = decomposition(nombre);

    longueur = tab.length;
    //calcultailleauto(longueur);
    decalagex = 0;

    for (var i = 0; i < longueur; i++) {

        chiffre = tab[i];
        rang = i;

        var a = rang % 3;
        var coefficient = Math.pow(base, Math.floor(rang / 3));
        var coef = Math.pow(base, rang);
        var chiffre = Math.floor(nombre / coef) % base
        var coeffdecompositionreel = Math.pow(base, rang - rangv);
        var coefficientnm1 = Math.pow(base, rang - 1);
        var chiffrenm1 = Math.floor(nombre / coefficientnm1) % base

        switch (a) {

            case 0:
                //decalagextext = decalagex + 0.5 * coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                //intervalle = coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                if (chiffre != 0) {
                    intervalle = coefficient * (chiffre + 1.5) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                }
                else {
                    if (rang != rangv) {
                        intervalle = coefficient * (1.5) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                    }
                    else {
                        intervalle = coefficient * (2.5) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                    }
                }
                console.log("chiffre" + chiffre);
                decalagex = decalagex + intervalle;
                decalagextext = decalagex - intervalle / 2;
                decalagey = dessin.height - coefficient * base * (cote - espaceentrecube) * taille + espaceentrecolonne;
                if ((affichedecnombre) && ((chiffre != 0))) {
                    placercubenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.font = (Math.min(intervalle / (5 + rang), 50)) + 'px serif';
                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";
                    //contextdessin.textAlign = "left";

                    chaine = chiffre.toString() + "×" + coef.toString();
                    if (rang - rangv < 0) {
                        var coeffdecompositionreel2 = Math.pow(base, rangv - rang);

                        chaine = chiffre.toString() + "/" + coeffdecompositionreel2.toString();
                    } else {
                        chaine = chiffre.toString() + "×" + coeffdecompositionreel.toString();
                    }
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                    if (rang == rangv) //affichage unité en rouge très léger
                    {
                        if (chiffre == 0) {
                            placercuberouge(dessin.width - decalagex, 60, true, coefficient);
                        } else {
                            placercuberouge(dessin.width - decalagex, 60, false, coefficient);
                        }
                    }

                } else {

                    placercubenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
                    if (rang == rangv) //affichage unité en rouge très léger
                    {
                        if (chiffre == 0) {
                            placercuberouge(dessin.width - decalagex, 10, true, coefficient);
                        } else {
                            placercuberouge(dessin.width - decalagex, 10, false, coefficient);
                        }
                    }
                }

                break;
            case 1:
                decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                //intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                intervalle = coefficient * (base + base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                if ((chiffre != 0)) {
                    decalagex = decalagex + intervalle;
                }
                decalagextext = decalagex - intervalle / 2;
                if ((affichedecnombre) && ((chiffre != 0))) {
                    placerlignenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.font = (Math.min(intervalle / (5 + rang), 50)) + 'px serif';
                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";
                    contextdessin.textAlign = "left";
                    chaine = chiffre.toString() + "×" + coef.toString();
                    if (rang - rangv < 0) {
                        var coeffdecompositionreel2 = Math.pow(base, rangv - rang);

                        chaine = chiffre.toString() + "/" + coeffdecompositionreel2.toString();
                    } else {
                        chaine = chiffre.toString() + "×" + coeffdecompositionreel.toString();
                    }
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                } else {
                    placerlignenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
                }
                break;
            case 2:
                if ((chiffrenm1 == 0)) {
                    decalagex = decalagex + intervalle;
                }
                decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                //intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * taille + espaceentrecolonne;
                if (chiffrenm1 != "0") {
                    intervalle = coefficient * (chiffrenm1 + base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                }
                else {
                    intervalle = coefficient * (1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                }
                if ((chiffre != 0)) {
                    decalagex = decalagex + intervalle;
                }

                decalagextext = decalagex - (coefficient * (2 * base + 1) * (cote - espaceentrecube) * taille + espaceentrecolonne) / 2;
                if ((affichedecnombre) && ((chiffre != 0))) {
                    placerplaquenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.font = (Math.min((coefficient * (2 * base + 1) * (cote - espaceentrecube) * taille + espaceentrecolonne) / (5 + rang), 50)) + 'px serif';
                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";
                    //contextdessin.textAlign = "left";
                    chaine = chiffre.toString() + "×" + coef.toString();
                    if (rang - rangv < 0) {
                        var coeffdecompositionreel2 = Math.pow(base, rangv - rang);

                        chaine = chiffre.toString() + "/" + coeffdecompositionreel2.toString();
                    } else {
                        chaine = chiffre.toString() + "×" + coeffdecompositionreel.toString();
                    }
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                } else {
                    placerplaquenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
                }
                break;
        }
    }
    if (decalagex > dessin.width) {
        document.getElementById("tailletroppetite").style.display = "";
    } else {
        document.getElementById("tailletroppetite").style.display = "none";

    }
}





document.getElementById("canvas-dessin").focus();
changergp1molette();
resizeCanvas();

function rafraichit() {
    nombre = document.getElementById('nombre').value.toString();
    base = Number(document.getElementById('base').value);
    if ((nombre.indexOf(',') > -1) || (nombre.indexOf('.') > -1)) { //c'est un nombre décimal
        document.getElementById('base').value = 10;
        dessinerlafiguredec(nombre);
        document.getElementById('triage').style.display = "none";
        document.getElementById('boutontas').style.display = "none";
    } else {

        document.getElementById('boutontas').style.display = "";



        if (trier) {
            document.getElementById('triage').style.display = "none";
            dessinerlafigure(nombre)
        } else {
            document.getElementById('triage').style.display = "";
            malrange(nombre, rangrangement);
        }
    }

}



window.addEventListener('resize', resizeCanvas, false);


function resizeCanvas() {
    var largeur_contenant = document.body.offsetWidth;
    var hauteur_contenant = document.body.offsetHeight;
    largeur_contenu = largeur_contenant - 130;
    hauteur_contenu = hauteur_contenant;

    dessin.width = largeur_contenu;
    dessin.height = hauteur_contenu;
    rafraichit()

}


if (dessin.addEventListener)
    dessin.addEventListener('DOMMouseScroll', wheel, false);
dessin.onmousewheel = dessin.onmousewheel = wheel;


/** activation de la molette */
function handle_av(delta) {
    if (delta < 0) {
        nombre = document.getElementById('nombre').value;
        nombrech = document.getElementById('nombre').value.toString();
        if (nombre > 0)
            document.getElementById('nombre').value = Number(nombre) - Math.pow(10, rangmaxapv);


    } else {
        nombre = document.getElementById('nombre').value;
        nombrech = document.getElementById('nombre').value.toString();

        document.getElementById('nombre').value = Number(nombre) + Math.pow(10, rangmaxapv);

    }
    rafraichit();

}
window.addEventListener('keydown', touche, false);

function touche(evt) {
    if (evt.key == "ArrowRight") {
        taille = document.getElementById('taille').value;

        if (taille > 0.02) {
            document.getElementById('taille').value = Number(taille) - 0.02
        }
        rafraichit();

    }
    if (evt.key == "ArrowLeft") {
        document.getElementById('taille').value = Number(taille) + 0.02
        rafraichit();

    }

}

function handle(delta) {
    if (delta < 0) {
        taille = document.getElementById('taille').value;

        if (taille > 0.02)
            document.getElementById('taille').value = Number(taille) - 0.02


    } else {
        document.getElementById('taille').value = Number(taille) + 0.02

    }
    rafraichit();

}

function gererangtropeleve(val) {
    nb = document.getElementById('nombre').value;
    rangmax = Math.floor(Math.log(nb) / Math.log(base));
    if (val > rangmax) {
        val = rangmax;
    }
    rangrangement = val;
    document.getElementById('rangrangement').value = rangrangement
}

function wheel(event) {
    var delta = 0;
    if (!event) event = window.event;
    if (event.wheelDelta) {
        delta = event.wheelDelta / 120;
    } else if (event.detail) {
        delta = -event.detail / 3;
    }
    if (delta)
        handle(delta);
    if (event.preventDefault)
        event.preventDefault();
    event.returnValue = false;
}

function decompositionmalrange(nombre, rangerapartirde) {
    var tab = decomposition(nombre);
    var tabretour = []
    for (i = 0; i < rangerapartirde; i++) {
        tabretour.push(tab[i])
    }
    tas = 0;
    for (i = rangerapartirde; i < tab.length; i++) {
        tas = tas + Math.pow(base, i - rangerapartirde) * tab[i];
    }
    tabretour.push(tas);
    return tabretour

}

function affichagetriage() {
    if (trier) {
        document.getElementById("boutontas").value = "Rassembler";
        document.getElementById("triage").style.display = "none";
    } else {
        document.getElementById('rangrangement').value = 0;
        document.getElementById("boutontas").value = "Décomposer";
        document.getElementById("triage").style.display = "";
    }
}

function malrange(nombre, rangerapartirde) {
    clearCanvas(dessin);
    var tab = decompositionmalrange(nombre, rangerapartirde);
    longueur = tab.length;
    //calcultailleauto(longueur);
    decalagex = 0;


    //rangement décomposé de la première partie

    for (var i = 0; i < rangerapartirde; i++) {
        chiffre = tab[i];
        rang = i;

        var a = rang % 3;
        var coefficient = Math.pow(base, Math.floor(rang / 3));
        var coef = Math.pow(base, rang);

        switch (a) {

            case 0:

                decalagextext = decalagex + 0.5 * coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                intervalle = coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                decalagex = decalagex + intervalle;

                decalagey = dessin.height - coefficient * base * (cote - espaceentrecube) * taille + espaceentrecolonne;
                if (affichedecnombre) {
                    placercubenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.beginPath()
                    contextdessin.font = (Math.min(intervalle / (4 + rang), 50)) + 'px serif';

                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";

                    chaine = chiffre.toString() + "×" + coef.toString();
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                    contextdessin.closePath();
                } else {

                    placercubenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);

                }

                break;
            case 1:
                decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                decalagex = decalagex + intervalle;
                if (affichedecnombre) {
                    placerlignenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.beginPath()
                    contextdessin.font = (Math.min(intervalle / (4 + rang), 50)) + 'px serif';
                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";
                    chaine = chiffre.toString() + "×" + coef.toString();
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                    contextdessin.closePath();
                } else {
                    placerlignenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
                }
                break;
            case 2:
                decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
                intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * taille + espaceentrecolonne;
                decalagex = decalagex + intervalle;
                if (affichedecnombre) {
                    placerplaquenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                    contextdessin.beginPath()
                    contextdessin.font = (Math.min(intervalle / (4 + rang), 50)) + 'px serif';
                    contextdessin.textBaseline = "top";
                    contextdessin.textAlign = "center";
                    chaine = chiffre.toString() + "×" + coef.toString();
                    contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
                    contextdessin.closePath();
                } else {
                    placerplaquenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
                }
                break;
        }
    }
    // rangement en tas de la seconde partie du nombre

    chiffre = tab[rangerapartirde];
    rang = rangerapartirde;
    var a = rang % 3;
    var coefficient = Math.pow(base, Math.floor(rang / 3));
    var coef = Math.pow(base, rang);

    switch (a) {

        case 0:
            decalagextext = decalagex + 0.5 * coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
            intervalle = coefficient * (base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
            decalagex = decalagex + intervalle;

            decalagey = dessin.height - coefficient * base * (cote - espaceentrecube) * taille + espaceentrecolonne;
            if (affichedecnombre) {
                if (rang < 0) {
                    var coef2 = Math.pow(base, -rang);

                    chaine = chiffre.toString() + "/" + coef2.toString();
                } else {
                    chaine = chiffre.toString() + "×" + coef.toString();
                }

                placercubenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                contextdessin.font = '20px serif';
                longueurtexte = contextdessin.measureText(chaine).width;

                contextdessin.font = (Math.min(20 * intervalle / longueurtexte, 50)) + 'px serif';


                //alert(intervalle+" "+chiffre)
                contextdessin.textBaseline = "top";
                contextdessin.textAlign = "center";


                contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
            } else {
                placercubenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
            }

            break;
        case 1:
            decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
            intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
            decalagex = decalagex + intervalle;
            if (affichedecnombre) {
                placerlignenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);
                contextdessin.textBaseline = "top";
                contextdessin.textAlign = "center";
                if (rang < 0) {
                    var coef2 = Math.pow(base, -rang);

                    chaine = chiffre.toString() + "/" + coef2.toString();
                } else {
                    chaine = chiffre.toString() + "×" + coef.toString();
                }
                contextdessin.font = '20px serif';
                longueurtexte = contextdessin.measureText(chaine).width;
                contextdessin.font = (Math.min(20 * intervalle / longueurtexte, 50)) + 'px serif';
                contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
            } else {
                placerlignenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);

            }
            break;
        case 2:
            decalagextext = decalagex + 0.5 * coefficient * (2 * base + 1) * (cote - espaceentrecube) * sin30 * taille + espaceentrecolonne;
            intervalle = coefficient * (2 * base + 1) * (cote - espaceentrecube) * taille + espaceentrecolonne;
            decalagex = decalagex + intervalle;
            if (affichedecnombre) {
                placerplaquenb(dessin.width - decalagex, 60, Number(chiffre), coefficient);

                contextdessin.textBaseline = "top";
                contextdessin.textAlign = "center";
                if (rang < 0) {
                    var coef2 = Math.pow(base, -rang);

                    chaine = chiffre.toString() + "/" + coef2.toString();
                } else {
                    chaine = chiffre.toString() + "×" + coef.toString();
                }
                contextdessin.font = '20px serif';
                longueurtexte = contextdessin.measureText(chaine).width;
                contextdessin.font = (Math.min(20 * intervalle / longueurtexte, 50)) + 'px serif';
                contextdessin.fillText(chaine, dessin.width - decalagextext, 5);
            } else {
                placerplaquenb(dessin.width - decalagex, 10, Number(chiffre), coefficient);
            }
            break;
    }
    if (decalagex > dessin.width) {
        document.getElementById("tailletroppetite").style.display = "";
    } else {
        document.getElementById("tailletroppetite").style.display = "none";

    }
}
