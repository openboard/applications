# Cuboscope
Application pour représenter la décomposition décimale d’un nombre entier.
## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Aout 2021
## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditez les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions.

## Versions
Version 10
version initiale

## Sources
* Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
