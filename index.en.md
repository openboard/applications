# ![](favicon.ico) Openboard applications repository

[ [Français](index.md) | [English](index.en.md) ]

A bundle of applications for the interactive whiteboard software called Openboard.

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;"> Mise à disposition sur la
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px black solid;">
    <img src="https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/logo_forge.svg" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>

## Background

The OpenBoard version 1.7 comes with some new features. But the applications must be updated, have a look at this [guide](https://github.com/OpenBoard-org/OpenBoard/wiki/How-to-update-a-widget-to-the-new-API).

The aim of this project is to share, maintain and adapt some useful applications to support OpenBoard >= 1.7.

The eventual modifications applied to the original applications are:
- Support of Openbaord 1.7 and superior (with the new web engine based on Chromium accordingly to this [guide](https://github.com/OpenBoard-org/OpenBoard/wiki/How-to-update-a-widget-to-the-new-API) )
- Add the persistence of the parameters. If we open an old document with an application, this application should be display in the same way. So we have to store and restore the application parameters in the document file. This feature was not implemented for every applications.
- Wrap HTML5 free applications in OpenBoard format.

## Download and install applications


### Automatic installers

<div class="boutons">

[Windows installer](https://forge.apps.education.fr/openboard/applications/-/jobs/226195/artifacts/file/openboard-applications-v0.1.9.0591a967.exe)

[Linux package (DEB)](https://forge.apps.education.fr/openboard/applications/-/jobs/226195/artifacts/file/openboard-applications_0.1.9.0591a967_all.deb)

</div>

### Manual installation

Follow the [wiki page](https://forge.apps.education.fr/openboard/applications/-/wikis/home/Comment-t%C3%A9l%C3%A9charger-et-installer-les-applications-%3F).


## Applications list

| Names         | Descriptions          | OB < 1.7      | OB >= 1.7     | Licenses      | Authors       | Sources |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- |
| [Jeu_de_Des](Jeu_de_Des.wgt/) | Throw of the dice. | [x] | [x] | CC BY-NC-SA          | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [Etiquettes](Etiquettes.wgt/) | Generate words labels. | [ ]  | [x]           | CC BY-NC-SA   | François Le Cléac'h | [ici](https://www.openedu.fr/openboard/applications-pour-openboard/) |
| [Num_Etiquettes](Num_Etiquettes.wgt/) | Generate numbers words labels in french language. | [ ]  | [x]           | CC BY-NC-SA   | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [compte_est_bon](compte_est_bon.wgt/) | Game to compute the target number. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [fraction](fraction.wgt/) | Show the mathematics representation of a fraction. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [fraction_bande](fraction_bande.wgt/) | Show the fraction of a horizontal band. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [fraction_quadrillage](fraction_quadrillage.wgt/) | Show a grid view of a fraction. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [fraction_disque](fraction_disque.wgt/) | Show a disc view of a fraction. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [droite_graduee](droite_graduee.wgt/) | Shows an axis. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [droite_fraction](droite_fraction.wgt/) | Shows an axis with fractions. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [here](https://www.openedu.fr/open-sankore/applications-pour-sankore/) |
| [sonometre](sonometre.wgt/) | Measure the sound level. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/openboard/applications-pour-openboard/) and [ici](https://www.openedu.fr/2024/06/26/sonometre/) |
| [cuboscope](cuboscope.wgt/) | Show decimal decomposition of an integer. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [here](https://www.mathix.org/cuboscope/) |
| [mathadorix](mathadorix.wgt/) | Similar to compte_est_bon. | [ ] | [x] | GPLv3        | Arnaud DURAND | [here](https://mathix.org/mathador) |
| [glisse-nombre](glisse-nombre.wgt/) | Tool for x or : by 10, 100, 1000, and so on. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [here](https://mathix.org/glisse-nombre/index.html) and [here](https://mathix.org/glisse-nombre/glisse-nombre.zip)|
| [division_assistee](division_assistee.wgt/) | Division practice | [ ] | [x] | GPLv2 | Arnaud DURAND | [here](https://www.mathix.org/division_assistee/) and [here](https://mathix.org/linux/archives/13371)|
| [carre-magique](carre-magique.wgt/) | Magic square generator | [ ] | [x] | CC BY-NC-SA v3 | Arnaud DURAND | [here](https://mathix.org/linux/archives/11258) and [here](https://mathix.org/carre-magique/)|
| [puzzle-zukei](puzzle-zukei.wgt/) | Puzzles by Naoki Inaba. | [x] | [x] | GPL v2      | Arnaud DURAND | [here](https://mathix.org/puzzle-zukei/) and [here](https://mathix.org/linux/archives/9444)|
| [nombre_en_lettres](nombre_en_lettres.wgt/) | Write number with letters. | [ ] | [x]  | GPL v2        | Arnaud DURAND | [here](https://www.mathix.org/nombre_en_lettres/) and [here](https://mathix.org/linux/archives/9938)|
| [rullo_somme](rullo_somme.wgt/) | Game of numbers addition. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://mathix.org/rullo_somme/) and [ici](https://mathix.org/linux/archives/11000)|
| [rullo_produit](rullo_produit.wgt/) | Game on divisibility rules. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://mathix.org/rullo_produit/) and [ici](https://mathix.org/linux/archives/11000)|
| [labynombre](labynombre.wgt/) | Game of numbers comparison. | [x] | [x] | GPL v2 | Arnaud DURAND | [ici](https://www.mathix.org/labynombre/) and [ici](https://mathix.org/linux/archives/13224)|
| [conversion](conversion.wgt/) | Conversion table of measures. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://www.mathix.org/conversion/) et [ici](https://mathix.org/linux/archives/11334)|
| [conversions_sesamath](conversions_sesamath.wgt/) | Tables of numbering system and conversion. | [x] | [x] | AGPL 3 | Thomas CRESPIN | [ici](https://sesaprof.sesamath.net/doc/outils_tableaux/index.html) and [ici](https://sesaprof.sesamath.net/pages/prof_divers_outils.php)|
| [Seyes](Seyes.wgt/) | Display and write in a Seyes notebook page. | [ ] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/seyes) and [ici](https://educajou.forge.apps.education.fr/seyes/)|
| [Syllabux](Syllabux.wgt/) | Display french syllables. | [ ] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://educajou.forge.apps.education.fr/syllabux/) and [ici](https://forge.apps.education.fr/educajou/syllabux)|
| [Tuxtimer](Tuxtimer.wgt/) | A graphic timer. | [x] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/tuxtimer) and [ici](https://educajou.forge.apps.education.fr/tuxtimer/)|
| [automultiples](automultiples.wgt/) | Show the multiples of a number. | [ ] | [x] | GPLv3 | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/automultiples) and [ici](https://educajou.forge.apps.education.fr/automultiples/)|
| [compteur](compteur.wgt/) | Counter to add/sub units, tens, hundreds, tenth, ... | [ ] | [x] | GPLv3 | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/compteur) et [ici](https://educajou.forge.apps.education.fr/compteur/)|
| [horloge-ecole](horloge-ecole.wgt/) | An interactive clock. | [x] | [x] | GPLv3 | Thierry MUNOZ | [ici](https://forge.aeif.fr/ThierryM/horloge-ecole) and [ici](https://thierrym.forge.aeif.fr/horloge-ecole/)|
| [babybot](babybot.wgt/) | Code initiation with a bee-bot simulator. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)|
| [Bee bot défi 30 fleurs](beebot-30-fleurs.wgt/) | Bee-bot - mid-level coding. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)|
| [Bee bot entraînement](beebot-entrainement.wgt/) | Bee-bot - free training. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)|
| [spotlight](spotlight.wgt/) | To be focus on some parts of the screen. | [x] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/spotlight/) et [ici](https://educajou.forge.apps.education.fr/spotlight)|
| [fonds_seyes](https://github.com/OpenBoard-org/OpenBoard/issues/640) | Enable the parameter in order to show Seyes background. Available only in the [Windows installer](https://forge.apps.education.fr/openboard/applications/-/jobs/226195/artifacts/file/openboard-applications-v0.1.9.0591a967.exe).| [ ] | [x] | N/A | N/A | [ici](https://github.com/OpenBoard-org/OpenBoard/issues/640)|
| [sans_plein_ecran](https://github.com/OpenBoard-org/OpenBoard/wiki/Parameters#app-section) | Enable the window mode of OB. Available only in the [Windows installer](https://forge.apps.education.fr/openboard/applications/-/jobs/226195/artifacts/file/openboard-applications-v0.1.9.0591a967.exe).| [ ] | [x] | N/A | N/A | [ici](https://github.com/OpenBoard-org/OpenBoard/wiki/Parameters#app-section)|

## Applications documentation list

(This list is compiled automatically.)

``` {.include}
docs.html
```
