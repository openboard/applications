# Sonomètre
Application pour Openboard

Mesurer le volume sonore.

## Auteur
François Le Cléac'h https://openedu.fr
Juin 2024

## Contributeur
Sébastien Vallet

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 1.0
* Mesure du bruit environnant.

### 1.1
* Gestion des langues.
* Sauvegarde des paramètres du sonomètre.

### 1.2
* Sensibilité minimale à 1 pour éviter de couper le micro.
* Suppression de l'API obsolète ```createScriptProcessor``` et diminution de la charge du CPU.

## Sources

### Icones :
* VU Alex Wendpap https://www.flaticon.com/fr/
* Solar Linear Icons https://www.svgrepo.com/

