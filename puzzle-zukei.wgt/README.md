# Puzzle Zukkei
Retrouver des figures géométriques dans les puzzles de Naoki Inaba.

## Auteur
Arnaud DURAND [https://www.mathix.org](https://www.mathix.org)
Aout 2018

## Licence
GPL v2

## Versions
Version 1
version initiale

## Sources
* Arnaud DURAND [https://mathix.org/puzzle-zukei/](https://mathix.org/puzzle-zukei/) et [https://mathix.org/linux/archives/9444](https://mathix.org/linux/archives/9444)
