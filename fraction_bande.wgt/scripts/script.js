var	langue_defaut="fr",syslang=langue_defaut,
	morceau_bande = new Array,// Tableau conservant les couleurs des morceaux
	lbl=new Array,//Etiquettes
	mode_defaut='edition',mode=mode_defaut,//Mode d'affichage
	margex=40,margey=40; // épaisseur de la fenêtre sankore
	longueur_bande=500, longueur_min=100,longueur_max=1300,pas=100,// Hauteur et longueur de la bande unité, longeur min et max
	hauteur_bande=100,hauteur_min=50,hauteur_max=800,hauteur_pas=50,// Hauteur et longueur de la bande unité, longeur min et max
	epaisseur_ligne=3,epaisseur_ligne_min=1,epaisseur_ligne_max=5, // Epaisseur du contour
	nb_parts=1,//Nombre de morceaux dans la bande, modifié par la suite
	nb_parts_min=1,nb_parts_max=100, // Nombre de morceaux maximum dans la bande
	papier = Raphael("bande","100%","100%"),// Occupe tout le div
	taille_police=14, taille_police_max=40,taille_police_min=6, epaisseur_barre=2,
	// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	//Couleur des éléments
	couleur_fraction="#"+rouge, couleur_ligne="#"+noir,couleur_etiquette="#"+noir,couleur_fond="#"+blanc,
	// Styles des étiquettes
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_etiquette},
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};

// #####################################
// ##### INITIALISATIONS
// #####################################
function init_reglages(){
	//Curseurs
	// Nombre de parts d'une unité
	$( "#parts" ).attr({
	       "max" : nb_parts_max,
	       "min" : nb_parts_min,
		"value":nb_parts,
		"step":1
    });
	// Dimensions de la bande
	$( "#longueur_bande" ).attr({
	       "max" : longueur_max,
	       "min" : longueur_min,
		"value":longueur_bande,
		"step":pas
    });
	$( "#hauteur_bande" ).attr({
	       "max" : hauteur_max,
	       "min" : hauteur_min,
		"value":hauteur_bande,
		"step":hauteur_pas
    });
	$( "#epaisseur_ligne" ).attr({
	       "max" : epaisseur_ligne_max,
	       "min" : epaisseur_ligne_min,
		"value":epaisseur_ligne,
		"step":1
    });
	// Police de caractère
	$( "#taille_police" ).attr({
	       "max" : taille_police_max,
	       "min" : taille_police_min,
		"value":taille_police,
		"step":1
    });
    //Mise à jour des paramètres du Widget    	
	$("#parts").next('label').text(nb_parts);// Affiche la valeur initiale du nombre de parts
	$("#longueur_bande").next('label').text(longueur_bande);// Affiche la valeur initiale du nombre de parts
	$("#hauteur_bande").next('label').text(hauteur_bande);// Affiche la valeur initiale du nombre de parts
	$("#epaisseur_ligne").next('label').text(epaisseur_ligne);// Affiche la taille de police initiale
	$("#taille_police").next('label').text(taille_police);// Affiche la taille de police initiale
	$("#fraction").val(couleur_fraction).change();// Adaptation du colorpicker
	$("#couleur_ligne").val(couleur_ligne).change();// Adaptation du colorpicker
}
function init_palette() {
//Initialisation des palettes de couleurs
	// Palette pour les morceaux
	$('#fraction').colorPicker({pickerDefault: couleur_fraction, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fraction=newValue;bande(nb_parts);}});
	// Palette pour les lignes
	$('#couleur_ligne').colorPicker({pickerDefault: couleur_ligne, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_ligne=newValue;bande(nb_parts);}});	
	// Palette pour le fond
	$('#couleur_fond').colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fond=newValue;bande(nb_parts);}});	
	// Palette pour les étiquettes
	$('#etiquette').colorPicker({pickerDefault: couleur_etiquette,colors: palette_base,showHexField: false,onColorChange : function(id, newValue) { couleur_etiquette=newValue; bande(nb_parts);}}); //Couleur des nombres de la fraction
}
function maj_palette() {
// Sélection de la couleur active pour l'aperçu
	$("#couleur_ligne").val(couleur_ligne).change();// Couleur active
	$("#couleur_fond").val(couleur_fond).change();// Couleur active
	$("#fraction").val(couleur_fraction).change();// Couleur active
	$("#etiquette").val(couleur_etiquette).change();// Couleur active
// Définition des styles
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_etiquette};
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
}

function init_lang(){
	 // Traduction de l'interface
	$('#reglages').attr('title',sankoreLang[syslang].Reglages);
	$('#txt_parts').text(sankoreLang[syslang].Parts);
	$('#txt_bande').text(sankoreLang[syslang].Bande);
	$('#txt_fond').text(sankoreLang[syslang].Fond);
	$('#txt_longueur').text(sankoreLang[syslang].Longueur);
	$('#txt_hauteur').text(sankoreLang[syslang].Hauteur);
	$('#txt_epaisseur').text(sankoreLang[syslang].Epaisseur);
	$('#txt_etiquettes').text(sankoreLang[syslang].Etiquettes);
	$('#txt_taille').text(sankoreLang[syslang].Taille);
	 
	  $('#infos').attr('title',sankoreLang[syslang].Infos);
	  $('#infos').html(sankoreLang[syslang].Txt_infos);
	}
async function init() {
	//Initialisation des couleurs de la bande (vide par défaut)
	for (var k=0; k < nb_parts_max; k++){morceau_bande[k]=(false);}
	if (window.sankore) {
	// Arrivée sur le widget 
	window.widget.onenter.connect(() => {$('#bouton_reglages').show();});// Affiche le bouton des paramètres
	// Sortie du Widget
	window.widget.onleave.connect(() => { 
		$('#bouton_reglages').hide();// Cache le bouton
		//Sauvegarde des paramètres au format 'chaine de caractères'
		window.sankore.setPreference('Nombre de parts', nb_parts);
		window.sankore.setPreference('longueur_bande', longueur_bande);
		window.sankore.setPreference('hauteur_bande', hauteur_bande);
		window.sankore.setPreference('Couleur fraction', couleur_fraction);
		window.sankore.setPreference('Couleur ligne', couleur_ligne);
		window.sankore.setPreference('Epaisseur ligne', epaisseur_ligne);
		window.sankore.setPreference('Couleur etiquette', couleur_etiquette);
		window.sankore.setPreference('Couleur fond', couleur_fond);
		window.sankore.setPreference('Etiquettes',$("#etiquettes").prop('checked'));//Affichage des étiquettes
		window.sankore.setPreference('Taille Police', taille_police);
		window.sankore.setPreference('mode', mode);
		window.sankore.setPreference('Selection', JSON.stringify(morceau_bande));// Références des cases coloriées		
	});		
	//Détection de la langue
	try{
		syslang = window.sankore.lang.substr(0,2);
		sankoreLang[syslang].search;
	} catch(e){
		syslang = langue_defaut;
                }
	// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('Nombre de parts')) {
		nb_parts=parseInt(await window.sankore.async.preference('Nombre de parts'));
		longueur_bande=parseInt(await window.sankore.async.preference('longueur_bande'));
		hauteur_bande=parseInt(await window.sankore.async.preference('hauteur_bande'));
		couleur_fraction=await window.sankore.async.preference('Couleur fraction');
		couleur_ligne=await window.sankore.async.preference('Couleur ligne');
		epaisseur_ligne=await window.sankore.async.preference('Epaisseur ligne');
		couleur_etiquette=await window.sankore.async.preference('Couleur etiquette');
		couleur_fond=await window.sankore.async.preference('Couleur fond');
		$("#etiquettes").prop('checked',JSON.parse(await window.sankore.async.preference('Etiquettes')));//Valeur logique stockée sous forme de chaîne, donc à convertir en boolean
		taille_police=await window.sankore.async.preference('Taille Police');
		mode=await window.sankore.async.preference('mode');
		morceau_bande=JSON.parse(await window.sankore.async.preference('Selection'));// Découpage de la chaîne en valeurs du tableau
		}
	}
	init_lang();//Traduction de l'interface
	init_reglages();//Mise à jour des paramètres
	init_palette(); //Initialisation des palettes
	// Boite de dialogue des paramètres
	$( "#reglages" ).dialog({
	autoOpen: false,
	position: {my: "left top", at: "left bottom", of: '#widget'}, 
	beforeClose: function() {mode='vue'},
	close: function() {
		mode_affichage(mode);
		}
	});
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'left top', at: 'center center', of: '#widget'}
	});
	//Trace la bande partagée
	mode_affichage(mode);// Mode d'affichage
	bande(nb_parts);
}
// #####################################
// ##### GESTION DES ÉVÉNEMENTS
// #####################################
// Un clic sur le bouton fait apparaître la boite de dialogue des paramètres
$("#bouton_reglages").click(function(){
	mode_affichage('edition');
});
// Affichage des infos
$('#bouton_infos').click(function(){
	$('#infos').dialog('open');
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul1,coul2;
	$('#widget').toggleClass('mode_sombre');
	if ($('#widget').hasClass('mode_sombre')) {
		coul1='#'+blanc;
		coul2='#'+noir;
		} else {
		coul1='#'+noir;
		coul2='#'+blanc;
	};
	couleur_ligne=coul1;
	couleur_etiquette=coul1;
	couleur_fond=coul2;
	bande(nb_parts);
	});

// Affichage des étiquettes
$('#etiquettes').click(function(){
	affiche_etiquettes();
	});
// Réactions aux déplacements des curseurs
	// Partage de la bande
$('#parts').mousemove(function(){
	nb_parts=parseInt(this.value);// recupere la valeur du curseur
		$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	bande(nb_parts);
	});
	// Dimensions de la bande
$('#longueur_bande').mousemove(function(){
		longueur_bande=parseInt(this.value);// recupere la valeur du curseur
		$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
		widgetX=Math.max(longueur_bande,$('#reglages').width()),widgetY=Math.max(window.widget.height,$('#reglages').height());
		redimensionne(widgetX,widgetY);
		bande(nb_parts);
	});
$('#hauteur_bande').mousemove(function(){
		hauteur_bande=parseInt(this.value);// recupere la valeur du curseur
		$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
		widgetX=longueur_bande,widgetY=Math.max(window.widget.height,$('#reglages').height());
		redimensionne(widgetX,widgetY);
		bande(nb_parts);
	});
$('#epaisseur_ligne').mousemove(function(){
		epaisseur_ligne=parseInt(this.value);// recupere la valeur du curseur
		$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
		widgetX=longueur_bande,widgetY=Math.max(window.widget.height,$('#reglages').height());
		redimensionne(widgetX,widgetY);
		bande(nb_parts);
	});
	// Taille de la police
$('#taille_police').mousemove(function(){
		taille_police=parseInt(this.value);// recupere la valeur du curseur
		style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
		$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne 
		widgetX=longueur_bande,widgetY=Math.max(window.widget.height,$('#reglages').height());
		redimensionne(widgetX,widgetY);
		bande(nb_parts);
	});
// #####################################
// ##### MODE AFFICHAGE
// #####################################
function mode_affichage(style_affichage){
	mode=style_affichage;
	// ##### MODE VUE #####
	if (mode=="vue") {
		$("#bouton_reglages").hide();
		// On cache les réglages
		$( "#reglages" ).dialog( "close" );//Ouverture des paramètres
		adapte_canvas("bande",longueur_bande,hauteur_bande);
		var widgetX=$("#bande").width(),widgetY=$("#bande").height();
		redimensionne(widgetX,widgetY);
	};
	// ##### MODE EDITION #####
	if (mode=="edition") {
		// On affiche les réglages
		adapte_canvas("bande",longueur_bande,hauteur_bande);
		var widgetX=50+Math.max($("#bande").width(),$("#reglages").width()),widgetY=Math.max(window.widget.height,$("#bande").height());
		redimensionne(widgetX,widgetY);
		$( "#reglages" ).css( "display","" );//Affichage des paramètres
		$( "#reglages" ).dialog( "option", "position", { my: "left top", at: "left bottom", of:"#widget" } );
		$( "#reglages" ).dialog( "open" );//Ouverture des paramètres
	};
}
// #####################################
// ##### MODIFICATION DE LA COULEUR
// #####################################
function couleur(couleur_part) {
	if (couleur_part) {return couleur_fraction;} else {return couleur_fond;};
}
// #####################################
// ##### AFFICHAGE / MASQUAGE DES ETIQUETTES
// #####################################
function affiche_etiquettes() {
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
// Affichage de l'étiquette sous la forme a/b)	
	if ($("#etiquettes").prop('checked')) {
		for	(var cpt=0;cpt<nb_parts;cpt++){ // Boucle sur le nombre de morceaux
			if (nb_parts==1) {
				lbl[cpt+"a"].show();
			}
			else {
				lbl[cpt+"a"].show();lbl[cpt+"-"].show();lbl[cpt+"b"].show();
			}
		}
	}
else  {
	for	(var cpt=0;cpt<nb_parts;cpt++){ // Boucle sur le nombre de morceaux
		if (nb_parts==1) {
			lbl[cpt+"a"].hide();
		} 
		else{
			lbl[cpt+"a"].hide();lbl[cpt+"-"].hide();lbl[cpt+"b"].hide()};
		}
	}
}
// #####################################
// ##### ADAPTATION DU CANVAS
// #####################################
function adapte_canvas(id,largeur,hauteur) {
//Adaptation de la hauteur et largeur du canvas
	$('#'+id).css({'width':largeur+'px','height':hauteur+'px'});
}
// #####################################
// ##### TRACAGE DE LA BANDE
// #####################################
function bande(nb_parts) {
	maj_palette();
	//Tracé d'une barre horizontale de la fraction
	function barre(cx, cy, taille, params) {
		return papier.path(["M", cx-taille/2, cy, "L", cx+taille/2, cy, "z"]).attr(params);
	};
	// Préparation du Canvas
	adapte_canvas("bande",longueur_bande,hauteur_bande); // On adapte les dimensions du Canvas
	papier.clear(); // Efface le dessin précédent
	var x=0,y=0,x_etiquette,y_etiquette,// Coordonnées de démarrage du dessin
	dx=longueur_bande/nb_parts,dy=hauteur_bande;//Dimension de la bande ou du morceau
	if (nb_parts==1) {
		// Création de la bande
		var p=papier.rect(x,y,dx,dy);// Définition du rectangle (morceau de bande)
		p.attr({stroke:couleur_ligne, fill: couleur(morceau_bande[0]),"stroke-width":epaisseur_ligne,cursor:"pointer"})// Couleur du contour et apparence du curseur
			.data("numero", 0) // Sauvegarde des coordonnées du morceau
			.click(function() { // Gestion du clic sur la bande
				morceau_bande[0]=!morceau_bande[0];// Inversion de la couleur
				this.attr({fill:couleur(morceau_bande[0])});// On attribue la couleur à la case
				if (couleur_fond=='transparent') {bande(nb_parts)}; // Gestion bug transparence
				});
		// Étiquette
		lbl["0a"] = papier.text(dx/2 , dy/2,"1 unité").attr(style_etiquette).hide(); // Ecriture de l'unité
		}
	else {
	//Partage de la bande
	for	(var cpt=0;cpt<nb_parts;cpt++){ // Boucle sur le nombre de morceaux
		// Création du morceau
		var p=papier.rect(x,y,dx,dy)// Définition du rectangle (morceau de bande)
		p.attr({stroke:couleur_ligne, fill: couleur(morceau_bande[cpt]),"stroke-width":epaisseur_ligne,cursor:"pointer"})// Couleur du contour et apparence du curseur
			.data("numero", cpt) // Sauvegarde des coordonnées du morceau
			.click(function() { // Gestion du clic sur la bande
					morceau_bande[this.data("numero")]=!morceau_bande[this.data("numero")];// Inversion de la couleur
					this.attr({fill:couleur(morceau_bande[this.data("numero")])});// On attribue la couleur à la case
					if (couleur_fond=='transparent') {bande(nb_parts)}; // Gestion bug transparence
					});
		// Étiquette
		x_etiquette=x+dx/2,y_etiquette=y+dy/2;// Coordonnées
		lbl[cpt+"a"] = papier.text(x_etiquette , y_etiquette-taille_police/2-2 ,"1").attr(style_etiquette).hide();// Ecriture du numérateur
		lbl[cpt+"-"] = barre( x_etiquette, y_etiquette, taille_police, style_barre).hide();// Tracé de la barre de fraction
		lbl[cpt+"b"] = papier.text(x_etiquette , y_etiquette+taille_police/2+2,nb_parts).attr(style_etiquette).hide();// Ecriture du dénominateur
		x+=dx;// On passe au morceau suivant
		}
	}
	affiche_etiquettes(); // Affiche ou non les étiquettes (valeurs) sur les fractions en fonction de la Checkbox
}
// #####################################
// ##### ADAPTATION DE LA FENETRE OPENBOARD
// #####################################
function redimensionne(longueur,hauteur) {
// Redimensionne la fenêtre
longueur+=margex,hauteur+=margey; // épaisseur de la fenêtre sankore
if (window.sankore)
	window.sankore.resize(longueur,hauteur);
}
