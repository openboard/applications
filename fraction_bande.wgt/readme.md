# Bande et fractions
Application pour Opens-Sankoré / Openboard 
Découper une bande unité en fractions

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Mars 2024

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 0.1
affichage d'une bande

### 0.2
partage de la bande en morceaux

### 0.3
coloration d'un morceau au clic

### 1.0
Sauvegarde des couleurs des cases pour éviter de repartir sur une bande vierge quand on modifie le nombre de morceaux

### 1.1
Le clic sur une case ayant déjà la couleur de la palette l'efface.

### 1.2
Bouton pour effacer le dessin

### 1.3
Change toutes les cases ayant la couleur de la fraction lors du changement de couleur.

### 1.4
Ajout d'une palette pour les couleurs des lignes.

### 1.5
Simplification du code

### 1.6
Affichage des valeurs des fractions (étiquettes)

### 2.0
Sauvegarde/Restauration des paramètres du widget permettant de ne pas revenir à zéro lors de navigation entre les pages ou lors de la copie du widget.

### 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Ajout de paramètres
    * Taille des polices utilisées
    * épaisseur des ligne
    * hauteur de la bande
    * couleurs des nombres, lignes, fond...
    * mode sombre (noir&blanc) pour un affichage sur tableau noir.
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures

### 3.1
* Portage de l'application vers **Openboard 1.7**
* Gestion de la transparence

## Sources

### Icones :
* Solar Linear Icons [https://www.svgrepo.com/](https://www.svgrepo.com/)

### Bibliothèques :
* Raphael JS [https://dmitrybaranovskiy.github.io/raphael/](https://dmitrybaranovskiy.github.io/raphael/)
* Laktek colorPicker [https://github.com/laktek/really-simple-color-picker](https://github.com/laktek/really-simple-color-picker)
* Jquery 3.7.1 [https://jquery.com/](https://jquery.com/)

