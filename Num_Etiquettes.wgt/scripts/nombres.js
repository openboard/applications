// ########################
// ### INITALISATION
// ########################
//##### Variables globales 
//##### Définition des valeurs initiales
	var 	taille_police=30,
		nom_police='Cursive standard',
		gras='',
		italique='',
		souligne='',
		couleur_police='blue',
		couleur_bordure='black',
		couleur_fond='white',
		rayon_bordure=5,
		style_bordure='solid',
		epaisseur_bordure=3;
		
var nombre_de_bandes=$('.bande').length+1; // Compte le nombre de bandes dans la boîte des paramètres
var cpt=0;decallage=25,echelle=1;

// Initialisation de l'application
function init(){
	//##### Cache les miniatures des paramètres en fonction des étiquettes inutilisées par défaut
	$(".bande.cache").each(function(){ /* Les bandes entièrement masquées */
		$("#param_"+$(this).attr("id")+" .miniature").addClass("desactive"); /* on désactive toutes les étiquettes */
	});
	$(".etiquette.cache").each(function(){ /* Les aurtres étiquettes masquées dans des bandes activées */
		$("#mini_"+$(this).attr("id")).addClass("desactive"); /* on désactive toutes les étiquettes */
	});
	//##### Mise à jour des icones de début de lignes dans les paramètres Tout Afficher/Masquer
	$(".bande").each(function(){ /* Pour chaque bande d'étiquettes */
		switch ($("#param_"+$(this).attr("id")+" .miniature.desactive").length) { /* En fonction de l'état des miniatures de la bande*/
		case 0: /* Si toutes les miniatures sont activées */
			$("#mini_"+$(this).attr("id")).addClass("desactive"); /* On affiche l'icone "Tout désactiver" */
			break;
		default : /* Sinon */
			$("#mini_"+$(this).attr("id")).removeClass("desactive");/* On affiche l'icone "Tout activer" */
			break;
		}
	});	
}
//#################
//##### ONGLETS
//#################
//##### Gestion des onglets
function onglet(evt, nom_onglet,zone) {
	var i, titre_onglet=".contenu_onglet."+zone;titre_lien=".lien_onglet."+zone;
	
	//On masque le contenu de tous les onglets
	for (i = 0; i < $(titre_onglet).length; i++) {
		$(titre_onglet).eq(i).removeClass('bloc');
	}
	// On désactive les liens
	for (i = 0; i < $(titre_lien).length; i++) {
		$(titre_lien).eq(i).removeClass('active')
	}
	//On affiche le contenu de l'onglet sélectionné
	$("#"+nom_onglet).addClass('bloc');
	// On active le lien pour l'onglet sur lequel on a cliqué
	evt.currentTarget.className += " active"; 
	//~ console.log($('button').eq(1).prop("class"));
	
}

// ##############################
// ### SAUVEGARDE DU WIDGET
// ### EN CAS DE COPIE/CHANEMENT DE PAGE
// ##############################
$(async function(){
      if (window.widget) {
        // Quand on quitte le widget
	window.widget.onleave = function(){
	// Enregistrement des états des bandes
	var tableau1= [];
		// Parcours de toutes les bandes
		$('.bande').each(function(){
			// Sauvegarde des classes assignées la bande (bande / cache)
			tableau1.push($(this).prop('class'));
		});
	
	// Enregistrement des états des étiquettes
	window.sankore.setPreference('bandes',tableau1);
	var tableau2= [];
		// Parcours de toutes les étiquettes
		$('.etiquette').each(function(){
			// Sauvegarde des classes assignées l'étiquette (etiquette / cache)
			tableau2.push($(this).prop('class'));
		});
		// Enregistrement des données sur lesétiquettes
		window.sankore.setPreference('etiquettes',tableau2);
	
	// Enregistrement des états des onglets
	var tableau3=[];
	//~ // Parcours de toutes les onglets (boutons)
	$('.lien_onglet').each(function(){
		//~ // Sauvegarde des classes assignées à chaque bouton (actif ou non)
		tableau3.push($(this).prop('class'));
	});
	// Enregistrement des données sur les onglets
	window.sankore.setPreference('etats_onglets',tableau3);
	// Enregistrement des états des contenus des onglets
	var tableau4=[];
	//~ // Parcours de toutes les onglets (boutons)
	$('.contenu_onglet').each(function(){
		//~ // Sauvegarde des classes assignées à chaque bouton (actif ou non)
		tableau4.push($(this).prop('class'));
	});
	// Enregistrement des données sur les onglets
	window.sankore.setPreference('contenus_onglets',tableau4);
	}
	}
	if(window.sankore){
		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('etiquettes')) {
			// On découpe les mots de la chaîne séparés suivant les virgules
			// et on les place dans un tableau
			var tableau1 = (await window.sankore.async.preference('etiquettes').split(',')), /* État des étiquettes */
				tableau2 = (await window.sankore.async.preference('bandes').split(',')), /* État des bandes */
				tableau3 = (await window.sankore.async.preference('etats_onglets').split(',')), /* État des onglets */
				tableau4 = (await window.sankore.async.preference('contenus_onglets').split(',')); /* État des contenus des onglets*/
			
			// Traitement des étiquettes 
			// Initialisation de l'Index du tableau
			var cpt=0;
			$('.etiquette').each(function(){
				// Association de l'apparence de chaque étiquette
				$(this).prop('class',tableau1[cpt]);
				// On passe à l'élément suivant du tableau
				cpt++;
			});
	
			// Traitement des bandes
			// Initialisation de l'Index du tableau
			cpt=0;
			$('.bande').each(function(){
				// Association de l'apparence de chaque bande
				$(this).prop('class',tableau2[cpt]);
				// On passe à l'élément suivant du tableau
				cpt++;
			});
			
			// Traitement des onglets
			// Initialisation de l'Index du tableau
			cpt=0;
			$('.lien_onglet').each(function(){
				// Association de l'apparence de chaque onglet
				// Correspondance terme à terme entre le tableau et les onglets
				$(this).prop('class',tableau3[cpt]);
				// On passe à l'élément suivant du tableau
				cpt++;
			});
			// Traitement des contenus des onglets
			// Initialisation de l'Index du tableau
			cpt=0;
			$('.contenu_onglet').each(function(){
				// Association de l'apparence de chaque onglet
				// Correspondance terme à terme entre le tableau et les onglets
				$(this).prop('class',tableau4[cpt]);
				// On passe à l'élément suivant du tableau
				cpt++;
			});
		}	
	}
	});
// ############################################
// ### Action quand on clique sur une étiquette
// ############################################
	$( ".etiquette" ).click(function() {
				
	//##### Sélection du Canvas pour y dessiner les étiquettes
	var canvas = document.getElementById('canevas');
	var context = canvas.getContext('2d');

	//##### Paramètres de l'étiquettes
	//##### Taille et nom de la police
	var font=italique+gras+taille_police+'pt "'+nom_police+'"' ;
	var posX=-600,posY=-400;
	var texte=$(this).text(),couleur_police=$(this).css("color"),couleur_fond=$(this).css("background-color");
		
	//##### Marge
	var margex=taille_police+10,margey=taille_police*2;
	//##### Police
	context.font = font ;
	//##### Récupération des dimensions du texte
	var nb_etiquettes = context.measureText(texte);
	var largeur = nb_etiquettes.width,hauteur=taille_police;
		//##### Ajustement du Canvas pour accueillir le texte
		canvas.width=largeur+margex;
		canvas.height=hauteur+margey;
		//##### Coordonnées pour centrer le texte sur l'étiquette
		var x = canvas.width/2;
		var y = canvas.height/2;		
	
	//##### Dessin de la bordure
	//##### Récupération de la bordure d'étiquette choisie
	RectArrondi(context,0,0,canvas.width,canvas.height,rayon_bordure,epaisseur_bordure*2,couleur_bordure,couleur_fond);
	//### Mise en forme du texte
	context.font =font;
	context.textAlign = 'center';
	context.textBaseline = 'middle';
	context.fillStyle = couleur_police;
	//##### Ajout du texte
	context.fillText(texte, x, y);
	
	//##### Transfert de l'étiquette sur la page
	//##### Qualité à 0.1 pour gagner du temps de chargement sur la page
	var adresse=canvas.toDataURL('image/png',0.1);
	// Décallage pour voir toutes les étiquettes
	if (window.sankore)
		window.sankore.addObject(adresse,0,0,(-200+cpt*decallage),(-200+cpt*decallage));
		
	cpt++;//Compteur qui incrémente le nombre d'étiquettes pour les afficher décallés.
});

//################ 
//##### RECTARRONDI
//################
// Fonction traÃ§ant un rectangle correspondant à l'étiquette
function RectArrondi(ctxt,x, y, w, h, radius, epaisseur,bordure,fond){
	//##### Conversion du code couleur en chaîne de caractères
	fond=fond.toString();
	//##### Déduction de l'épaisseur de la bordure
	x=x+epaisseur; // Abscisse de départ
	y=y+epaisseur; // Ordonnée de départ
	w=w-epaisseur*2; // Largeur, sans compter la bordure
	h=h-epaisseur*2; // Hauteur, sans compter la bordure
	//##### Dessin du rectangle
	var r = x + w;
	var b = y + h;
	ctxt.beginPath();
  	//##### Couleur et épaisseur de la bordure du rectangle
	ctxt.strokeStyle=bordure;
	ctxt.lineWidth=epaisseur;
	//##### Tracé du rectangle
	ctxt.moveTo(x+radius, y);
	ctxt.lineTo(r-radius, y);
	ctxt.quadraticCurveTo(r, y, r, y+radius);
	ctxt.lineTo(r, y+h-radius);
	ctxt.quadraticCurveTo(r, b, r-radius, b);
	ctxt.lineTo(x+radius, b);
	ctxt.quadraticCurveTo(x, b, x, b-radius);
	ctxt.lineTo(x, y+radius);
	ctxt.quadraticCurveTo(x, y, x+radius, y);
	ctxt.stroke();
	//##### Remplissage du fond du rectangle
	ctxt.fillStyle = fond;
	ctxt.fill();
}

// ######################################
// ### Action quand on clique sur les miniatures des paramètres
// ######################################
$( ".miniature" ).click(function() {
	var 	nom=$(this).attr("id").replace("mini_",""),//On récupère l'identifiant de l'étiquette en enlevant "mini_" de l'identifiant
		nom_bande=$(this).parents("div").attr("id"),//On récupère l'identifiant de la bande
		icone_bande=nom_bande.replace("param","mini"); // identifiant de l'icone en début de ligne Afficher/Masquer TOUT
	
	$(this).toggleClass("desactive"); // On inverse l'état de la miniature (actif/inactif)
	$("#"+nom).toggleClass("cache");// On inverse l'état de l'étiquette correspondante Afficher/masquer
	$("#"+nom+"s").toggleClass("cache");// On inverse l'état de l'étiquette correspondante (pluriel) Afficher/masquer 

	/* État de l'icone de début de ligne */
	switch ($("#"+nom_bande+" .miniature.desactive").length) { /* En fonction de l'état des miniatures de la bande*/
		case 0: /* Si toutes les miniatures sont activées */
			$("#"+icone_bande).addClass("desactive"); /* On affiche l'icone "Tout désactiver" */
			break;
		case ($("#"+nom_bande+" .miniature").length): /* Si elles sont toutes désactivées */
			$("#"+nom_bande.replace("param_","")).addClass("cache");// On masque la bande 
			break;
		default : /* Sinon */
			$("#"+icone_bande).removeClass("desactive");/* On affiche l'icone "Tout activer" */
			$("#"+nom_bande.replace("param_","")).removeClass("cache");// On affiche la bande car au moins 1 étiquette est affichée
			break;
		}
});

// ###################################
// ### Cocher/décocher TOUT
// ###################################
	for (i=0;i<nombre_de_bandes;i++){
	$( "#mini_bande"+i ).click(function() {	//Clic sur l'icone de début de ligne
		var 	nom_bande=$(this).attr("id").replace("mini_","");//on récupère l'identifiant de la bande correspondante
		
		$(this).toggleClass("desactive"); // On inverse l'état de l'icône en début de ligne (actif/inactif)
		
		if ($(this).hasClass("desactive")){
			$("#param_"+nom_bande+" .miniature").removeClass("desactive");
			$("#"+nom_bande).removeClass("cache");// On affiche la bande 
			$("#"+nom_bande+" .etiquette").removeClass("cache"); // On affiche toutes les étiquettes
		}else{
			$("#param_"+nom_bande+" .miniature").addClass("desactive");
			$("#"+nom_bande).addClass("cache");// On masque la bande 
			$("#"+nom_bande+" .etiquette").addClass("cache");
		}
	});
}
