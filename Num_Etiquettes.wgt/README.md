# Étiquettes de numération

Application permettant de générer des étiquettes de numération manipulables sur une page.

## Auteur

François Le Cléac'h [https://openedu.fr](https://openedu.fr)

## Licence

CC BY-NC-SA

Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions.

## Prérequis

L'application utilise la police Cursive Standard.
Si elle n'est pas installée sur votre machine
il est possible de l'obtenir grâce au lien ci-dessous.
[https://www.dafont.com/fr/cursive-2.font](https://www.dafont.com/fr/cursive-2.font)

## Version

Version 2.5 Février 2021

À faire :
- Personnalisation des étiquettes (couleur/police/fond).

