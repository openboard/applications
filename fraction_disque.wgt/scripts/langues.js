var version = "na", author = "na", authorHref = "na";
if(window.sankore)
{
	version = window.widget.version;
	author = window.widget.author;
	author = window.widget.authorHref;
}

var sankoreLang = {
"fr":{
	Reglages:"Réglages",
	txt_etiquettes:"Étiquettes",
	txt_fond:"Fond",
	txt_fractions:"Fractions",
	txt_longueur:"Rayon",
	txt_epaisseur:"Épaisseur",
	txt_contour:"Contour",
	txt_taille:"Taille",
	Infos:"À propos",
	Txt_infos:"<h1 style='text-align:center;'>"+name+"</h1>"
	+"<h2>Application pour Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+author+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+authorHref+"'>"+authorHref+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothèques :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"en":{
	Reglages:"Preferences",
	txt_etiquettes:"Labels",
	txt_fond:"Background",
	txt_fractions:"Fractions",
	txt_longueur:"Radius",
	txt_epaisseur:"Thickness",
	txt_contour:"Stroke",
	txt_taille:"Size",
	Infos:"About",
	Txt_infos:"<h1 style='text-align:center;'>Disc's Fractions</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+author+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+authorHref+"'>"+authorHref+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Libary </b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"de":{
	Reglages:"Einstellungen",
	txt_etiquettes:"Etiketten",
	txt_fond:"Hintergrund",
	txt_fractions:"Brüche",
	txt_longueur:"Radius",
	txt_epaisseur:"Dicke",
	txt_contour:"Kontur ",
	txt_taille:"Größe",
	Infos:"Über",
	Txt_infos:"<h1 style='text-align:center;'>Bruchteil der Scheibe</h1>"
	+"<h2>App für Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+author+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+authorHref+"'>"+authorHref+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Bibliothek :</b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
}
};

