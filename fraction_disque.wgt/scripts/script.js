var	langue_defaut="fr",syslang=langue_defaut,// Français par défaut
	tranche = new Array, // Tableau conservant les couleurs des cases
	lbl=new Array,//Etiquettes
	mode_defaut='edition',mode=mode_defaut,//Mode d'affichage
	// Couleurs de la palette
	palette_base=[noir="000",gris="999",gris_clair="ccc",blanc="fff",bleu="38d",rouge="f00",vert="4b3",orange="f80",jaune="ff0",rose="b7b",marron='a52',transparent='transparent'],
	couleur_fraction="#"+rouge, couleur_ligne="#"+noir,couleur_etiquette="#"+noir,couleur_fond="#"+blanc,couleur_barre="#"+noir,
	widgetX,widgetY,
	margex=30,margey=30, // épaisseur de la fenêtre sankore
	x0 = 10,// Abscisse de démarrage du grille (gauche) et marge autour du dessin
	y0 = 10,// Ordonnée de démarrage du Canvas (haut) et marge autour du dessin
	papier = Raphael("camembert","100%","100%"),// Occupe tout le div
	epaisseur_ligne=3,epaisseur_ligne_min=1,epaisseur_ligne_max=5, // Epaisseur du contour
	rayon=100, rayon_min=50,rayon_max=300, // Rayon du cercle
	parts=1, // Nombre de parts du cercle
	nombre_parts_max=20,nombre_parts_min=1,
	taille_police=14, taille_police_max=40,taille_police_min=6, epaisseur_barre=2,
	// Styles des étiquettes
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_barre},
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};

// #####################################
// ##### INITIALISATIONS
// #####################################
function init_reglages(){
	//Curseurs
	// Nombre de parts d'une unité
	$( "#parts" ).attr({
	       "max" : nombre_parts_max,
	       "min" : nombre_parts_min,
		"value":parts,
		"step":1
    });
    // Rayon du disque
	$( "#rayon" ).attr({
	       "max" : rayon_max,
	       "min" : rayon_min,
		"value":rayon,
		"step":1
    });
    //Épaisseur du contour
    $( "#epaisseur" ).attr({
	       "max" : epaisseur_ligne_max,
	       "min" : epaisseur_ligne_min,
		"value":epaisseur_ligne,
		"step":1
    });
    //Taille de la police de caractères
    $( "#taille" ).attr({
	       "max" : taille_police_max,
	       "min" : taille_police_min,
		"value":taille_police,
		"step":1
    });
}
function init_palette() {
//initialisation de la palette de couleurs
	// Palette pour les fraction coloriée
	$('#fond').colorPicker({pickerDefault: couleur_fond, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fond=newValue;trace_cercle();}}); //Couleur des parts
	// Palette pour les fraction coloriée
	$('#fraction').colorPicker({pickerDefault: couleur_fraction, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_fraction=newValue;trace_cercle();}}); //Couleur des parts
	// Palette pour les lignes;
	$('#contour').colorPicker({pickerDefault: couleur_ligne, colors: palette_base, showHexField: false, onColorChange : function(id, newValue) { couleur_ligne=newValue;trace_cercle();}}); //Couleur des lignes
	// Palette pour les étiquettes;
	$('#etiquette').colorPicker({
		pickerDefault: couleur_etiquette, 
		colors: palette_base, 
		showHexField: false, 
		onColorChange : function(id, newValue) {
			couleur_etiquette=newValue;
			couleur_barre=newValue;
			style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
			style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_barre},
			trace_cercle();
		}
	}); //Couleur des nombres de la fraction
}
function init_lang(){
	 // Traduction de l'interface
	$('#reglages').attr('title',sankoreLang[syslang].Reglages);
	$('#txt_etiquettes').text(sankoreLang[syslang].txt_etiquettes);
	$('#txt_fond').text(sankoreLang[syslang].txt_fond);
	$('#txt_fractions').text(sankoreLang[syslang].txt_fractions);
	$('#txt_longueur').text(sankoreLang[syslang].txt_longueur);
	$('#txt_epaisseur').text(sankoreLang[syslang].txt_epaisseur);
	$('#txt_contour').text(sankoreLang[syslang].txt_contour);
	$('#txt_taille').text(sankoreLang[syslang].txt_taille);
	// Mise à jours de la boîte de dialogue des informations
	$('#infos').attr('title',sankoreLang[syslang].Infos);
	$('#infos').html(sankoreLang[syslang].Txt_infos);
	}
	// ##### INITIALISATION DU CERCLE
async function init() { 
	// Initialisation des palettes & aperçus
	init_palette();
	// Initialisation des morceaux du camembert
	for (var cpt=0;cpt<nombre_parts_max;cpt++){
				tranche[cpt]=false; //Fractions non coloriées par défaut		
			}
	if(window.sankore) {
	//Détection de la langue
	try{
		syslang = window.sankore.lang.substr(0,2);
		sankoreLang[syslang].search;
        } catch(e){
		syslang = langue_defaut;
	}
	// Arrivée sur le widget 
	window.widget.onenter.connect(() => {$('#bouton_reglages').show();});// Affiche le bouton des paramètres
	// Sortie du Widget
	window.widget.onleave.connect(() => { 
		// Gestion du widget
	// Quand on quitte le widget
		$('#bouton_reglages').hide();// Cache le bouton
		//Sauvegarde des paramètres au format 'chaine de caractères'
		window.sankore.setPreference('Nombre de parts', parts);
		window.sankore.setPreference('Rayon', rayon);
		window.sankore.setPreference('Couleur fraction', couleur_fraction);
		window.sankore.setPreference('Couleur ligne', couleur_ligne);
		window.sankore.setPreference('Couleur fond', couleur_fond);
		window.sankore.setPreference('Taille police', taille_police);
		window.sankore.setPreference('Epaisseur', epaisseur_ligne);
		window.sankore.setPreference('Couleur etiquette', couleur_etiquette);
		window.sankore.setPreference('Etiquettes',$("#etiquettes").prop('checked'));//Affichage des étiquettes
		window.sankore.setPreference('mode', mode);
		window.sankore.setPreference('Tranche', JSON.stringify(tranche));
	});		
	// Quand on revient sur le widget, on récupère les paramètres stockés
	if (await window.sankore.preference('Nombre de parts')) {
		parts=parseInt(await window.sankore.async.preference('Nombre de parts'));
		rayon=parseInt(await window.sankore.async.preference('Rayon'));
		couleur_fraction=await window.sankore.async.preference('Couleur fraction');
		couleur_ligne=await window.sankore.async.preference('Couleur ligne');
		couleur_fond=await window.sankore.async.preference('Couleur fond');
		couleur_etiquette=await window.sankore.async.preference('Couleur etiquette');
		couleur_barre=couleur_etiquette;
		taille_police=await window.sankore.async.preference('Taille police');
		epaisseur_ligne=await window.sankore.async.preference('Epaisseur');
		style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
		$("#etiquettes").prop('checked',JSON.parse(await window.sankore.async.preference('Etiquettes')));// Valeur logique stockée sous forme de chaîne, donc à convertir en booléen
		mode=await window.sankore.async.preference('mode'); // Récupération du mode d'affichage
		tranche=JSON.parse(await window.sankore.async.preference('Tranche'));
	} 
	$("#fraction").val(couleur_fraction).change();// Aperçu de la couleur de la fraction colorée
	$("#contour").val(couleur_ligne).change();// Aperçu de la couleur de contour
	$("#fond").val(couleur_fond).change();// Aperçu de la couleur de contour
	$("#parts").next('label').text(parts);// Affiche la valeur initiale du nombre de parts
	$("#rayon").next('label').text(rayon);// Affiche la valeur initiale du rayon du disque
	$("#epaisseur").next('label').text(epaisseur_ligne);// Affiche la valeur initiale du rayon du disque
	$("#taille").next('label').text(taille_police);// Taille initiale de la police de caractère
	}
	// Traduction de l'interface
	init_lang();
	// Initialisation des paramètres
	init_reglages();
	// Boite de dialogue des paramètres
	$( "#reglages" ).dialog({
		autoOpen: false,
		position: {my: "left top", at: "left bottom", of: '#widget'}, 
		beforeClose: function() {mode='vue'},
		close: function() {
			mode_affichage(mode);
		}
	});
	// Boite de dialogue Infos
	$( "#infos" ).dialog({
		autoOpen: false,
		width:"auto",
		position: {my: 'left top', at: 'center center', of: '#widget'}, 
	});
	mode_affichage(mode);// Mode d'affichage
	//Tracer du cercle
	trace_cercle();
}
// #####################################
// ##### GESTION DES ÉVÉNEMENTS
// #####################################
// Un clic sur le bouton fait apparaître la boite de dialogue des paramètres
$("#bouton_reglages").click(function(){
	mode_affichage('edition');
});
// Affichage des infos
$('#bouton_infos').click(function(){
	$('#infos').dialog('open');
	});
// Affichage des étiquettes
$('#etiquettes').click(function(){
	affiche_etiquettes();
	});
//  Mode sombre
$('#bouton_mode_sombre').click(function(){
	var coul1,coul2;
	$('body').toggleClass('mode_sombre');
	if ($('body').hasClass('mode_sombre')) {
		coul1='#'+blanc;
		coul2='#'+noir;
		} else {
		coul1='#'+noir;
		coul2='#'+blanc;
	};
		couleur_ligne=coul1;
		couleur_etiquette=coul1;
		couleur_barre=couleur_etiquette;
		couleur_fond=coul2;
		style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
		style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_barre},
	$("#contour").val(couleur_ligne).change();// Mise à jour des aperçu des palettes
	$("#etiquette").val(couleur_etiquette).change();
	$("#fond").val(couleur_fond).change();
	trace_cercle();
	});
// Réactions aux déplacements des curseurs
	// Partage du disque
$('#parts').mousemove(function(){
	parts=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_cercle();
	});
	// Rayon du disque
$('#rayon').mousemove(function(){
	rayon=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_cercle();
	});
	// Épaisseur du contour
$('#epaisseur').mousemove(function(){
	epaisseur_ligne=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_cercle();
	});// Épaisseur du contour
$('#taille').mousemove(function(){
	taille_police=parseInt(this.value);// recupere la valeur du curseur
	$(this).next('label').text($(this).val());// Affiche la valeur en bout de ligne
	trace_cercle();
	});
// ##### INDICATION DE LA COULEUR DE LA PART
function couleur(couleur_part) { //fonction renvoyant la couleur de la part
	//Si la part est coloriée, on renvoie la couleur de la palette, sinon, celle par défaut.
	if (!couleur_part) {return couleur_fond} else {return couleur_fraction};
}
// #####################################
// ##### MODE AFFICHAGE
// #####################################
function mode_affichage(style_affichage){
	mode=style_affichage;
	// ##### MODE VUE #####
	if (mode=="vue") {
		$("#bouton_reglages").hide();
		// On cache les réglages
		$( "#reglages" ).dialog( "close" );//Fermeture des paramètres
		adapte_canvas("camembert",(rayon+x0)*2,(rayon+y0)*2);
		widgetX=$("#camembert").width();widgetY=$("#camembert").height();
		redimensionne(widgetX,widgetY);
	};
	// ##### MODE EDITION #####
	if (mode=="edition") {
		// On affiche les réglages
		adapte_canvas("camembert",(rayon_max+y0)*2,(rayon_max+x0)*2);
		widgetX=Math.max($("#camembert").width(),$("#reglages").width());widgetY=($("#camembert").height()+$("#reglages").height());
		redimensionne(widgetX+25,widgetY+25);
		$( "#reglages" ).dialog( "open" );//Ouverture des paramètres
	};
}
// ##### TRAÇAGE DU CAMEMBERT
Raphael.fn.camembert = function (cx, cy, r,nb_parts, couleur_cercle) {//Fonction créant le svg à placer sur le canvas
	// fonction traçant un secteur du cercle
	function secteur(cx, cy, r, startAngle, endAngle, params) {
		var x1 = cx + r * Math.cos(-startAngle * rad),
		x2 = cx + r * Math.cos(-endAngle * rad),
		y1 = cy + r * Math.sin(-startAngle * rad),
		y2 = cy + r * Math.sin(-endAngle * rad);
		return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
	};
	//Tracé d'une barre horizontale de la fraction
	function barre(cx, cy, taille, params) {
		return paper.path(["M", cx-taille/2, cy, "L", cx+taille/2, cy, "z"]).attr(params);
	};
	var paper = this, chart = this.set(), rad = Math.PI / 180, angle = 0, total = nb_parts, angleplus = 360 / nb_parts;
	style_barre={"stroke-width":epaisseur_barre , "stroke":couleur_barre},
	style_etiquette={fill: couleur_etiquette, stroke: "none", opacity: 1, "font-size": taille_police};
	// si il y a 1 seule part (1 unité)
	if (nb_parts==1) {
		p = paper.circle(cx, cy, r);
		p.attr({stroke : couleur_cercle, "stroke-width": epaisseur_ligne, fill:couleur(tranche[0])});
		p.click(function (){
			tranche[0]=!tranche[0];
			p.attr({fill:couleur(tranche[0])});
			if (couleur_fond=='transparent') {trace_cercle()}; // Gestion bug transparence
		});
		p.hover(function(e){e.currentTarget.style.cursor="pointer";},function(e){e.currentTarget.style.cursor="default";});//Modification du curseur au survol du disque
		lbl["0a"] = papier.text(cx , cy,"1 unité").attr(style_etiquette).hide();
		chart.push(p);
	} else {
	// Sinon, découpage en nb_parts
		for (i = 0; i < nb_parts; i++) {
			var  popangle = angle + (angleplus / 2), // Secteur d'angle du morceau
				delta = r-taille_police*1.5, // Écart de l'étiquette du secteur angulaire par rapport au centre du cercle
				x_etiquette=cx + delta * Math.cos(-popangle * rad), y_etiquette=cy + delta * Math.sin(-popangle * rad), // Coordonnées de l'étiquette
			p = secteur(cx, cy, r, angle, angle + angleplus, {fill: couleur(tranche[i]) , stroke: couleur_cercle, "stroke-width": epaisseur_ligne});//tranche[i] indique si la part doit être coloriée ou non
			p.data("numero",i);//sauvegarde du numero de la part
			p.click(function () {// Gestion du clic sur le morceau
				var idx=this.data("numero"); //récupération de l'index du morceau
				tranche[idx]=!tranche[idx]; // Inversion de l'état (colorié ou non)
				this.attr({fill:couleur(tranche[idx])}); // Couleur de la part en fonction de l'état
				if (couleur_fond=='transparent') {trace_cercle()}; // Gestion bug transparence
				});
			p.hover(function(e){e.currentTarget.style.cursor="pointer";},function(e){e.currentTarget.style.cursor="default";});//Modification du curseur au survol du disque
			//Définition des Etiquettes
			lbl[i+"a"] = papier.text(x_etiquette,y_etiquette-taille_police/2-2,"1").attr(style_etiquette).hide();// Ecriture du numérateur
			lbl[i+"-"] = barre( x_etiquette, y_etiquette, taille_police, style_barre).hide();// Tracé de la barre de fraction
			lbl[i+"b"] = papier.text(x_etiquette, y_etiquette+taille_police/2+2,nb_parts).attr(style_etiquette).hide();// Ecriture du dénominateur
			// Passage au morceau suivant
			angle += angleplus;//Augmentation de l'angle, pour passer à la part suivante
			chart.push(p);//On ajoute le secteur aux autres dans le Canvas
		}
	}
	affiche_etiquettes(); // Affiche ou non les étiquettes (valeurs) sur les fractions en fonction de la Checkbox
	return chart;//On envoie le dessin
};
// ##### TRACER DU CERCLE
function trace_cercle(){
	// Trace un cercle partagé en parts
	papier.clear();// On efface le Canvas
	papier.camembert(rayon+x0, rayon+y0, rayon, parts, couleur_ligne);//On trace le camembert
}
// ##### REDIMENSIONNEMENT DE LA FENETRE
function redimensionne(largeur,longueur) {
// Redimensionne le widget sankore
if(window.sankore) 
	window.sankore.resize(largeur+margex,longueur+margey);
}
// ##### ADAPTATION DU CANVAS
function adapte_canvas(id,hauteur,largeur) {
//Adaptation de la hauteur et largeur du canvas
	$('#'+id).width(largeur);
	$('#'+id).height(hauteur);
	$('#widget').width(largeur+10);
	$('#widget').height(hauteur+10);
}
// ##### AFFICHAGE / MASQUAGE DES ETIQUETTES
function affiche_etiquettes() {
// Affichage de l'étiquette sous la forme a/b)
	// Vérifie l'affichage des étiquettes
	if ($("#etiquettes").prop('checked')) {
		for	(var cpt=0;cpt<parts;cpt++){ // Boucle sur le nombre de morceaux
			if (parts==1) {
				lbl[cpt+"a"].show();
			}
			else {
				lbl[cpt+"a"].show();lbl[cpt+"-"].show();lbl[cpt+"b"].show();
			}
		}
	}	else  {
	for	(var cpt=0;cpt<parts;cpt++){ // Boucle sur le nombre de morceaux
		if (parts==1) {
			lbl[cpt+"a"].hide();
		} 
		else{
			lbl[cpt+"a"].hide();lbl[cpt+"-"].hide();lbl[cpt+"b"].hide()};
		}
	}
}
