# Factions du disque
Application pour Opens-Sankoré / Openboard 
Représenter une fraction sous forme de secteur de disque

## Auteur
François Le Cléac'h [https://openedu.fr](https://openedu.fr)
Mars 2024

## Licence
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

## Versions

### 0.1
Adaptation du script Pie.js du site Raphael.js

### 0.2
Suppression du tableau <td> HTML contenant les valeurs du camembert
Remplacement par une variable indiquant le nombre de parts.

### 0.3
Ajout de la fonction Onclick sur les parts du camembert

### 0.4
Modification de la feuille de style

### 0.5
Modification des couleurs du cercle, taille et couleur des étiquettes à afficher.

### 1.0
Adaptation/Intégration du script Pie.js au widget "fraction_quadrillage.wgt"

### 1.1
Adaptation de la fenêtre "Réglages" (boutons, icones, onclick...)

### 1.2
ajout d'un colorpicker pour choisir la couleur de la fraction

### 1.3
Sauvegarde des couleurs des cases pour éviter 
de repartir sur une figure vierge quand on modifie le nombre
de parts

### 1.4
Le clic sur une case coloriée la fait redevenir blanche.
Adaptation des fenêtre à l'affichage dans Sankoré

### 1.5
Passage en mode Édition par double clic sur le dessin
Simplification du script

### 1.6
Ajout d'un bouton pour modifier la couleur des lignes
Simplification du script

### 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Ajout de paramètres
    * Taille des polices utilisées
    * épaisseur des lignes/graduations
    * hauteur des graduations
    * couleurs des nombres, fractions, graduations
    * mode sombre pour un affichage sur tableau noir.
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures
### 3.1
* Portage de l'application vers **Openboard 1.7**
* Gestion de la transparence

## Sources

### Icones :
* Solar Linear Icons [https://www.svgrepo.com/](https://www.svgrepo.com/)

### Bibliothèques :
* Raphael JS [https://dmitrybaranovskiy.github.io/raphael/](https://dmitrybaranovskiy.github.io/raphael/)
* Laktek colorPicker [https://github.com/laktek/really-simple-color-picker](https://github.com/laktek/really-simple-color-picker)
* Jquery 3.7.1 [https://jquery.com/](https://jquery.com/)

