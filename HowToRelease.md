# How to release ?

## 1. Prepare the files

 - Complete the `changelog.template` file with every modifications.
 - Check if the documentation `index.md.template` and `index.en.md.template` are up to date.
 - And add and commit every modifications.

## 2. Verification

 - Verify if the packages could be created :
   ```
   ./packages.sh
   ```
 - 2 files EXE and DEB must be created
 - And we can read :
```
************************************************************************************************
*   Check the integrity of the applications...
************************************************************************************************
...
The program is finished with no error.
```
 - Test of the 2 generated files...

## 3. Create a tag

Now, we can tag the last commit as a reference for the release.
For example, this command will create a new Tag called `0.1.4` from the last commit:
```
git tag 0.1.4
```

## 4. Push the tag

The previous tag is local. We have to push it.
Example:
```
git push origin 0.1.4
```

It will :
 - push the tag
 - and launch the release from the CI.

 Now the release is online on Gitlab releases page.

## 5. Update the documentation with the new release links

Now the release is online on Gitlab releases page.
But the documentation uses the pervious download links.
We have to update, commit and push them:

   ```
   ./get_and_update_release_links.sh
   git add ./index.md ./index.en.md
   git commit -m "Update the download links after the release."
   git push
   ```

That's all!
