# Ce script génère la liste des fichiers Markdown contenus dans un dossier *.wgt, triés par dossier parent et nom de fichier.

# On met toujours en premier le fichier index.md qui se trouve à la racine du répertoire
#echo 'index.md' > files.txt
# Ensuite on met les fichiers à la racine puis les fichiers dans les dossiers et sous-dossiers
find . -maxdepth 2 -mindepth 2 -type f -name "*.md" -not -name "index.md" >> files.txt

prev_dir=""
echo "<section class=\"general-toc\"><ul>" >> docs.html
while read file; do
  # Nom du dossier parent
  dir="$(dirname "$file")"
  # Si le dossier parent est différent du précédent, on ajoute une section de titre
  if [ "$dir" != "$prev_dir" ]; then
    if [ -n "$prev_dir" ]; then
      echo "</li>" >> docs.html
    fi
    echo "<li>"  >> docs.html
  fi

  # On ajoute un lien à la liste

  # Si le fichier contient un titre de niveau 1 en markdown, on définit le nom du fichier par ce titre
  filename="$(grep -m 1 -e "^#\ " "${file}" | sed "s/^#\ //")"
  # Sinon on reprend le nom du fichier dans le répertoire
  if [ -z "$filename" ]; then
	filename=$(basename "${file}" .md)
  fi

  if [ "$dir" != "." ]; then
      echo "<a href=\"$(echo "${file%.md}.html" | sed 's|./||')\">${filename}</a> : (/${dir#./})"  >> docs.html
    else
      echo "<a href=\"$(echo "${file%.md}.html" | sed 's|./||')\">${filename}</a>" >> docs.html
  fi

  prev_dir="$dir"
done < files.txt

# On ferme les balises HTML
echo "</ul></section>" >> docs.html

# On supprime le fichier temporaire
rm files.txt
