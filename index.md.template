# ![](favicon.ico) Dépôt d'applications OpenBoard

[ [Français](index.md) | [English](index.en.md) ]

Une collection d'applications pour le logiciel de tableau blanc interactif OpenBoard.

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;"> Mise à disposition sur la
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px black solid;">
    <img src="https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/logo_forge.svg" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>

## Contexte

La version 1.7 d'OpenBoard apporte de nouvelles possibilités, mais nécessite la mise à jour de certaines applications, voir ce [guide](https://github.com/OpenBoard-org/OpenBoard/wiki/How-to-update-a-widget-to-the-new-API).

Ce projet a pour objectif de partager, maintenir et améliorer des versions de ces applications, compatibles avec Openboard >= 1.7.

Les éventuelles modifications apportées aux applications d'origine sont :

- Support de OpenBoard 1.7 ou supérieur (du nouveau moteur web basé sur Chromium en suivant ce [guide](https://github.com/OpenBoard-org/OpenBoard/wiki/How-to-update-a-widget-to-the-new-API) )
- Ajout de la persistance des paramètres. Si on ouvre un ancien document avec une application, celle-ci doit s'afficher à l'identique. Pour cela ses paramètres doivent être enregistrés et restaurés. Cette fonctionnalité n'existe pas toujours.
- Empaquetage d'applications web au format OpenBoard.

## Téléchargement et installation des applications

### Méthode automatique

<div class="boutons">

[Installeur Windows](WIN_LATEST_RELEASE_LINK)

[Installeur Linux (DEB)](DEB_LATEST_RELEASE_LINK)

</div>

### Méthode manuelle

Suivre les indications de [la page wiki](https://forge.apps.education.fr/openboard/applications/-/wikis/home/Comment-t%C3%A9l%C3%A9charger-et-installer-les-applications-%3F).


## Liste des applications

### Gestion de classe :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [Jeu_de_Des](Jeu_de_Des.wgt/) | Lancer de **dés**. | [x]  | [x]           | CC BY-NC-SA   | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Jeu_de_Des.wgt) |
| [sonometre](sonometre.wgt/) | Mesurer le **volume sonore**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/openboard/applications-pour-openboard/) et [ici](https://www.openedu.fr/2024/06/26/sonometre/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=sonometre.wgt) |
| [Tuxtimer](Tuxtimer.wgt/) | Un **compte à rebours** graphique. | [x] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/tuxtimer) et [ici](https://educajou.forge.apps.education.fr/tuxtimer/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Tuxtimer.wgt) |
| [spotlight](spotlight.wgt/) | Pour mettre en évidence certaines zone de l'écran. | [x] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/spotlight/) et [ici](https://educajou.forge.apps.education.fr/spotlight)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=spotlight.wgt) |

### Français :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [nombre_en_lettres](nombre_en_lettres.wgt/) | Écrire un **nombre en lettres**. | [ ] | [x] | GPL v2 | Arnaud DURAND | [ici](https://www.mathix.org/nombre_en_lettres/) et [ici](https://mathix.org/linux/archives/9938)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=nombre_en_lettres.wgt) |
| [Seyes](Seyes.wgt/) | Ecrire sur une **page Seyes**. | [ ] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/seyes) et [ici](https://educajou.forge.apps.education.fr/seyes/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Seyes.wgt) |
| [Syllabux](Syllabux.wgt/) | **Syllabaire** avec des consonnes et voyelles pour former des syllabes. | [ ] | [x] | GNU/GPL | Arnaud CHAMPOLLION | [ici](https://educajou.forge.apps.education.fr/syllabux/) et [ici](https://forge.apps.education.fr/educajou/syllabux)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Syllabux.wgt) |
| [Etiquettes](Etiquettes.wgt/) | Génére des **étiquettes** manipulables sur une page. | [ ]  | [x]           | CC BY-NC-SA   | François Le Cléac'h | [ici](https://www.openedu.fr/openboard/applications-pour-openboard/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Etiquettes.wgt) |

### Mathématiques - Numération :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [Num_Etiquettes](Num_Etiquettes.wgt/) | Génére des **étiquettes de numération manipulables** sur une page. | [ ]  | [x]           | CC BY-NC-SA   | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=Num_Etiquettes.wgt) |
| [fraction](fraction.wgt/) | Affiche une **fraction**. | [ ] | [x]         | CC BY-NC-SA   | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=fraction.wgt) |
| [fraction_bande](fraction_bande.wgt/) | Affiche la **fraction** d'une **bande** horizontale. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=fraction_bande.wgt) |
| [fraction_quadrillage](fraction_quadrillage.wgt/) | Affiche une **fraction** sous forme de **grille**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=fraction_quadrillage.wgt) |
| [fraction_disque](fraction_disque.wgt/) | Affiche une **fraction** sous forme d'un **disque**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=fraction_disque.wgt) |
| [droite_graduee](droite_graduee.wgt/) | Affiche une **droite graduée**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=droite_graduee.wgt) |
| [droite_fraction](droite_fraction.wgt/) | Affiche une **droite** graduée en **fraction**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=droite_fraction.wgt) |
| [cuboscope](cuboscope.wgt/) | Affiche la **décomposition décimale** d'un entier. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://www.mathix.org/cuboscope/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=cuboscope.wgt) |
| [glisse-nombre](glisse-nombre.wgt/) | **Glisse nombre**, pour x ou : par 10, 100, 1000, etc. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://mathix.org/glisse-nombre/index.html) et [ici](https://mathix.org/glisse-nombre/glisse-nombre.zip)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=glisse-nombre.wgt) |
| [labynombre](labynombre.wgt/) | Jeux de **comparaison** de relatifs ou de décimaux. | [x] | [x] | GPL v2 | Arnaud DURAND | [ici](https://www.mathix.org/labynombre/) et [ici](https://mathix.org/linux/archives/13224)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=labynombre.wgt) |
| [compteur](compteur.wgt/) | **Compteur** pour ajouter/soustraire des unités, dizaines, centaines, dixièmes, etc. | [ ] | [x] | GPLv3 | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/compteur) et [ici](https://educajou.forge.apps.education.fr/compteur/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=compteur.wgt) |

### Mathématiques - Calcul :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [compte_est_bon](compte_est_bon.wgt/) | Jeu le **compte est bon**. | [ ] | [x] | CC BY-NC-SA | François Le Cléac'h | [ici](https://www.openedu.fr/open-sankore/applications-pour-sankore/) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=compte_est_bon.wgt) |
| [mathadorix](mathadorix.wgt/) | Similaire à **compte_est_bon**. | [ ] | [x] | GPLv3       | Arnaud DURAND | [ici](https://mathix.org/mathador) | [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=mathadorix.wgt) |
| [division_assistee](division_assistee.wgt/) | Exerciseur de **division posée** | [ ] | [x] | GPLv2 | Arnaud DURAND | [ici](https://www.mathix.org/division_assistee/) et [ici](https://mathix.org/linux/archives/13371)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=division_assistee.wgt) |
| [carre-magique](carre-magique.wgt/) | Générateur de **carrés magiques** | [ ] | [x] | CC BY-NC-SA v3 | Arnaud DURAND | [ici](https://mathix.org/linux/archives/11258) et [ici](https://mathix.org/carre-magique/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=carre-magique.wgt) |
| [rullo_somme](rullo_somme.wgt/) | Jeu d'**addition** de nombres. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://mathix.org/rullo_somme/) et [ici](https://mathix.org/linux/archives/11000)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=rullo_somme.wgt) |
| [rullo_produit](rullo_produit.wgt/) | Jeu sur les **critères de divisibilité**. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://mathix.org/rullo_produit/) et [ici](https://mathix.org/linux/archives/11000)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=rullo_produit.wgt) |
| [automultiples](automultiples.wgt/) | Affiche les **multiples** d'un nombre. | [ ] | [x] | GPLv3 | Arnaud CHAMPOLLION | [ici](https://forge.apps.education.fr/educajou/automultiples) et [ici](https://educajou.forge.apps.education.fr/automultiples/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=automultiples.wgt) |

### Mathématiques - Géométrie :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [puzzle-zukei](puzzle-zukei.wgt/) | **Puzzles** de Naoki Inaba. | [x] | [x] | GPL v2      | Arnaud DURAND | [ici](https://mathix.org/puzzle-zukei/) et [ici](https://mathix.org/linux/archives/9444)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=puzzle-zukei.wgt) |
| [babybot](babybot.wgt/) | Initiation au **codage** avec une simulation de la **bee-bot**. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=babybot.wgt) |
| [beebot-30-fleurs](beebot-30-fleurs.wgt/) | **Bee bot** niveau élémentaire. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=beebot-30-fleurs.wgt) |
| [beebot-entrainement](beebot-entrainement.wgt/) | **Bee bot** entraînement libre. | [x] | [x] | CC BY NC SA | Florent NOUGUEZ | [ici](https://classedeflorent.fr/outils/telechargements-licences-libres.php#beebot)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=beebot-entrainement.wgt) |

### Mathématiques - Grandeurs et mesures :

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [conversion](conversion.wgt/) | Tableau de **conversion de grandeurs**. | [x] | [x] | CC BY-NC-SA | Arnaud DURAND | [ici](https://www.mathix.org/conversion/) et [ici](https://mathix.org/linux/archives/11334)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=conversion.wgt) |
| [conversions_sesamath](conversions_sesamath.wgt/) | **Tableaux de numération** et de conversion. | [x] | [x] | AGPL 3 | Thomas CRESPIN | [ici](https://sesaprof.sesamath.net/doc/outils_tableaux/index.html) et [ici](https://sesaprof.sesamath.net/pages/prof_divers_outils.php)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=conversions_sesamath.wgt) |
| [horloge-ecole](horloge-ecole.wgt/) | Une **horloge interactive**. | [x] | [x] | GPLv3 | Thierry MUNOZ | [ici](https://forge.aeif.fr/ThierryM/horloge-ecole) et [ici](https://thierrym.forge.aeif.fr/horloge-ecole/)| [Télécharger](https://forge.apps.education.fr/openboard/applications/-/archive/main/applications-main.zip?path=horloge-ecole.wgt) |

### Réglages d'OpenBoard :

OpenBoard peut être personnalisé avec [son fichier de configuration](https://github.com/OpenBoard-org/OpenBoard/wiki/Configuration). Les paramètres suivants sont modifiables via [l'installeur Windows](WIN_LATEST_RELEASE_LINK) uniquement.

| Noms          | Descriptions          | OB < 1.7      | OB >= 1.7     | Licences      | Auteurs       | Sources | Téléchargements |
| ------------- | --------------------- | ------------- | ------------- | ------------- | ------------- | ------- | -------------- |
| [fonds_seyes](https://github.com/OpenBoard-org/OpenBoard/issues/640) | Active le paramètre pour afficher des **fonds Seyes** dans OB. Disponible uniquement dans l'installeur Windows.| [ ] | [x] | N/A | N/A | [ici](https://github.com/OpenBoard-org/OpenBoard/issues/640)| N/A, ceci n'est pas une application. |
| [sans_plein_ecran](https://github.com/OpenBoard-org/OpenBoard/wiki/Parameters#app-section) | Active le paramètre pour afficher OB en mode fenetré et non en plein-écran, le mode par défaut. Disponible uniquement dans l'installeur Windows.| [ ] | [x] | N/A | N/A | [ici](https://github.com/OpenBoard-org/OpenBoard/wiki/Parameters#app-section)| N/A, ceci n'est pas une application. |


## Liste de la documentation des applications

(Cette liste est générée automatiquement)

``` {.include}
docs.html
```

## Comment contribuer ?

1. En décrivant un problème que vous constatez.

Les problèmes peuvent provenir des installeurs ou bien des applications elles-mêmes.
Merci de créer un [ticket ici](https://forge.apps.education.fr/openboard/applications/-/issues/new) en fournissant un maximum d'informations susceptibles d'aider la résolution du problème.

2. En proposant une nouvelle application.

  - Soit elle existe déjà et vous souhaitez la voir ajoutée au dépôt. Dans ce cas, il faut aussi créer un [ticket ici](https://forge.apps.education.fr/openboard/applications/-/issues/new) pour décrire l'application (Où est-elle accessible ? Quelle est la licence ? etc.).
  - Soit elle existe déjà et vous voulez faire le travail d'adaptation à OpenBoard vous-même. Dans ce cas, [suivez ce guide](https://forge.apps.education.fr/openboard/applications/-/wikis/home/Comment-adapter-une-application-web-pour-l%E2%80%99int%C3%A9grer-%C3%A0-OpenBoard-%3F) et proposez le résultat de votre travail dans un [ticket](https://forge.apps.education.fr/openboard/applications/-/issues/new) puis faites une `demande de fusion`.
  - Soit vous avez créé une application géniale et vous voulez la voir ajoutée au dépôt. Dans ce cas, vous recevrez un joyeux **merci**. Et il faudra aussi créer un [ticket](https://forge.apps.education.fr/openboard/applications/-/issues/new) pour la décrire.
